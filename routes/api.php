<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return response()->json([
//        'message' => 'success',
//        'data' => [
//            'user' => $request->user(),
//        ]
//    ]);
//});

//Route::get('test', function (Request $request) {
//    \Illuminate\Support\Facades\Log::info($request);
//});

Route::namespace('Api\User')->name('api.')->group(function () {
    Route::get('/search', 'SearchController@search')->name('user.search');
    Route::get('/itemsearch', 'SearchController@itemSearch');
    Route::get('/branches', 'RestaurantController@getBranches');
    Route::get('/branch-details', 'RestaurantController@getCurrentBranch');
    Route::resource('cuisine', 'CuisineController');
    Route::resource('restaurant', 'RestaurantController');
    Route::resource('user', 'UserController')->only(['store']);
    Route::resource('review', 'ReviewController')->only(['index']);
    Route::post('/login', 'UserController@login');
    Route::post('/forgot_password', 'UserController@forgot_password');
    Route::post('/reset_password', 'UserController@resetPassword'); 
});

Route::namespace('Api\User')->name('api.')->middleware('auth:api')->group(function () {
    Route::resource('address', 'AddressController');
    Route::resource('reservation', 'ReservationController');
    Route::post('/validateTime', 'DeliveryController@validateOrder');
    Route::resource('delivery', 'DeliveryController');
    Route::resource('takeaway', 'TakeawayController');
    Route::post('/restaurant/favourite', 'RestaurantController@favourite')->name('restaurant.favourite');
    Route::get('/user/user', 'UserController@getUser');
    Route::get('/user/favourites', 'RestaurantController@favourites');
    Route::get('/user/order', 'UserController@orders');
    Route::get('/user/getorder', 'UserController@getOrder');
    Route::get('/user/get-order-status', 'UserController@getOrderStatus');
    // Route::get('/user/payment-methods', 'UserController@getCardList');
    Route::get('/user/payment-methods', 'UserController@getMethodList');
    Route::post('/user/add-payment-method', 'UserController@saveCardDetails');
    Route::post('/user/default-payment-method', 'UserController@updateCard');
    Route::get('/user/favouriteItems', 'UserController@favouriteItems');
    Route::post('/user/addFavourite', 'UserController@addToFavourite');
    Route::get('/user/validateCode', 'UserController@validatePostCode');
    Route::resource('user', 'UserController')->only(['update', 'show']);
    // Route::resource('user', 'UserController'); 
    Route::resource('review', 'ReviewController')->only(['store']);
    Route::post('promotion/validate', 'PromotionController@validatePromocode')->name('promotion.validate');  
    Route::post('/user/addItemReview', 'UserController@addItemReview');
});

//web routes
Route::namespace('Api\Web')->prefix('web')->name('web.')->group(function () {
    Route::get('/popular-items', 'PageController@popularItems')->name('web.popular-items');
    Route::get('/menu-categories', 'MenuController@all')->name('web.menu-categories');
    Route::get('/menu-items', 'MenuItemController@byMenu')->name('web.menu-items');
    Route::get('/all-menu-items', 'MenuItemController@all')->name('web.all-menu-items');
    Route::get('/menu-item', 'MenuItemController@get')->name('web.menu-item');
    Route::get('/module', 'PageController@getModuleStatus');
    Route::get('/version', 'PageController@getVersion');
    Route::get('/deals', 'MenuController@getDeals')->name('web.deals');
    Route::get('/offers', 'MenuController@getOffers')->name('web.offers');
    Route::get('/offerItems', 'MenuController@getOffersItemByID')->name('web.offerItems');
}); 
