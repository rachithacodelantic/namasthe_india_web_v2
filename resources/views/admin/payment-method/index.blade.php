@extends('layouts.admin')
@section('content')
    <div id="payment_methods">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'payment-method'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <div class="select-box">
                                <br />
                                <button class="btn btn-warning" style="color: #fff;" data-toggle="modal" data-target="#add-method">Add New Method</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 menu-description">
                                <div class="">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active table-responsive">
                                                <table class="requests-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Method</th>
                                                            <th>Branch</th> 
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="payment_method in payment_methods">
                                                            <td style="width: 40%;">@{{ payment_method.name }}</td>
                                                            <td style="width: 50%;">@{{ payment_method.branch }}</td>
                                                            <td style="width: 40%;">
                                                                <a href="#" class="btn btn-danger" @click.prevent="deleteMethod(payment_method.id)"><i class="fa fa-times"></i></a> 
                                                            </td> 
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous"
                                           @click.prevent="jumpToPage(pagination.first_page)">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li v-for="page in pagination.last_page"
                                        :class="getCurrentPage(page)"><a href="#" @click.prevent="jumpToPage(page)">@{{
                                            page }}</a>
                                    </li>
                                    <li>
                                        <a href="#" aria-label="Next" @click.prevent="jumpToPage(pagination.last_page)">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
        <div class="modal fade add-new-item-modal" id="add-method" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4>Add Payment Method</h4>
                        <hr> 
                        <div class="row" :class="{'has-error':payment_method_validation}"> 
                            <div class="col-md-6 col-sm-6">
                                <label>Select Branch</label> 
                                <select v-model="branch" class="selectpicker">
                                @foreach($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                @endforeach
                                </select>
                                <span v-if="payment_method_validation.branches" class="help-block">
                                    @{{ payment_method_validation.branches[0] }}
                                </span>
                            </div> 
                            <div class="col-md-6 col-sm-6">
                                <label>Select Method</label> 
                                <select v-model="method" class="selectpicker"> 
                                    <option value="Cash">Cash</option>
                                    <option value="Pay Online">Pay Online</option> 
                                    <option value="Mobile">Mobile</option>  
                                </select> 
                            </div>  
                        </div>
                        <input type="submit" name="" value="Create" @click.prevent="createMethod">
                    </div>
                </div> 
            </div>
        </div> 
    </div> 
    <script type="text/javascript">
        var data = { 
            branches:{!! json_encode($branches) !!},
            payment_methods: {}, 
            method: '',
            branch: '',
            description: '', 
            pagination: {},
            payment_method_validation: [],
        };

        var menus = new Vue({
            data: data,
            el: '#payment_methods',
            mounted: function () {
                this.getBranches();
            },
            methods: {
                move(id, arrFrom, arrTo) {
                    var index = arrFrom.findIndex(function(el) {
                        return el.id == id;
                    });
                    var item = arrFrom[index]; 
                    arrFrom.splice(index, 1);
                    arrTo.push(item);
                },
                getBranches: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    }
                    axios.post('{{route('admin.payment-method.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                    }).then(function (response) { 
                        $this.payment_methods = response.data.data.payment_methods; 
                        $this.pagination = {
                            current_page: response.data.data.payment_methods.current_page,
                            from: response.data.data.payment_methods.from,
                            last_page: response.data.data.payment_methods.last_page,
                            // path: response.data.data.deals.path,
                            per_page: response.data.data.payment_methods.per_page,
                            to: response.data.data.payment_methods.to,
                            total: response.data.data.payment_methods.total,
                        };
                    }).catch(function (error) {
                        console.log(error);
                    }); 
                }, 
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                createMethod: function (page) {
                    let $this = this; 
                    axios.post('{{route('admin.payment-method.store')}}', {
                        _token: '{{csrf_token()}}',
                        name: $this.method,
                        branches_id: $this.branch,
                    }).then(function (response) {
                        if (response.data.message == 'success') {
                            jQuery('#add-method').modal('hide');
                            $this.getBranches();
                            $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                        }
                    }).catch(function (error) {
                        if (error.response.status == 422) {
                            $this.payment_method_validation = error.response.data.errors;
                        } 
                        if (error.response.status == 401) {
                            $.alert({
                                title: '{{__('Oh Sorry!')}}',
                                content: 'Please Login to Continue!',
                                theme: 'error'
                            });
                        }
                        if (error.response.status == 402) {
                            $.alert({
                                title: '{{__('Oh Sorry!')}}',
                                content: error.response.data.message,
                                theme: 'error'
                            });
                        }
                    });
                },
                deleteMethod: function (method_id) {
                    let $this = this; 
                    var selected_methods = this.payment_methods.filter(function (method) {
                        return method.id == method_id;
                    });   
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to delete this method?',
                        theme: 'error',
                        buttons: {
                            confirm: function () {
                                axios.post('{{route('admin.payment-method.delete')}}', {
                                    _token: '{{csrf_token()}}',
                                    method_id: method_id
                                }).then(function (response) {
                                    if (response.data.message == 'success') {
                                        $.alert({
                                            title: 'Success!',
                                            content: response.data.result,
                                            theme: 'success'
                                        });
                                        $this.getBranches();
                                    }
                                }).catch(function (error) {
                                    console.log(error);
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });
                }
            }
        });

    </script>

@endsection

<!-- Edit Item Modal -->
