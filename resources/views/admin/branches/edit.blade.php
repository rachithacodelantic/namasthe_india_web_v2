@extends('layouts.admin')
@section('content')
    <div id="admin-branch">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'branches'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <!-- <div class="select-box">
                                <br />
                                <a href="{{ URL::to('admin/branches/create') }}" class="btn btn-warning" style="color: #fff;">Add New Branch</a> 
                            </div> -->
                        </div> 
                        <div class="row" style="margin: 0;">
                            <div class="col-md-12 col-sm-12 col-xs-12 menu-description">
                                <br />
                                <div class="branch-profile-section">
                                    <h2>{{$branch->name}}</h2>
                                    <div class="branch-profile-form">
                                        <br />
                                        <div class="row">
                                            <div class="col-md-9 col-xs-12">
                                                <form action="{{ URL::to('admin/branches') }}/{{$branch->id}}" method="post">
                                                    @csrf
                                                    @method('put')
                                                    <h3>Basic Information</h3>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Restaurant Name*</label>
                                                            <input type="text"
                                                                   class="form-control" name="name"
                                                                   value="{{$branch->name}}"
                                                                   placeholder="Enter Branch Name">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Email address*</label>
                                                            <input type="email"
                                                                   class="form-control" name="address"
                                                                   value="{{$branch->email}}"
                                                                   placeholder="Enter Email Address">
                                                        </div>
                                                    </div> 
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Restaurant
                                                                Country*</label>
                                                            <input class="form-control" name="country"
                                                                   value="{{$branch->country}}">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Restaurant Street
                                                                Name*</label>
                                                            <input type="text"
                                                                   class="form-control" name="county"
                                                                   value="{{$branch->county}}"
                                                                   placeholder="Enter Street Name">
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Restaurant City*</label>
                                                            <input class="form-control" name="city"
                                                                   value="{{$branch->city}}">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Restaurant
                                                                Postcode*</label>
                                                            <input type="text"
                                                                   class="form-control" name="postcode"
                                                                   value="{{$branch->postcode}}"
                                                                   placeholder="Enter Zip/Postal Code">
                                                        </div>
                                                    </div>  
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Phone Number*</label>
                                                            <input type="tel"
                                                                   class="form-control" name="phone"
                                                                   value="{{$branch->phone}}"
                                                                   placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <h3>Restaurant Attributes</h3>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Cuisine*</label>
                                                            <select class="form-control select2" name="cuisines[]" multiple="multiple">>
                                                                @foreach($cuisines as $cuisine)
                                                                    <option
                                                                        @foreach($branch->cuisines as $restaurant_cuisine)
                                                                        @if($restaurant_cuisine->id==$cuisine->id)
                                                                        selected="selected"
                                                                        @endif
                                                                        @endforeach
                                                                        value="{{$cuisine->id}}">{{$cuisine->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Parking
                                                                Available* </label>
                                                            <select class="form-control" name="parking">
                                                                <option @if($branch->parking) selected="selected"
                                                                        @endif value="true">
                                                                    Available
                                                                </option>
                                                                <option @if(!$branch->parking) selected="selected"
                                                                        @endif value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Delivery Available </label>
                                                            <select class="form-control" name="delivery">
                                                                <option @if($branch->delivery) selected="selected"
                                                                        @endif value="true">
                                                                    Available
                                                                </option>
                                                                <option @if(!$branch->delivery) selected="selected"
                                                                        @endif value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Takeaway
                                                                Available</label>
                                                            <select class="form-control" name="takeaway">
                                                                <option @if($branch->takeaway) selected="selected"
                                                                        @endif value="true">
                                                                    Available
                                                                </option>
                                                                <option @if(!$branch->takeaway) selected="selected"
                                                                        @endif value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Reservation
                                                                Available</label>
                                                            <select class="form-control" name="reserve">
                                                                <option @if($branch->reserve) selected="selected"
                                                                        @endif value="true">
                                                                    Available
                                                                </option>
                                                                <option @if(!$branch->reserve) selected="selected"
                                                                        @endif value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div> 
                                                    <br/>
                                                    <div class="row">
                                                        @if($branch->id != 1)
                                                            <div class="col-md-6 col-sm-6">
                                                                <label>Restaurant Admin* </label>
                                                                <!-- <span class="">
                                                                    {{ $branch->user }}
                                                                </span> -->
                                                                <select class="form-control" name="users_id"> 
                                                                    @if($branch->users_id == $branch->user_id)
                                                                        <option value="{{$branch->users_id}}">{{ $branch->user }}</option>
                                                                    @endif
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}">
                                                                            {{$user->email}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        @endif   
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Number of Seats*</label>
                                                            <input min="1" type="number"
                                                                   class="form-control" name="seats"
                                                                   value="{{$branch->seats}}"
                                                                   placeholder="Enter Number of Seats">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Latitude </label> 
                                                            <input type="text" class="form-control" name="latitude" value="{{$branch->latitude}}" placeholder="Enter Latitude">
                                                        </div> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Longtitude</label>
                                                            <input min="1" type="number" class="form-control" name="longtitude" value="{{$branch->longtitude}}" placeholder="Enter Longtitude">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <label>Description</label>
                                                            <textarea class="form-control m-editor" name="description"
                                                                      placeholder="Enter restaurant description" rows="5" cols="10">{{$branch->description}}</textarea>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <p>Reservation Status</p>
                                                            <label class="container-checkbox">
                                                                <p>Reservation Full</p><input type="checkbox" :checked="reserve_flag=='0'" @change.prevent="setReserveStatus(0)" value="0" name="reserve_status">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <label class="container-checkbox">
                                                                <p>Reservation Free</p><input type="checkbox" :checked="reserve_flag=='1'" @change.prevent="setReserveStatus(1)" value="1" name="reserve_status">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <br/> 
                                                    <div class="branch-hours-row">
                                                        <h3>Restaurant Hours</h3>
                                                        <div class="row profile_class-section">
                                                            <div class="col-xs-12">
                                                                <div class="row" v-for="(opening_hour, index) in opening_hours">
                                                                    <div class="col-md-9">
                                                                        <br/>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <p>Day</p>
                                                                                <select :name="'days['+index+'][day]'" v-model="opening_hour.day">
                                                                                    <option value="">Select Day</option>
                                                                                    @foreach($weekdays as $weekday_key => $weekday)
                                                                                        <option value="{{$weekday_key}}">{{$weekday}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <p>Opening Time</p>
                                                                                <select :name="'days['+index+'][opening_time]'"
                                                                                        v-model="opening_hour.opening_time">
                                                                                    <option value="">Select Opening Time</option>
                                                                                    @foreach($times as $time)
                                                                                        <option value="{{$time}}">{{$time}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <p>Closing Time</p>
                                                                                <select :name="'days['+index+'][closing_time]'"
                                                                                        v-model="opening_hour.closing_time">
                                                                                    <option value="">Select Closing Time</option>
                                                                                    @foreach($times as $time)
                                                                                        <option value="{{$time}}">{{$time}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <p>&nbsp;</p>
                                                                        <button type="button" @click="removeHour(index)" class="btn btn-danger"><i
                                                                                class="fa fa-trash"></i> Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-xs-12" style="padding: 10px 0 0;">
                                                                        <button class="btn btn-success" type="button" @click.prevent="addHour"><i
                                                                                class="fa fa-plus"></i> Add New
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12">
                                                            <input style="padding: 10px;font-size: 14px;" type="submit" class="btn btn-success" value="Save Changes">
                                                        </div>
                                                    </div>
                                                    <br/><br/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>   
                    </div>
                </div>
            </div>
        </section> 
    </div> 
    <script type="text/javascript">
        const data = { 
            opening_hours: {!! json_encode($branch->openingHours) !!},
            reserve_flag: {{$branch->reserve_status}},
        }; 

        const restaurant = new Vue({
            data: data,
            el: '#admin-branch',
            mounted: function () {

            },
            methods: { 
                addHour: function () {
                    this.opening_hours.push({
                        day: '',
                        opening_time: '',
                        closing_time: '',
                    });
                },
                removeHour: function (index) {
                    this.opening_hours.splice(index, 1);
                },
                setReserveStatus: function (status) { 
                    this.reserve_flag = status;
                },
            }
        });

    </script>

@endsection 
