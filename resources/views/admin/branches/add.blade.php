@extends('layouts.admin')
@section('content')
    <div id="admin-branch">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'branches'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <div class="select-box">
                                <br /> 
                            </div>
                        </div> 
                        <div class="row" style="margin: 0;">
                            <div class="col-md-12 col-sm-12 col-xs-12 menu-description">
                                <br />
                                <div class="branch-profile-section">
                                    <h2>Add New Branch</h2>
                                    <div class="branch-profile-form">
                                        <br />
                                        <div class="row">
                                            <div class="col-md-9 col-xs-12">
                                                <form action="{{ URL::to('admin/branches') }}" method="post">
                                                    @csrf
                                                    @method('post')
                                                    <h3>Basic Information</h3>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch Name*</label>
                                                            <input type="text"
                                                                   class="form-control" name="name" value="{{old('name')}}" 
                                                                   placeholder="Enter Branch Name">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Email address*</label>
                                                            <input type="email"
                                                                   class="form-control" name="address" value="{{old('address')}}" 
                                                                   placeholder="Enter Email Address">
                                                        </div>
                                                    </div> 
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch
                                                                Country*</label>
                                                            <input class="form-control" name="country" value="{{old('country')}}" placeholder="Enter Country Name">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch Street
                                                                Name*</label>
                                                            <input type="text" class="form-control" name="county" value="{{old('county')}}" placeholder="Enter Street Name">
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch City*</label>
                                                            <input class="form-control" name="city" value="{{old('city')}}" placeholder="Enter City">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch
                                                                Postcode*</label>
                                                            <input type="text" class="form-control" name="postcode" value="{{old('postcode')}}" placeholder="Enter Zip/Postal Code">
                                                        </div>
                                                    </div>  
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Phone Number*</label>
                                                            <input type="tel" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <h3>Branch Attributes</h3>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Cuisine*</label>
                                                            <select class="form-control select2" name="cuisines[]" multiple="multiple">
                                                                @foreach($cuisines as $cuisine)
                                                                    <option value="{{$cuisine->id}}">{{$cuisine->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Parking
                                                                Available* </label>
                                                            <select class="form-control" name="parking">
                                                                <option value="true">
                                                                    Available
                                                                </option>
                                                                <option value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Delivery Available </label>
                                                            <select class="form-control" name="delivery">
                                                                <option value="true">
                                                                    Available
                                                                </option>
                                                                <option value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Takeaway
                                                                Available</label>
                                                            <select class="form-control" name="takeaway">
                                                                <option value="true">
                                                                    Available
                                                                </option>
                                                                <option value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Is Reservation
                                                                Available</label>
                                                            <select class="form-control" name="reserve">
                                                                <option value="true">
                                                                    Available
                                                                </option>
                                                                <option value="false">
                                                                    Not Available
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div> 
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Branch Admin* </label> 
                                                            <select class="form-control" name="users_id">  
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}">
                                                                        {{$user->email}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Number of Seats*</label>
                                                            <input min="1" type="number" class="form-control" name="seats" value="{{old('seats')}}" placeholder="Enter Number of Seats">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Latitude </label> 
                                                            <input type="text" class="form-control" name="latitude" value="{{old('latitude')}}" placeholder="Enter Latitude">
                                                        </div> 
                                                        <div class="col-md-6 col-sm-6">
                                                            <label>Longtitude</label>
                                                            <input min="1" type="number" class="form-control" name="longtitude" value="{{old('longtitude')}}" placeholder="Enter Longtitude">
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <label>Branch Description</label>
                                                            <textarea class="form-control m-editor" name="description" placeholder="Enter restaurant description" rows="5" cols="10">{{old('description')}}</textarea>
                                                        </div>
                                                    </div>
                                                    <br/> 
                                                    <div class="branch-hours-row">
                                                        <h3>Branch Hours</h3>
                                                        <div class="row profile_class-section">
                                                            <div class="col-xs-12">
                                                                <div class="row" v-for="(opening_hour, index) in opening_hours">
                                                                    <div class="col-md-9">
                                                                        <br/>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <p>Day</p>
                                                                                <select :name="'days['+index+'][day]'" v-model="opening_hour.day">
                                                                                    <option value="">Select Day</option>
                                                                                    @foreach($weekdays as $weekday_key => $weekday)
                                                                                        <option value="{{$weekday_key}}">{{$weekday}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <p>Opening Time</p>
                                                                                <select :name="'days['+index+'][opening_time]'"
                                                                                        v-model="opening_hour.opening_time">
                                                                                    <option value="">Select Opening Time</option>
                                                                                    @foreach($times as $time)
                                                                                        <option value="{{$time}}">{{$time}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <p>Closing Time</p>
                                                                                <select :name="'days['+index+'][closing_time]'"
                                                                                        v-model="opening_hour.closing_time">
                                                                                    <option value="">Select Closing Time</option>
                                                                                    @foreach($times as $time)
                                                                                        <option value="{{$time}}">{{$time}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <p>&nbsp;</p>
                                                                        <button type="button" @click="removeHour(index)" class="btn btn-danger"><i
                                                                                class="fa fa-trash"></i> Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-xs-12" style="padding: 10px 0 0;">
                                                                        <button class="btn btn-success" type="button" @click.prevent="addHour"><i
                                                                                class="fa fa-plus"></i> Add New
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12">
                                                            <input style="padding: 10px;font-size: 14px;" type="submit" class="btn btn-success" value="Save Changes">
                                                        </div>
                                                    </div>
                                                    <br/><br/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>   
                    </div>
                </div>
            </div>
        </section> 
    </div> 
    <script type="text/javascript">
        const data = { 
            opening_hours: [],
        }; 

        const restaurant = new Vue({
            data: data,
            el: '#admin-branch',
            mounted: function () {

            },
            methods: { 
                addHour: function () {
                    this.opening_hours.push({
                        day: '',
                        opening_time: '',
                        closing_time: '',
                    });
                },
                removeHour: function (index) {
                    this.opening_hours.splice(index, 1);
                },
            }
        });

    </script>

@endsection 
