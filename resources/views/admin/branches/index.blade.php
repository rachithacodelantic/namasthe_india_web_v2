@extends('layouts.admin')
@section('content')
    <div id="branches">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'branches'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <!-- <div class="select-box">
                                <br />
                                <a href="{{ URL::to('admin/branches/create') }}" class="btn btn-warning" style="color: #fff;">Add New Branch</a>
                            </div> -->
                        </div>
                        <div class="row ">
                            <div class="col-md-12 col-sm-12 col-xs-12 menu-description">
                                <div class="">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active table-responsive">
                                                <table class="requests-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Branch Name</th>
                                                            <th>Country</th>
                                                            <th>Email</th>
                                                            <th>Phone Number</th>
                                                            <th>Status</th>
                                                            <th>Set Status</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="branch in branches">
                                                            <td>@{{ branch.name }}</td>
                                                            <td>@{{ branch.country }}</td>
                                                            <td>@{{ branch.email }}</td>
                                                            <td>@{{ branch.phone }}</td>
                                                            <td>
                                                                <label class="label label-success" v-if="branch.active">Active</label>
                                                                <label class="label label-danger" v-if="!branch.active">Removed</label>
                                                            </td>
                                                            <td>
                                                                <a href="#" v-if="branch.active && branch.id != 1" class="btn btn-danger"
                                                                   @click.prevent="deleteBranch(branch.id)"><i class="fa fa-times"></i></a>

                                                                <a href="#" v-if="!branch.active && branch.id != 1" class="btn btn-success"
                                                                   @click.prevent="deleteBranch(branch.id)"><i class="fa fa-check"></i></a>
                                                            </td>
                                                            <td>
                                                                <a v-if="branch.active"
                                                                   :href="'{{ URL::to('admin/branches') }}/'+branch.id+'/edit'"><img
                                                                        src="{{asset('master-admin/img/view.png')}}"></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous"
                                           @click.prevent="jumpToPage(pagination.first_page)">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li v-for="page in pagination.last_page"
                                        :class="getCurrentPage(page)"><a href="#" @click.prevent="jumpToPage(page)">@{{
                                            page }}</a>
                                    </li>
                                    <li>
                                        <a href="#" aria-label="Next" @click.prevent="jumpToPage(pagination.last_page)">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        var data = {
            menus: '',
            branches: {},
            deals_item_validation: [],
            deal: {},
            name: '',
            description: '',
            price: '',
            vat_category: '',
            pagination: {},
        };

        var menus = new Vue({
            data: data,
            el: '#branches',
            mounted: function () {
                this.getBranches();
            },
            methods: {
                move(id, arrFrom, arrTo) {
                    var index = arrFrom.findIndex(function(el) {
                        return el.id == id;
                    });
                    var item = arrFrom[index];

                    arrFrom.splice(index, 1);
                    arrTo.push(item);
                },
                getBranches: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    }
                    axios.post('{{route('admin.branches.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                    }).then(function (response) {
                        console.log(response.data.data.branches.data); 
                        $this.branches = response.data.data.branches.data;
                        // console.log($this.branches); 
                        $this.pagination = {
                            current_page: response.data.data.branches.current_page,
                            from: response.data.data.branches.from,
                            last_page: response.data.data.branches.last_page,
                            // path: response.data.data.deals.path,
                            per_page: response.data.data.branches.per_page,
                            to: response.data.data.branches.to,
                            total: response.data.data.branches.total,
                        };
                    }).catch(function (error) {
                        console.log(error);
                    }); 
                },
                jumpToPage: function (page) {
                    this.getDeals(page);
                },
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                deleteBranch: function (branch_id) {
                    let $this = this; 
                    var selected_branches = this.branches.filter(function (branch) {
                        return branch.id == branch_id;
                    }); 
                    // console.log(selected_branches);  
                    var message = 'reactivate'; 
                    if (selected_branches[0].active) {
                        message = 'deactivate';
                    }

                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to ' + message + ' this branch?',
                        theme: 'error',
                        buttons: {
                            confirm: function () {
                                axios.post('{{route('admin.branch.deactive')}}', {
                                    _token: '{{csrf_token()}}',
                                    branch_id: branch_id
                                }).then(function (response) {
                                    if (response.data.message == 'success') {
                                        $.alert({
                                            title: 'Success!',
                                            content: response.data.result,
                                            theme: 'success'
                                        });
                                        $this.getBranches();
                                    }
                                }).catch(function (error) {
                                    console.log(error);
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });
                }
            }
        });

    </script>

@endsection

<!-- Edit Item Modal -->
