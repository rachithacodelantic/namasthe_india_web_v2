@extends('layouts.admin')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> 
    <div id="offers">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'offers'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <div class="select-box">
                                <button class="add-new offers-new" data-toggle="modal" data-target="#add-new-item-modal">Add New
                                    Item +
                                </button>
                                <div class="sort">{{__('Sort by:')}}
                                    <select v-model="sort" @change.prevent="sortMenu">
                                        <option value="asc">Sort by A-Z</option>
                                        <option value="desc">Sort by Z-A</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-12" v-for="offer in offers">
                                <div class="item-list">
                                    <div class="row ">
                                        <div class="col-md-9 col-sm-9 col-xs-12 menu-description">
                                            <div class="">
                                                <h3>@{{ offer.name }}
                                                    &nbsp; <a
                                                        class="text-red" @click.prevent="editOfferItem(offer)">edit
                                                        item</a> &nbsp;
                                                    <button @click.prevent="forceDeleteOfferItem(offer.id)"
                                                            class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>
                                                        Delete
                                                    </button>
                                                </h3>
                                                <p>@{{ offer.description }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="img-box">
                                                <img :src="offer.image" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item-cost">
                                        <div class="col-md-9 col-sm-9"></div>
                                        <div class="col-md-3 col-sm-3">
                                            <h5>‎£ @{{ currency(offer.price) }}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous"
                                               @click.prevent="jumpToPage(pagination.first_page)">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <li v-for="page in pagination.last_page"
                                            :class="getCurrentPage(page)"><a href="#" @click.prevent="jumpToPage(page)">@{{
                                                page }}</a>
                                        </li>
                                        <li>
                                            <a href="#" aria-label="Next" @click.prevent="jumpToPage(pagination.last_page)">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 

        <div class="modal fade add-new-item-modal modal-lg" id="add-new-item-modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4>Add New Offer Item</h4>
                        <hr>
                        <div class="form-group" :class="{'has-error':offer_item_validation.name}">
                            <p>Item Name*</p>
                            <input type="text" placeholder="Enter Item Name" name="" v-model="name">
                            <span class="help-block" v-if="offer_item_validation.name">
                                @{{ offer_item_validation.name[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':offer_item_validation.description}">
                            <p>Item Description</p>
                            <textarea placeholder="Enter Item Description" rows="6" v-model="description"
                                      maxlength="255"></textarea>
                            <span class="help-block" v-if="offer_item_validation.description">
                                @{{ offer_item_validation.description[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':offer_item_validation.price}">
                            <p>Item Price*</p>
                            <input type="number" placeholder="Enter Price" name="" v-model="price" min="0" step="0.01">
                            <span class="help-block" v-if="offer_item_validation.price">
                                @{{ offer_item_validation.price[0] }}
                            </span>
                        </div> 
                        <div class="form-group" :class="{'has-error':offer_item_validation.start}">
                            <p>Start Date*</p> 
                            <!-- <date-picker type="text" placeholder="Enter Date" name="" v-model="start"></date-picker>  -->
                            <input type="text" placeholder="Enter Date" name="" id="from_date" v-model="start" readonly="readonly"> 
                            <span class="help-block" v-if="offer_item_validation.start">
                                @{{ offer_item_validation.start[0] }}
                            </span>
                        </div> 
                        <div class="form-group" :class="{'has-error':offer_item_validation.close}">
                            <p>End Date*</p>
                            <!-- <date-picker type="text" placeholder="Enter Date" name="" v-model="close"></date-picker>  -->
                            <input type="text" placeholder="Enter Date" name="" id="to_date" v-model="close" readonly="readonly">
                            <span class="help-block" v-if="offer_item_validation.close">
                                @{{ offer_item_validation.close[0] }}
                            </span>
                        </div> 
                        <div class="form-group" :class="{'has-error':offer_item_validation.vat_category}">
                            <p>Vat Category</p>
                            <select name="" class="form-control" v-model="vat_category">
                                <option value="food" selected="selected">Food</option>
                                <option value="alcohol">Alcohol</option>
                            </select>
                            <span class="help-block" v-if="offer_item_validation.vat_category">
                                @{{ offer_item_validation.vat_category[0] }}
                            </span>
                        </div> 

                        <div class="img-upload-box">
                            Click or Drag Item Image to Upload 
                            <input type="file" id="create-image">
                            <p class="help-block">Max filesize: 1MB (Image size should 267 * 145)</p>
                        </div>

                        <div class="form-group">
                            <img src="" alt="" class="img-responsive" id="create-preview" style="display: none;">
                        </div>
                        <hr>
                        <div class="form-group">
                            <p>Menu Category</p>
                            <select v-model="selected_menu_id" class="form-control" @change="getMenuItems">
                                <option v-for="menu in menus" :value="menu.id">@{{ menu.name }}</option>
                            </select>
                        </div>
                        <div class="menu-items-multi-select" :class="{'has-error':offer_item_validation.selectedMenuItemsValues}">
                            <select v-model="availableMenuItemsValues" multiple="multiple" class="available-items">
                                <option v-for="menuItem in availableMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <div class="btn-multi-select">
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="addMenuItems">></button>
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="removeMenuItems"><</button>
                            </div>
                            <select v-model="selectedMenuItemsValues" multiple="multiple" class="selected-items">
                                <option v-for="menuItem in selectedMenuItems" :key="menuItem.id" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <span class="help-block" v-if="offer_item_validation.selectedMenuItemsValues">
                                @{{ offer_item_validation.selectedMenuItemsValues[0] }}
                            </span>
                        </div>
                        <br/>
                        <input type="submit" name="" value="Submit" @click.prevent="createOfferItem">
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Item Modal -->
        <div class="modal fade add-new-item-modal" id="edit-item-modal" role="dialog"> 
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4>Edit Offer Item</h4>
                        <hr>
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.name}">
                            <p>Item Name*</p>
                            <input type="text" placeholder="Enter Item Name" name="" v-model="offer.name">
                            <span class="help-block" v-if="offer_item_update_validation.name">
                                @{{ offer_item_update_validation.name[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.description}">
                            <p>Item Description*</p>
                            <textarea placeholder="Enter Item Description" rows="6"
                                      v-model="offer.description" maxlength="255"></textarea>
                            <span class="help-block" v-if="offer_item_update_validation.description">
                                @{{ offer_item_update_validation.description[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.price}">
                            <p>Item Price*</p>
                            <input type="text" placeholder="Enter Price" name="" v-model="offer.price" min="0"
                                   step="0.01">
                            <span class="help-block" v-if="offer_item_update_validation.price">
                                @{{ offer_item_update_validation.price[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.start}">
                            <p>Start Date*</p> 
                            <!-- <input type="text" v-model="start" id="datepicker1"/> -->
                            <!-- <date-picker type="text" placeholder="Enter Date" name="" v-model="start" id="datepicker1"></date-picker>  -->
                            <input type="text" placeholder="Enter Date" name="" v-model="start" id="datepicker1" readonly="readonly">
                            <span class="help-block" v-if="offer_item_update_validation.start">
                                @{{ offer_item_update_validation.start[0] }}
                            </span>
                        </div> 
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.close}">
                            <p>End Date*</p>
                            <!-- <date-picker type="text" placeholder="Enter Date" name="" v-model="close"></date-picker>  -->
                            <input type="text" placeholder="Enter Date" name="" v-model="close" id="datepicker2" readonly="readonly">
                            <span class="help-block" v-if="offer_item_update_validation.close">
                                @{{ offer_item_update_validation.close[0] }}
                            </span>
                        </div> 
                        <div class="form-group" :class="{'has-error':offer_item_update_validation.vat_category}">
                            <p>Vat Category*</p>
                            <select name="" v-model="offer.vat_category">
                                <option value="food">Food</option>
                                <option value="alcohol">Alcohol</option>
                            </select>
                            <span class="help-block" v-if="offer_item_update_validation.vat_category">
                                @{{ offer_item_validation.vat_category[0] }}
                            </span>
                        </div> 

                        <div class="img-upload-box">
                            Click or Drag Item Image to Upload 
                            <input type="file" id="edit-image">
                            <p class="help-block">Max filesize: 1MB (Image size should 267 * 145)</p>
                        </div>

                        <div class="form-group">
                            <img src="" alt="" class="img-responsive" id="edit-preview" style="display: none">
                        </div>
                        <hr>
                        <div class="form-group">
                            <p>Menu Category</p>
                            <select v-model="selected_menu_id" class="form-control" @change="getMenuItems">
                                <option v-for="menu in menus" :value="menu.id">@{{ menu.name }}</option>
                            </select>
                        </div>
                        <div class="menu-items-multi-select" :class="{'has-error':offer_item_update_validation.selectedMenuItemsValues}">
                            <select v-model="availableMenuItemsValues" multiple="multiple" class="available-items">
                                <option v-for="menuItem in availableMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <div class="btn-multi-select">
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="addMenuItems">></button>
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="removeMenuItems"><</button>
                            </div>
                            <select v-model="selectedMenuItemsValues" multiple="multiple" class="selected-items">
                                <option v-for="menuItem in selectedMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <span class="help-block" v-if="offer_item_update_validation.selectedMenuItemsValues">
                                @{{ offer_item_update_validation.selectedMenuItemsValues[0] }}
                            </span>
                        </div>
                        <br/> 
                        <input type="submit" name="" value="Submit" @click.prevent="updateOfferItem">
                    </div>
                </div> 
            </div>
        </div>
    </div> 
    <script type="text/javascript">

        $(document).ready(function() {
            $("#from_date").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }); 
            $("#to_date").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });  
            $("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }); 
            $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }); 
        });

        var data = {
            menus:{!! json_encode($menus) !!},
            offers: {},
            offer_item_validation: [],
            offer_item_update_validation: [],
            offer: {}, 
            name: '',
            description: '',
            price: '',
            start: '',
            close: '',
            vat_category: '',
            sort: 'asc',
            pagination: {},
            menu_id: '',
            selected_menu_id: 0,
            availableMenuItems: [],
            selectedMenuItems: [],
            availableMenuItemsValues: [],
            selectedMenuItemsValues: [],
            pickedMenuItems: []
        }; 
        // console.log(data.deals);

        var menus = new Vue({
            data: data,
            el: '#offers',
            mounted: function () {
                this.getMenus();
                this.getOffers();
            },
            methods: {
                move(id, arrFrom, arrTo) {
                    var index = arrFrom.findIndex(function(el) {
                        return el.id == id;
                    });
                    var item = arrFrom[index]; 
                    arrFrom.splice(index, 1);
                    arrTo.push(item);
                },
                addMenuItems() {
                    var selected = this.availableMenuItemsValues.slice(0);

                    for (var i = 0; i < selected.length; ++i) {
                        this.move(selected[i], this.availableMenuItems, this.selectedMenuItems);
                        this.pickedMenuItems.push(selected[i]);
                    }
                },
                removeMenuItems() {
                    var selected = this.selectedMenuItemsValues.slice(0);

                    for (var i = 0; i < selected.length; ++i) {
                        this.move(selected[i], this.selectedMenuItems, this.availableMenuItems);
                        this.pickedMenuItems.splice (this.pickedMenuItems.indexOf (selected[i]), 1);
                    }

                },
                getMenuItems: function () {
                    let $this = this;
                    axios.post('{{route('admin.menu-items.get')}}', {
                        _token: '{{csrf_token()}}',
                        menu_id: $this.selected_menu_id
                    })
                        .then(function (response) {
                            $this.availableMenuItems = response.data.data.menuItems;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                getMenus: function () {
                    let $this = this;
                    axios.post('{{route('admin.menus.get')}}', {
                        _token: '{{csrf_token()}}',
                    })
                        .then(function (response) {
                            $this.menus = response.data.data.menus;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                getOffers: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    }
                    axios.post('{{route('admin.offers.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                    }).then(function (response) {
                        console.log(response);
                        $this.offers = response.data.data.offers.data;
                        $this.sortMenu();
                        $this.pagination = {
                            current_page: response.data.data.offers.current_page,
                            from: response.data.data.offers.from,
                            last_page: response.data.data.offers.last_page,
                            path: response.data.data.offers.path,
                            per_page: response.data.data.offers.per_page,
                            to: response.data.data.offers.to,
                            total: response.data.data.offers.total,
                        };
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                jumpToPage: function (page) {
                    this.getOffers(page);
                },
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                createOfferItem: function () {
                    let $this = this;
                    let post_data = {
                        _token: '{{csrf_token()}}',
                        name: $this.name,
                        description: $this.description,
                        price: $this.price,
                        start: $this.start,
                        close: $this.close,
                        vat_category: $this.vat_category,
                        selectedMenuItemsValues: $this.pickedMenuItems,
                    };

                    if (document.getElementById('create-image').files.length) {
                        let file = document.getElementById('create-image').files[0]; 
                        this.getFile(file).then(function (data) {

                            post_data.image = data; 
                            axios.post('{{route('admin.offers.store')}}', post_data).then(function (response) {
                                if (response.data.message == 'success') {
                                    jQuery('#add-new-item-modal').modal('hide');
                                    $this.getOffers();
                                    $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                                    $this.name = "";
                                    $this.description = "";
                                    $this.price = "";
                                    $this.start = "";
                                    $this.close = "";
                                    $this.vat_category = "";
                                    $this.selected_menu_id = "";
                                    $this.selectedMenuItemsValues = [];
                                }
                            }).catch(function (error) {
                                if (error.response.status == 422) {
                                    $this.offer_item_validation = error.response.data.errors;
                                }

                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please Login to Continue!',
                                        theme: 'error'
                                    });
                                }
                            });
                        });

                    } else {
                        axios.post('{{route('admin.offers.store')}}', post_data)
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    jQuery('#add-new-item-modal').modal('hide');
                                    $this.getOffers();
                                    $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                                    $this.name = "";
                                    $this.description = "";
                                    $this.price = "";
                                    $this.start = "";
                                    $this.close = "";
                                    $this.vat_category = "";
                                    $this.selected_menu_id = "";
                                    $this.selectedMenuItemsValues = [];
                                }
                            }).catch(function (error) {
                                if (error.response.status == 422) {
                                    $this.offer_item_validation = error.response.data.errors;
                                }

                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please Login to Continue!',
                                        theme: 'error'
                                    });
                                }
                            });
                    } 
                },
                editOfferItem: function (offer) {
                    // console.log(offer); 
                    this.offer = offer;
                    this.availableMenuItems = offer.availableMenuItems;
                    this.selectedMenuItems = offer.selectedMenuItems;
                    jQuery('#edit-item-modal').modal('show');
                },
                updateOfferItem: function () {
                    let $this = this;
                    let put_data = {
                        _token: '{{csrf_token()}}',
                        name: $this.offer.name,
                        description: $this.offer.description,
                        price: $this.offer.price,
                        start: $this.offer.start,
                        close: $this.offer.close,
                        vat_category: $this.offer.vat_category,
                        selectedMenuItemsValues: $this.pickedMenuItems 
                    };

                    if (document.getElementById('edit-image').files.length) {
                        let file = document.getElementById('edit-image').files[0];
                        this.getFile(file).then(function (data) {
                            put_data.image = data;
                            axios.put('{{route('admin.offers.index')}}/' + $this.offer.id, put_data).then(function (response) {
                                if (response.data.message == 'success') {
                                    jQuery('#edit-item-modal').modal('hide');
                                    $this.getOffers();
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Updated Successfully!',
                                        theme: 'success'
                                    });
                                    $this.offer = {};
                                    //location.reload();
                                }
                            }).catch(function (error) {
                                if (error.response.status == 422) {
                                    $this.deal_item_update_validation = error.response.data.errors;
                                } 
                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please Login to Continue!',
                                        theme: 'error'
                                    });
                                }
                            });
                        });
                    } else {
                        axios.put('{{route('admin.offers.index')}}/' + $this.offer.id, put_data).then(function (response) {
                            if (response.data.message == 'success') {
                                jQuery('#edit-item-modal').modal('hide');
                                $this.getOffers();
                                $.alert({title: 'Success!', content: 'Updated Successfully!', theme: 'success'});
                                $this.offer = {};
                                //location.reload();
                            }
                        }).catch(function (error) {
                            if (error.response.status == 422) {
                                $this.offer_item_update_validation = error.response.data.errors;
                            } 
                            if (error.response.status == 401) {
                                $.alert({
                                    title: '{{__('Oh Sorry!')}}',
                                    content: 'Please Login to Continue!',
                                    theme: 'error'
                                });
                            }
                        });
                    }
                }, 
                getFile: function (file) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = () => resolve(reader.result);
                        reader.onerror = error => reject(error);
                    });
                },
                sortMenu: function () {
                    this.offers = _.orderBy(this.offers, ['name'], [this.sort]);
                },
                forceDeleteOfferItem: function (id) {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to delete this item?',
                        theme: 'error',
                        buttons: {
                            confirm: function () {
                                axios.post('{{route('admin.offer-item.force-delete')}}', {
                                    _token: '{{csrf_token()}}',
                                    offer_id: id
                                }).then(function (response) {
                                    if (response.data.message == 'success') { 
                                        $.alert({
                                            title: 'Success!',
                                            content: 'Offer item deleted successfully!',
                                            theme: 'success'
                                        });
                                        location.reload();
                                    }
                                }).catch(function (error) {
                                    console.log(error);
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });


                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });

                    return formatter.format(price)

                },
            }
        });

        document.querySelector('#create-image').addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.querySelector('#create-preview').style.display = 'block';
                    document.querySelector('#create-preview').setAttribute('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
        document.querySelector('#edit-image').addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.querySelector('#edit-preview').style.display = 'block';
                    document.querySelector('#edit-preview').setAttribute('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>

@endsection

<!-- Edit Item Modal -->
