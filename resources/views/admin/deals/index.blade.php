@extends('layouts.admin')
@section('content')
    <div id="deals">
        <section>
            <div class="container">
                <div class="row res-admin">
                    <div class="col-md-2 col-sm-3 res-admin-side">
                        @include('includes.admin-side-bar',['active'=>'deals'])
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="filter-greybox">
                            <div class="select-box">
                                <button class="add-new deals-new" data-toggle="modal" data-target="#add-new-item-modal">Add New
                                    Item +
                                </button>
                                <div class="sort">{{__('Sort by:')}}
                                    <select v-model="sort" @change.prevent="sortMenu">
                                        <option value="asc">Sort by A-Z</option>
                                        <option value="desc">Sort by Z-A</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-12" v-for="deal in deals">
                                <div class="item-list">
                                    <div class="row ">
                                        <div class="col-md-9 col-sm-9 col-xs-12 menu-description">
                                            <div class="">
                                                <h3>@{{ deal.name }}
                                                    &nbsp; <a
                                                        class="text-red" @click.prevent="editDealItem(deal)">edit
                                                        item</a> &nbsp;
                                                    <button @click.prevent="forceDeleteDealItem(deal.id)"
                                                            class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>
                                                        Delete
                                                    </button>
                                                </h3>
                                                <p>@{{ deal.description }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="img-box">
                                                <img :src="deal.image" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item-cost">
                                        <div class="col-md-9 col-sm-9"></div>
                                        <div class="col-md-3 col-sm-3">
                                            <h5>‎£ @{{ currency(deal.price) }}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous"
                                               @click.prevent="jumpToPage(pagination.first_page)">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <li v-for="page in pagination.last_page"
                                            :class="getCurrentPage(page)"><a href="#" @click.prevent="jumpToPage(page)">@{{
                                                page }}</a>
                                        </li>
                                        <li>
                                            <a href="#" aria-label="Next" @click.prevent="jumpToPage(pagination.last_page)">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
        <div class="modal fade add-new-item-modal modal-lg" id="add-new-item-modal" role="dialog"> 
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4>Add New Deal Item</h4>
                        <hr>
                        <div class="form-group" :class="{'has-error':deals_item_validation.name}">
                            <p>Item Name*</p>
                            <input type="text" placeholder="Enter Item Name" name="" v-model="name">
                            <span class="help-block" v-if="deals_item_validation.name">
                                @{{ deals_item_validation.name[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':deals_item_validation.description}">
                            <p>Item Description</p>
                            <textarea placeholder="Enter Item Description" rows="6" v-model="description"
                                      maxlength="255"></textarea>
                            <span class="help-block" v-if="deals_item_validation.description">
                                @{{ deals_item_validation.description[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':deals_item_validation.price}">
                            <p>Item Price*</p>
                            <input type="number" placeholder="Enter Price" name="" v-model="price" min="0" step="0.01">
                            <span class="help-block" v-if="deals_item_validation.price">
                                @{{ deals_item_validation.price[0] }}
                            </span>
                        </div>

                        <div class="form-group" :class="{'has-error':deals_item_validation.vat_category}">
                            <p>Vat Category</p>
                            <select name="" class="form-control" v-model="vat_category">
                                <option value="food" selected="selected">Food</option>
                                <option value="alcohol">Alcohol</option>
                            </select>
                            <span class="help-block" v-if="deals_item_validation.vat_category">
                                @{{ deals_item_validation.vat_category[0] }}
                            </span>
                        </div> 

                        <div class="img-upload-box">
                            Click or Drag Item Image to Upload
                            <input type="file" id="create-image">
                            <p class="help-block">Max filesize: 1MB</p>
                        </div>

                        <div class="form-group">
                            <img src="" alt="" class="img-responsive" id="create-preview" style="display: none;">
                        </div>
                        <hr>
                        <div class="form-group">
                            <p>Menu Category</p>
                            <select v-model="selected_menu_id" class="form-control" @change="getMenuItems">
                                <option v-for="menu in menus" :value="menu.id">@{{ menu.name }}</option>
                            </select>
                        </div>
                        <div class="menu-items-multi-select" :class="{'has-error':deals_item_validation.selectedMenuItemsValues}">
                            <select v-model="availableMenuItemsValues" multiple="multiple" class="available-items">
                                <option v-for="menuItem in availableMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <div class="btn-multi-select">
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="addMenuItems">></button>
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="removeMenuItems"><</button>
                            </div>
                            <select v-model="selectedMenuItemsValues" multiple="multiple" class="selected-items">
                                <option v-for="menuItem in selectedMenuItems" :key="menuItem.id" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <span class="help-block" v-if="deals_item_validation.selectedMenuItemsValues">
                                @{{ deals_item_validation.selectedMenuItemsValues[0] }}
                            </span>
                        </div>
                        <br/>
                        <input type="submit" name="" value="Submit" @click.prevent="createDealItem">
                    </div>
                </div>
            </div>
        </div>

        <!-- Edit Item Modal -->
        <div class="modal fade add-new-item-modal" id="edit-item-modal" role="dialog">

            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4>Edit Deal Item</h4>
                        <hr>
                        <div class="form-group" :class="{'has-error':deal_item_update_validation.name}">
                            <p>Item Name*</p>
                            <input type="text" placeholder="Enter Item Name" name="" v-model="deal.name">
                            <span class="help-block" v-if="deal_item_update_validation.name">
                                @{{ deal_item_update_validation.name[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':deal_item_update_validation.description}">
                            <p>Item Description*</p>
                            <textarea placeholder="Enter Item Description" rows="6"
                                      v-model="deal.description" maxlength="255"></textarea>
                            <span class="help-block" v-if="deal_item_update_validation.description">
                                @{{ deal_item_update_validation.description[0] }}
                            </span>
                        </div>
                        <div class="form-group" :class="{'has-error':deal_item_update_validation.price}">
                            <p>Item Price*</p>
                            <input type="text" placeholder="Enter Price" name="" v-model="deal.price" min="0"
                                   step="0.01">
                            <span class="help-block" v-if="deal_item_update_validation.price">
                                @{{ deal_item_update_validation.price[0] }}
                            </span>
                        </div>

                        <div class="form-group" :class="{'has-error':deal_item_update_validation.vat_category}">
                            <p>Vat Category*</p>
                            <select name="" v-model="deal.vat_category">
                                <option value="food">Food</option>
                                <option value="alcohol">Alcohol</option>
                            </select>
                            <span class="help-block" v-if="deal_item_update_validation.vat_category">
                                @{{ menu_item_validation.vat_category[0] }}
                            </span>
                        </div>

                        @if(Auth::user()->role == 'admin')  
                            <div class="form-group" :class="">
                                <p>Select Slider</p> 
                                <span class="" v-if="deal.slider_id">
                                    @{{ deal.slider_id[0].name }} <a @click.prevent="removeSlider(deal.slider_id, menu_item)" class="btn btn-danger btn-xs">*</a>
                                </span> 
                                <select name="" v-model="deal.slider">
                                    <option value="0">No Slider</option>
                                    <option v-for="slider in sliders" :value="slider.id">@{{ slider.name }}</option>
                                </select> 
                            </div>
                        @endif

                        <div class="img-upload-box">
                            Click or Drag Item Image to Upload
                            <input type="file" id="edit-image">
                            <p class="help-block">Max filesize: 1MB</p>
                        </div>

                        <div class="form-group">
                            <img src="" alt="" class="img-responsive" id="edit-preview" style="display: none">
                        </div>
                        <hr>
                        <div class="form-group">
                            <p>Menu Category</p>
                            <select v-model="selected_menu_id" class="form-control" @change="getMenuItems">
                                <option v-for="menu in menus" :value="menu.id">@{{ menu.name }}</option>
                            </select>
                        </div>
                        <div class="menu-items-multi-select" :class="{'has-error':deal_item_update_validation.selectedMenuItemsValues}">
                            <select v-model="availableMenuItemsValues" multiple="multiple" class="available-items">
                                <option v-for="menuItem in availableMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <div class="btn-multi-select">
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="addMenuItems">></button>
                                <button class="add-item-varient-btn multi-select-button" @click.prevent="removeMenuItems"><</button>
                            </div>
                            <select v-model="selectedMenuItemsValues" multiple="multiple" class="selected-items">
                                <option v-for="menuItem in selectedMenuItems" :value="menuItem.id">@{{menuItem.name}}</option>
                            </select>
                            <span class="help-block" v-if="deal_item_update_validation.selectedMenuItemsValues">
                                @{{ deal_item_update_validation.selectedMenuItemsValues[0] }}
                            </span>
                        </div>
                        <br/>

                        <input type="submit" name="" value="Submit" @click.prevent="updateDealItem">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        var data = {
            menus:{!! json_encode($menus) !!},
            deals: {},
            deals_item_validation: [],
            deal_item_update_validation: [],
            deal: {},
            sliders:{!! json_encode($sliders) !!},
            name: '',
            description: '',
            price: '',
            vat_category: '',
            sort: 'asc',
            pagination: {},
            menu_id: '',
            selected_menu_id: 0,
            availableMenuItems: [],
            selectedMenuItems: [],
            availableMenuItemsValues: [],
            selectedMenuItemsValues: [],
            pickedMenuItems: []
        }; 
        // console.log(data.deals);

        var menus = new Vue({
            data: data,
            el: '#deals',
            mounted: function () {
                this.getMenus();
                this.getDeals();
            },
            methods: {
                move(id, arrFrom, arrTo) {
                    var index = arrFrom.findIndex(function(el) {
                        return el.id == id;
                    });
                    var item = arrFrom[index];

                    arrFrom.splice(index, 1);
                    arrTo.push(item);
                },
                addMenuItems: function () {
                    var selected = this.availableMenuItemsValues.slice(0);

                    for (var i = 0; i < selected.length; ++i) {
                        this.move(selected[i], this.availableMenuItems, this.selectedMenuItems);
                        this.pickedMenuItems.push(selected[i]);
                    }
                },
                removeMenuItems: function () {
                    var selected = this.selectedMenuItemsValues.slice(0);

                    for (var i = 0; i < selected.length; ++i) {
                        this.move(selected[i], this.selectedMenuItems, this.availableMenuItems);
                        this.pickedMenuItems.splice (this.pickedMenuItems.indexOf (selected[i]), 1);
                    }

                },
                getMenuItems: function () {
                    let $this = this;
                    axios.post('{{route('admin.menu-items.get')}}', {
                        _token: '{{csrf_token()}}',
                        menu_id: $this.selected_menu_id
                    })
                        .then(function (response) {
                            $this.availableMenuItems = response.data.data.menuItems;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                getMenus: function () {
                    let $this = this;
                    axios.post('{{route('admin.menus.get')}}', {
                        _token: '{{csrf_token()}}',
                    })
                        .then(function (response) {
                            $this.menus = response.data.data.menus;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                getDeals: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    }
                    axios.post('{{route('admin.deals.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                    })
                        .then(function (response) {
                            $this.deals = response.data.data.deals.data;
                            $this.sortMenu();
                            $this.pagination = {
                                current_page: response.data.data.deals.current_page,
                                from: response.data.data.deals.from,
                                last_page: response.data.data.deals.last_page,
                                path: response.data.data.deals.path,
                                per_page: response.data.data.deals.per_page,
                                to: response.data.data.deals.to,
                                total: response.data.data.deals.total,
                            };
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                jumpToPage: function (page) {
                    this.getDeals(page);
                },
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                createDealItem: function () {
                    let $this = this;
                    let post_data = {
                        _token: '{{csrf_token()}}',
                        name: $this.name,
                        description: $this.description,
                        price: $this.price,
                        vat_category: $this.vat_category,
                        selectedMenuItemsValues: $this.pickedMenuItems,
                    };

                    if (document.getElementById('create-image').files.length) {
                        let file = document.getElementById('create-image').files[0];

                        this.getFile(file).then(function (data) {

                            post_data.image = data;

                            axios.post('{{route('admin.deals.store')}}', post_data)
                                .then(function (response) {
                                    if (response.data.message == 'success') {
                                        jQuery('#add-new-item-modal').modal('hide');
                                        // $this.getDeals();
                                        $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                                        location.reload();
                                        // $this.name = "";
                                        // $this.description = "";
                                        // $this.price = "";
                                        // $this.vat_category = "";
                                        // $this.selected_menu_id = "";
                                        // $this.selectedMenuItemsValues = [];
                                    }
                                })
                                .catch(function (error) {
                                    if (error.response.status == 422) {
                                        $this.deals_item_validation = error.response.data.errors;
                                    }

                                    if (error.response.status == 401) {
                                        $.alert({
                                            title: '{{__('Oh Sorry!')}}',
                                            content: 'Please Login to Continue!',
                                            theme: 'error'
                                        });
                                    }
                                });
                        });

                    } else {
                        axios.post('{{route('admin.deals.store')}}', post_data)
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    jQuery('#add-new-item-modal').modal('hide');
                                    // $this.getDeals();
                                    $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                                    location.reload();
                                    // $this.name = "";
                                    // $this.description = "";
                                    // $this.price = "";
                                    // $this.vat_category = "";
                                    // $this.selected_menu_id = "";
                                    // $this.selectedMenuItemsValues = [];
                                }
                            })
                            .catch(function (error) {
                                if (error.response.status == 422) {
                                    $this.deals_item_validation = error.response.data.errors;
                                }

                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please Login to Continue!',
                                        theme: 'error'
                                    });
                                }
                            });
                    } 
                },
                editDealItem: function (deal) {
                    // console.log(deal);
                    if(deal.slider_id.length == 0){ 
                        deal.slider_id = 0;
                    }
                    this.deal = deal;
                    this.availableMenuItems = deal.availableMenuItems;
                    this.selectedMenuItems = deal.selectedMenuItems;
                    jQuery('#edit-item-modal').modal('show');
                },
                updateDealItem: function () {
                    let $this = this;
                    let put_data = {
                        _token: '{{csrf_token()}}',
                        name: $this.deal.name,
                        description: $this.deal.description,
                        price: $this.deal.price,
                        vat_category: $this.deal.vat_category,
                        selectedMenuItemsValues: $this.pickedMenuItems,
                        slider: $this.deal.slider,
                    };

                    if (document.getElementById('edit-image').files.length) {
                        let file = document.getElementById('edit-image').files[0];
                        this.getFile(file).then(function (data) {
                            put_data.image = data;
                            axios.put('{{route('admin.deals.index')}}/' + $this.deal.id, put_data)
                                .then(function (response) {
                                    if (response.data.message == 'success') {
                                        jQuery('#edit-item-modal').modal('hide');
                                        // $this.getDeals();
                                        $.alert({
                                            title: 'Success!',
                                            content: 'Updated Successfully!',
                                            theme: 'success'
                                        });
                                        // $this.deal = {};
                                        location.reload();
                                    }
                                })
                                .catch(function (error) {
                                    if (error.response.status == 422) {
                                        $this.deal_item_update_validation = error.response.data.errors;
                                    }

                                    if (error.response.status == 401) {
                                        $.alert({
                                            title: '{{__('Oh Sorry!')}}',
                                            content: 'Please Login to Continue!',
                                            theme: 'error'
                                        });
                                    }
                                });
                        });
                    } else {
                        axios.put('{{route('admin.deals.index')}}/' + $this.deal.id, put_data)
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    jQuery('#edit-item-modal').modal('hide');
                                    // $this.getDeals();
                                    $.alert({title: 'Success!', content: 'Updated Successfully!', theme: 'success'});
                                    // $this.deal = {};
                                    location.reload();
                                }
                            })
                            .catch(function (error) {
                                if (error.response.status == 422) {
                                    $this.deal_item_update_validation = error.response.data.errors;
                                }

                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please Login to Continue!',
                                        theme: 'error'
                                    });
                                }
                            });
                    }
                },
                removeSlider: function(menu_item){  
                    // console.log(menu_item[0].menu_items_id); 
                    let $this = this;
                    var deal_id = menu_item[0].menu_items_id; 
                    axios.post('{{route('admin.deal.delete-slider')}}', {
                        _token: '{{csrf_token()}}',
                        deal_id: deal_id
                    }).then(function (response) {
                        //console.log(response); 
                        // $this.getMenus();    
                        $this.deal.slider_id = 0;
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                getFile: function (file) {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = () => resolve(reader.result);
                        reader.onerror = error => reject(error);
                    });
                },
                sortMenu: function () {
                    this.deals = _.orderBy(this.deals, ['name'], [this.sort]);
                },
                forceDeleteDealItem: function (id) {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to delete this item?',
                        theme: 'error',
                        buttons: {
                            confirm: function () {
                                axios.post('{{route('admin.deal-item.force-delete')}}', {
                                    _token: '{{csrf_token()}}',
                                    deal_id: id
                                }).then(function (response) {
                                    if (response.data.message == 'success') {
                                        // $this.getDeals();
                                        $.alert({
                                            title: 'Success!',
                                            content: 'Deal item deleted successfully!',
                                            theme: 'success'
                                        });
                                        location.reload();
                                    }
                                }).catch(function (error) {
                                    console.log(error);
                                });
                            },
                            cancel: function () {

                            },
                        }
                    });


                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });

                    return formatter.format(price)

                },
            }
        });

        document.querySelector('#create-image').addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.querySelector('#create-preview').style.display = 'block';
                    document.querySelector('#create-preview').setAttribute('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
        document.querySelector('#edit-image').addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.querySelector('#edit-preview').style.display = 'block';
                    document.querySelector('#edit-preview').setAttribute('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>

@endsection

<!-- Edit Item Modal -->
