@extends('layouts.master-admin')

@section('content')

    <div class="row" id="pagesettings">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading single-project-nav">
                @include('includes.master-admin-page-header',['active'=>'home-page'])
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active table-responsive">
                        <div class="restaurant-profile-form">
                            <div class="col-md-9 col-xs-12">
                                <form action="{{ url('master-admin/page-settings/store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="homepage" name="section">
                                    <div id="menu1" class="tab-pane active">
                                        <div class="card">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    @if(count(config('setting_fields.homepage', [])) )
                                                        @foreach(config('setting_fields.homepage', []) as $section => $fields)
                                                            @foreach($fields['elements'] as $field)
                                                                @includeIf('fields.' . $field['type'] )
                                                            @endforeach
                                                            <hr>
                                                        @endforeach
                                                    @endif
 
                                                    <label class="control-label" for="upload">Main Slider Images</label>
                                                    <div class="col-sm-12">
                                                        <div class="input-group col-xs-12" v-for="(image, index) in images" style="margin-bottom: 10px;">
                                                            <div class="col-sm-5" style="padding: 0;">
                                                                <textarea class="form-control" disabled="true">@{{image.name}}</textarea>
                                                            </div>
                                                            <div class="col-sm-4" style="padding: 0;"> 
                                                                <img v-if="image.path" class="img-responsive"
                                                                     style="max-height: 50px;margin-left: 20px;" :src="image.path" alt="">
                                                                <input type="hidden" v-if="image.path" name="images[]"
                                                                       :value="image.id">
                                                            </div>
                                                            <div class="col-sm-1" style="padding: 0;">
                                                                <a href="#" class="btn btn-danger btn-sm pull-right"
                                                                   style="color: rgb(255, 255, 255);"
                                                                   @click.prevent="removeImage(index, image.id)">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <label class="control-label" for="upload">Slider Name</label>
                                                        <input type="text" class="form-control" style="margin-bottom: 15px;" name="slider_name">
                                                        <br />
                                                        <textarea class="form-control m-editor" id="middle_section_text" name="banner_text"></textarea>
                                                        <br/>
                                                        <input type="file" class="form-control" style="margin-bottom: 15px;" id="upload" name="uploads">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <input type="submit" class="btn btn-approve" value="Save Changes">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <script type="text/javascript"> 
        // $(document).ready(function(){ 
        //     $("#home_main_banner").attr("multiple", "true");
        //     $("#home_main_banner").attr("name", "home_main_banner[]");
        //     $("#home_main_banner").attr("id", "home_main_banner[]");
        // });

        var data = { 
            images: {!! json_encode($images) !!}
        };
        // console.log(data.images);
        var pagesettings = new Vue({
            data: data,
            el: '#pagesettings',
            mounted: function () {
                var vm = this;
                //alert();
            },
            methods: { 
                removeImage: function (index, image) { 
                    this.images.splice(index, 1);

                    axios.post('{{route('master-admin.page.remove-image')}}', {
                        _token: '{{csrf_token()}}',
                        image_id: image
                    }).then(function (response) {
                        console.log(response);
                        if (response.data.message == 'success') {
                            
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }
        });

    </script>

@endsection
