@extends('layouts.app')
@section('title', 'offers')
@section('content')
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pop_up.css') }}" rel="stylesheet"> 
    <link href="{{ asset('css/specialoffers.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://themes.audemedia.com/html/goodgrowth/css/owl.theme.default.min.css">
    <script src="https://unpkg.com/vue-star-rating/dist/star-rating.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js" type="text/javascript">
    </script> 
    <style>
        .active {
            color: #fce703;
        }

        .offers_header {
            color: #bc040b !important;
        }
        
        .jq-toast-wrap.bottom-left {
            position: fixed;
            top: 14%;
            left: 76%;
        } 
    </style> 
    <div id="restaurant">
        <div class="row container-fluid">
            <div class="col-sm-12 col-md-9 col-lg-9" id="item-card-figure">
                <div class="row" id="menu-bar-row">
                    <div class="cata-sub-nav">
                        <button class="nav-prev">
                            <i class="fa fa-caret-left"></i>
                        </button>
                        <ul class="menu-list" role="tablist">
                            <li>
                                <a class="list-item" id="special_offer_a" data-toggle="tab" href="#desserts" role="tab" aria-controls="home"
                                    aria-selected="true" @click.prevent="setOffer()">Special Offers</a>
                            </li>
                            <li v-for="menu in menus">
                                <a class="list-item" :id="menu.id + '_menu'" data-toggle="tab" href="#desserts" role="tab" aria-controls="home"
                                    aria-selected="true" @click.prevent="setMenu(menu)">@{{ menu . name }}</a>
                            </li>
                        </ul>
                        <button class="nav-next">
                            <i class="fa fa-caret-right"></i>
                        </button>
                    </div>
                </div>
                <br />
                <div class="row items-sec"> 
                    <div v-if="show_offer == 1" v-for="offer in offers" class="col-sm-12 col-md-4 col-lg-4 figure-item">
                        <div class="item-card">
                            <h3>@{{ offer . name }}</h3>
                            <img v-if="offer.image" class="img-responsive item-img" :src="offer.image" alt="Image">
                            <img v-if="!offer.image" class="img-responsive item-img" src="{{ asset('img/default.jpg') }}"
                                alt="Image">
                            <p>@{{ offer . description . substring(0, 100) + '..' }}</p>
                            <span class="item-price">
                                {{ setting('currency') }}@{{ currency(offer . price) }}
                            </span>
                            @if (setting('static_site') == 0)
                                <button @click.prevent="addOfferItem(offer)" id="add-to-cart-btn"
                                    class="btn btn-default btn-sm">ADD TO CART</button>
                            @endif
                        </div>
                    </div>
                    <div v-if="show_menu == 1" class="col-sm-12 col-md-4 col-lg-4" id="menu-item" v-for="menu_item in selected_menu_items">
                        <div class="menu-item-card">
                            <img class="img-responsive vege-flag" v-if="menu_item.vegitarian_flag == 1"
                                src="{{ asset('img/vegi.jpg') }}">
                            <div class="item-img-sec">
                                <img v-if="menu_item.logo" :src="menu_item.logo" class="img-responsive" alt="Image">
                                <img v-if="!menu_item.logo" src="{{ asset('img/default.jpg') }}" class="img-responsive"
                                    alt="Image">
                            </div>
                            <div class="item-desc-sec">
                                <h4>@{{ menu_item . name }}</h4>
                                <div class="rating-star">
                                    <star-rating v-bind:rating="menu_item.rating" style="font-size: 22px;">
                                    </star-rating>
                                </div>
                                <p class="desc">@{{ menu_item . description }}</p>
                                <div class="row pricing-bar-row">
                                    <div class="col-sm-5 col-md-5 no-right-pd">
                                        <p class="item-price">{{ setting('currency') }}
                                            @{{ currency(menu_item . price) }}</p>
                                    </div>
                                    <div class="col-sm-5 col-md-5 no-left-pd">
                                        @if (setting('static_site') == 0)
                                            <div class="input-group count-btn">
                                                <span class="minus-btn minus" id="minus-btn">-</span>
                                                <input class="number-input" v-bind:id="menu_item.id+'-textbox'"
                                                    type="text" value="1">
                                                <span class="plus-btn plus" id="plus-btn">+</span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-2 col-md-2 no-left-pd">
                                        <a id="fav-btn" class="selected" v-if="menu_item.favoured"
                                            @click="setFavourite(menu_item)">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </a>
                                        <a id="fav-btn" v-if="!menu_item.favoured" @click="setFavourite(menu_item)">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 select-dropdown no-right-pd"
                                        v-if="menu_item.variants.length">
                                        <select v-bind:id="menu_item.id" placeholder="Select">
                                            <option v-for="variant in menu_item.variants" v-bind:value="variant.id">
                                                @{{ variant . name }} -
                                                ‎{{ setting('currency') }}
                                                @{{ currency(variant . pivot . price) }}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-md-6 no-left-pd" v-if="menu_item.variants.length">
                                        @if (setting('static_site') == 0)
                                            <button id="add-to-cart-btn" v-if="menu_item.available_status == 1"
                                                @click.prevent="addToCart(menu_item)" class="btn btn-default btn-sm">Add
                                                to Cart</button>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 col-md-6 no-left-pd no-right-pd btn-without-dropdown-sec"
                                        v-if="menu_item.variants.length == 0">
                                        @if (setting('static_site') == 0)
                                            <button id="add-to-cart-btn" v-if="menu_item.available_status == 1"
                                                @click.prevent="addToCart(menu_item)" class="btn btn-default btn-sm">Add
                                                to Cart</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3" id="right-side-cart-figure">
                <div class="row make-order-txt-row">
                    <h3>MAKE YOUR ORDER NOW<h3>
                </div>
                @if (setting('static_site') == 0)
                    <div class="row cart-items-count-row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <h5>@{{ itemCount }} Item(s) in the cart</h5>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            {{-- <a class="remove-btn">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                <span>Empty Cart</span>
                            </a> --}}
                        </div>
                    </div>
                @endif

                <div id="cart-items-figure" v-if="cart.length">
                    <div class="cart-items-row" v-for="(item,index) in cart" :key="'delivery_cart'+item.id+'__'+index">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4 class="item-name">@{{ item . name }}</h4>
                                <span class="sub-item-name sub" v-if="item.deal_items" v-for="d_item in item.deal_items">
                                    @{{ d_item . name }}
                                </span>
                                <span class="sub-item-name sub" v-if="item.offer_items" v-for="o_item in item.offer_items">
                                    @{{ o_item . name }}
                                </span>
                                <span class="sub-item-name" v-if="item.show_variant">
                                    (@{{ item . show_variant . name }})
                                </span>
                                <span class="sub-item-name" v-if="item.show_crust">
                                    @{{ item . show_crust . name }} - {{ setting('currency') }}
                                    @{{ currency(item . show_crust . pivot . price) }}
                                </span>
                                <span class="sub-item-name sub" v-if="item.show_addons" v-for="addon in item.show_addons">
                                    @{{ addon . name }} - {{ setting('currency') }}
                                    @{{ currency(addon . pivot . price) }}
                                </span>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4 class="item-price" v-if="!item.show_variant">{{ setting('currency') }}
                                    @{{ currency(item . price) }}</h4>
                                <h4 class="item-price" v-if="item.show_variant">{{ setting('currency') }}
                                    @{{ currency(item . show_variant . pivot . price) }}</h4>
                            </div>
                        </div>
                        <div class="row bottom-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <a class="remove-btn"
                                    @click.prevent="removeFromCart(item, item.show_variant, item.show_crust)">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                    <span>Remove</span>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group count-btn">
                                    <span class="minus-btn" id="minus-btn"
                                        @click.prevent="removeFromCart(item, item.show_variant, item.show_crust)">-</span>
                                    <input id="number-input" class="number-input" type="text" v-bind:value="item.quantity">
                                    <span class="plus-btn" id="plus-btn"
                                        @click.prevent="increaseItemCount(item, item.show_variant, item.show_crust)">+</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($restaurant->takeaway)
                        <div class="row checkbox-option-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4>Take Away</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <input type="checkbox" id="scales" name="scales" @click.prevent="setMethod('takeaway')"
                                    v-model="takeaway">
                                <label for="scales"></label>
                            </div>
                        </div>
                    @endif

                    @if ($restaurant->delivery)
                        <div class="row checkbox-option-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4>Delivery</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <input type="checkbox" id="horns" name="horns" @click.prevent="setMethod('delivery')"
                                    v-model="delivery">
                                <label for="horns"></label>
                            </div>
                        </div>
                    @endif

                    <div class="input-option-row">
                        <input v-if="delivery" type="text" id="post-code" v-click-outside="getDeliveryCharge" class="form-control" name="postCode" v-model="postCode"
                            placeholder="Enter Postal Code" autocomplete="off">
                        <input type="text" class="form-control" name="promoCode" placeholder="Enter Promo Code">
                    </div>
                    <div class="amount-figure" v-if="cart.length">
                        <div class="row sub-amount-row">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Sub Total</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(sub_total) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>V.A.T.</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(vat_value) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row" v-if="delivery">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Delivery</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(deliveryCost) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row" v-if="delivery">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Site Discount</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(lbl_restaurant_discount) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row total-amount-row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Total</h4>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 v-if="delivery" class="tot-price">
                                    <span>{{ setting('currency') }}</span>@{{ currency(total + vat_value + deliveryCost- restaurant_discount) }}
                                </h4>
                                <h4 v-if="takeaway" class="tot-price">
                                    <span>{{ setting('currency') }}</span> @{{ currency(total + vat_value- restaurant_discount) }}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <a href="#" @click.prevent="checkout('{{ route('delivery.create') }}')" id="proceed-btn"
                        class="btn btn-default btn-sm">Proceed to Checkout</a>
                </div>
                <div class="empty-cart-sec" v-if="!cart.length">
                    <h4>Your cart is empty</h4>
                    <img class="img-responsive" src="img/cart.png">
                    <p>Your cart is empty. Get your cravings satisfied by adding your favourites into the cart.</p>
                </div>
            </div>
            <!---- modal section ---->
            <div class="modal" id="addons-modal" style="z-index: 9999999;">
                <div class="row modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="col-md-6 left-content">
                        <div class="row" id="pop-up-img">
                            <img v-if="!menu_item" src="{{ asset('img/default.jpg') }}" class="spectrum1" alt="meow">
                            <img v-if="menu_item" :src="menu_item.logo" alt="pizza">
                        </div>
                        <div class="row" id="pop-up-img">
                            <h5 id="img-info-pop-up">@{{ menu_item . name }} </h5>
                            <div style="width: 305px;word-break: break-all;min-height: 90px;border-bottom: 1px solid #b7b7b7;">
                                <p style="border-bottom: none;width: auto;">@{{ menu_item . description }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 right-content">
                        <div class="row" id="pop-up-title">
                            <div v-if="show_crust_area == 1" class="wrapper center-block">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading active" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                    href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Select Crust
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                            aria-labelledby="headingOne">
                                            <div class="row panel-body">
                                                <ul class="donate-now">
                                                    <li v-for="(crust_type, index) in menu_item.crust_types"
                                                        style="padding: 2px;min-height: 50px;">
                                                        <input type="radio"
                                                            @click="calcModalItems('', '', crust_type.pivot.price)"
                                                            :id="crust_type.id" name="crust" :value="crust_type.id"
                                                            v-model="selected_crust" v-bind:checked="index === 0" />
                                                        <label :for="crust_type.id" style="font-size: 11px;">@{{ crust_type . name }}
                                                            {{ setting('currency') }}@{{ currency(crust_type . pivot . price) }}
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="show_varient_area == 1" class="wrapper center-block">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading-one active" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                    href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                    Select Size
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                            aria-labelledby="headingOne">
                                            <div class="row panel-body">
                                                <ul class="donate-now">
                                                    <li v-for="(variant, index) in menu_item.variants"
                                                        style="padding: 2px;min-height: 50px;">
                                                        <!-- <input type="radio" @click="calcModalItems('', variant.pivot.price, '')" :id="variant.id" :value="variant.id" v-model="selected_varient" name="size" v-bind:checked="index === 0" /> -->
                                                        <input type="radio" @click="calcModalItems('', variant.pivot.price, '')"
                                                            :id="variant.id" :value="variant.id" v-model="selected_varient"
                                                            name="size" />
                                                        <label :for="variant.id" class="varient-list" style="font-size: 11px;">@{{ variant . name }}
                                                            {{ setting('currency') }}@{{ currency(variant . pivot . price) }}</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="show_addon_area == 1" class="wrapper center-block">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading-two active" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                    href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                    Select Extra
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse " role="tabpanel"
                                            aria-labelledby="headingOne">
                                            <div class="pill-container">
                                                <div class="row">
                                                    <div class="col-sm-4 button-align-popup" v-for="addon in addons"
                                                        style="padding: 3px;min-height: 50px;">
                                                        <input type="checkbox"
                                                            @click="calcModalAddons(addon.id, addon.pivot.price, $event)"
                                                            :id="addon.id" name="selector" v-model="selected_addons"
                                                            :value="addon.id">
                                                        <label class="selector option-a" :for="addon.id"
                                                            style="min-height: 40px;padding: 0;"> @{{ addon . name }}
                                                            ({{ setting('currency') }}@{{ currency(addon . pivot . price) }})</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" v-if="pop_up_error">
                                <div class="col-xs-3 col-sm-3 col-md-6">
                                    <span style="color: #ff0000;">@{{ pop_up_error }}</span>
                                </div>
                            </div>
                            <div class="row" id="s-cart-right-details-3">
                                <div class="col-xs-3 col-sm-3 col-md-6">
                                    <h5 id="item-total">Total</h5>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-6">
                                    <h5 id="item-total-price">{{ setting('currency') }}@{{ currency(modalTotal) }}</h5>
                                </div>
                            </div>
                            @if (setting('static_site') == 0)
                                <div class="row pop-up" id="s-cart-right-details-3" style="border-top:none;">
                                    <div class="col-xs-3 col-sm-3 col-md-8">
                                        <div class="add-remove" id="add-remove-s-cart">
                                            <div class="input-group" id="input-group-s-cart">
                                                <div class="number" id="number-s-cart">
                                                    <span class="minus" id="minus-s-cart">-</span>
                                                    <input class="number-box" v-bind:id="menu_item.id+'-addonBox'"
                                                        v-model="modalQty" type="text" value="1" />
                                                    <span class="plus" id="plus-s-cart">+</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-4 modal-buy">
                                        <div id="add-to-cart-h-mid" @click.prevent="addAddons(menu_item, '')"
                                            class="btn btn-default btn-sm btn-custom-sm">BUY NOW </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!---- //modal section ---->
        </div>
    </div>

    <script type="text/javascript">
        var data = {
            menus:{!! json_encode($restaurant->menus) !!},
            deals:{!! json_encode($restaurant->deals) !!},
            cuisines: {!! json_encode($restaurant->menus) !!},
            offers:{!! json_encode($restaurant->offers) !!}, 
            selected_menu_items: [],
            selected_varient: {},
            selected_crust: {},
            show_crust_area: 0,
            show_varient_area: 0,
            show_addon_area: 0,
            style: 'list',
            reservation: false,
            delivery: false,
            takeaway: false,
            cart: {!! json_encode($cart) !!},
            total: 0,
            vat_value: 0,
            vat:{!! json_encode(getVat(),true) !!},
            sub_total: 0,
            requests: '{{$requests}}',
            cart_date: '{{$cart_date}}',
            cart_time: '{{$cart_time}}',
            restaurant:{!! json_encode($restaurant) !!},
            favoured: false,
            menu_id: 0,
            type: '{{$type}}',
            restaurant_id: '{{$restaurant_id}}',
            date: '{{\Carbon\Carbon::now()->toDateString()}}',
            time: '{{\Carbon\Carbon::now()->format('h:i A')}}',
            head_count: '',
            phone: '',
            email: '',
            reservation_validation: {},
            review_validation: {},
            step: 1,
            reservation_data: [],
            reservation_requests: '',
            rating: 5,
            review: '',
            sorted_reviews: [],
            sort: 'desc',
            variants: [],
            addons: [],
            menu_item: {},
            selected_modal_item: {},
            selected_addons: [],
            selected_variant: {},
            reservation_restaurant: {},
            reservation_settings: {},
            guest: false,
            remainingCount1: 255,
            remainingCount2: 255,
            remainingCount3: 255,
            loading: false,
            deals_menus: {},
            selected_deal_items: {},
            selectedDealItem: {},
            deal_id: '',
            deal_name: '',
            deal_price: '',
            deal_items: [],
            offers_menus: {},
            selected_offer_items: {},
            selectedOfferItem: {},
            offer_id: '',
            offer_name: '',
            offer_price: '',
            offer_items: [],
            pagination: {}, 
            menu_items_paginate: {},
            itemCount: 0,
            modalTotal: 0,
            modalVarients: 0,
            modalCrust: 0,
            modalAddons: 0,
            modalQty: 0,
            postCode: '',
            // deliveryCost:{!! json_encode($restaurant->delivery_cost) !!},
            deliveryCost:'',
            pop_up_error: '',
            lbl_restaurant_discount: 0,
            restaurant_discount: 0,
            site_discount: 0,
            new_post_code: 0,
            selected_menu_id: '{{$menu_id}}',
            show_offer: 1,
            show_menu: 0, 
        };

        var restaurant = new Vue({
            el: '#restaurant',
            data: data,
            mounted: function () { 
                
                if (this.restaurant.id != this.restaurant_id) {
                    this.cart = [];
                }

                if (this.type == 'delivery') {  
                    this.setMethod('delivery', false);
                } else if (this.type == 'takeaway') { 
                    this.setMethod('takeaway', false);
                }

                // if (this.type == 'delivery') { 
                //     this.setDeliveryItems(this.menus[0].menu_items); 

                //     this.menu_id = this.menus[0].id;
                //     // this.getMenu(); 
                //     this.setMethod('delivery', false);
                // } else if (this.type == 'takeaway') {
                //     this.setTakeawayItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('takeaway', false);
                // } else if (this.type == 'reservation') {
                //     this.setReservationItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('reservation', false);
                // } else {
                //     this.setReservationItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('reservation', false);
                // } 

                this.getTotals();

                if (this.restaurant.user) {
                    this.favoured = true;
                } 
                this.sorted_reviews = this.restaurant.reviews; 
                @if(auth()->check())
                    @if(auth()->user()->email == setting('guest_email_id'))
                        this.guest = true;
                    @endif
                @endif  
                $('#special_offer_a').parent().addClass('active'); 
            },
            directives: {
                'click-outside': {
                  bind: function(el, binding, vNode) {
                    // Provided expression must evaluate to a function.
                    if (typeof binding.value !== 'function') {
                        const compName = vNode.context.name
                      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                      if (compName) { warn += `Found in component '${compName}'` }
                      
                      console.warn(warn)
                    }
                    // Define Handler and cache it on the element
                    const bubble = binding.modifiers.bubble
                    const handler = (e) => {
                      if (bubble || (!el.contains(e.target) && el !== e.target)) {
                        binding.value(e)
                      }
                    }
                    el.__vueClickOutside__ = handler

                    // add Event Listeners
                    document.addEventListener('click', handler)
                        },
                  
                    unbind: function(el, binding) {
                        // Remove Event Listeners
                        document.removeEventListener('click', el.__vueClickOutside__)
                        el.__vueClickOutside__ = null 
                    }
                }
            },
            methods: {
                setMenu: function (menu) {
                    this.show_offer = 0;
                    this.show_menu = 1;
                    if (this.delivery) {
                        // this.setDeliveryItems(menu.menu_items.data);
                        this.setDeliveryItems(menu.menu_items);  
                    }
                    if (this.takeaway) {
                        this.setTakeawayItems(menu.menu_items);
                    }
                    if (this.reservation) {
                        this.setReservationItems(menu.menu_items);
                    } 
                    
                    if (menu.id == 0) {
                        this.menu_id = 0;
                    } else {
                        this.menu_id = menu.menu_items.menu_id;
                    } 
                },
                setDeliveryItems: function (menu_items) {
                    // var menu_items = menu_items.data;
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.delivery;
                    });
                    // this.equalHeight();
                },
                setTakeawayItems: function (menu_items) {
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.takeaway;
                    });
                    // this.equalHeight();
                },
                setMethod: function (method, exception = false) {

                    if (this.cart.length && exception && method != 'reservation') {
                        var $this = this; 
                    }

                    const menu = this.getCurrentMenu(); 

                    switch (method) {
                        case 'reservation':
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            this.setReservationItems(menu.menu_items);
                            break;
                        case 'delivery':
                            this.reservation = false;
                            this.delivery = true;
                            this.takeaway = false;
                            this.setDeliveryItems(menu.menu_items);
                            break;
                        case 'takeaway':
                            this.reservation = false;
                            this.delivery = false;
                            this.takeaway = true;
                            this.setTakeawayItems(menu.menu_items);
                            break;
                        default:
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            this.setTakeawayItems(menu.menu_items);
                            break;
                    } 
                    this.sendAjax();
                },
                setOffer: function () {
                    this.show_offer = 1;
                    this.show_menu = 0;
                },
                calcModalItems: function (menu_item, modalVarients, modalCrust) { 
                    if(menu_item){
                        if(menu_item.variants.length){
                            // this.modalVarients = menu_item.variants[0].pivot.price; 
                            this.modalVarients = 0; 
                            this.show_varient_area = 1;
                        }else{
                            this.show_varient_area = 0;
                        }

                        if(menu_item.crust_types.length != 0){
                            // this.modalCrust = menu_item.crust_types[0].pivot.price;  
                            this.modalCrust = 0;
                            this.show_crust_area = 1;
                        }else{
                            this.show_crust_area = 0;
                            this.modalCrust = 0;
                        }
                        if(menu_item.addons.length != 0){  
                            this.show_addon_area = 1;
                        }else{
                            this.show_addon_area = 0; 
                        }
                        console.log(menu_item); 
                        this.modalTotal = this.modalVarients + this.modalCrust;
                    } 
                    if(modalVarients){
                        $('.varient-list').css('background-color','');
                        $('.varient-list').css('color',''); 
                        this.modalVarients = modalVarients;  
                        this.modalTotal = this.modalVarients + this.modalCrust + this.modalAddons;
                    } 
                    if(modalCrust){
                        this.modalCrust = modalCrust;  
                        this.modalTotal = this.modalVarients + this.modalCrust + this.modalAddons;
                    }
                },
                calcModalAddons: function(addon_id, addon_price, event){
 
                    if (event.target.checked) { 
                        this.modalAddons = this.modalAddons + addon_price;
                    }else{
                        this.modalAddons = this.modalAddons - addon_price;
                    } 
                    this.modalTotal = this.modalVarients + this.modalCrust + this.modalAddons;
                },
                addToCart: function (menu_item) {

                    this.pop_up_error = '';
                    this.selected_addons = [];  
                    this.selected_crust = {}; 
                    this.selected_variant = jQuery('#' + menu_item.id).val(); 
                    var varient_id = jQuery('#' + menu_item.id).val();  
                    var qty = jQuery('#' + menu_item.id + '-textbox').val(); 

                    var varient = menu_item.variants.filter(function (item) {
                        return item.id == varient_id;
                    });   
                    this.selected_variant = varient;  

                    // this.modalVarients = menu_item.variants[0].pivot.price; 

                    if (menu_item.addons.length) {

                        // this.variants = menu_item.variants;
                        this.addons = menu_item.addons;

                        // if (menu_item.variants.length) {
                        //     this.selected_variant = this.variants[0].id;
                        // }
                        this.menu_item = menu_item; 
                        this.item_image = menu_item.logo; 

                        if(menu_item.variants.length){
                            this.modalVarients = menu_item.variants[0].pivot.price; 
                        }
                        if(menu_item.crust_types.length){
                            this.modalCrust = menu_item.crust_types[0].pivot.price; 
                        }

                        this.calcModalItems(menu_item);
                        this.modalQty = 1;

                        this.modalVarients = 0; 
                        this.modalCrust = 0; 
                        this.modalAddons = 0;
                        this.modalTotal = 0;
                        $('.varient-list').css('background-color','#ebebeb');
                        $('.varient-list').css('color','#4d4d4d');
                        jQuery('#addons-modal').modal('show');  
                        return false; 
                    } 

                    for(var q = 1; q <= qty; q++){
                        this.addMenuItem(menu_item, varient_id); 
                    }
                    // this.addMenuItem(menu_item); 
                },
                addMenuItem: function (menu_item, varient_id) {
                    var $this = this;   
                    const selected_item = this.cart.filter(function (item) {
                        if(jQuery.isEmptyObject(varient_id)){
                            return item.id == menu_item.id;
                        }else{ 
                            return item.id == menu_item.id && item.selected_variant == varient_id;
                        } 
                    });    

                    if (selected_item.length) {   
                        selected_item[0].quantity++;
                        this.$forceUpdate();
                        this.selected_variant = {};
                        this.selected_addons = []; 
                    } else {    
                        var $this = this; 
                        menu_item.quantity = 1;
                        menu_item.type = 'single'; 
                        menu_item.selected_variant = this.selected_variant;  

                        var show_addons = [];  
                        menu_item.addons.forEach(function (addon) {
                            $this.selected_addons.forEach(function (selected_addon) {
                                if (addon.id == selected_addon) {
                                    show_addons.push(addon);
                                }
                            });
                        }); 

                        var addon_varient_id = varient_id;   
                        if (this.selected_variant.length != 0) {  

                            var varient = menu_item.variants.filter(function (item) {
                                return item.id == addon_varient_id;
                            });  
                            if(varient.length){  
                                var item = this.cart.push(Object.assign({}, menu_item));
                                this.selected_variant = varient;
                                var selected_addon_varient = this.selected_variant[0];
                                var selected_addon_varient_id = this.selected_variant[0].id;
                                this.cart[item - 1].show_variant = selected_addon_varient;
                                this.cart[item - 1].selected_variant = selected_addon_varient_id; 
                            } 
                        }else{  
                            var item = this.cart.push(Object.assign({}, menu_item)); 
                            var selected_addon_varient = '';
                            var selected_addon_varient_id = '';
                            this.cart[item - 1].show_variant = selected_addon_varient;
                            this.cart[item - 1].selected_variant = selected_addon_varient_id; 
                        }      
                        this.selected_addons = []; 
                        this.selected_variant = {};
                    } 

                    this.getTotals();
                    this.sendAjax(); 
                    jQuery('#addons-modal').modal('hide');  
                },
                addAddons: function (menu_item, varient_id) {
                    var $this = this;   
                    varient_id = $this.selected_varient;  
                    var qty = jQuery('#' + menu_item.id + '-addonBox').val();    
                    var crust_id = $this.selected_crust;  
                    var get_summery = 1;      

                    const selected_item = this.cart.filter(function (item) { 
                        return item.id == menu_item.id && item.selected_variant == varient_id && item.selected_crust == crust_id; 
                    });    

                    if (selected_item.length) {  

                        console.log('f2'); 
                        selected_item[0].quantity++;
                        this.$forceUpdate();
                        this.selected_variant = {};
                        this.selected_addons = []; 
                    } else {  
                        console.log('f5');  
                        var $this = this;  
                        menu_item.quantity = qty;
                        menu_item.type = 'single';   
                        var show_addons = [];  
                        menu_item.addons.forEach(function (addon) {
                            $this.selected_addons.forEach(function (selected_addon) {
                                if (addon.id == selected_addon) {
                                    show_addons.push(addon);
                                }
                            });
                        }); 

                        var addon_varient_id = varient_id;   
                        if (this.selected_variant.length != 0) {  
 
                            if (menu_item.addons.length) { 
                                var varient = menu_item.variants.filter(function (item) {
                                    return item.id == addon_varient_id;
                                });    

                                if(varient.length){
                                    // var item = this.cart.push(menu_item);  
                                    var item = this.cart.push(Object.assign({}, menu_item));
                                    this.selected_variant = varient;
                                    var selected_addon_varient = this.selected_variant[0];
                                    var selected_addon_varient_id = this.selected_variant[0].id;
                                    this.cart[item - 1].show_variant = selected_addon_varient;
                                    this.cart[item - 1].selected_variant = selected_addon_varient_id; 
                                }else{ 
                                    $this.pop_up_error = 'Please select size.';
                                    get_summery = 0;
                                    return false;
                                } 
                            }else{
                                var selected_addon_varient = this.selected_variant[0];
                                var selected_addon_varient_id = this.selected_variant[0].id;
                                this.cart[item - 1].show_variant = selected_addon_varient;
                                this.cart[item - 1].selected_variant = selected_addon_varient_id;
                            } 
                        }  
                          
                        if (crust_id === parseInt(crust_id, 10)){ 
                            var crust = menu_item.crust_types.filter(function (item) {
                                return item.id == crust_id;
                            }); 
                            var selected_addon_crust = crust[0];
                            var selected_addon_crust_id = crust[0].id;
                            this.cart[item - 1].show_crust = selected_addon_crust;
                            this.cart[item - 1].selected_crust = selected_addon_crust_id;  
                        } 
                        if (show_addons) { 
                            this.cart[item - 1].selected_addons = this.selected_addons;
                            this.cart[item - 1].show_addons = show_addons; 
                        }   

                        this.selected_addons = []; 
                        this.selected_variant = {};
                    }  
 
                    this.getTotals();
                    this.sendAjax();   

                    console.log(this.cart); 
                    jQuery('#addons-modal').modal('hide');  
                    this.$forceUpdate(); 
                },
                removeItemFromCart: function (menu_item, varient_id, crust_id) { 
                    if(varient_id && crust_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });   
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        } 
                    }else if(varient_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            console.log(item.selected_variant); 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });    
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                console.log(item); 
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }  
                    }else if(crust_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  

                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }   
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }  
                    }
                    this.getTotals();
                    this.sendAjax();
                },
                removeFromCart: function (menu_item, varient_id, crust_id) {

                    if(varient_id && crust_id){ 
                        const selected_item = this.cart.filter(function (item) {  
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }
                    }else if(varient_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        } 
                    }else if(crust_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }  
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) {
                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this;
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }  
                    }
                    this.getTotals();
                    this.sendAjax(); 
                },
                increaseItemCount: function (menu_item, varient_id, crust_id) {

                    if(varient_id && crust_id){
                        const selected_item = this.cart.filter(function (item) {  
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) { 
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                    selected_item[0].quantity++; 
                                    this.selected_variant = {};
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        } 
                    }else if(varient_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                    selected_item[0].quantity++;
                                    // this.$forceUpdate();
                                    this.selected_variant = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        } 
                    }else if(crust_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                    selected_item[0].quantity++; 
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        }  
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id) {
                                    selected_item[0].quantity++;
                                    // this.$forceUpdate();
                                    this.selected_variant = {};
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        }  
                    }  
                    this.getTotals();
                    this.sendAjax();
                }, 
                addOfferItem: function (offer) {
                    var $this = this;
                    let arr = [];

                    this.offers_menus = [];
                    this.offer_id = offer.id;
                    this.offer_name = offer.name.toUpperCase();
                    this.offer_price = offer.price;
                    this.vat_category = offer.vat_category;

                    const selected_item = this.cart.filter(function (item) { 
                        return item.id == offer.id && item.type == 'offer'; 
                    });

                    if (selected_item.length) {   
                        selected_item[0].quantity++;
                        this.$forceUpdate(); 
                    }else{

                        axios.post('{{route('user.offers.menu')}}', {
                            _token: '{{csrf_token()}}',
                            offer_id: offer.id
                        }).then(function (response) {
                            $this.offers_menus = response.data.data.items;
                            $this.offers_menus.forEach(function(offer_menu) {
                                $this.selected_offer_items[offer_menu.id] = offer_menu.menu_items.length ? offer_menu.menu_items[0].offer_items_id : null;
                            });

                            $this.offers_menus.forEach(function(offer_menu) {
                                var selectedItem = offer_menu.menu_items.find(m => m.offer_items_id == $this.selected_offer_items[offer_menu.id]);
                                arr.push(selectedItem);
                            }); 

                            let offer_data = {
                                offer_items: arr,
                                id: $this.offer_id,
                                name: $this.offer_name,
                                price: $this.offer_price,
                                vat_category: $this.vat_category,
                                quantity: 1,
                                type: 'offer',
                            };
                            console.log(offer_data);
                            var item = $this.cart.push(offer_data);
                            $this.selected_offer_items = {};
                            $this.getTotals();
                            $this.sendAjax(); 
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }

                    // var $this = this;

                    // let arr = [];
                    // $this.deals_menus.forEach(function(deal_menu) {
                    //     var selectedItem = deal_menu.menu_items.find(m => m.deals_items_id == $this.selected_deal_items[deal_menu.id]);
                    //     arr.push(selectedItem);
                    // });

                    

                    // var item = this.cart.push(deal_data);

                    // this.selected_deal_items = {};
                    // this.getTotals();
                    // this.sendAjax();
                    // jQuery('#deal-item-modal').modal('hide');
                }, 
                getTotals: function () {
                    this.total = 0; 
                    this.sub_total = 0; 
                    const $this = this; 
                    var vat = 0;
                    var count = 0;
                    var restaurant_discount = 0;

                    this.cart.forEach(function (item) {
                        var price = item.price;   

                        if (item.show_variant) {
                            price = item.show_variant.pivot.price;
                        }

                        if (item.show_crust) {
                            price = price + item.show_crust.pivot.price;
                        } 

                        if (item.show_addons) {
                            item.show_addons.forEach(function (addon) {

                                  // $this.total += parseFloat(addon.pivot.price);
                                  // $this.sub_total += parseFloat(addon.pivot.price);
                                  // total_item_price += parseFloat(addon.pivot.price);
                                price = price + addon.pivot.price;
                            });
                        }

                        var total_item_price = parseFloat(price);

                        $this.total += item.quantity * price;
                        $this.sub_total += item.quantity * price;

                        var vat_percentage = 0; 

                        switch (item.vat_category) {
                            case 'food':
                                vat_percentage = $this.vat.food;
                                break;
                            case 'alcohol':
                                vat_percentage = $this.vat.alcohol;
                                break;
                        } 
                        vat += total_item_price * vat_percentage * item.quantity;
                        count = +count + +item.quantity;  
                        
                        if ($this.restaurant.discount == 1) {  
                            // console.log($this.restaurant.discount_type);
                            restaurant_discount = $this.getPromotionPrice($this.restaurant.discount_type, $this.restaurant.discount_value, $this.sub_total);
                            $this.restaurant_discount = restaurant_discount;
                            $this.lbl_restaurant_discount = restaurant_discount; 
                        } 
                    });  
                    this.vat_value = vat;
                    this.itemCount = count;  
                    var sum_val = this.currency(this.total+this.vat_value+this.deliveryCost- restaurant_discount); 
                    jQuery('#total-val').html(sum_val);  
                },
                getPromotionPrice(type, value, price) {
                    if (type == 'percentage') {
                        return price * value * 0.01;
                    } else {
                        return value;
                    }
                }, 
                sendAjax: function () {
                    const cart = this.cart;

                    let restaurant_id = this.restaurant.id;

                    let cart_date = this.cart_date;
                    let cart_time = this.cart_time;

                    let type = 'takeaway';

                    if (this.delivery) {
                        type = 'delivery';
                    }

                    if (this.takeaway) {
                        type = 'takeaway';
                    }

                    if (this.reservation) {
                        type = 'reservation';
                    } 

                    let requests = this.requests;

                    axios.post('{{route('cart.store')}}', {
                        _token: '{{csrf_token()}}',
                        cart: cart,
                        requests: requests,
                        type: type,
                        cart_date: cart_date,
                        cart_time: cart_time,
                        restaurant_id: restaurant_id,
                    }).then(function (response) {
                        // console.log(response);
                    }).catch(function (error) {
                        // console.log(error);
                    }); 
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    }); 
                    return formatter.format(price) 
                },
                getDeliveryCharge: function () {
                    if (this.delivery) { 
                        var post_code = jQuery('#post-code').val();   

                        if(post_code != this.new_post_code){
                            var $this = this;
                            $this.deliveryCost = 0;
                            axios.post('{{route('user.restaurant.delivery-charge')}}', {
                                _token: '{{csrf_token()}}',
                                restaurant_id: '{{$restaurant->id}}',
                                postcode: $this.postCode,
                                cart_time: $this.cart_time,
                                cart_date: $this.cart_date,
                            }).then(function (response) { 
                                if (response.data.message == 'success') { 
                                    $this.deliveryCost = response.data.delivery;  
                                    $.toast({
                                        heading: 'Post code added',
                                        text: 'Post code added',
                                        showHideTransition: 'fade',
                                        hideAfter: 3000,
                                        icon: 'success'
                                    });
                                    $this.new_post_code = post_code;
                                    $this.getTotals(); 
                                }  
                                $this.loading = false;
                            }).catch(function (error) { 
                                console.log(error);
                                $.toast({
                                    heading: 'Error',
                                    text: 'Sorry we do not delivery to this location.',
                                    showHideTransition: 'fade',
                                    hideAfter: 3000,
                                    icon: 'error'
                                });
                                $this.new_post_code = post_code;
                                $this.getTotals(); 
                                return false;
                            }); 
                        }
                    }
                },
                checkout: function (url) {
                    this.loading = true;
                    this.sendAjax();
                    var cart_time = moment('{{\Carbon\Carbon::now()->toDateString()}} ' + this.cart_time);
                    var delivery_time = '{{$restaurant->delivery_minutes}}';
                    if (delivery_time == '') {
                        delivery_time = 0;
                    }
                    var now = moment('{{\Carbon\Carbon::now()->toDateTimeString()}}');
                    var future_time = now.add(delivery_time, 'minutes'); 
                    var cart_date = this.cart_date;

                    if (cart_date == '') {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please select a valid date',
                            theme: 'error'
                        });
                        // this.cart_time = now;
                        return false;
                    }

                    // if (future_time.isAfter(cart_time)) {
                    //     $.alert({
                    //         title: '{{__('Oh Sorry!')}}',
                    //         content: 'Please select a time after ' + future_time.format('YYYY-MM-DD h:mm a'),
                    //         theme: 'error'
                    //     });
                    //     // this.cart_time = now;
                    //     return false;
                    // }

                    if (this.delivery) {   

                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            postcode: $this.postCode,
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        }).then(function (response) {
                                if (response.data.message == 'success') {
                                    $this.deliveryCost = response.data.delivery; 
                                    setTimeout(function(){
                                        window.location.href = url;
                                    }, 1000);
                                } else { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Error occurred. Please try again.',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                $this.loading = false;
                        }).catch(function (error) {
 
                            $this.loading = false;
                            if (error.response.status == 400) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    hideAfter: 3000,
                                    icon: 'error'
                                });
                                return false;
                            }
                            if (error.response.status == 419) { 
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: 'Please login to continue.',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }  
                            if (error.response.status == 403) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }
                            if (error.response.status == 402) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                }); 
                                setTimeout(function(){
                                    window.location.href = '{{route('user.address',['delivery'=>true])}}';
                                }, 2000);
                                return false;
                            }
                            if (error.response.status == 401) {
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: 'Please login to continue',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }
                        });

                    } else if (this.takeaway) {
                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-takeaway-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        })
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    window.location.href = '{{route('takeaway.create')}}'
                                } else { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Error occurred. Please try again',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                $this.loading = false;
                            })
                            .catch(function (error) {
                                $this.loading = false;
                                if (error.response.status == 400) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: error.response.data.message,
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }  
                                if (error.response.status == 419) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Please login to continue',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                if (error.response.status == 401) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Please login to continute!',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                            });

                    } else {
                        window.location.href = url;
                    }
                },
                getCurrentMenu: function () {
                    const $this = this;
                    const menu = this.menus.filter(function (menu) {
                        return menu.id == $this.menu_id
                    });
                    return menu[0];
                },
                getMenu: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    } 

                    axios.post('{{route('menu.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                        menu_id: $this.menu_id
                    })
                        .then(function (response) {
                            $this.setDeliveryItems(response.data.data.menu.menu_items);
                            $this.sortMenu();

                            this.pagination = {
                                current_page: response.data.data.menu.menu_items_pagination.current_page,
                                from: response.data.data.menu.menu_items_pagination.from,
                                last_page: response.data.data.menu.menu_items_pagination.last_page,
                                path: response.data.data.menu.menu_items_pagination.path,
                                per_page: response.data.data.menu.menu_items_pagination.per_page,
                                to: response.data.data.menu.menu_items_pagination.to,
                                total: response.data.data.menu.menu_items_pagination.total,
                            };
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                jumpToPage: function (page) {
                    this.getMenu(page);
                }, 
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                sortMenu: function () {
                    this.selected_menu_items = _.orderBy(this.selected_menu_items, ['name'], ['asc']);
                },
            }
        }); 

        (function($) { 
            $(".cata-sub-nav").on('scroll', function() {
                $val = $(this).scrollLeft();

                if($(this).scrollLeft() + $(this).innerWidth()>=$(this)[0].scrollWidth){
                $(".nav-next").hide();
                } else {
                $(".nav-next").show();
                }

                if($val == 0){
                $(".nav-prev").hide();
                } else {
                $(".nav-prev").show();
                }
            });
            console.log( 'init-scroll: ' + $(".nav-next").scrollLeft() );
            $(".nav-next").on("click", function(){
                $(".cata-sub-nav").animate( { scrollLeft: '+=460' }, 200);
            });
            $(".nav-prev").on("click", function(){
                $(".cata-sub-nav").animate( { scrollLeft: '-=460' }, 200);
            }); 
        })(jQuery); 

        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        } 
    </script>   
@endsection
