@extends('layouts.app')
@section('title', 'reservation')
@section('content')
    <link href="css/reservations.css" rel="stylesheet">
    <style>
        .contactus_header{
          color:#bc040b !important;
        }
    </style>
    <div class="main-header">
         <div class="row">
            <img src="{{ asset('storage/top_banner.png')) }}"  class="img-responsive" style="width:100%" alt="Image">
         </div>
      </div>
      <div class="container-fluid" id="right-content-reserve-align">
         <div class="row">
            <div class="col-xs-6 col-md-4">
               <div class="row top-contents">
          
                  <h3>RESERVE A <span id="span-text-color">TABLE</span></h3>
                  <h6>Contact Our Restaurants</h6>
                
               </div>
               <div class="row">
               
                  <div class="left-content-form-text">
                     <div class="col-xs-4 col-md-12">
                        <h3><b>London</b><br></h3>
                        <p>+44 20 7937 5363 / +44 20 7937 5363 <br>
                           info@restaurnatname.com
                        </p>
                     </div>
                  </div>
                  
               </div>
               <div class="row">
             
                  <div class="left-content-form-text">
                     <div class="col-xs-4 col-md-12">
                        <h3><b>Bermingham</b><br></h3>
                        <p>+44 20 7937 5363 / +44 20 7937 5363 <br>
                           info@restaurnatname.com
                        </p>
                     </div>
                  </div>
               
               </div>
               <div class="row">
               
                  <div class="left-content-form-text">
                     <div class="col-xs-4 col-md-12">
                        <h3><b>London</b><br></h3>
                        <p>+44 20 7937 5363 / +44 20 7937 5363 <br>
                           info@restaurnatname.com
                        </p>
                     </div>
                  </div>
                   
               </div>
            </div>
            <div class="col-xs-6 col-8" id="right-content-contact-us">
       
               <div class="row right-content-bot">
                  <div class="row">
                     <div class="tab tab-res">
                        <button class="tablinks" onclick="openCity(event, 'online')">
                           <p>Find a Table</p>
                        </button>
                        <button class="tablinks" onclick="openCity(event, 'delivery')">
                           <p>Your Details</p>
                        </button>
                     </div>
                  </div>
                  <hr>
                  <div id="online" class="tabcontent">
                     <form class="rec-details">
                        <div class="form-row">
                           <div class="col-md-4"><input class="date" type="date" placeholder="Date"></input></div>
                           <div class="col-md-4"> <input class="time" type="time" placeholder="Time"></input></input></div>
                           <div class="form-group col-md-4">
                              <select id="inputState" placeholder="Pax" class="form-control">
                                 <option selected>Pax</option>
                                 <option>1 person</option>
                                 <option>2</option>
                                 <option>3</option>
                                 <option>4</option>
                                 <option>5</option>
                                 <option>more</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-row">
                           <div class="col-md-6">
                              <select id="inputState" placeholder="smoking" class="form-control">
                                 <option selected>Smoking or Non-smoking</option>
                                 <option>Skoking</option>
                                 <option>Non-smoking</option>
                              </select>
                           </div>
                           <div class="col-md-6">
                              <select id="inputState" placeholder="branch" class="form-control">
                                 <option selected>
                                    Branch
                                 </option>
                                 <option>London</option>
                                 <option>Liverpool</option>
                                 <option>Bristol</option>
                                 <option> Berkshire</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-row">
                           <div class="col-md-6" id="message"><input type="text" name="message" placeholder="Message"></div>
                           <div class="col-md-6" ></div>
                        </div>
                        <div class="form-row">
                           <div class="col-md-12" >
                              <input id="add-to-cart-h-mid" type="button" onclick="myFunction()" value="NEXT">
                           </div>
                        </div>
                     </form>
                  </div>
                  <div id="delivery" class="tabcontent">
                     <form id="myForm" action="/action_page.php">
                        <div class="row">
                           <div class="col-md-6"><input type="text" name="name" placeholder="First Name"></div>
                           <div class="col-md-6"><input type="text" name="name" placeholder="Last Name"></div>
                           <div class="col-md-6"><input type="text" name="phone" placeholder="Phone Number"></div>
                           <div class="col-md-6"><input type="text" name="email" placeholder="E-mail"></div>
                           <div class="col-md-6" id="reservation-details">
                              <h3>Your reservation details </h3>
                              <div class="row">
                                 <div class="col-md-2"><i class="fa fa-calendar" aria-hidden="true"></i> </div>
                                 <div class="col-md-8">
                                    <p id="picked-date">Thursday, August 27</p>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-2"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                                 <div class="col-md-8">
                                    <p id="picked-time">7:30 pm</p>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-2"><i class="fa fa-user" aria-hidden="true"></i></div>
                                 <div class="col-md-8">
                                    <p id="no-of-people"> 2 People</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6" id="message">
                              <h3>  A note from the restaurant</h3>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                           </div>
                        </div>
                        <input id="add-to-cart-h-mid" type="button" onclick="myFunction()" value="CONFIRM RESERVATION">
                     </form>
                  </div>
               </div>
            
               <div id="online" class="tabcontent">
                  <form id="myForm" action="/action_page.php">
                     <div class="row">
                        <div class="col-md-4"><input class="date" type="date" placeholder="Date"></input></div>
                        <div class="col-md-4"> <input class="time" type="time" placeholder="Time"></input></input></div>
                        <div class="form-group col-md-4">
                           <select id="inputState" placeholder="Pax" class="form-control">
                              <option selected>Pax</option>
                              <option>1 person</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>more</option>
                           </select>
                        </div>
                        <div class="col-md-6"><input type="text" name="email" placeholder="E-mail"></div>
                        <div class="col-md-6"><input type="text" name="phone" placeholder="Telephone"></div>
                        <div class="col-md-6"><input type="text" name="subject" placeholder="Subject"></div>
                        <div class="col-md-6" id="message"><input type="text" name="message" placeholder="Message"></div>
                     </div>
                     <input id="add-to-cart-h-mid" type="button" onclick="myFunction()" value="NEXT">
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
@endsection
