@extends('layouts.app')
@section('title', 'deals')
@section('content')

    <link href="{{ asset('css/specialoffers.css') }}" rel="stylesheet">
    <style>
        .active {
            color: #fce703;
        }

        .deals_header {
            color: #028F38 !important;
        }
        
        .jq-toast-wrap.bottom-left {
            position: fixed;
            top: 14%;
            left: 76%;
        }

    </style>
    <div id="restaurant">
        <div class="row container-fluid">
            <div class="col-sm-12 col-md-9 col-lg-9" id="item-card-figure">
                <div class="row">
                    <div v-for="deal_item in deals" class="col-sm-12 col-md-4 col-lg-4 figure-item">
                        <div class="item-card">
                            <h3>@{{ deal_item . name }}</h3>
                            <img v-if="deal_item.image" class="img-responsive item-img" :src="deal_item.image" alt="Image">
                            <img v-if="!deal_item.image" class="img-responsive item-img"
                                src="{{ asset('img/default.jpg') }}" alt="Image">
                            <p>@{{ deal_item . description . substring(0, 100) + '..' }}</p>
                            <span class="item-price">
                                {{ setting('currency') }}@{{ currency(deal_item . price) }}
                            </span>
                            @if (setting('static_site') == 0)
                                <button @click.prevent="selectDealMenuItem(deal_item,null)" id="add-to-cart-btn"
                                    class="btn btn-default btn-sm">ADD TO CART</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3" id="right-side-cart-figure">
                <div class="row make-order-txt-row">
                    <h3>MAKE YOUR ORDER NOW<h3>
                </div>
                @if (setting('static_site') == 0)
                    <div class="row cart-items-count-row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <h5>@{{ itemCount }} Item(s) in the cart</h5>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            {{-- <a class="remove-btn">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                <span>Empty Cart</span>
                            </a> --}}
                        </div>
                    </div>
                @endif

                <div id="cart-items-figure" v-if="cart.length">
                    <div class="cart-items-row" v-for="(item,index) in cart" :key="'delivery_cart'+item.id+'__'+index">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4 class="item-name">@{{ item . name }}</h4>
                                <span class="sub-item-name sub" v-if="item.deal_items" v-for="d_item in item.deal_items">
                                    @{{ d_item . name }}
                                </span>
                                <span class="sub-item-name sub" v-if="item.offer_items" v-for="o_item in item.offer_items">
                                    @{{ o_item . name }}
                                </span>
                                <span class="sub-item-name" v-if="item.show_variant">
                                    (@{{ item . show_variant . name }})
                                </span>
                                <span class="sub-item-name" v-if="item.show_crust">
                                    @{{ item . show_crust . name }} - {{ setting('currency') }}
                                    @{{ currency(item . show_crust . pivot . price) }}
                                </span>
                                <span class="sub-item-name sub" v-if="item.show_addons" v-for="addon in item.show_addons">
                                    @{{ addon . name }} - {{ setting('currency') }}
                                    @{{ currency(addon . pivot . price) }}
                                </span>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4 class="item-price" v-if="!item.show_variant">{{ setting('currency') }}
                                    @{{ currency(item . price) }}</h4>
                                <h4 class="item-price" v-if="item.show_variant">{{ setting('currency') }}
                                    @{{ currency(item . show_variant . pivot . price) }}</h4>
                            </div>
                        </div>
                        <div class="row bottom-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <a class="remove-btn"
                                    @click.prevent="removeFromCart(item, item.show_variant, item.show_crust)">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                    <span>Remove</span>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group count-btn">
                                    <span class="minus-btn" id="minus-btn"
                                        @click.prevent="removeFromCart(item, item.show_variant, item.show_crust)">-</span>
                                    <input id="number-input" class="number-input" type="text" v-bind:value="item.quantity">
                                    <span class="plus-btn" id="plus-btn"
                                        @click.prevent="increaseItemCount(item, item.show_variant, item.show_crust)">+</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($restaurant->takeaway)
                        <div class="row checkbox-option-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4>Take Away</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <input type="checkbox" id="scales" name="scales" @click.prevent="setMethod('takeaway')"
                                    v-model="takeaway">
                                <label for="scales"></label>
                            </div>
                        </div>
                    @endif

                    @if ($restaurant->delivery)
                        <div class="row checkbox-option-row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <h4>Delivery</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <input type="checkbox" id="horns" name="horns" @click.prevent="setMethod('delivery')"
                                    v-model="delivery">
                                <label for="horns"></label>
                            </div>
                        </div>
                    @endif

                    <div class="input-option-row">
                        <input v-if="delivery" type="text" id="post-code" v-click-outside="getDeliveryCharge" class="form-control" name="postCode" v-model="postCode"
                            placeholder="Enter Postal Code" autocomplete="off">
                        <input type="text" class="form-control" name="promoCode" placeholder="Enter Promo Code">
                    </div>
                    <div class="amount-figure" v-if="cart.length">
                        <div class="row sub-amount-row">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Sub Total</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(sub_total) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>V.A.T.</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(vat_value) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row" v-if="delivery">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Delivery</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(deliveryCost) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row sub-amount-row" v-if="delivery">
                            <div class="col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Site Discount</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 class="amount">
                                    <span>{{ setting('currency') }}</span> @{{ currency(lbl_restaurant_discount) }}
                                </h4>
                            </div>
                        </div>
                        <div class="row total-amount-row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 no-left-pd">
                                <h4>Total</h4>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 no-right-pd">
                                <h4 v-if="delivery" class="tot-price">
                                    <span>{{ setting('currency') }}</span>@{{ currency(total + vat_value + deliveryCost- restaurant_discount) }}
                                </h4>
                                <h4 v-if="takeaway" class="tot-price">
                                    <span>{{ setting('currency') }}</span> @{{ currency(total + vat_value- restaurant_discount) }}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <a href="#" @click.prevent="checkout('{{ route('user.restaurant.menu') }}')" id="proceed-btn"
                        class="btn btn-default btn-sm">Proceed to Checkout</a>
                </div>
                <div class="empty-cart-sec" v-if="!cart.length">
                    <h4>Your cart is empty</h4>
                    <img class="img-responsive" src="img/cart.png">
                    <p>Your cart is empty. Get your cravings satisfied by adding your favourites into the cart.</p>
                </div>
            </div>
        </div>

        <!---- modal section ---->
        <div class="modal fade" id="deal-item-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            style="z-index: 9999999;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Select Your Menu Item</h4>
                    </div>
                    <div class="modal-body">
                        <input v-model="deal_id" type="hidden" />
                        <input v-model="deal_name" type="hidden" />
                        <input v-model="deal_price" type="hidden" />
                            <div class="row" v-for="deal_menu in deals_menus">
                                <h4 class="deal-item-menu-title" style="margin: 10px 0;text-align: left;">
                                    @{{ deal_menu . name }}</h4>
                                <div v-for="item in deal_menu.menu_items" class="item-view-cls">
                                    <input type="radio" v-model="selected_deal_items[deal_menu.id]"
                                        :value="item.deals_items_id" :name="deal_menu.id" :key="item.deals_items_id" />
                                    @{{ item . name }}
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="add-to-cart" type="button" style="float: right" @click.prevent="addDealItem()">Add to
                            cart
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!---- //modal section ---->
    </div>

    <script type="text/javascript"> 

        var data = {
            menus:{!! json_encode($restaurant->menus) !!},
            deals:{!! json_encode($restaurant->deals) !!},
            cuisines: {!! json_encode($restaurant->menus) !!}, 
            selected_menu_items: [],
            style: 'list',
            reservation: false,
            delivery: false,
            takeaway: false,
            cart: {!! json_encode($cart) !!},
            total: 0,
            vat_value: 0,
            vat:{!! json_encode(getVat(),true) !!},
            sub_total: 0,
            requests: '{{$requests}}',
            cart_date: '{{$cart_date}}',
            cart_time: '{{$cart_time}}',
            restaurant:{!! json_encode($restaurant) !!},
            favoured: false,
            menu_id: 0,
            type: '{{$type}}',
            restaurant_id: '{{$restaurant_id}}',
            date: '{{\Carbon\Carbon::now()->toDateString()}}',
            time: '{{\Carbon\Carbon::now()->format('h:i A')}}',
            head_count: '',
            phone: '',
            email: '',
            reservation_validation: {},
            review_validation: {},
            step: 1,
            reservation_data: [],
            reservation_requests: '',
            rating: 5,
            review: '',
            sorted_reviews: [],
            sort: 'desc',
            variants: [],
            addons: [],
            menu_item: {},
            selected_addons: [],
            selected_variant: {},
            reservation_restaurant: {},
            reservation_settings: {},
            guest: false,
            remainingCount1: 255,
            remainingCount2: 255,
            remainingCount3: 255,
            loading: false,
            deals_menus: {},
            selected_deal_items: {},
            selectedDealItem: {},
            deal_id: '',
            deal_name: '',
            deal_price: '',
            deal_items: [],
            offers_menus: {},
            selected_offer_items: {},
            selectedOfferItem: {},
            offer_id: '',
            offer_name: '',
            offer_price: '',
            offer_items: [],
            pagination: {}, 
            menu_items_paginate: {},
            itemCount: 0,
            postCode: '',
            deliveryCost:{!! json_encode($restaurant->delivery_cost) !!},
            lbl_restaurant_discount: 0,
            restaurant_discount: 0,
            site_discount: 0,
            new_post_code: 0,
        };

        var restaurant = new Vue({
            el: '#restaurant',
            data: data,
            mounted: function () { 
                
                if (this.restaurant.id != this.restaurant_id) {
                    this.cart = [];
                }

                if (this.type == 'delivery') {  
                    this.setMethod('delivery', false);
                } else if (this.type == 'takeaway') { 
                    this.setMethod('takeaway', false);
                }

                // if (this.type == 'delivery') { 
                //     this.setDeliveryItems(this.menus[0].menu_items); 

                //     this.menu_id = this.menus[0].id;
                //     // this.getMenu(); 
                //     this.setMethod('delivery', false);
                // } else if (this.type == 'takeaway') {
                //     this.setTakeawayItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('takeaway', false);
                // } else if (this.type == 'reservation') {
                //     this.setReservationItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('reservation', false);
                // } else {
                //     this.setReservationItems(this.menus[0].menu_items);

                //     this.menu_id = this.menus[0].id;

                //     this.setMethod('reservation', false);
                // } 

                this.getTotals();

                if (this.restaurant.user) {
                    this.favoured = true;
                } 
                this.sorted_reviews = this.restaurant.reviews; 
                @if(auth()->check())
                    @if(auth()->user()->email== setting('guest_email_id'))
                        this.guest = true;
                    @endif
                @endif  
            },
            directives: {
                'click-outside': {
                  bind: function(el, binding, vNode) {
                    // Provided expression must evaluate to a function.
                    if (typeof binding.value !== 'function') {
                        const compName = vNode.context.name
                      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                      if (compName) { warn += `Found in component '${compName}'` }
                      
                      console.warn(warn)
                    }
                    // Define Handler and cache it on the element
                    const bubble = binding.modifiers.bubble
                    const handler = (e) => {
                      if (bubble || (!el.contains(e.target) && el !== e.target)) {
                        binding.value(e)
                      }
                    }
                    el.__vueClickOutside__ = handler

                    // add Event Listeners
                    document.addEventListener('click', handler)
                        },
                  
                    unbind: function(el, binding) {
                        // Remove Event Listeners
                        document.removeEventListener('click', el.__vueClickOutside__)
                        el.__vueClickOutside__ = null 
                    }
                }
            },
            methods: {
                setMenu: function (menu) {

                    if (this.delivery) {
                        // this.setDeliveryItems(menu.menu_items.data);
                        this.setDeliveryItems(menu.menu_items);  
                    }
                    if (this.takeaway) {
                        this.setTakeawayItems(menu.menu_items);
                    }
                    if (this.reservation) {
                        this.setReservationItems(menu.menu_items);
                    } 
                    
                    if (menu.id == 0) {
                        this.menu_id = 0;
                    } else {
                        this.menu_id = menu.menu_items.menu_id;
                    } 
                },
                setDeliveryItems: function (menu_items) {
                    // var menu_items = menu_items.data;
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.delivery;
                    });
                    // this.equalHeight();
                },
                setTakeawayItems: function (menu_items) {
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.takeaway;
                    });
                    // this.equalHeight();
                },
                setMethod: function (method, exception = false) {
                    if (this.cart.length && exception && method != 'reservation') {
                        var $this = this; 
                    }

                    const menu = this.getCurrentMenu(); 

                    switch (method) {
                        case 'reservation':
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            // this.setReservationItems(menu.menu_items);
                            break;
                        case 'delivery':
                            this.reservation = false;
                            this.delivery = true;
                            this.takeaway = false;
                            // this.setDeliveryItems(menu.menu_items);
                            break;
                        case 'takeaway':
                            this.reservation = false;
                            this.delivery = false;
                            this.takeaway = true;
                            // this.setTakeawayItems(menu.menu_items);
                            break;
                        default:
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            this.setTakeawayItems(menu.menu_items);
                            break;
                    } 
                    this.sendAjax();
                },
                addToCart: function (menu_item) {
                    this.selected_addons = [];  
                    this.selected_variant = jQuery('#' + menu_item.id).val(); 
                    var varient_id = jQuery('#' + menu_item.id).val();  
                    var qty = jQuery('#' + menu_item.id + '-textbox').val(); 

                    var varient = menu_item.variants.filter(function (item) {
                        return item.id == varient_id;
                    });  
                     
                    this.selected_variant = varient; 

                    if (menu_item.addons.length) {

                        // this.variants = menu_item.variants;
                        this.addons = menu_item.addons;

                        // if (menu_item.variants.length) {
                        //     this.selected_variant = this.variants[0].id;
                        // }
                        this.menu_item = menu_item; 
                        // jQuery('#addons-modal').modal('show');  
                        return false; 
                    }

                    for(var q = 1; q <= qty; q++){
                        this.addMenuItem(menu_item, varient_id); 
                    }
                    // this.addMenuItem(menu_item); 
                },
                selectDealMenuItem(deal) {
                    var $this = this;
                    this.deals_menus = [];
                    this.deal_id = deal.id;
                    this.deal_name = deal.name.toUpperCase();
                    this.deal_price = deal.price;
                    this.vat_category = deal.vat_category;
                    axios.post('{{route('user.deals.menu')}}', {
                        _token: '{{csrf_token()}}',
                        deal_id: deal.id
                    }).then(function (response) {
                        $this.deals_menus = response.data.data.items;
                        $this.deals_menus.forEach(function(deal_menu) {
                            $this.selected_deal_items[deal_menu.id] = deal_menu.menu_items.length ? deal_menu.menu_items[0].deals_items_id : null;
                        });
                    }).catch(function (error) {
                        console.log(error);
                    }); 
                    jQuery('#deal-item-modal').modal('show');
                },
                addDealItem: function () {
                    var $this = this;

                    let arr = [];
                    $this.deals_menus.forEach(function(deal_menu) {
                        var selectedItem = deal_menu.menu_items.find(m => m.deals_items_id == $this.selected_deal_items[deal_menu.id]);
                        arr.push(selectedItem);
                    });

                    let deal_data = {
                        deal_items: arr,
                        id: $this.deal_id,
                        name: $this.deal_name,
                        price: $this.deal_price,
                        vat_category: $this.vat_category,
                        quantity: 1,
                        type: 'deal',
                    };

                    var item = this.cart.push(deal_data);

                    this.selected_deal_items = {};
                    this.getTotals();
                    this.sendAjax();
                    jQuery('#deal-item-modal').modal('hide');
                },
                addMenuItem: function (menu_item, varient_id) {
                    var $this = this; 

                    const selected_item = this.cart.filter(function (item) {
                        return item.id == menu_item.id && item.selected_variant == varient_id;
                    });

                    console.log(selected_item);

                    if (selected_item.length) {
                        
                        if (menu_item.variants.length) {
                            var $this = this;
                            // menu_item.quantity = 1;
                            menu_item.type = 'single'; 
                            // menu_item.selected_variant = this.selected_variant; 

                            var show_addons = [];

                            menu_item.addons.forEach(function (addon) {
                                $this.selected_addons.forEach(function (selected_addon) {
                                    if (addon.id == selected_addon) {
                                        show_addons.push(addon);
                                    }
                                });
                            });  

                            // var item = this.cart.push(Object.assign({}, menu_item)); 

                            selected_item[0].quantity++;
                            this.$forceUpdate();

                            console.log(selected_item[0]); 

                            if (this.selected_variant.length != 0) {
                                // this.cart[item - 1].show_variant = this.selected_variant;
                                // this.cart[item - 1].selected_variant = this.selected_variant.id;

                                // this.cart[item - 1].show_variant = this.selected_variant[0];
                                // this.cart[item - 1].selected_variant = this.selected_variant[0].id;
                                
                                selected_item[0].show_variant = this.selected_variant[0];
                                selected_item[0].selected_variant = this.selected_variant[0].id;
                            } 

                            // if (show_addons) {
                            //     this.cart[item - 1].selected_addons = this.selected_addons;
                            //     this.cart[item - 1].show_addons = show_addons;
                            // } 

                            this.selected_addons = []; 
                            this.selected_variant = {};
                        } else {
                            selected_item[0].quantity++;
                            this.$forceUpdate();
                            this.selected_variant = {};
                            this.selected_addons = [];
                        }
                    } else {  

                        var $this = this; 
                        menu_item.quantity = 1;
                        menu_item.type = 'single'; 
                        // menu_item.selected_variant = this.selected_variant;  
                        var show_addons = [];

                        menu_item.addons.forEach(function (addon) {
                            $this.selected_addons.forEach(function (selected_addon) {
                                if (addon.id == selected_addon) {
                                    show_addons.push(addon);
                                }
                            });
                        }); 

                        var item = this.cart.push(menu_item); 
                        // if (this.selected_variant) {
                        if (this.selected_variant.length != 0) {
                            // this.cart[item - 1].show_variant = this.selected_variant;
                            // this.cart[item - 1].selected_variant = this.selected_variant.id;
                            this.cart[item - 1].show_variant = this.selected_variant[0];
                            this.cart[item - 1].selected_variant = this.selected_variant[0].id;
                        }

                        if (show_addons) {
                            this.cart[item - 1].selected_addons = this.selected_addons;
                            this.cart[item - 1].show_addons = show_addons;
                        }  

                        this.selected_addons = []; 
                        this.selected_variant = {};
                    } 

                    this.getTotals();
                    this.sendAjax(); 
                    // jQuery('#addons-modal').modal('hide');  
                },
                removeItemFromCart: function (menu_item, varient_id, crust_id) { 
                    if(varient_id && crust_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });   
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        } 
                    }else if(varient_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            console.log(item.selected_variant); 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });    
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                console.log(item); 
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }  
                    }else if(crust_id){ 
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  

                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }   
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id) {
                                    $this.cart.splice(index, 1);
                                }
                            }); 
                        }  
                    }
                    this.getTotals();
                    this.sendAjax();
                },
                removeFromCart: function (menu_item, varient_id, crust_id) {

                    if(varient_id && crust_id){ 
                        const selected_item = this.cart.filter(function (item) {  
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }
                    }else if(varient_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        } 
                    }else if(crust_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  

                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this; 
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }  
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) {
                            if (selected_item[0].quantity > 1) {
                                selected_item[0].quantity--;
                                this.$forceUpdate();
                            } else {
                                let $this = this;
                                this.cart.forEach(function (item, index) {
                                    if (selected_item[0].id == item.id) {
                                        $this.cart.splice(index, 1);
                                    }
                                });
                            }
                        }  
                    }
                    this.getTotals();
                    this.sendAjax(); 
                },
                increaseItemCount: function (menu_item, varient_id, crust_id) {

                    if(varient_id && crust_id){
                        const selected_item = this.cart.filter(function (item) {  
                            return item.id == menu_item.id && item.selected_variant == varient_id.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) { 
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant && selected_item[0].selected_crust == item.selected_crust) {
                                    selected_item[0].quantity++; 
                                    this.selected_variant = {};
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        } 
                    }else if(varient_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_variant == varient_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_variant == item.selected_variant) {
                                    selected_item[0].quantity++;
                                    // this.$forceUpdate();
                                    this.selected_variant = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        } 
                    }else if(crust_id){
                        const selected_item = this.cart.filter(function (item) { 
                            return item.id == menu_item.id && item.selected_crust == crust_id.id; 
                        });  
                        if (selected_item.length > 0) {  
 
                            let $this = this; 
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id && selected_item[0].selected_crust == item.selected_crust) {
                                    selected_item[0].quantity++; 
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        }  
                    }else{
                        const selected_item = this.cart.filter(function (item) {
                            return item.id == menu_item.id
                        });  
                        if (selected_item.length > 0) { 
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id) {
                                    selected_item[0].quantity++;
                                    // this.$forceUpdate();
                                    this.selected_variant = {};
                                    this.selected_crust = {};
                                    this.selected_addons = []; 
                                }
                            }); 
                        }  
                    }  
                    this.getTotals();
                    this.sendAjax();
                },
                getTotals: function () {
                    this.total = 0; 
                    this.sub_total = 0; 
                    const $this = this; 
                    var vat = 0;
                    var count = 0;
                    var restaurant_discount = 0;

                    this.cart.forEach(function (item) {
                        var price = item.price;   

                        if (item.show_variant) {
                            price = item.show_variant.pivot.price;
                        }

                        if (item.show_crust) {
                            price = price + item.show_crust.pivot.price;
                        } 

                        if (item.show_addons) {
                            item.show_addons.forEach(function (addon) {
                                  // $this.total += parseFloat(addon.pivot.price);
                                  // $this.sub_total += parseFloat(addon.pivot.price);
                                  // total_item_price += parseFloat(addon.pivot.price);
                                price = price + addon.pivot.price;
                            });
                        }

                        var total_item_price = parseFloat(price);

                        $this.total += item.quantity * price;
                        $this.sub_total += item.quantity * price;

                        var vat_percentage = 0; 

                        switch (item.vat_category) {
                            case 'food':
                                vat_percentage = $this.vat.food;
                                break;
                            case 'alcohol':
                                vat_percentage = $this.vat.alcohol;
                                break;
                        } 
                        vat += total_item_price * vat_percentage * item.quantity;
                        count += item.quantity; 
                        
                        if ($this.restaurant.discount == 1) {  
                            // console.log($this.restaurant.discount_type);
                            restaurant_discount = $this.getPromotionPrice($this.restaurant.discount_type, $this.restaurant.discount_value, $this.sub_total);
                            $this.restaurant_discount = restaurant_discount;
                            $this.lbl_restaurant_discount = restaurant_discount; 
                        } 
                    });  
                    this.vat_value = vat;
                    this.itemCount = count; 
                    var sum_val = this.currency(this.total+this.vat_value+this.deliveryCost- restaurant_discount); 
                    jQuery('#total-val').html(sum_val);   
                },
                getPromotionPrice(type, value, price) {
                    if (type == 'percentage') {
                        return price * value * 0.01;
                    } else {
                        return value;
                    }
                }, 
                sendAjax: function () {
                    const cart = this.cart;

                    let restaurant_id = this.restaurant.id;

                    let cart_date = this.cart_date;
                    let cart_time = this.cart_time;

                    let type = 'takeaway';

                    if (this.delivery) {
                        type = 'delivery';
                    }

                    if (this.takeaway) {
                        type = 'takeaway';
                    }

                    if (this.reservation) {
                        type = 'reservation';
                    } 

                    let requests = this.requests;

                    axios.post('{{route('cart.store')}}', {
                        _token: '{{csrf_token()}}',
                        cart: cart,
                        requests: requests,
                        type: type,
                        cart_date: cart_date,
                        cart_time: cart_time,
                        restaurant_id: restaurant_id,
                    }).then(function (response) {
                        // console.log(response);
                    }).catch(function (error) {
                        // console.log(error);
                    }); 
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    }); 
                    return formatter.format(price) 
                },
                getDeliveryCharge: function () {
                    if (this.delivery) { 
                        var post_code = jQuery('#post-code').val();   

                        if(post_code != this.new_post_code){
                            var $this = this;
                            $this.deliveryCost = 0;
                            axios.post('{{route('user.restaurant.delivery-charge')}}', {
                                _token: '{{csrf_token()}}',
                                restaurant_id: '{{$restaurant->id}}',
                                postcode: $this.postCode,
                                cart_time: $this.cart_time,
                                cart_date: $this.cart_date,
                            }).then(function (response) { 
                                if (response.data.message == 'success') { 
                                    $this.deliveryCost = response.data.delivery;  
                                    $.toast({
                                        heading: 'Post code added',
                                        text: 'Post code added',
                                        showHideTransition: 'fade',
                                        hideAfter: 3000,
                                        icon: 'success'
                                    });
                                    $this.new_post_code = post_code;
                                    $this.getTotals(); 
                                }  
                                $this.loading = false;
                            }).catch(function (error) { 
                                console.log(error);
                                $.toast({
                                    heading: 'Error',
                                    text: 'Sorry we do not delivery to this location.',
                                    showHideTransition: 'fade',
                                    hideAfter: 3000,
                                    icon: 'error'
                                });
                                $this.new_post_code = post_code;
                                $this.getTotals(); 
                                return false;
                            }); 
                        }
                    }
                },
                checkout: function (url) {
                    window.location.href = '{{route('user.restaurant.menu')}}';
                    this.loading = true;
                    this.sendAjax();
                    var cart_time = moment('{{\Carbon\Carbon::now()->toDateString()}} ' + this.cart_time);
                    var delivery_time = '{{$restaurant->delivery_minutes}}';
                    if (delivery_time == '') {
                        delivery_time = 0;
                    }
                    var now = moment('{{\Carbon\Carbon::now()->toDateTimeString()}}');
                    var future_time = now.add(delivery_time, 'minutes'); 
                    var cart_date = this.cart_date;

                    if (cart_date == '') {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please select a valid date',
                            theme: 'error'
                        });
                        // this.cart_time = now;
                        return false;
                    }

                    // if (future_time.isAfter(cart_time)) {
                    //     $.alert({
                    //         title: '{{__('Oh Sorry!')}}',
                    //         content: 'Please select a time after ' + future_time.format('YYYY-MM-DD h:mm a'),
                    //         theme: 'error'
                    //     });
                    //     // this.cart_time = now;
                    //     return false;
                    // }

                    if (this.delivery) {   

                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            postcode: $this.postCode,
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        }).then(function (response) {
                                if (response.data.message == 'success') {
                                    $this.deliveryCost = response.data.delivery; 
                                    setTimeout(function(){
                                        window.location.href = url;
                                    }, 1000);
                                } else { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Error occurred. Please try again.',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                $this.loading = false;
                        }).catch(function (error) {
 
                            $this.loading = false;
                            if (error.response.status == 400) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    hideAfter: 3000,
                                    icon: 'error'
                                });
                                return false;
                            }
                            if (error.response.status == 419) { 
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: 'Please login to continue.',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }  
                            if (error.response.status == 403) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }
                            if (error.response.status == 402) {
                                
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: error.response.data.message,
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                }); 
                                setTimeout(function(){
                                    window.location.href = '{{route('user.address',['delivery'=>true])}}';
                                }, 2000);
                                return false;
                            }
                            if (error.response.status == 401) {
                                $.toast({
                                    heading: '{{__('Oh Sorry!')}}',
                                    text: 'Please login to continue',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                                return false;
                            }
                        });

                    } else if (this.takeaway) {
                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-takeaway-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        })
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    window.location.href = '{{route('takeaway.create')}}'
                                } else { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Error occurred. Please try again',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                $this.loading = false;
                            })
                            .catch(function (error) {
                                $this.loading = false;
                                if (error.response.status == 400) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: error.response.data.message,
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }  
                                if (error.response.status == 419) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Please login to continue',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                                if (error.response.status == 401) { 
                                    $.toast({
                                        heading: '{{__('Oh Sorry!')}}',
                                        text: 'Please login to continute!',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                    return false;
                                }
                            });

                    } else {
                        window.location.href = url;
                    }
                },
                getCurrentMenu: function () {
                    const $this = this;
                    const menu = this.menus.filter(function (menu) {
                        return menu.id == $this.menu_id
                    });
                    return menu[0];
                },
                getMenu: function (page = '') {
                    let $this = this;
                    var page_url = '';
                    if (page) {
                        page_url = '?page=' + page;
                    } 

                    axios.post('{{route('menu.get')}}'+ page_url, {
                        _token: '{{csrf_token()}}',
                        menu_id: $this.menu_id
                    })
                        .then(function (response) {
                            // this.selected_menu_items = response.data.data.menu.menu_items;
                            // this.menu_items_paginate = response.data.data.menu.menu_items;

                            $this.setDeliveryItems(response.data.data.menu.menu_items);
                            $this.sortMenu();

                            this.pagination = {
                                current_page: response.data.data.menu.menu_items_pagination.current_page,
                                from: response.data.data.menu.menu_items_pagination.from,
                                last_page: response.data.data.menu.menu_items_pagination.last_page,
                                path: response.data.data.menu.menu_items_pagination.path,
                                per_page: response.data.data.menu.menu_items_pagination.per_page,
                                to: response.data.data.menu.menu_items_pagination.to,
                                total: response.data.data.menu.menu_items_pagination.total,
                            };
                            // $this.setDeliveryItems(this.menu_items_paginate); 
                            // $this.sortMenu();
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                jumpToPage: function (page) {
                    this.getMenu(page);
                }, 
                getCurrentPage: function (page) {
                    if (page == this.pagination.current_page) {
                        return 'active';
                    } else {
                        return '';
                    }
                },
                sortMenu: function () {
                    this.selected_menu_items = _.orderBy(this.selected_menu_items, ['name'], ['asc']);
                },
            }
        });
    </script> 

@endsection
