@extends('layouts.app')
@section('title', 'user/deliveries')
@section('content') 
<link href="{{asset('css/my_account.css')}}" rel="stylesheet"> 
<!-- Content goes here -->
<div class="content-wrap text-center" id="deliveries">
    <div class="row">
        <div class="col-md-4 left-content">
            <div class="row">
                <h6>MY ACCOUNT</h6>
            </div>
            <div class="row">
                @include('includes.user-header', ['active'=>'details']) 
            </div>
        </div>
        <div class="col-md-8 right-content">
           <div class="container row right-content-top">
              <div class="col-xs-3 col-sm-3 col-md-4">
                 <h6 style="font-family: 'Montserrat';font-weight: bold;font-size: 18px;">Deliveries</h6>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-4 edit-content" id="edit">  
              </div>
           </div>
           <div class="container row right-content-mid">
                <div id="myDetails" class="tabcontent">
                    <div class="row">
                        <hr>
                        <div class="row reservations-section">
                            <div class="col-md-7">
                                <div v-for="delivery in deliveries">
                                    <div class="row reservations-row">
                                        <div class="col-md-8" style="font-family: 'Montserrat';font-weight: bold;text-align: left;padding-left: 5%;"> 
                                            <p>Order ID: @{{ delivery.id }}</p>
                                            <!--<p>@{{ delivery.time }}</p>-->
                                            <p>No. of Items: @{{ delivery.delivery_items.length }} </p> 
                                        </div>
                                        <!--<div class="col-md-4" style="font-family: 'Montserrat';font-weight: bold;">  -->
                                        <!--</div>-->
                                        <!--<div class="col-md-4" style="padding: 0 15px 0 0;"> -->
                                        <!--    <div class="reservation-time text-right" style="font-family: 'Montserrat';font-weight: bold;">-->
                                        <!--        <a href="" style="color: #bc040b;text-decoration: underline;font-weight: 500;" @click.prevent="details(delivery)">View Details</a>-->
                                        <!--    </div>-->
                                        <!--</div>-->
                                        <div class="col-md-4" style="font-family: 'Montserrat';font-weight: bold;">
                                            <div class="contact-icons">
                                                <a>
                                                    <img src="{{asset('img/Feedback.png')}}">
                                                </a>
                                                <a><img src="{{asset('img/Phone.png')}}"></a> 
                                            </div>
                                            <div class="reservation-time">
                                                <a href="#" @click.prevent="details(delivery)">
                                                    <p>@{{ delivery.time }} <img
                                                            src="{{asset('img/back-arrow.png')}}">
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="yellow-hr">
                                </div> 
                                {{ $paginate->links() }}
                            </div>
                            <div class="col-md-5" style="padding-left: 10%;">
                                <div class="order-summary-box my-account-summary-box" v-if="delivery.id">
                                    <div class="row" style="font-family: 'Montserrat';font-weight: 600;text-align:left;">
                                        <h3 style="text-align:center;">Order Summary </h3> 
                                        <div class="row order-item-row" v-for="delivery_item in delivery.delivery_items" v-if="delivery_item.deal_item" style="text-align:left;">
                                            <div class="col-md-8 col-xs-8 item-title"> @{{
                                                delivery_item.deal_item.name }} (@{{
                                                delivery_item.quantity }})
                                                <span>
                                                    <span v-for="d_item in delivery_item.selected_deal_menu_items">
                                                        <br><sub>@{{ d_item.menu_item.name }}</sub>
                                                    </span> 
                                                </span>
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery_item.deal_item.price) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-row" v-for="delivery_item in delivery.delivery_items" v-if="delivery_item.offer_item" style="text-align:left;">
                                            <div class="col-md-8 col-xs-8 item-title"> @{{
                                                delivery_item.offer_item.name }} (@{{
                                                delivery_item.quantity }})
                                                <span> 
                                                    <span v-for="o_item in delivery_item.selected_offer_menu_items">
                                                        <br><sub style="font-size: 65%;">@{{ o_item.menu_item.name }}</sub>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery_item.offer_item.price) }}
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="row order-item-row" v-for="delivery_item in delivery.delivery_items" v-if="delivery_item.menu_item">
                                            <div class="col-md-8 col-xs-8 item-title"> 
                                                <span style="font-size: 12px;">@{{ delivery_item.menu_item.name }} (@{{ delivery_item.quantity }})</span>
                                                <span v-if="delivery_item.variant"><br><sub>(@{{ delivery_item.variant.variant.name }})</sub></span>
                                                <span v-if="delivery_item.crust"><br><sub>@{{ delivery_item.crust.crust.name }}</sub></span>
                                                <span v-if="delivery_item.addon_menu_items">
                                                    <span
                                                        v-for="addon in delivery_item.addon_menu_items">
                                                        <br><sub>@{{ addon.addon.name }}</sub>
                                                    </span>
                                                </span>
                                                <span v-if="delivery.menu_item_id&&delivery.menu_item_id==delivery_item.menu_item_id"><br><sub>@{{ delivery.menu_item.promo_code }}</sub></span>
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;">
                                                <div class="row" v-if="!delivery_item.variant">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery_item.menu_item.price) }}
                                                    </div>
                                                </div>
                                                <div class="row" v-if="delivery_item.variant">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery_item.variant.price) }}
                                                    </div>
                                                </div>
                                                <!--<span v-if="!delivery_item.variant">{{ setting('currency') }} &nbsp;&nbsp; @{{ currency(delivery_item.menu_item.price) }} </span>-->
                                                <!--<span v-if="delivery_item.variant">{{ setting('currency') }} &nbsp;&nbsp; @{{ currency(delivery_item.variant.price) }} </span>-->
                                                <span v-if="delivery_item.crust">
                                                    <br><sub>{{ setting('currency') }} &nbsp;&nbsp;&nbsp; @{{ currency(delivery_item.crust.price) }}</sub>
                                                </span>
                                                <span v-if="delivery_item.addon_menu_items">
                                                    <span
                                                        v-for="addon in delivery_item.addon_menu_items">
                                                        <br><sub>{{ setting('currency') }} &nbsp;&nbsp; @{{ currency(addon.price) }}</sub>
                                                    </span>
                                                </span>
                                                <span v-if="delivery.menu_item_id&&delivery.menu_item_id==delivery_item.menu_item_id"><br><sub>@{{ currency(delivery.reduction) }}</sub></span>
                                            </div>
                                        </div> 
                                        <hr>
                                        <div class="row order-item-subtotal-row">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Subtotal</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(total) }}
                                                    </div>
                                                    <!--<span style="text-align:left !important;">{{ setting('currency') }}</span>&nbsp;&nbsp; <span style="text-align:right !important;">@{{ currency(total) }}</span>-->
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row" v-if="delivery.vat"> 
                                            <div class="col-md-8 col-xs-8 item-sub-total"> V.A.T</div>
                                            <div class="col-md-4 col-xs-4 item-price" style="">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.vat) }}
                                                    </div>
                                                    <!--<span style="text-align:left !important;">{{ setting('currency') }}</span>&nbsp;&nbsp; <span style="text-align:right !important;">@{{ currency(delivery.vat) }}</span>-->
                                                </div> 
                                            </div> 
                                        </div> 
                                        <div class="row order-item-subtotal-row">
                                            <div class="col-md-8 col-xs-8 item-sub-total">{{__("Delivery")}}</div>
                                            <div class="col-md-4 col-xs-4 item-price" style="">  
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.delivery_charge) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row"
                                             v-if="delivery.promotion_id||delivery.site_promotion_id">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Promocode</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        - {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.reduction) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row" v-if="delivery.restaurant_discount">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Discount</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        - {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.restaurant_discount) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row" v-if="delivery.site_discount">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Discount</div>
                                            <div class="col-md-4 col-xs-4 item-price" style="">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        - {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.restaurant_discount) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-total-row">
                                            <div class="col-md-8 col-xs-8 item-total">Total</div>
                                            <div class="col-md-4 col-xs-4 item-price item-total" style="">  
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(delivery.total) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row order-item-subtotal-row" v-if="!delivery.vat">
                                            <div class="col-md-12 col-xs-12 item-price" style="text-align:left !important;">(VAT already inclusive)</div>
                                        </div>
                                        <hr>
                                        <div class="row order-status-row">
                                            <div class="col-md-5 col-xs-5  order-status">Order Status</div>
                                            <div class="col-md-7 col-xs-7 order-status order-status-time" style="text-align:right;"> 
                                            <span v-if="delivery.restaurant_status=='declined'" class="text-danger">Declined</span>
                                            <span v-if="delivery.restaurant_status=='pending'"
                                                      class="text-warning">Pending</span>
                                                <span v-if="delivery.restaurant_status=='accepted'"
                                                      class="text-success">Accepted</span>
                                            </div>
                                        </div>  
                                        <div v-if="delivery.restaurant_status != 'declined'">
                                            <div class="row progress">
                                                <div class="progress-bar" role="progressbar"
                                                     aria-valuemin="0" aria-valuemax="100"
                                                     :style="'width:'+delivery.progress+'%'">
                                                    
                                                </div>
                                            </div>
                                            <p class="progress-label col-md-4 col-xs-4">Initiated</p>
                                            <p class="progress-label col-md-4 col-xs-4 text-center">Dispatched</p>
                                            <p class="progress-label col-md-4 col-xs-4 text-right">Delivered</p>
                                        </div>
                                        <!-- <div class="row allergy-row">
                                            <p class="allergy-info-title"> Instructions For Rider</p>
                                            <textarea>@{{ delivery.instructions }}</textarea>
                                        </div>
                                        <div class="row allergy-row">
                                            <p class="allergy-info-title"> Special Requets</p>
                                            <p class="allergy-information">@{{ delivery.requests}}</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div>
            <br /><br /><br /> 
        </div>
    </div>  
</div>  
<script type="text/javascript">
        const data1 = {
            deliveries:{!! json_encode($deliveries) !!},
            delivery: [],
            total: 0,
            ticket: {user: {}},
            user_message: '',
            vat:{!! json_encode(getVat(),true) !!},
            vat_value: 0,
            characterCount: 255,
        };

        const deliveries = new Vue({
            data: data1,
            el: '#deliveries',
            mounted: function () {
                @if(request()->delivery_id)
                    this.delivery = this.deliveries.filter(function (delivery) {
                    return delivery.id == '{{request()->delivery_id}}'
                })[0];
                // this.getTotals();
                @endif
                // jQuery('#total-val').html('0.00'); 
            },
            methods: {
                details: function (delivery) {
                    this.delivery = delivery;
                    this.getTotalVals();
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });

                    return formatter.format(price) 
                },
                getTotalVals: function () { 
                    let total = 0; 
                    var vat = 0; 
                    var $this = this; 

                    this.delivery.delivery_items.forEach(function (delivery_item) {

                        if (delivery_item.menu_item) {
                            var price = delivery_item.menu_item.price;
                            if (delivery_item.variant) {
                                price = delivery_item.variant.price;
                            }

                            if (delivery_item.crust) {
                                price = price + delivery_item.crust.price;
                            }

                            if (delivery_item.addon_menu_items) {
                                delivery_item.addon_menu_items.forEach(function (addon) {
                                    // total += parseFloat(addon.price);
                                    price = price + addon.price;
                                });
                            }

                            total += delivery_item.quantity * price;

                            var vat_percentage = 0;

                            switch (delivery_item.menu_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            }

                            vat += price * vat_percentage * delivery_item.quantity;
                        }
                        if (delivery_item.deal_item) {
                            var price = delivery_item.deal_item.price;
                            total += delivery_item.quantity * price;

                            var vat_percentage = 0;

                            switch (delivery_item.deal_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            } 
                            vat += price * vat_percentage * delivery_item.quantity;
                        } 
                        if (delivery_item.offer_item) {
                            var price = delivery_item.offer_item.price; 
                            total += delivery_item.quantity * price;

                            var vat_percentage = 0;

                            switch (delivery_item.offer_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            } 
                            vat += price * vat_percentage * delivery_item.quantity;
                        } 
                    });

                    this.vat_value = vat;
                    console.log(total);
                    this.total = total; 
                    // var sum_val = this.currency(this.total+this.vat_value); 
                 //   jQuery('#total-val').html(sum_val); 
                },
                openTicket: function (ticket, delivery) {
                    if (ticket) {
                        this.ticket = ticket;
                    } else {
                        this.ticket = {user: {}};
                    }
                    this.delivery = delivery;
                    jQuery('#review-response-modal').modal('show');
                },
                saveMessage: function () {

                    if (this.ticket.resolved) {
                        $.alert({title: 'Error!', content: 'This ticket is already resolved!', theme: 'error'});
                    }

                    let $this = this;
                    axios.post('{{route('ticket.user.message')}}', {
                        _token: '{{csrf_token()}}',
                        message: $this.user_message,
                        ticket_id: $this.ticket.id,
                        resolved: $this.ticket.resolved,
                        delivery_id: $this.delivery.id
                    })
                        .then(function (response) {
                            if (response.data.message == 'success') {
                                if (response.data.data.ticket_message) {
                                    $this.delivery.ticket = response.data.data.ticket;
                                    $this.ticket = $this.delivery.ticket;
                                    // $this.ticket.messages.push(response.data.data.ticket_message);
                                }
                                $this.restaurant_message = '';
                                jQuery('#review-response-modal').modal('hide');
                                $.alert({title: 'Success!', content: 'Updated Successfully!', theme: 'success'});
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                countCharacters: function () {
                    this.characterCount = 255 - this.user_message.length;
                }
            }
        }) 

    function openCity(evt, cityName) { 
        window.location.href = cityName;
    }

</script>

@endsection
