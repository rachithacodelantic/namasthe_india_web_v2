@extends('layouts.app')
@section('title', 'user/profile')
@section('content') 
<link href="{{asset('css/my_account.css')}}" rel="stylesheet"> 
<!-- Content goes here -->
<div class="content-wrap text-center">
    <div class="row">
        <div class="col-md-4 left-content">
            <div class="row">
                <h6>MY ACCOUNT</h6>
            </div>
            <div class="row">
                @include('includes.user-header', ['active'=>'details']) 
            </div>
        </div>
        <div class="col-md-8 right-content">
           <div class="container row right-content-top">
              <div class="col-xs-3 col-sm-3 col-md-4">
                 <h6>My Details</h6>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-4 edit-content" id="edit">  
              </div>
           </div>
           <div class="container row right-content-mid">
              <div id="myDetails" class="tabcontent">
                 <div class="row">
                    <hr>
                    <form class="rec-details " method="post" action="{{route('user.profile.post')}}">
                        @csrf 
                        <div class="form-row">
                            <div class="form-group @if($errors->has('first_name')) has-error @endif col-md-6">
                                <label for="inputFirstName">First Name</label>
                                <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}" id="first_name" placeholder="Enter First Name">
                                
                            </div>
                            <div class="form-group @if($errors->has('last_name')) has-error @endif col-md-6">
                                <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}" id="last_name" placeholder="Enter Last Name">
                                
                            </div>
                            <div class="form-group @if($errors->has('email')) has-error @endif col-md-6">
                                <label for="email">Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" value="{{$user->email}}" id="email" placeholder="Enter Email address" disabled="true">
                                
                            </div> 
                        </div>
                        <div class="form-row">
                            <div class="form-group @if($errors->has('phone')) has-error @endif col-md-6">
                                <label for="phone">Contact Number <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="phone" value="{{$user->phone}}" id="phone" placeholder="Enter Contact Number">
                                
                            </div>
                        </div>
                        <br />
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="submit" value="Save Changes" class="btn btn-save-changes" style="font-family: 'Montserrat';">
                            </div>
                        </div>
                    </form>
                </div> 
              </div> 
           </div>
        </div>
    </div> 
</div>  
<script type="text/javascript"> 
    
    function openCity(evt, cityName) { 
        window.location.href = cityName;
    }

</script>

@endsection
