@extends('layouts.app')
@section('title', 'user/reservations')
@section('content') 
<link href="{{asset('css/my_account.css')}}" rel="stylesheet"> 
<!-- Content goes here -->
<div class="content-wrap text-center" id="">
    <div class="row">
        <div class="col-md-4 left-content">
            <div class="row">
                <h6>MY ACCOUNT</h6>
            </div>
            <div class="row">
                @include('includes.user-header', ['active'=>'details']) 
            </div>
        </div>
        <div class="col-md-8 right-content">
            <div class="container row right-content-top">
              <div class="col-xs-3 col-sm-3 col-md-4">
                 <h6>Reservations</h6>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-4 edit-content" id="edit">  
              </div>
            </div>
            <div class="container row right-content-mid">
                <div id="myDetails" class="tabcontent">
                    <div class="row">
                        <hr>
                        <div class="row reservations-section"> 
                            <div class="col-md-12">
                                @foreach($reservations as $reservation) 
                                    <div>
                                        <div class="row reservations-row">
                                            <div class="col-md-6" style="font-family: 'Montserrat';font-weight: bold;">
                                                <h3>{{$reservation->id}}</h3>
                                                <p>No. of People: {{$reservation->head_count}} </p>
                                                <p>Booking #: {{ $reservation->id }}</p>
                                                <p>{{ $reservation->requests }}</p>
                                            </div>
                                            <div class="col-md-6" style="font-family: 'Montserrat';font-weight: bold;">
                                                <div class="contact-icons">
                                                    <a> <img src="{{asset('img/Map.png')}}"></a>
                                                    <a> <img style="width: 25px;" src="{{asset('img/Phone.png')}}"></a>
                                                    <!-- <a href="'mailto:'+{{$restaurant->email}}"> <img
                                                            src="{{asset('img/Mail.png')}}"></a> -->
                                                </div>
                                                <div class="reservation-time">
                                                    <a href="#" @click.prevent="details(reservation)">
                                                        <p>{{ $reservation->date }} {{$reservation->time}} <!-- <img
                                                                src="{{asset('img/back-arrow.png')}}"> -->
                                                        </p>
                                                    </a>
                                                </div>
                                                <div class="reservation-time">
                                                    <p>Order Status: {{ $reservation->restaurant_status }}</p>
                                                </div>
                                            </div>
                                        </div> 
                                        <hr class="yellow-hr">
                                    </div> 
                                @endforeach
                                {{ $reservations->links() }}
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div>
        </div>  
    </div> 
</div> 
<script type="text/javascript">
    // const data1 = {
    //     reservations:{!! json_encode($reservations) !!},
    //     reservation: {restaurant: {}}
    // };

    // const reservation = new Vue({
    //     data: data1,
    //     el: '#reservation',
    //     mounted: function () {
    //         @if(request()->reservation_id)
    //             this.reservation = this.reservations.filter(function (reservation) {
    //             return reservation.id == '{{request()->reservation_id}}'
    //         })[0];
    //             this.getTotals();
    //         @endif

    //     },
    //     methods: {
    //         details: function (reservation) {
    //             this.reservation = reservation;
    //         },
    //         getTotals: function () {
    //                 let total = 0;

    //                 var vat = 0;

    //                 var $this = this;


    //                 this.delivery.delivery_items.forEach(function (delivery_item) {

    //                     if (delivery_item.menu_item) {
    //                         var price = delivery_item.menu_item.price;
    //                         if (delivery_item.variant) {
    //                             price = delivery_item.variant.price;
    //                         }

    //                         if (delivery_item.addon_menu_items) {
    //                             delivery_item.addon_menu_items.forEach(function (addon) {
    //                                 total += parseFloat(addon.price);
    //                             });
    //                         }

    //                         total += delivery_item.quantity * price;

    //                         var vat_percentage = 0;

    //                         switch (delivery_item.menu_item.vat_category) {
    //                             case 'food':
    //                                 vat_percentage = $this.vat.food;
    //                                 break;
    //                             case 'alcohol':
    //                                 vat_percentage = $this.vat.alcohol;
    //                                 break;
    //                         }

    //                         vat += price * vat_percentage * delivery_item.quantity;
    //                     }
    //                     if (delivery_item.deal_item) {
    //                         var price = delivery_item.deal_item.price;
    //                         total += delivery_item.quantity * price;

    //                         var vat_percentage = 0;

    //                         switch (delivery_item.deal_item.vat_category) {
    //                             case 'food':
    //                                 vat_percentage = $this.vat.food;
    //                                 break;
    //                             case 'alcohol':
    //                                 vat_percentage = $this.vat.alcohol;
    //                                 break;
    //                         }

    //                         vat += price * vat_percentage * delivery_item.quantity;
    //                     }


    //                 });

    //                 this.vat_value = vat;
    //                 this.total = total;
    //             },
    //     }
    // });

    function openCity(evt, cityName) { 
        window.location.href = cityName;
    }

</script>

@endsection
