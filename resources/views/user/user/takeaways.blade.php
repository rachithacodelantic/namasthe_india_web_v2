@extends('layouts.app')
@section('title', 'user/takeaways')
@section('content') 
<link href="{{asset('css/my_account.css')}}" rel="stylesheet"> 
<!-- Content goes here -->
<div class="content-wrap text-center" id="takeaways">
    <div class="row">
        <div class="col-md-4 left-content">
            <div class="row">
                <h6>MY ACCOUNT</h6>
            </div>
            <div class="row">
                @include('includes.user-header', ['active'=>'details']) 
            </div>
        </div>
        <div class="col-md-8 right-content">
           <div class="container row right-content-top">
              <div class="col-xs-3 col-sm-3 col-md-4">
                 <h6>Takeaways</h6>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-4 edit-content" id="edit">  
              </div>
           </div>
            <div class="container row right-content-mid">
                <div id="myDetails" class="tabcontent">
                    <div class="row">
                        <hr>
                        <div class="row reservations-section">
                            <div class="col-md-7">
                                <div v-for="takeaway in takeaways">
                                    <div class="row reservations-row">
                                        <div class="col-md-8" style="font-family: 'Montserrat';font-weight: bold;text-align: left;padding-left: 5%;"> 
                                            <!--<h3>@{{ takeaway.restaurant.name }}</h3>-->
                                            <p>No. of Items: @{{ takeaway.takeaway_items.length }} </p>
                                            <p>Booking #: @{{ takeaway.id }}</p>
                                        </div>
                                        <div class="col-md-4" style="font-family: 'Montserrat';font-weight: bold;">
                                            <div class="contact-icons">
                                                <a href="#" @click.prevent="openTicket(takeaway.ticket,takeaway)">
                                                    <img src="{{asset('img/Feedback.png')}}">
                                                </a>
                                                <a :href="'tel:'+takeaway.restaurant.phone"> <img
                                                        src="{{asset('img/Phone.png')}}"></a>
                                                <!-- <a :href="'mailto:'+takeaway.restaurant.email"> <img src="{{asset('img/Mail.png')}}"></a> -->
                                            </div>
                                            <div class="reservation-time">
                                                <a href="#" @click.prevent="details(takeaway)">
                                                    <p>@{{ takeaway.time }} <img
                                                            src="{{asset('img/back-arrow.png')}}">
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="yellow-hr">
                                </div>

                                {{ $paginate->links() }}
                            </div>
                            <div class="col-md-5" style="padding-left: 10%;">
                                <div class="order-summary-box my-account-summary-box" v-if="takeaway.id"> 
                                    <div class="row" style="font-family: 'Montserrat';font-weight: 600;text-align:left;">
                                        <h3 style="text-align:center;">Order Summary </h3> 
                                        <div class="row order-item-row" v-for="takeaway_item in takeaway.takeaway_items" v-if="takeaway_item.deal_item">
                                            <div class="col-md-8 col-xs-8 item-title"> @{{
                                                takeaway_item.deal_item.name }} (@{{
                                                takeaway_item.quantity }})
                                                <span>
                                                    <span v-for="d_item in takeaway_item.selected_deal_menu_items">
                                                        <br><sub>@{{ d_item.menu_item.name }}</sub>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;"> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway_item.deal_item.price) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row order-item-row" v-for="takeaway_item in takeaway.takeaway_items" v-if="takeaway_item.offer_item">
                                            <div class="col-md-8 col-xs-8 item-title"> @{{
                                                takeaway_item.offer_item.name }} 
                                                (@{{ takeaway_item.quantity }})
                                                 <span>
                                                    <span v-for="o_item in takeaway_item.selected_offer_menu_items">
                                                        <br><sub>@{{ o_item.name }}</sub>
                                                    </span>
                                                </span> 
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;"> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway_item.offer_item.price) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row order-item-row" v-for="takeaway_item in takeaway.takeaway_items" v-if="takeaway_item.menu_item">
                                            <div class="col-md-8 col-xs-8 item-title">  
                                                <span style="font-size: 12px;">@{{ takeaway_item.menu_item.name }} (@{{ takeaway_item.quantity }})</span>
                                                <span v-if="takeaway_item.variant"><br><sub>(@{{ takeaway_item.variant.variant.name }})</sub></span>
                                                <span v-if="takeaway_item.crust"><br><sub>@{{ takeaway_item.crust.name }}</sub></span>
                                                <!--<span v-if="takeaway_item.takeaway_item_addons">-->
                                                <!--    <span-->
                                                <!--        v-for="addon in takeaway_item.takeaway_item_addons">-->
                                                <!--        <br><sub>@{{ addon.addon.name }}</sub>-->
                                                <!--    </span>-->
                                                <!--</span>-->
                                                <span v-if="takeaway_item.addon_menu_items">
                                                    <span
                                                        v-for="addon in takeaway_item.addon_menu_items">
                                                        <br><sub>@{{ addon.addon.name }}</sub>
                                                    </span>
                                                </span>
                                                <span v-if="takeaway.menu_item_id&&takeaway.menu_item_id==takeaway_item.menu_item_id"><br><sub>@{{ takeaway.menu_item.promo_code }}</sub></span>
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="text-align:right !important;">
                                                <div class="row" v-if="!takeaway_item.variant">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway_item.menu_item.price) }}
                                                    </div>
                                                </div>
                                                <div class="row" v-if="takeaway_item.variant">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway_item.variant.price) }}
                                                    </div>
                                                </div>
                                                <!--{{ setting('currency') }}&nbsp;&nbsp;@{{ currency(takeaway_item.menu_item.price) }}-->
                                                <!--<span v-if="takeaway_item.variant"><br><sub>{{ setting('currency') }}&nbsp;&nbsp; @{{ currency(takeaway_item.variant.price) }}</sub></span>-->
                                                <!--<span v-if="takeaway_item.crust"><br><sub>{{ setting('currency') }} &nbsp;&nbsp; @{{ currency(takeaway_item.crust.price) }}</sub></span>-->
                                                <!--<span v-if="takeaway_item.takeaway_item_addons">-->
                                                <!--    <span-->
                                                <!--        v-for="addon in takeaway_item.takeaway_item_addons">-->
                                                <!--        <br><sub>{{ setting('currency') }} @{{ currency(addon.addon.price) }}</sub>-->
                                                <!--    </span>-->
                                                <!--</span>-->
                                                <span v-if="takeaway_item.crust">
                                                    <br><sub>{{ setting('currency') }} &nbsp;&nbsp;&nbsp; @{{ currency(takeaway_item.crust.price) }}</sub>
                                                </span>
                                                <span v-if="takeaway_item.addon_menu_items">
                                                    <span
                                                        v-for="addon in takeaway_item.addon_menu_items">
                                                        <br><sub>{{ setting('currency') }} &nbsp;&nbsp; @{{ currency(addon.price) }}</sub>
                                                    </span>
                                                </span>
                                                <span v-if="takeaway.menu_item_id&&takeaway.menu_item_id==takeaway_item.menu_item_id"><br><sub>@{{ currency(takeaway.reduction) }}</sub></span>
                                            </div>
                                        </div> 
                                        <hr>
                                        <div class="row order-item-subtotal-row">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Subtotal</div>
                                            <div class="col-md-4 col-xs-4 item-price" style="">  
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(total) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row" v-if="takeaway.vat">
                                            <div class="col-md-8 col-xs-8 item-sub-total"> V.A.T</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway.vat) }}
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row"
                                             v-if="takeaway.promotion_id||takeaway.site_promotion_id">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Promocode</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway.reduction) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row" v-if="takeaway.restaurant_discount">
                                            <div class="col-md-8 col-xs-8 item-sub-total" style="text-align:left !important;">Discount</div>
                                            <div class="col-md-4 col-xs-4 item-price" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway.restaurant_discount) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-subtotal-row"
                                             v-if="takeaway.site_discount">
                                            <div class="col-md-8 col-xs-8 item-sub-total">Discount
                                            </div>
                                            <div class="col-md-4 col-xs-4 item-price" style="">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway.site_discount) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row order-item-total-row">
                                            <div class="col-md-8 col-xs-8 item-total">Total</div>
                                            <div class="col-md-4 col-xs-4 item-price item-total" style=""> 
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6" style="text-align:left !important;">
                                                        {{ setting('currency') }}&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-6 col-xs-6" style="text-align:right !important;padding:0;">
                                                        @{{ currency(takeaway.total) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row order-item-subtotal-row" v-if="!takeaway.vat">
                                            <div class="col-md-12 col-xs-12 item-price">(VAT already inclusive)</div>
                                        </div>
                                        <hr>
                                        <div class="row order-status-row">
                                            <div class="col-md-5 col-xs-5  order-status">Order Status</div>
                                            <div class="col-md-7 col-xs-7 order-status order-status-time"
                                                 style="text-align:right;">

                                            <span v-if="takeaway.restaurant_status=='declined'" class="text-danger">Declined</span>
                                                <span v-if="takeaway.restaurant_status=='pending'"
                                                      class="text-warning">Pending</span>
                                                <span v-if="takeaway.restaurant_status=='accepted'"
                                                      class="text-success">Accepted</span>
                                            </div>
                                        </div> 
                                        <div v-if="takeaway.restaurant_status!='declined'">
                                            <div class="row progress">
                                                <div class="progress-bar" role="progressbar"
                                                     aria-valuemin="0" aria-valuemax="100"
                                                     :style="'width:'+takeaway.progress+'%'">
                                                    
                                                </div>
                                            </div>
                                            <p class="progress-label col-md-4 col-xs-4">Initiated</p>
                                            <p class="progress-label col-md-4 col-xs-4 text-center">Dispatched</p>
                                            <p class="progress-label col-md-4 col-xs-4 text-right">Collected</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div>
           <br /><br /><br /> 
        </div>
    </div> 
    <!-- Modal --> 
</div> 
 
<script type="text/javascript">
        const data1 = {
            takeaways:{!! json_encode($takeaways) !!},
            takeaway: [],
            total: 0,
            ticket: {user: {}},
            user_message: '',
            vat:{!! json_encode(getVat(),true) !!},
            vat_value: 0,
        };

        const takeaways = new Vue({
            data: data1,
            el: '#takeaways',
            mounted: function () {
                @if(request()->takeaway_id)
                    this.takeaway = this.takeaways.filter(function (takeaway) {
                    return takeaway.id == '{{request()->takeaway_id}}'
                })[0];
                // this.getTotals();
                @endif
                // jQuery('#total-val').html('0.00'); 
            },
            methods: {
                details: function (takeaway) {
                    this.takeaway = takeaway;
                    this.getTotalVals();
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    }); 
                    return formatter.format(price) 
                },
                getTotalVals: function () {
                    let total = 0; 
                    var vat = 0; 
                    var $this = this;

                    this.takeaway.takeaway_items.forEach(function (takeaway_item) {

                        if (takeaway_item.menu_item) {
                            var price = takeaway_item.menu_item.price;
                            if (takeaway_item.variant) {
                                price = takeaway_item.variant.price;
                            }
                            
                            if (takeaway_item.crust) {
                                price = price + takeaway_item.crust.price;
                            } 

                            if (takeaway_item.addon_menu_items) {
                                takeaway_item.addon_menu_items.forEach(function (addon) {
                                    // total += parseFloat(addon.addon.price);
                                    price = price + addon.price;
                                });
                            }

                            total += takeaway_item.quantity * price; 
                            var vat_percentage = 0;

                            switch (takeaway_item.menu_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            }

                            vat += price * vat_percentage * takeaway_item.quantity;
                        }
                        if (takeaway_item.deal_item) {
                            var price = takeaway_item.deal_item.price;
                            total += takeaway_item.quantity * price;

                            var vat_percentage = 0;

                            switch (takeaway_item.deal_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            } 
                            vat += price * vat_percentage * takeaway_item.quantity;
                        }
                        if (takeaway_item.offer_item) {
                            var price = takeaway_item.offer_item.price;
                            total += takeaway_item.quantity * price;

                            var vat_percentage = 0;

                            switch (takeaway_item.offer_item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            } 
                            vat += price * vat_percentage * takeaway_item.quantity;
                        }
                    });
                    this.vat_value = vat;
                    this.total = total;
                },
                openTicket: function (ticket, takeaway) {
                    if (ticket) {
                        this.ticket = ticket;
                    } else {
                        this.ticket = {user: {}};
                    }
                    this.takeaway = takeaway;
                    jQuery('#review-response-modal').modal('show');
                },
                saveMessage: function () {
                    let $this = this;
                    axios.post('{{route('ticket.user.message')}}', {
                        _token: '{{csrf_token()}}',
                        message: $this.user_message,
                        ticket_id: $this.ticket.id,
                        resolved: $this.ticket.resolved,
                        takeaway_id: $this.takeaway.id
                    })
                        .then(function (response) {
                            if (response.data.message == 'success') {
                                if (response.data.data.ticket_message) {
                                    $this.takeaway.ticket = response.data.data.ticket;
                                    $this.ticket = $this.takeaway.ticket;
                                    // $this.ticket.messages.push(response.data.data.ticket_message);
                                }
                                $this.restaurant_message = '';
                                jQuery('#review-response-modal').modal('hide');
                                $.alert({title: 'Success!', content: 'Updated Successfully!', theme: 'success'});
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            }
        })

    function openCity(evt, cityName) { 
        window.location.href = cityName;
    }

</script>

@endsection
