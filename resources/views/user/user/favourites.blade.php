@extends('layouts.app')
@section('title', 'user/favourites')
@section('content') 
<link href="{{asset('css/my_account.css')}}" rel="stylesheet"> 
<link href="{{asset('css/menu.css')}}" rel="stylesheet">
<link href="{{asset('css/make_your_order_now.css')}}" rel="stylesheet">
<link href="{{asset('css/pop_up.css')}}" rel="stylesheet">
<script src="https://unpkg.com/vue-star-rating/dist/star-rating.min.js"></script>

<link href="css/pop_up.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://themes.audemedia.com/html/goodgrowth/css/owl.theme.default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js" type="text/javascript"></script>

<style> 
  .active{
    color: #fce703;
  }
</style> 
<div class="content-wrap text-center" id="address">
    <div class="row">
        <div class="col-md-4 left-content">
            <div class="row">
                <h6>MY ACCOUNT</h6>
            </div>
            <div class="row">
                @include('includes.user-header', ['active'=>'details']) 
            </div>
        </div>
        <div class="col-md-8 right-content">
           <div class="container row right-content-top">
              <div class="col-xs-3 col-sm-3 col-md-4">
                 <h6>My Favourites</h6>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-4 edit-content" id="edit">  
              </div>
           </div>
            <div class="container row right-content-mid">
              <div id="myDetails" class="tabcontent">
                  <div class="row">
                    <hr>
                    @foreach($user->menuItems as $menu_item) 
                      <div class="col-md-3">
                        <a href="{{route('user.restaurant.menu')}}">
                          <div class="favourites-card">
                            @if($menu_item->logo)
                                <img style="width: 100px;height: 100px;" src="{{ asset('storage/'.$menu_item->logo) }}">
                            @else
                                <img src="{{asset('img/default.jpg')}}" alt="">
                            @endif
                            <h5 style="font-family: 'Montserrat';color: #bc040b;">{{$menu_item->name}}</h5> 
                            <p style="font-family: 'Montserrat';color: #666;font-size: 12px;">{{ substr($menu_item->description, 0, 50) }}</p>
                            <div class="rating">
                              <div class="star-rating">
                                <section class='rating-widget'>  
                                  <div class='rating-stars text-center'> 
                                    <ul id='stars' style="height: 20px;">   
                                      <star-rating v-bind:rating="menu_item.rating" style="font-size: 14px;"></star-rating>
                                    </ul> 
                                  </div>
                                </section>            
                              </div> 
                            </div>
                          </div>
                        </a>
                      </div>
                    @endforeach
                </div> 
            </div> 
          </div>
          <br /><br /><br /> 
        </div>
    </div>  
</div> 

<script type="text/javascript">
        document.querySelectorAll('.favourites-card img').forEach(function (favourite_image) {
            favourite_image.style.height = favourite_image.clientWidth + 'px';
        }); 

        window.addEventListener('load', function () {
            var max_height = 0;
            document.querySelectorAll('.favourites-card').forEach(function (favourite) {
                if (favourite.clientHeight > max_height) {
                    max_height = favourite.clientHeight;
                }
            });
            document.querySelectorAll('.favourites-card').forEach(function (favourite) {
                favourite.style.height = max_height + 'px';
            });
        }); 
    
    function openCity(evt, cityName) { 
        window.location.href = cityName;
    }

</script>

@endsection
