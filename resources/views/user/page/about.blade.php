@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/about_us.css') }}" rel="stylesheet">

    <style>
        .about_header {
            color: #bc040b !important;
        }
    </style>

    <div class="main-img-figure">
        <img src="{{ asset('storage/' . setting('about_main_banner')) }}" class="img-responsive" alt="Image">
    </div> 
    <section class="vision-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h1 style="text-align: center;
    font-size: 46px; line-height: 64px; color: #F2F2F2;
    font-family: "Nexa-Heavy";" align="center"><span style=""><font color="#000000">About Us</font></span></h1>
                </div> 
            </div><br /><br /><br /><br />
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <img src="{{ asset('storage/' . setting('about_side_banner')) }}" class="img-responsive" alt="Image">
                </div> 
                <div class="col-sm-12 col-md-6 col-lg-6 content-figure">
                    <h3>WHO <span>WE ARE</span></h3>
                    <p>{{ setting('about_page_info') }}</p>
                    @if (setting('our_vision'))
                        <h5>Our Vission</h5>
                        <p>{{ setting('vision_info') }}</p>
                    @endif
                    @if (setting('our_mission'))
                        <h5>Our Mission</h5>
                        <p>{{ setting('mission_info') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    @if (setting('our_management_culture'))
        <section class="info-sec">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <h4>OUR MANAGEMENT</h4>
                        <p>{{ setting('our_management') }}</p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mob-right">
                        <h4>OUR CULTURE AND VALUES</h4>
                        <p>{{ setting('our_culture') }}</p>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection
