@extends('layouts.app')
@section('title', 'contact-us')
@section('content') 
<link href="{{asset('css/contact_us.css')}}" rel="stylesheet">
<link href="{{ asset('css/about_us.css') }}" rel="stylesheet">
    <style>
        .contactus_header{
          color:#bc040b !important;
        }
    </style>

    <div class="main-img-figure">
        <img src="{{ asset('storage/'.setting('contact_main_banner')) }}" class="img-responsive" alt="Image">
    </div>

    <div class="container-fluid" id="right-content-contact-us-align">
        <div class="row">
            <div class="col-xs-6 col-md-4">
                <div class="row top-contents">
                    <h3>GET <span id="span-text-color">IN TOUCH</span></h3>
                </div> 
                <div class="row"> 
                    <div class="row left-content-form-text">
                      <div class="col-xs-4 col-md-12"> 
                        <h3> 
                            {{ $branches[0]->county }} {{ $branches[0]->city }}<br> 
                            {{ $branches[0]->country }}<br>
                            {{ $branches[0]->province }} {{ $branches[0]->postcode }}<br>
                            {{ $branches[0]->phone }} <br> 
                            {{ $branches[0]->email }} 
                        </h3>
                      </div>
                    </div>
                </div>  
                @if($branch != $branches[0]->id)
                    <div class="row"> 
                        <div class="row left-content-form-text">
                            <div class="col-xs-4 col-md-12"> 
                                <h3><b>Direct Contact</b><br>
                                {{ $current->phone }} <br> 
                                {{ $current->email }} 
                                </h3>
                            </div>
                        </div> 
                    </div>  
                @endif 
            </div>
            <div class="col-xs-6 col-8" id="right-content-contact-us">
                @if(setting('contact_page_info'))
                        <p>{{ setting('contact_page_info') }} </p>  
                @endif
                <br /> 
                <div class="row"> 
                    <h3><b>Please Fill in the Blanks </b><br></h3> 
                </div> 
                <form id="myForm" action="{{route('user.contact.post')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="@if($errors->has('first_name')) has-error @endif">
                                <input type="text" name="first_name" id="first_name" placeholder="First Name">
                                @if($errors->has('first_name'))
                                    <span class="help-block">
                                        {{$errors->first('first_name')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="@if($errors->has('last_name')) has-error @endif">
                                <input type="text" name="last_name" id="last_name" placeholder="Last Name">
                                @if($errors->has('last_name'))
                                    <span class="help-block">
                                        {{$errors->first('last_name')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="@if($errors->has('email')) has-error @endif">
                                <input type="text" name="email" placeholder="E-mail">
                                @if($errors->has('email'))
                                    <span class="help-block">
                                        {{$errors->first('email')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="@if($errors->has('phone')) has-error @endif">
                                <input type="text" name="phone" placeholder="Telephone">
                                @if($errors->has('phone'))
                                    <span class="help-block">
                                        {{$errors->first('phone')}}
                                    </span>
                                @endif
                            </div>
                        </div> 
                        <div class="col-md-6" id="message">
                            <div class="@if($errors->has('message')) has-error @endif">
                                <input type="text" name="message" placeholder="Message">
                                @if($errors->has('message'))
                                    <span class="help-block">
                                        {{$errors->first('message')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <input id="add-to-cart-h-mid" type="submit" value="SEND MESSAGE"> 
                    </div>
                    <br /><br /><br />
                </form> 
            </div>
        </div>
    </div>
@endsection
