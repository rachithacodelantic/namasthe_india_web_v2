@extends('layouts.app')

@section('content')
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js">\x3C/script>')</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<!-- FlexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<style>
    .home_header{
        color:#028F38 !important;
    }
    .jq-toast-wrap.bottom-left {
        position: fixed;
        top: 14%;
        left: 76%;
    }
</style>
  <div id="restaurantIndex">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <ul class="carousel-indicators"> 
        @foreach($slider_images as $key => $slider)
          <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
        @endforeach
      </ul>  
      <div class="carousel-inner" role="listbox">
        @foreach($slider_images as $slider)
          <div class="item {{ $loop->first ? 'active' : '' }}">
            <img src="{{ $slider->path }}" alt="Image">
            <div class="carousel-caption main-caption" style="background: rgb(0 0 0 / 50%);padding:50px 150px 50px 50px;">
              <h2 id="summer-offer-text">{{ $slider->name }}</h2>
              <h1 id="off-online-text">{{ $slider->special_note }}</h1>
              <p id="small-text" style="">{{ $slider->description }}</p>
              @if($slider->price) 
                <h2 id="image-text-prize">{{ setting('currency') }}{{ $slider->price }}</h2>
                @if(setting('static_site') == 0)
                  @if($slider->type == 'menu_item') 
                    <a @click.prevent="addToCartSliderItem({{ $slider }})" id="add-to-cart" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>
                  @else
                    <a href="{{route('user.restaurant.menu')}}" id="add-to-cart" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>
                  @endif 
                @endif 
              @endif  
            </div>      
          </div> 
        @endforeach 
      </div>  
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>  

    <div class="content-wrap text-center top-selling-section">   
      {{-- <section class="content-top"> --}}
        <div class="container slider-sec">
          <div class="row">   
            @if($top_sale_product != null)
              <div class="col-md-7">
                <div class="row heading-row">
                  <div class="col-sm-12 col-md-12">
                    <h3 class="heading">THIS WEEK'S <span>TOP SELLING</span></h3> 
                  </div>
                </div>
                <img src="img\content-top-imgs\top-selling.png" class="img-responsive" style="width:100%;max-height: 300px;" alt="Image"> 
                <div class="carousel-caption" id="content-top-carousel-caption">
                  <h2 id="content-top-text1">{{ $top_sale_product->name }}</h2>
                  <h1 id="content-top-text2">{{ substr($top_sale_product->special_note, 0, 65) }}</h1> 
                  <h2 id="content-top-prize">{{ setting('currency') }}{{ $top_sale_product->price }}</h2>
                  @if(setting('static_site') == 0)
                    <a @click.prevent="addToCartTopSell({{ $top_sale_product }})" id="add-to-cart" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>
                  @endif
                </div>  
              </div>
            @endif
            @if($new_tast)
              <div class="col-md-5"> 
                <div class="row heading-row">
                  <div class="col-sm-12 col-md-12">
                    <h3 class="heading">NEW <span>TASTE</span></h3> 
                  </div>
                </div>
                 <img src="img\content-top-imgs\new-taste.png"  class="img-responsive prod-img" style="width:100%; height:auto;max-height: 300px;" alt="Image">   
                 <div class="carousel-caption" id="content-top-carousel-caption">
                  <h2 id="new-taste-text1">{{ $new_tast->name }}</h2>
                   <P id="new-taste-text2">{{ $new_tast->description }}</P>
                   <h2 id="content-top-prize">{{ setting('currency') }}{{ $new_tast->price }}</h2>
                  @if(setting('static_site') == 0)
                    <a @click.prevent="addToCartTopSell({{ $new_tast }})" id="add-to-cart" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>
                  @endif
                </div> 
              </div>    
            @endif  
          </div>  
          @if($weekly_offers->isNotEmpty())
            <!--<div class="row">-->
            <!--  <div class="col-md-6">-->
            <!--    <h3>WEEKLY <span id="span-text-color">OFFERS</span></h3>-->
            <!--  </div>-->
            <!--</div> --> 
          @endif  
        </div>
      {{-- </section>   --}}
      <style>  
      @font-face {
      font-family: 'flexslider-icon';
      src: url('fonts/flexslider-icon.eot');
      src: url('fonts/flexslider-icon.eot?#iefix') format('embedded-opentype'), url('fonts/flexslider-icon.woff') format('woff'), url('fonts/flexslider-icon.ttf') format('truetype'), url('fonts/flexslider-icon.svg#flexslider-icon') format('svg');
      font-weight: normal;
      font-style: normal;
      } 
      .flex-container a:hover,
      .flex-slider a:hover {
      outline: none;
      }
      .slides,
      .slides > li,
      .flex-control-nav,
      .flex-direction-nav {
      margin: 0;
      padding: 0;
      list-style: none;
      }
      .flex-pauseplay span {
      text-transform: capitalize;
      } 
      .flexslider {
      margin: 0;
      padding: 0;
      }
      .flexslider .slides > li {
      display: none;
      -webkit-backface-visibility: hidden;
      }
      .flexslider .slides img {
      width: 100%;
      display: block;
      }
      .flexslider .slides:after {
      content: "\0020";
      display: block;
      clear: both;
      visibility: hidden;
      line-height: 0;
      height: 0;
      }
      html[xmlns] .flexslider .slides {
      display: block;
      }
      * html .flexslider .slides {
      height: 1%;
      }
      .no-js .flexslider .slides > li:first-child {
      display: block;
      } 
      .flexslider {
      margin: 0 0 60px;
      background: #fff;
       
      position: relative;
      zoom: 1;
      -webkit-border-radius: 4px;
      -moz-border-radius: 4px;
      border-radius: 4px;
      -webkit-box-shadow: '' 0 1px 4px rgba(0, 0, 0, 0.2);
      -moz-box-shadow: '' 0 1px 4px rgba(0, 0, 0, 0.2);
      -o-box-shadow: '' 0 1px 4px rgba(0, 0, 0, 0.2);
      box-shadow: '' 0 1px 4px rgba(0, 0, 0, 0.2);
      }
      .flexslider .slides {
      zoom: 1;
      }
      .flexslider .slides img { 
      -moz-user-select: none;
      height: 241px;
          width: 241px;
          object-fit: cover;
      }
      .flex-viewport {
      max-height: 2000px;
      -webkit-transition: all 1s ease;
      -moz-transition: all 1s ease;
      -ms-transition: all 1s ease;
      -o-transition: all 1s ease;
      transition: all 1s ease;
      }
      .loading .flex-viewport {
      max-height: 300px;
      }
      .carousel li {
      margin-right: 5px;
      }
      .flex-direction-nav {
      *height: 0;
      }
      .flex-direction-nav a {
        text-decoration: none;
        display: block;
        width: 35px;
        height: 35px; 
        position: absolute;
        top: 50%;
        z-index: 10;
        overflow: hidden;
        opacity: 0;
        cursor: pointer; 
        text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.3);
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out; 
      }
      .flex-direction-nav a:before {
        font-family: cursive;
          font-size: 41px;
          line-height: 0.75;
          display: inline-block;
          content: '<';
          color: #bc040b; 
          padding-left: 6px;
          padding-right: 9px;
      }
      .flex-direction-nav a.flex-next:before {
        content:'>';
      } 
      .flex-direction-nav .flex-prev {
      left: -50px;
      }

      .alignment-slider .flex-prev {
        left: -50px!important; 
        margin-top: 0;
        margin-left: 0; 
        background-color: transparent;
        opacity: 1;
        height: 45px;
      }
      #myTabContent-home .flex-prev{
        opacity: 1;
        background-color: transparent;
      } 

      #myTabContent-home .flex-next{
        opacity: 1;
          background-color: transparent;
          margin-top:-1.7%;
      } 

      .alignment-slider .flex-next {
        left: 105%; 
        opacity: 1; 
        margin-top: 0%; 
        background-color: transparent; 
      } 

      .flex-direction-nav .flex-next {
      right: -50px;
      text-align: right; 
      height: 45px;
      }
      .flexslider:hover .flex-direction-nav .flex-prev {
      opacity: 0.7;
      left: 10px;
      }
      .flexslider:hover .flex-direction-nav .flex-prev:hover {
      opacity: 1;
      }
      .flexslider:hover .flex-direction-nav .flex-next {
      opacity: 0.7;
      right: 10px;
      }
      .flexslider:hover .flex-direction-nav .flex-next:hover {
      opacity: 1;
      }
      .flex-direction-nav .flex-disabled {
      opacity: 0!important;
      filter: alpha(opacity=0);
      cursor: default;
      z-index: -1;
      }
      .flex-pauseplay a {
      display: block;
      width: 20px;
      height: 20px;
      position: absolute;
      bottom: 5px;
      left: 10px;
      opacity: 0.8;
      z-index: 10;
      overflow: hidden;
      cursor: pointer;
      color: #000;
      }
      .flex-pauseplay a:before {
      font-family: "flexslider-icon";
      font-size: 20px;
      display: inline-block;
      content: '\f004';
      }
      .flex-pauseplay a:hover {
      opacity: 1;
      }
      .flex-pauseplay a.flex-play:before {
      content: '\f003';
      }
      .flex-control-nav {
      width: 100%;
      position: absolute;
      bottom: -40px;
      text-align: center;
      display: none;
      }
      .flex-control-nav li {
      margin: 0 6px;
      display: inline-block;
      zoom: 1;
      *display: inline;
      }
      .flex-control-paging li a {
      width: 11px;
      height: 11px;
      display: block;
      background: #666;
      background: rgba(0, 0, 0, 0.5);
      cursor: pointer;
      text-indent: -9999px;
      -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
      -moz-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
      -o-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
      box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border-radius: 20px;
      }
      .flex-control-paging li a:hover {
      background: #333;
      background: rgba(0, 0, 0, 0.7);
      }
      .flex-control-paging li a.flex-active {
      background: #000;
      background: rgba(0, 0, 0, 0.9);
      cursor: default;
      }
      .flex-control-thumbs {
      margin: 5px 0 0;
      position: static;
      overflow: hidden;
      }
      .flex-control-thumbs li {
      width: 25%;
      float: left;
      margin: 0;
      }
      .flex-control-thumbs img {
      width: 100%;
      height: auto;
      display: block;
      opacity: .7;
      cursor: pointer;
      -moz-user-select: none;
      -webkit-transition: all 1s ease;
      -moz-transition: all 1s ease;
      -ms-transition: all 1s ease;
      -o-transition: all 1s ease;
      transition: all 1s ease;
      }
      .flex-control-thumbs img:hover {
      opacity: 1;
      }
      .flex-control-thumbs .flex-active {
      opacity: 1;
      cursor: default;
      } 
      @media screen and (max-width: 860px) {
        .flex-direction-nav .flex-prev {
          opacity: 1;
          left: 10px;
          margin-top: 68%;
          margin-left: 83%!important;
      }
      .flex-direction-nav .flex-next {
          opacity: 1;
          right: 10px;
          margin-left: -9%;
          margin-top: 80%;
      } 
      #myTabContent-home .flex-prev {
          opacity: 1;
          background-color: transparent;
          margin-top: -5%;
          margin-left: 1%!important;
      }
      }  
      .modal-backdrop {
        position: inherit !important;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 9999 !important;
        background-color: #000;
      }
      </style> 
      <script>
        $(document).ready(function() {

          $('.flexslider').flexslider({
            animation: "slide",
            touch: "true"
          }); 
        });
      </script>  
  
  @if($weekly_offers->isNotEmpty())
    <!--<section class="slider row container alignment-slider slider-sec"> -->
    <!--  <div class="row heading-row" style="margin-top: 20px">-->
    <!--    <div class="col-sm-12 col-md-12">-->
    <!--      <h3 class="heading">WEEKLY <span>OFFERS</span></h3> -->
    <!--    </div>-->
    <!--  </div>-->
    <!--  <div class="flexslider">-->
    <!--    <ul class="slides"> -->
    <!--      <li>   -->
    <!--        <div class="row">-->
    <!--          <?php $i = 0; ?> -->
    <!--          @foreach($deals as $deal) -->
    <!--            <div class="col-md-6"> -->
    <!--              <img src="{{ url('/storage/'.$deal->image) }}"  class="img-responsive offer-img" style="width:100%" alt="Image">  -->
    <!--              <div class="carousel-caption box-opacity" id="content-top-carousel-caption">-->
    <!--                <h2 id="new-taste-text1"><span id="weekly-offer-text-color">{{ $deal->name }}</span> </h2>-->
    <!--                <P style="word-break: break-all;" id="new-taste-text2"> -->
    <!--                  {{ \Illuminate\Support\Str::limit($deal->description, 95, $end='...') }}-->
    <!--                </P>-->
    <!--                <h2 id="content-top-prize">{{ setting('currency') }}{{ $deal->price }}</h2>-->
    <!--                @if(setting('static_site') == 0) -->
    <!--                  <a href="{{route('user.restaurant.deals')}}" id="add-to-cart" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>-->
    <!--                @endif-->
    <!--              </div> -->
    <!--            </div>-->
                <?php $i++; 
                //   if ($i % 2 == 0 && $i != count($deals)) { 
                //       echo '</div></li><li><div class="row">';
                //   }
                ?>
              @endforeach  
    <!--        </div>-->
    <!--      </li>-->
    <!--    </ul>-->
    <!--  </div>-->
    <!--</section> -->
    @if(!empty($deals))
      <!--<div id="see-all-offers" class="container-fluid"> -->
      <!--  <div class="row">-->
      <!--    <div class="col-md-2" id="see-all-offers-text-align">-->
      <!--      <a href="{{route('user.restaurant.deals')}}"><h4 id="see-all-offers-text">SEE ALL OFFERS</h4></a>-->
      <!--    </div>-->
      <!--    <div class="col-md-8"></div> -->
      <!--  </div>  -->
      <!--</div>-->
    @endif
  @endif   
  @if(setting('show_menu') == 1)
  <br /><br /><br />
    <section class="content-mid" id="web_slider">
      <div class="container menu-bar slider-sec">
        <section id="menu-mid"> 
          <h3>MENU</h3>  
          <div class="row secondary-nav d-none d-sm-block">
            <div class="snav-links">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <?php $count = 0; ?>
                  @foreach($menus as $menu) 
                    <?php if($count == 6) break; ?>
                      <li class="nav-item {{ $loop->first ? 'active' : '' }}">
                        <a class="nav-link btn-txt {{ $loop->first ? '' : '' }}" id="home-tab" data-toggle="tab" href="#{{ $menu->name }}" role="tab" aria-controls="home" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $menu->name }}</a>
                      </li> 
                    <?php $count++; ?>
                  @endforeach 
                </ul>
            </div>
          </div> 
        </section>  
        <div class="tab-content" id="myTabContent-home"> 
          @foreach($menus as $menu) 
            <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="{{ $menu->name }}"  role="tabpanel">
              <section class="slider">
                <div class="flexslider">
                  <ul class="slides"> 
                    <li>
                      <div class="container slider-sec"> 
                        <?php 
                          $count1 = 0;
                          $m = 0; 
                        ?>
                        @foreach($menu->menu_items as $menu_item)
                          <?php if($count1 == 9) break; ?>
                            <div class="col-lg-3"> 
                              <div class="card-content"> 
                                <a class="spectrum-a"> 
                                  <img class="spectrum1" src="{{ url('/storage/'.$menu_item->logo) }}" style="height: 200px;" alt="meow">
                                  <h4 class="spectrum-h2">
                                    {{ \Illuminate\Support\Str::limit($menu_item->name, 15, $end='...') }} 
                                  </h4>
                                  <div class="rating">
                                    <div class="star-rating">
                                      <section class='rating-widget'> 
                                        <div class='rating-stars text-center'>
                                          <ul id='stars'>
                                            <li class='star' title='Poor' data-value='1'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Fair' data-value='2'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Good' data-value='3'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Excellent' data-value='4'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='WOW!!!' data-value='5'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                          </ul>
                                        </div>  
                                      </section>             
                                    </div> 
                                  </div> 
                                  <p class="food-details">{{ $menu_item->description }}</p>
                                  <div class="pricing">
                                      <span><p>{{ setting('currency') }}{{ $menu_item->price }}</p></span>      
                                  </div> 
                                  @if(setting('static_site') == 0)
                                    <div class="add-remove" style="width: 100%;">
                                      <div class="input-group">
                                        <div class="number">
                                          <span class="minus">-</span> 
                                          <input v-bind:id="{{ $menu_item->id }}+'-textbox'" class="number-box" type="text" value="1"/>
                                          <span class="plus">+</span>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                </a>   
                                @if(setting('static_site') == 0)
                                  <a @click.prevent="addToCart({{ $menu_item }})" id="add-to-cart-h-mid" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>   
                                @endif
                                <a href="{{route('user.restaurant.menu')}}"><span id="heart24"><i class="fa fa-heart-o" aria-hidden="true"></i></span></a>
                              </div> 
                            </div>
                          <?php $count1++; 
                            $m++; 
                            if ($m % 4 == 0 && $m != count($menu->menu_items)) { 
                                echo '</div></li><li><div class="container">';
                            }
                          ?>
                        @endforeach 
                      </div> 
                    </li>
                  </ul> 
                </div>
              </section>
            </div> 
          @endforeach  
        </div>  
      </div>
    </section>
    
    <section class="content-mid" id="mobile_slider">
      <div class="container menu-bar slider-sec">
        <section id="menu-mid"> 
          <h3>MENU</h3>  
          <div class="row secondary-nav d-none d-sm-block">
            <div class="snav-links">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <?php $count = 0; ?>
                  @foreach($menus as $menu) 
                    <?php if($count == 6) break; ?>
                      <li class="nav-item {{ $loop->first ? 'active' : '' }}">
                        <a class="nav-link btn-txt {{ $loop->first ? '' : '' }}" id="home-tab" data-toggle="tab" href="#{{ $menu->name }}" role="tab" aria-controls="home" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $menu->name }}</a>
                      </li> 
                    <?php $count++; ?>
                  @endforeach 
                </ul>
            </div>
          </div> 
        </section>  
        <div class="tab-content" id="myTabContent-home"> 
          @foreach($menus as $menu) 
            <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="{{ $menu->name }}"  role="tabpanel">
              <section class="slider">
                <div class="flexslider">
                  <ul class="slides"> 
                    <li>
                      <div class="container slider-sec"> 
                        <?php 
                          $count1 = 0;
                          $m = 0; 
                        ?>
                        @foreach($menu->menu_items as $menu_item)
                          <?php if($count1 == 9) break; ?>
                            <div class="col-lg-3"> 
                              <div class="card-content"> 
                                <a class="spectrum-a"> 
                                  <img class="spectrum1" src="{{ url('/storage/'.$menu_item->logo) }}" alt="meow">
                                  <h4 class="spectrum-h2">
                                    {{ \Illuminate\Support\Str::limit($menu_item->name, 15, $end='...') }} 
                                  </h4>
                                  <div class="rating">
                                    <div class="star-rating">
                                      <section class='rating-widget'> 
                                        <div class='rating-stars text-center'>
                                          <ul id='stars'>
                                            <li class='star' title='Poor' data-value='1'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Fair' data-value='2'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Good' data-value='3'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Excellent' data-value='4'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='WOW!!!' data-value='5'>
                                              <i class='fa fa-star fa-fw'></i>
                                            </li>
                                          </ul>
                                        </div>  
                                      </section>             
                                    </div> 
                                  </div> 
                                  <p class="food-details">{{ $menu_item->description }}</p>
                                  <div class="pricing">
                                      <span><p>{{ setting('currency') }}{{ $menu_item->price }}</p></span>      
                                  </div> 
                                  @if(setting('static_site') == 0)
                                    <div class="add-remove" style="width: 100%;">
                                      <div class="input-group">
                                        <div class="number">
                                          <span class="minus">-</span> 
                                          <input v-bind:id="{{ $menu_item->id }}+'-textbox'" class="number-box" type="text" value="1"/>
                                          <span class="plus">+</span>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                </a>   
                                @if(setting('static_site') == 0)
                                  <a @click.prevent="addToCart({{ $menu_item }})" id="add-to-cart-h-mid" class="btn btn-default btn-sm btn-custom-sm">ADD TO CART</a>   
                                @endif
                                <a href="{{route('user.restaurant.menu')}}"><span id="heart24"><i class="fa fa-heart-o" aria-hidden="true"></i></span></a>
                              </div> 
                            </div>
                          <?php $count1++; 
                            $m++; 
                            if ($m % 1 == 0 && $m != count($menu->menu_items)) { 
                                echo '</div></li><li><div class="container">';
                            }
                          ?>
                        @endforeach 
                      </div> 
                    </li>
                  </ul> 
                </div>
              </section>
            </div> 
          @endforeach  
        </div>  
      </div>
    </section>
  @endif 

  <!-- TESTIMONIALS -->
  <section class="testimonials content-bottom">
    <div class="container">
      <h3 id="what-text">WHAT <span id="span-text-color">PEOPLE SAY</span></h3>
        <div class="row">
          <div class="col-sm-12">
            <div class="owl-controls">
              <div class="owl-nav">
                  <div class="owl-prev"></div>
                  <div class="owl-next"></div>
              </div>
            <div id="customers-testimonials" class="owl-carousel"> 
              @foreach($reviews as $review) 
                <div class="item">
                  <div class="shadow-effect">
                    <p>
                      {{ $review->review }}
                    </p>
                  </div>
                  <div class="testimonial-image"><img class="img-circle" src="/public/img/user.png" alt=""></div>
                  <p class = "testimonial-name">{{ $review->first_name }} {{ $review->last_name }}</p>
                  <a>Client</a>
                </div> 
              @endforeach 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>  
  <!---- download app section ---->
  <section class="download-app-section">
    <div class="row container-fluid">
        <div class="col-sm-12 col-md-6 col-lg-6 left-sec">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <img src="img\pre-footer-phone.png" class="phone-img" alt="Image">
                </div>
                <div class="col-sm-12 col-md-6 desc-sec">
                    <h3 class="sec-heading">DOWNLOAD OUR APP</h3>
                    <p class="para">We are now available on mobiles.Download our app from here to get a better food experience.</p>
                    <a href="https://play.google.com/store/apps/details?id=com.namastheindia" id="google-play-btn">
                        <img src="img\footer\google_play.png" alt="Image">
                    </a>
                    <a href="https://apps.apple.com/us/app/namasteindia-london/id1535193270" id="app-store-btn">
                        <img src="img\footer\app_store.png" alt="Image">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 right-sec">
            <h3 class="sec-heading">SIGN UP OUR NEWSLETTER</h3>
            <p class="para">Stay up-to-date with our promotions, sales, special offers and </p>
            <div class="input-group">
                <input type="text" class="form-control text-box email-placeholfer-text"
                    placeholder="Enter your email address" v-model="sub_email">
                <a href="#" class="btn btn-default btn-sm subscribe-btn"
                    @click.prevent="subscribeNewsletter()">SUBSCRIBE</a>
            </div>
        </div>
    </div>
  </section> 
  <!---- //download app section ----> 
</div>  
<!-- Branch modal -->
<div id="branchModal" class="modal modal-medium fade" data-keyboard="false" data-backdrop="static" style="z-index: 9999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> 
                <h4 class="modal-title">Choose Your Nearest Branch</h4>
            </div>
            <div class="modal-body" style="padding:0%;">   
                <br /><br />
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <span style="color: red;" v-if="pop_up_error">@{{ pop_up_error }}</span>
                        <select class="form-control" v-model="selectedBranch">  
                          @foreach($branches as $branch)
                            <option value="{{ $branch->id }}">{{ $branch->name }}</option> 
                          @endforeach
                        </select>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button @click.prevent="addSelectedBranch()" type="button" class="btn btn-warning">Continue</button>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</div> 
<script type="text/javascript">    

  // @if (count($branches) == 1)
  //   var $this = this;  
  //   let restaurant_id = 1; 
  //   axios.post('{{route('branch.pick')}}', {
  //       _token: '{{csrf_token()}}',
  //       selectedBranch: 1, 
  //       restaurant_id: restaurant_id
  //   }).then(function (response) { 
  //   }).catch(function (error) {
  //       console.log(error);
  //   });  
  // @else   
  //   var branch_id = '{{ session()->get('branch_id') }}';  
  //   $(window).on('load',function(){
  //     $('#branchModal').modal('show');
  //   }); 
  // @endif 
 
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  }); 
  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  }); 
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading-one').addClass('active');
  }); 
  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading-one').removeClass('active');
  }); 
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading-two').addClass('active');
  }); 
  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading-two').removeClass('active');
  }); 

  $(function(){
    // SyntaxHighlighter.all();
  });
  $(window).load(function(){
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 210,
      itemMargin: 5,
      minItems: 2,
      maxItems: 4,
      start: function(slider){
        $('body').removeClass('loading');
      }
    });
  });  
</script> 
<script type="text/javascript">   

  function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }
 
  var data = {  
    menus: '',
    deals: '',
    cuisines: '',
    selected_menu_items: [], 
    delivery: false,
    takeaway: false,
    cart: {!! json_encode($cart) !!},
    total: 0,
    vat:{!! json_encode(getVat(),true) !!},
    vat_value: 0,
    sub_total: 0, 
    @if(Route::currentRouteName()=='login')
        deliveryCost:0,
    @else
        deliveryCost:{!! json_encode($restaurant->delivery_cost) !!},
    @endif  
    requests: '',
    cart_date: '',
    cart_time: '',
    @if(Route::currentRouteName()=='login')
        restaurant:{},
    @else
        restaurant:{!! json_encode($restaurant) !!},
    @endif  
    favoured: false,
    menu_id: 0,
    type: '', 
    date: '{{\Carbon\Carbon::now()->toDateString()}}',
    time: '{{\Carbon\Carbon::now()->format('h:i A')}}', 
    variants: [],
    addons: [],
    menu_item: {},
    selected_addons: [],
    selected_variant: {}, 
    guest: false,   
    total_status: 2,
    deals_menus: {},
    selected_deal_items: {},
    selectedDealItem: {},
    deal_id: '',
    deal_name: '',
    deal_price: '',
    deal_items: [],
    offers_menus: {},
    selected_offer_items: {},
    selectedOfferItem: {},
    offer_id: '',
    offer_name: '',
    offer_price: '',
    offer_items: [],
    sub_email: '',
    selectedBranch: '',
    pop_up_error: '',
    restaurant_id: 1
  };   
  var restaurantIndex = new Vue({
    el: '#restaurantIndex',
    data: data,
    mounted: function () {  
        // console.log(value);
        this.getTotals(); 
    },
    methods: { 
      addSelectedBranch: function () {  
        var $this = this; 
        $this.pop_up_error = '';
        let restaurant_id = $this.restaurant_id;
        if($this.selectedBranch){
            axios.post('{{route('branch.pick')}}', {
                _token: '{{csrf_token()}}',
                selectedBranch: $this.selectedBranch, 
                restaurant_id: restaurant_id
            }).then(function (response) {
                console.log(response);
                $('#branchModal').modal('hide');
            }).catch(function (error) {
                console.log(error);
            }); 
        }else{
            $this.pop_up_error = 'Please Select Your Branch.';
        }
      },
      getTotals: function () { 
        this.total = 0; 
        this.sub_total = 0; 
        const $this = this; 
        var vat = 0;
        var count = 0;

        this.cart.forEach(function (item) {
          // console.log(item);
          var price = item.price;  

          if (item.show_variant) {
              price = item.show_variant.pivot.price;
          }

          if (item.show_crust) {
              price = price + item.show_crust.pivot.price;
          } 

          if (item.show_addons) {
              item.show_addons.forEach(function (addon) {

                  // $this.total += parseFloat(addon.pivot.price);
                  // $this.sub_total += parseFloat(addon.pivot.price);
                  // total_item_price += parseFloat(addon.pivot.price);
                  price = price + addon.pivot.price;
              });
          }

          var total_item_price = parseFloat(price);

          $this.total += item.quantity * price;
          $this.sub_total += item.quantity * price;

          var vat_percentage = 0;  

          switch (item.vat_category) {
              case 'food':
                  vat_percentage = $this.vat.food;
                  break;
              case 'alcohol':
                  vat_percentage = $this.vat.alcohol;
                  break;
          }  
          vat += total_item_price * vat_percentage * item.quantity; 
        }); 
        this.vat_value = vat; 
        var sum_val = this.currency(this.total+this.vat_value+this.deliveryCost);  
        jQuery('#total-val').html(sum_val);  
        this.$forceUpdate(); 
      },
      currency: function (price) {
          const formatter = new Intl.NumberFormat('en-US', {
              currency: 'USD',
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
          }); 
          return formatter.format(price) 
      },
      addToCart: function (menu_item) {   
        if (menu_item.addons.length != 0) {
          window.location.href = "{{route('user.restaurant.menu')}}";
        }else{
          this.pop_up_error = '';
          this.selected_addons = [];  
          this.selected_crust = {}; 
          var varient_id = 0;  
          this.selected_variant = 0;    
          var qty = jQuery('#' + menu_item.id + '-textbox').val(); 
          if (menu_item.variants.length) {
            varient_id = menu_item.variants[0].id; 
            this.selected_variant = menu_item.variants[0]; 
            console.log(this.selected_variant); 
          }else{
            varient_id = 0;  
          } 

          for(var q = 1; q <= qty; q++){
            this.addMenuItem(menu_item, varient_id); 
          } 

          $.toast({
            heading: 'Item Added',
            text: menu_item.name + ' Added to Cart',
            showHideTransition: 'fade',
            hideAfter: 3000,
            icon: 'success'
          }); 
        }
      },
      addToCartTopSell: function (menu_item) {   
        if (menu_item.addons.length != 0) {
          window.location.href = "{{route('user.restaurant.menu')}}";
        }else{
          this.pop_up_error = '';
          this.selected_addons = [];  
          this.selected_crust = {}; 
          var varient_id = 0;  
          this.selected_variant = 0;     
          var qty = jQuery('#' + menu_item.id + '-textbox').val(); 
          if (menu_item.variants.length) {
            varient_id = menu_item.variants[0].id; 
            this.selected_variant = menu_item.variants[0]; 
            console.log(this.selected_variant); 
          }else{
            varient_id = 0;  
          } 

          for(var q = 1; q <= qty; q++){
            this.addMenuItem(menu_item, varient_id); 
          } 

          $.toast({
            heading: 'Item Added',
            text: menu_item.name + ' Added to Cart',
            showHideTransition: 'fade',
            hideAfter: 3000,
            icon: 'success'
          });
          var menu_id = menu_item.menu_id; 
          var route = '{{route('user.restaurant.menu')}}' + '?menu=' + menu_id;
          setTimeout(function(){
            window.location.href = route; 
          }, 2000);
        }
      },
      addToCartSliderItem: function (menu_item) {   
        if (menu_item.addons.length != 0) {
          window.location.href = "{{route('user.restaurant.menu')}}";
        } 
        this.pop_up_error = '';
        this.selected_addons = [];  
        this.selected_crust = {}; 
        var varient_id = 0;  
        this.selected_variant = 0;   
        var qty = 1; 
        if (menu_item.variants.length) {
          varient_id = menu_item.variants[0].id; 
          this.selected_variant = menu_item.variants[0]; 
          console.log(this.selected_variant); 
        }else{
          varient_id = 0;  
        }  
        for(var q = 1; q <= qty; q++){
          this.addMenuItem(menu_item, varient_id); 
        }  
        $.toast({
          heading: 'Item Added',
          text: menu_item.name + ' Added to Cart',
          showHideTransition: 'fade',
          hideAfter: 3000,
          icon: 'success'
        });
        var menu_id = menu_item.menu_id; 
        var route = '{{route('user.restaurant.menu')}}' + '?menu=' + menu_id;
        setTimeout(function(){
          window.location.href = route; 
        }, 2000);
      },
      addMenuItem: function (menu_item, varient_id) {

        var $this = this;   
        // varient_id = $this.selected_varient;   
        const selected_item = this.cart.filter(function (item) { 
          if(jQuery.isEmptyObject(varient_id)){
            return item.id == menu_item.id;
          }else{ 
            return item.id == menu_item.id && item.selected_variant == varient_id;
          }   
        });   

        if (selected_item.length) {  
          if (menu_item.variants.length) {
            var $this = this; 
            menu_item.type = 'single';  
            var show_addons = []; 
            menu_item.addons.forEach(function (addon) {
                $this.selected_addons.forEach(function (selected_addon) {
                    if (addon.id == selected_addon) {
                        show_addons.push(addon);
                    }
                });
            });     

            selected_item[0].quantity++;
            this.$forceUpdate(); 

            if (this.selected_variant.length != 0) { 
                
              selected_item[0].show_variant = this.selected_variant;
              selected_item[0].selected_variant = this.selected_variant.id;
            }  

            this.selected_addons = []; 
            this.selected_variant = {};
          } else { 
              selected_item[0].quantity++;
              this.$forceUpdate();
              this.selected_variant = {};
              this.selected_addons = [];
          }
        } else {   
          var $this = this;  
          menu_item.quantity = 1;
          menu_item.type = 'single'; 
          menu_item.selected_variant = this.selected_variant;   
          var show_addons = []; 

          menu_item.addons.forEach(function (addon) {
              $this.selected_addons.forEach(function (selected_addon) {
                  if (addon.id == selected_addon) {
                      show_addons.push(addon);
                  }
              });
          }); 

          var item = this.cart.push(menu_item);  
          var addon_varient_id = varient_id;   
          // console.log(this.selected_variant);
          if (this.selected_variant.length != 0) {   
            this.cart[item - 1].show_variant = this.selected_variant;
            this.cart[item - 1].selected_variant = this.selected_variant.id;
                
          }   
          this.selected_addons = []; 
          this.selected_variant = {};
        }  
        this.getTotals();
        this.sendAjax();   
      },
      addOfferItem: function (offer_id, offer_name, offer_price, offer_vat_category) {  
        // var offer = offer; 
        var $this = this;
        let arr = [];
        // console.log(offer);
        
        // this.offers_menus = [];
        // this.offer_id = offer.id;
        // this.offer_name = offer.name;
        // this.offer_price = offer.price;
        // this.vat_category = offer.vat_category;
    
        this.offers_menus = [];
        this.offer_id = offer_id;
        this.offer_name = offer_name;
        this.offer_price = offer_price;
        this.vat_category = offer_vat_category;

        axios.post('{{route('user.offers.menu')}}', {
            _token: '{{csrf_token()}}',
            offer_id: offer_id
        }).then(function (response) {
            console.log(response.data);
            $this.offers_menus = response.data.data.items; 
            
            $this.offers_menus.forEach(function(offer_menu) {
                var selectedItem = offer_menu.menu_items.find(m => m.offer_items_id == $this.selected_offer_items[offer_menu.id]);
                arr.push(offer_menu.menu_items);
            });  

            let offer_data = {
                offer_items: arr,
                id: $this.offer_id,
                name: $this.offer_name,
                price: $this.offer_price,
                vat_category: $this.vat_category,
                quantity: 1,
                type: 'offer',
            }; 
            var item = $this.cart.push(offer_data);
            $this.selected_offer_items = {};
            $this.getTotals();
            $this.sendAjax();   
            $.toast({
              heading: 'Item Added',
              text: $this.offer_name + ' Added to Cart',
              showHideTransition: 'fade',
              hideAfter: 3000,
              icon: 'success'
            });
            setTimeout(function(){
              window.location.href = '{{route('user.restaurant.offers')}}';
            }, 2000);
        }).catch(function (error) {
            console.log(error);
        });
      },
      sendAjax: function () {
        const cart = this.cart; 
        let restaurant_id = this.restaurant.id; 
        let cart_date = this.cart_date;
        let cart_time = this.cart_time; 
        let type = 'takeaway';

        if (this.delivery) {
            type = 'delivery';
        } 
        if (this.takeaway) {
            type = 'takeaway';
        } 
        if (this.reservation) {
            type = 'reservation';
        }  
        let requests = this.requests;

        axios.post('{{route('cart.store')}}', {
          _token: '{{csrf_token()}}',
          cart: cart,
          requests: requests,
          type: type,
          cart_date: cart_date,
          cart_time: cart_time,
          restaurant_id: restaurant_id
        }).then(function (response) {
          // console.log(response);
        }).catch(function (error) {
          console.log(error);
        }); 
      },
      subscribeNewsletter: function () {   
        // var offer = offer; 
        var $this = this; 
        var sub_email = $this.sub_email; 
        
        if(sub_email == ''){
            // $this.signup.email = 'The email field is required.';
            swal(
                '{{__('Oh Sorry!')}}', 
                'The email field is required.', 
                'error'
            );
        }else{
            if (!validateEmail(sub_email)) {
                // $this.signup.email = 'The email is not valied.'; 
                swal(
                    '{{__('Oh Sorry!')}}', 
                    'The email is not valied.', 
                    'error'
                );
            }else{ 
                axios.post('{{route('user.subscribe')}}', {
                    _token: '{{csrf_token()}}',
                    sub_email: sub_email,
                }).then(function (response) {  
                //   $.toast({
                //     heading: 'Suscribed Info',
                //     text: response.data.message,
                //     showHideTransition: 'fade',
                //     hideAfter: 3000,
                //     icon: 'success'
                //   });
                    swal(
                        'Success!', 
                        response.data.message, 
                        'success'
                    ); 
                    $this.sub_email = '';
                }).catch(function (error) { 
                    if (error.response.status == 419) { 
                    //   $.toast({
                    //     heading: '{{__('Oh Sorry!')}}',
                    //     text: error.response.data.message,
                    //     showHideTransition: 'fade',
                    //     hideAfter: 3000,
                    //     icon: 'error'
                    //   });
                    
                        swal(
                            '{{__('Oh Sorry!')}}', 
                            error.response.data.message, 
                            'error'
                        );
                        return false; 
                    }
                }); 
            }
        } 
      },
    }
  }); 

</script>  

@endsection
