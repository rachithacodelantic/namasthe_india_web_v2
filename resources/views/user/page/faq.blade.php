@extends('layouts.app')

@section('content')  
    <div class="main-img-figure">
        <img src="{{ asset('storage/' . setting('about_main_banner')) }}" class="img-responsive" alt="Image">
    </div>
    <br /><br /><br />
    <section class="vision-sec">
        <div class="container">
            <div class="row"> 
                <div class="col-sm-12 col-md-12 col-lg-12 content-figure">
                    {!! setting('faq_list') !!}
                </div>
            </div>
        </div>
    </section>
    <br /><br /><br />

@endsection
