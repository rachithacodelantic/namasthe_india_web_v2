@extends('layouts.app')
@section('title', 'location')
@section('content')
    <link href="{{ asset('css/locations.css') }}" rel="stylesheet">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
        var directionDisplay;
        var directionsService = new google.maps.DirectionsService();
        var map;

        function initializeMap(selectObject) {

            var value = selectObject.value;
            var partsOfStr = value.split(',');

            var latlng = new google.maps.LatLng(7.750033, 80.6500523);
            var options = {
                zoom: 7,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map($('#map_canvas')[0], options);
            var latLng = new google.maps.LatLng(partsOfStr[0], partsOfStr[1]);
            var marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }

    </script>

    <div id="location">
        <div class="row container-fluid">
            <div class="col-sm-12 col-md-4 col-lg-4 left-figure">
                <div class="row heading-row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <h3>Our <span>Location</span></h3>
                    </div>
                </div> 
                @foreach ($branches as $branch)
                    <div class="figure-item"> 
                        <p>
                            <i class="fa fa-map-marker"></i>
                            {{ $branch->county }} {{ $branch->city }}<br />{{ $branch->country }}<br /> {{ $branch->province }} {{ $branch->postcode }}
                        </p>
                        <p>
                            <i class="fa fa-phone"></i>
                            {{ $branch->phone }}
                        </p> 
                    </div>
                @endforeach
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 right-figure">
                <div class="row heading-row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <h3>Map</h3>
                    </div>
                </div>
                <div class="row heading-row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2481.4004706734827!2d0.047731515236819635!3d51.54255526595396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a64c602f601d%3A0xeebed52bf15ca9e1!2sHigh%20St%20N%2C%20Manor%20Park%2C%20London%20E12%206SL%2C%20UK!5e0!3m2!1sen!2slk!4v1576491643099!5m2!1sen!2slk"
                                        frameborder="0" style="border:0;width:100%;height:400px;" allowfullscreen=""></iframe>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $("#select-location").change();
        });

    </script>

@endsection
