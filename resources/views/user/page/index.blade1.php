@extends('layouts.app')

@section('content')
    <section class="main">
    <div id="demo" class="carousel slide" data-ride="carousel"> 
      <ul class="carousel-indicators"> 
        @foreach($slider_images as $key => $slider)
          <li data-target="#demo" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
        @endforeach
      </ul>
       
      <div class="carousel-inner">
        @foreach($slider_images as $slider)
          <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image: url('{{ $slider->path }}');">
            <h4>{{ $slider->name }}</h4> 
            <p>{{ $slider->description }}</p> 
            @if($slider->price)
              <h1 style="color: #fd5d24;">${{ $slider->price }}</h1>
              <button>Buy now</button>
            @endif 
          </div>
        @endforeach 
      </div>


  <!-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

  <ol class="carousel-indicators">
   @foreach( $slider_images as $photo )
      <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
   @endforeach
  </ol>

  <div class="carousel-inner" role="listbox">
    @foreach( $slider_images as $photo )
       <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
           <img class="d-block img-fluid" src="{{ $photo->path }}" alt="{{ $photo->name }}">
              <div class="carousel-caption d-none d-md-block">
                 <h3>{{ $photo->name }}</h3>
                 <p>{{ $photo->descriptoin }}</p>
              </div>
       </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> -->


    </div>
  </section>
  <section class="pro">
    <div class="container">
      <div class="row">
        @if($top_sale_product != null)
          <div class="col-md-7">
            <h1>This week’s Top Selling</h1>
            <div class="spro_selling spro">
              <h1>{{ $top_sale_product[0]->name }}</h1>
              <div class="ht">
              <h2>HOT - Delicious -spice</h2>
              <h3>${{ $top_sale_product[0]->price }}</h3>
               <button>Buy now</button>
            </div>
            </div>
          </div>
        @endif
        @if($new_tast)
          <div class="col-md-5">
            <h1>New taste</h1>
            <div class="new_taste spro">
              <h1>{{ $new_tast->name }}</h1>
              <p>{{ $new_tast->description }}</p>
               <button>Buy now</button>
            </div>
          </div>
        @endif
        <div class="col-md-12">
          <h1>Weekly Offers </h1>
        </div>
        <div class="col-md-6">
          <div class="off1 spro offer">
            <div class="black">
              <h1>Rustic Garlic Chicken</h1>
              <p>Mushrooms, tomatoes, onions, black olives and bell  peppers with a double layer of mozzarella cheese.</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="off2 spro offer">
            <div class="black">
              <h1>Rustic Garlic Chicken</h1>
              <p>Mushrooms, tomatoes, onions, black olives and bell  peppers with a double layer of mozzarella cheese.</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <h5>see all offers</h5>
        </div>
      </div>
    </div>
  </section>
  @if(setting('show_menu') == 1)
    <section class="ash">
      <div class="container">
        <h1>Menu</h1>
         <!-- Nav tabs -->
      <ul class="nav nav-tabs  justify-content-center">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu1">Menu 1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu2">Menu 2</a>
        </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div id="home" class=" tab-pane active">
          <div class="row">
            <div class="col-md-3">
              <img src="img/p1.png" class="img-fluid">
              <h2>Veg Treat</h2>
              <p>This thin-crust, Roman-inspired pizza  tops a 10" thin crust pie</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>
            <div class="col-md-3">
              <img src="img/p2.png" class="img-fluid">
              <h2>Veg Treat</h2>
              <p>This thin-crust, Roman-inspired pizza  tops a 10" thin crust pie</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>
            <div class="col-md-3">
              <img src="img/p3.png" class="img-fluid">
              <h2>Veg Treat</h2>
              <p>This thin-crust, Roman-inspired pizza  tops a 10" thin crust pie</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>
            <div class="col-md-3">
              <img src="img/p4.png" class="img-fluid">
              <h2>Veg Treat</h2>
              <p>This thin-crust, Roman-inspired pizza  tops a 10" thin crust pie</p>
              <h3>$13.99</h3>
              <button>Buy now</button>
            </div>

          </div>
        </div>
        <div id="menu1" class=" tab-pane fade"><br>
          <h3>Menu 1</h3>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <div id="menu2" class=" tab-pane fade"><br>
          <h3>Menu 2</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
        </div>
      </div> 
      </div>
    </section>
  @endif
<section class="down">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 org">
        <div class="row">
          <div class="col-md-6">
            <img src="img/ph.png" class="img-fluid ph">
          </div>
          <div class="col-md-6 org_side">
            <h1>Download our APP</h1>
            <p>Lorem ipsum dolor sit amet,</p><p> consectetur adipiscing elit, sed do </p>
            <div class="row">
              <div class="col-md-6 col-6">
                <img src="img/google_play.png" class="img-fluid">
              </div>
              <div class="col-md-6 col-6">
                <img src="img/app_store.png" class="img-fluid">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 ash_dark">
        <h1>Sign up our news letter</h1>
        <p>Stay up-to-date with our promotions, sales, Special offers and other discount information.</p>
        <form method="post" action="{{url('newsletter')}}">
          @csrf
          <div class="row">
            <div class="col-md-9 col-9">
              <input type="email" placeholder="Enter your email address" name="email">
            </div>
            <div class="col-md-3 col-3">
              <input type="submit" value="sign up" name="" class="button">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
 
    <script type="text/javascript">
        window.addEventListener('load', function () {
            // jQuery('.home-page-slider').slick({
            //     dots: false,
            //     arrows: false,
            //     infinite: true,
            //     speed: 10000,
            //     slidesToShow: 2.5,
            //     slidesToScroll: 1,
            //     autoplay: true,
            //     autoplaySpeed: 0,
            //     hoverable: true,
            //     cssEase: 'linear',
            //     responsive: [
            //         {
            //             breakpoint: 767,
            //             settings: {
            //                 slidesToShow: 1.5,
            //             }
            //         },
            //     ]
            // });
        });
    </script> 

    <script type="text/javascript">
        window.addEventListener('load', function () {
            // jQuery('.popular-items-slider').slick({
            //     dots: false,
            //     arrows: true,
            //     infinite: true,
            //     speed: 200,
            //     slidesToShow: 4,
            //     slidesToScroll: 1,
            //     autoplay: true,
            //     autoplaySpeed: 3000,
            //     responsive: [
            //         {
            //             breakpoint: 767,
            //             settings: {
            //                 slidesToShow: 1,
            //             }
            //         },
            //     ]
            // });
        });
    </script> 


    <script type="text/javascript">
        window.addEventListener('load', function () {
            //jQuery('.col-md-15 img').height(jQuery('.col-md-15 img').width());

            //jQuery('.index-content-mob img').height(jQuery('.index-content-mob img').width() / 2);
        });

        function setFavourite(menu_item_id) {
            axios.post('{{route('user.favourite-menu-item')}}', {
                _token: '{{csrf_token()}}',
                menu_item_id: menu_item_id
            })
                .then(function (response) {
                    $.alert({title: 'Success!', content: response.data.message, theme: 'success'});
                })
                .catch(function (error) {
                    if (error.response.status == 401) {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please Login to Continue!',
                            theme: 'error'
                        });
                    }
                });
        }
    </script> 

    <script type="application/javascript">
        var now = new Date();
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";

        var checkTime = function() {
            var today = weekday[now.getDay()];
            var timeDiv = document.getElementById('timeDiv');
            var dayOfWeek = now.getDay();
            var hour = now.getHours();
            var minutes = now.getMinutes();

            //add AM or PM
            var suffix = hour >= 12 ? "PM" : "AM";

            // add 0 to one digit minutes
            if (minutes < 10) {
                minutes = "0" + minutes
            };

            if ((dayOfWeek == 0 || dayOfWeek == 6) && hour >= 13 && hour <= 23) {
                hour = ((hour + 11) % 12 + 1); //i.e. show 1:15 instead of 13:15
                timeDiv.innerHTML = 'it\'s ' + today + ' ' + hour + ':' + minutes + suffix + ' - we\'re open!';
                timeDiv.className = 'open';
            }

            else if ((dayOfWeek == 3 || dayOfWeek == 4 || dayOfWeek == 5) && hour >= 16 && hour <= 23) {
                hour = ((hour + 11) % 12 + 1);
                timeDiv.innerHTML = 'it\'s ' + today + ' ' + hour + ':' + minutes + suffix + ' - we\'re open!';
                timeDiv.className = 'open';
            }

            else {
                if (hour == 0 || hour > 12) {
                    hour = ((hour + 11) % 12 + 1); //i.e. show 1:15 instead of 13:15
                }
                timeDiv.innerHTML = 'It\'s ' + today + ' ' + hour + ':' + minutes + suffix + ' - we\'re closed!';
                timeDiv.className = 'closed';
            }
        };

        var currentDay = weekday[now.getDay()];
        var currentDayID = "#" + currentDay; //gets todays weekday and turns it into id
        $(currentDayID).toggleClass("op_today"); //hightlights today in the view hours modal popup

        //setInterval(checkTime, 1000);
        //checkTime();
    </script>

@endsection
