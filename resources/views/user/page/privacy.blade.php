@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/about_us.css') }}" rel="stylesheet">

    <div class="main-img-figure">
        <img src="{{ asset('storage/' . setting('privacy_main_banner')) }}" class="img-responsive" alt="Image">
    </div>

    <!-- Content goes here -->
    <div class="info-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    {!! setting('privacy_page_info') !!}
                </div>
            </div>
        </div>
    </div>
@endsection
