@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/checkout.css') }}" rel="stylesheet">
    <script src="{{ asset('js/checkout.js') }}"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        .jq-toast-wrap.bottom-left {
            position: fixed;
            top: 14%;
            left: 76%;
        } 

        .cart-float-btn-mob { 
            background: #bc040b;
            height: auto;
            width: 100px;
            position: fixed;
            right: 20px;
            bottom: 20px;
            border-radius: 5%;
            z-index: 99999;
            display: none;
            padding: 5px;
            color: #fff; 
        }

        @media only screen and (max-width: 768px){
            .cart-float-btn-mob {
                display: block;
            }
        }

    </style> 
    <div class="content-wrap text-center confirm-address" id="delivery">
        <div class="row">
            <div class="col-md-6 left-content">
                <div class="row left-content-top">
                    <h5>CHECKOUT</h5>
                    <h6 class="left-margin">Delivery / Pick Up Information</h6>
                    <hr>
                </div>
                <div class="row left-content-mid"> 
                    <div class="col-sm-4 col-md-5 del-addr">
                        <div v-if="orderMethod == 'delivery'">
                            <h6>Postal Code</h6>   
                            <input style="width:150px;" type="text" id="post-code" autocomplete="off" v-click-outside="getDeliveryCharge" v-model="postcode" class="form-control" placeholder="Enter Post Code" required="true">
                        </div>
                        @if ($address)
                            <h6>Delivery Address</h6>
                            <textarea id="edt-txt" rows="4" cols="18" readonly style="white-space: normal;text-align: left;text-align-last: left; border: 1px solid #ccc; background: #FFF">
                                    {{ $address->address }}
                                    {{ $address->street }}
                                    {{ $address->city }}
                                    {{ $address->postcode }}
                            </textarea>
                            <i class="fa fa-pencil" id="btn-txt-edit" aria-hidden="true"><u>Edit</u></i>
                        @endif
                    </div> 
                    <div v-if="orderMethod == 'delivery'" class="col-sm-7 col-md-7 del-date-time">
                        <h6>Pick Up/Delivery Time/Postcode and Special Instructions</h6>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="datepicker" v-model="date" class="form-control" placeholder="Delivery Date" required="true" readonly="readonly">
                            </div>
                            <div class="col-md-6">
                                <input id="timepicker" v-model="time" class="form-control" placeholder="Delivery Time" required="true" readonly="readonly" />
                            </div>
                        </div> 
                        <!--<div class="row"> -->
                        <!--    <br />-->
                        <!--    <div class="col-md-6">-->
                        <!--        <input type="text" id="post-code" autocomplete="off" v-click-outside="getDeliveryCharge" v-model="postcode" class="form-control" placeholder="Enter Post Code" required="true">-->
                        <!--    </div> -->
                        <!--</div>-->
                    </div>
                    <div v-if="orderMethod == 'takeaway'" class="col-sm-7 del-date-time">
                        <h6>Pick Up/Delivery Time and Special Instructions</h6>
                        <div class="col-md-6">
                            <input type="text" id="datepicker" v-model="date" class="form-control" placeholder="Takeaway Date" required="true">
                        </div>
                        <div class="col-md-6">
                            <input id="timepicker" v-model="time" class="form-control" placeholder="Takeaway Time" required="true" />
                        </div>
                    </div>
                </div>
                <div class="row left-content-bot">
                    <h6 v-if="orderMethod == 'delivery'" class="left-margin">Recipient Details</h6>
                    <h6 v-if="orderMethod == 'takeaway'" class="left-margin">Collector Details</h6>
                    <form class="rec-details delivery-form" id="delivery-form" action="{{ route('delivery.store') }}"
                        method="post">
                        <input type="hidden" id="cart_date" v-model="cart_date" name="date"
                            value="{{ session('cart_date') }}">
                        <input type="hidden" id="time" v-model="cart_time" name="time" value="{{ session('cart_time') }}">
                        <input type="hidden" id="restaurant_id" name="restaurant_id" value="{{ session('restaurant_id') }}">
                        <input type="hidden" id="requests" name="requests" value="{{ session('requests') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <select id="inputState" class="form-control">
                                    <option selected>Mr</option>
                                    <option>Mrs</option>
                                    <option>Miss</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="firstNameRec" v-model="recipient_fname"
                                    placeholder="First Name" required="true">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="lastNameRec" v-model="recipient_lname"
                                    placeholder="Last Name" required="true">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="mobileRec" v-model="recipient_mobile"
                                    placeholder="Mobile" required="true" onkeypress="javascript:return isNumber(event)" maxlength="15">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="landlineRec" v-model="recipient_land"
                                    placeholder="Landline" onkeypress="javascript:return isNumber(event)" maxlength="15">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="email" class="form-control" id="emailRec" v-model="recipient_email"
                                    placeholder="E-mail">
                            </div>
                        </div>
                        <div v-if="orderMethod == 'delivery'" class="form-row">
                            <div class="form-group col-md-8">
                                <textarea class="form-control" id="FormControlTextareaSpNotes" v-model="instructions"
                                    rows="3" placeholder="Instructions for your Rider"></textarea>
                            </div>
                        </div>
                        <div v-if="orderMethod == 'takeaway'" class="form-row">
                            <div class="form-group col-md-8">
                                <textarea class="form-control" id="FormControlTextareaSpNotes" v-model="instructions"
                                    rows="3" placeholder="Special Note"></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-6 right-content">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-12" id="help-text-check">
                        <h5>Need Help? Call 0112 234 345</h5>
                    </div>
                </div>
                <div class="row right-content-top">
                    <h6 class="left-margin">Add Payment Details</h6>
                    <hr>
                </div>
                <div class="row right-content-mid">
                    <h6 class="left-margin">Sender Details</h6>
                    <div class="rec-details">
                        <div class="form-row">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1"
                                    @click="setRecipientDetails($event)">
                                <label v-if="orderMethod == 'delivery'" class="form-check-label" for="defaultCheck1">
                                    Same as Recipient Details
                                </label>
                                <label v-if="orderMethod == 'takeaway'" class="form-check-label" for="defaultCheck1">
                                    Same as Collector Details
                                </label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <select id="inputState" class="form-control">
                                    <option selected>Mr</option>
                                    <option>Mrs</option>
                                    <option>Miss</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="inputFirstName" v-model="sender_fname"
                                    placeholder="First Name" value="" required="true">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="inputLastName" v-model="sender_lname"
                                    placeholder="Last Name" value="" required="true">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="inputMobile" v-model="sender_mobile"
                                    placeholder="Mobile" value="" required="true" onkeypress="javascript:return isNumber(event)" maxlength="15">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="inputLandline" v-model="sender_land"
                                    placeholder="Landline" value="" onkeypress="javascript:return isNumber(event)" maxlength="15">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="email" class="form-control" id="inputEmail" v-model="sender_email"
                                    placeholder="E-mail" value="" required="true">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row right-content-bot">
                    <h6 class="left-margin">Payment Details</h6>
                    <div class="tab left-margin">
                        @foreach ($payment_methods as $method)
                            <button class="tablinks {{ $loop->first ? 'active' : '' }}"
                                onclick="openCity(event, '{{ $method->name }}')">{{ $method->name }}</button>
                        @endforeach
                        <!-- <button class="tablinks" onclick="openCity(event, 'delivery')">Pay on Delivery</button>  -->
                    </div>
                    <input type="hidden" id="paymentType" value="Mobile">
                    <hr>
                    <div id="Pay Online" class="tabcontent">
                        <!-- <form class="rec-details"> -->
                        <div class="form-row">
                            <div class="form-radio">
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" checked><img class="img-responsive img-visa"
                                        src="{{ asset('img/visa.png') }}" id="logo" alt="logo">
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio"><img class="img-responsive img-master"
                                        src="{{ asset('img/master.png') }}" id="logo" alt="logo">
                                </label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="inputCardNo"
                                    placeholder="12 digit on front of the card">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <select id="inputState" class="form-control">
                                    <option selected>Month</option>
                                    <option>Jan</option>
                                    <option>Feb</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <select id="inputState" class="form-control">
                                    <option selected>Year</option>
                                    <option>1990</option>
                                    <option>1991</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" id="inputCVV" placeholder="CVV Number">
                            </div>
                        </div>
                        <!-- </form> -->
                    </div>
                    <div id="Cash" class="tabcontent">
                        <!-- <h3>Delivery</h3>  -->
                    </div>
                    <div id="Mobile" class="tabcontent">
                        <!-- <h3>Delivery</h3>  -->
                    </div>
                </div>
                <div v-if="orderMethod == 'delivery'" class="row total-bar">
                    <div class="col-md-4">
                        <h6>Delivery</h6>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <h6>{{ setting('currency') }} @{{ currency(delivery_cost) }}</h6>
                    </div>
                </div>
                <div class="row total-bar">
                    <div class="col-md-4">
                        <h6>TOTAL</h6>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <h6>{{ setting('currency') }} @{{ currency(total) }}</h6>
                    </div>
                </div>
                <div class="row total-bar">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <span style="color: #000;font-size: 12px;font-weight: bold;">(Site Discount
                            -{{ setting('currency') }}@{{ currency(lbl_restaurant_discount) }})</span>
                    </div>
                </div>
                <div class="row">
                    <input type="submit" @click.prevent="confirmOrder($event)" id="s-cart-add-to-cart" value="CONFIRM"
                        class="btn btn-default btn-sm btn-custom-sm">
                    <a style="margin-top:10px;float: right;font-size: 12px;text-decoration: underline;"
                        href="{{ route('user.restaurant.menu') }}">Back To Menu </a>
                </div>
                </form>
                <a href="{{route('user.restaurant.menu')}}" class="cart-float-btn-mob">
                    Order More
                </a>
            </div>
        </div>
    </div> 
    <script type="text/javascript"> 

        var data = {
            cart: {!! json_encode($cart) !!},
            total: 0,
            sub_total: 0, 
            validations: {},
            payment: 'Cash',
            date: '{{$cart_date}}',
            time: '{{$cart_time}}',
            default_time: '{{$cart_time}}',
            orderMethod: '{{$orderMethod}}',
            reservation: false,
            delivery: false,
            takeaway: false,
            vat:{!! json_encode(getVat(),true) !!},
            vat_value: 0,
            delivery_cost:{{$delivery_cost}},
            promocode: '',
            promocode_validation: '',
            promocode_error: '',
            promotion: {},
            restaurant:{!! json_encode($restaurant) !!},
            setting:{!! json_encode($setting) !!},
            lbl_restaurant_discount: 0,
            restaurant_discount: 0,
            site_discount: 0,
            addresses:{!! json_encode(\Illuminate\Support\Facades\Auth::user()->addresses) !!},
            guest: false,  
            @if($orderMethod == 'delivery')  
                postcode: '{{$postcode}}',
            @else 
                postcode: '',
            @endif
            remainingCount: 255,
            instructions: '',
            recipient_fname: '',
            recipient_lname: '',
            recipient_mobile: '+44',
            recipient_land: '+44',
            recipient_email: '', 
            @if(\Illuminate\Support\Facades\Auth::user()->email == setting('guest_email_id'))
                sender_fname: '',
                sender_lname: '',
                sender_mobile: '+44',
                sender_land: '+44',
                sender_email: '',
                // postcode: '',
            @else
                sender_fname: '{{ $user->first_name }}',
                sender_lname: '{{ $user->last_name }}',
                sender_mobile: '+44',
                sender_land: '+44',
                sender_email: '{{ $user->email }}', 
            @endif 
            new_post_code: 0,
        }; 
        var style = {
            base: {
                color: '#C7003B',
                fontFamily: '"Nexa-Bold","Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#555555'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        }; 
        var delivery = new Vue({
            el: '#delivery',
            data: data,
            mounted: function () {  
                this.getTotals();   
                @if(auth()->user()->email == setting('guest_email_id'))
                    this.guest = true;
                @endif 
            },
            directives: {
                'click-outside': {
                  bind: function(el, binding, vNode) {
                    // Provided expression must evaluate to a function.
                    if (typeof binding.value !== 'function') {
                        const compName = vNode.context.name
                      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                      if (compName) { warn += `Found in component '${compName}'` }
                      
                      console.warn(warn)
                    }
                    // Define Handler and cache it on the element
                    const bubble = binding.modifiers.bubble
                    const handler = (e) => {
                      if (bubble || (!el.contains(e.target) && el !== e.target)) {
                        binding.value(e)
                      }
                    }
                    el.__vueClickOutside__ = handler

                    // add Event Listeners
                    document.addEventListener('click', handler)
                        },
                  
                    unbind: function(el, binding) {
                        // Remove Event Listeners
                        document.removeEventListener('click', el.__vueClickOutside__)
                        el.__vueClickOutside__ = null 
                    }
                }
            },
            methods: {
                addQuantity: function (menu_item) {
                    console.log(menu_item);
                    const selected_item = this.cart.filter(function (item) {
                        return item.id == menu_item.id
                    }); 

                    if (selected_item.length) {  
                        selected_item[0].quantity++;
                        this.cart.push(selected_item[0]);
                    } else {
                        menu_item.quantity = 1;
                        this.cart.push(menu_item);
                    }

                    this.getTotals();
                    this.sendAjax();
                },
                removeQuantity: function (menu_item) {
                    const selected_item = this.cart.filter(function (item) {
                        return item.id == menu_item.id
                    });

                    if (selected_item.length) {
                        if (selected_item[0].quantity > 0) {
                            selected_item[0].quantity--;
                            this.$forceUpdate();
                        }
                    }

                    this.getTotals();
                    this.sendAjax();
                },
                getTotals: function () {
                    this.total = 0; 
                    this.sub_total = 0; 
                    const $this = this; 
                    var discount = 0; 
                    var site_discount = 0 
                    var restaurant_discount = 0; 
                    var vat = 0;

                    this.cart.forEach(function (item) {
                        var price = item.price;    
                        if (item.show_variant) {
                            price = item.show_variant.pivot.price;
                        } 
                        if (item.show_crust) {
                            price = price + item.show_crust.pivot.price;
                        }  
                        if (item.show_addons) {
                            item.show_addons.forEach(function (addon) { 
                                price = price + addon.pivot.price;
                            });
                        }

                        var total_item_price = parseFloat(price);

                        if ($this.promotion.method == 'menu_item') {
                            if ($this.promotion.id == item.id) {
                                discount = $this.getPromotionPrice(item.promo_type, item.promo_value, price);
                                $this.promotion.reduction = discount;
                                console.log(discount);
                            }
                        }

                        $this.sub_total += item.quantity * price; 
                        var vat_percentage = 0;

                        switch (item.vat_category) {
                            case 'food':
                                vat_percentage = $this.vat.food;
                                break;
                            case 'alcohol':
                                vat_percentage = $this.vat.alcohol;
                                break;
                        }

                        vat += total_item_price * vat_percentage * item.quantity;
                    }); 

                    if ($this.restaurant.discount == 1) {
                        if ($this.promotion.method == 'restaurant') {
                            discount = $this.getPromotionPrice($this.promotion.type, $this.promotion.value, $this.sub_total);
                            $this.promotion.reduction = discount;
                        }

                        if ($this.restaurant.promocode && $this.restaurant.promo_range) {
                            restaurant_discount = $this.getPromotionPrice($this.restaurant.discount_type, $this.restaurant.discount_value, $this.sub_total);
                            $this.restaurant_discount = restaurant_discount;
                            $this.lbl_restaurant_discount = restaurant_discount;
                        } 
                    } 

                    $this.vat_value = vat; 
                    $this.total = vat + $this.sub_total - discount + $this.delivery_cost - restaurant_discount - site_discount;   
                    var sum_val = this.currency(this.total); 
                    jQuery('#total-val').html(sum_val);  
                }, 
                getPromotionPrice(type, value, price) {
                    if (type == 'percentage') {
                        return price * value * 0.01;
                    } else {
                        return value;
                    }
                }, 
                sendAjax: function () {
                    const cart = this.cart;
                    let requests = this.requests;
                    axios.post('{{route('cart.store')}}', {
                        _token: '{{csrf_token()}}',
                        cart: cart,
                        requests: requests
                    }).then(function (response) {
                        console.log(response);
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    }); 
                    return formatter.format(price) 
                },
                getDeliveryCharge: function () {    
                    var post_code = jQuery('#post-code').val();    
                    if(post_code != this.new_post_code){
                        var $this = this;
                        $this.delivery_cost = 0;
                        axios.post('{{route('user.restaurant.delivery-charge')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            postcode: post_code,
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        }).then(function (response) { 
                            if (response.data.message == 'success') { 
                                $this.delivery_cost = response.data.delivery;  
                                $.toast({
                                    heading: 'Post code added',
                                    text: 'Post code added',
                                    showHideTransition: 'fade',
                                    hideAfter: 3000,
                                    icon: 'success'
                                });
                                $this.new_post_code = post_code;
                                $this.getTotals(); 
                            }  
                            $this.loading = false;
                        }).catch(function (error) { 
                            console.log(error);
                            $.toast({
                                heading: 'Error',
                                text: 'Sorry we do not delivery to this location.',
                                showHideTransition: 'fade',
                                hideAfter: 3000,
                                icon: 'error'
                            });
                            $this.new_post_code = post_code;
                            $this.getTotals(); 
                            return false;
                        }); 
                    } 
                },
                setFormat: function (event) {
                    let value = event.target.value;
                    let string = value.split('-').join('');

                    if (string.length) {
                        string = string.match(new RegExp('.{1,4}', 'g')).join('-');
                    } 
                },
                confirmOrder: function (event) { 
                    let $this = this;
                    this.sendAjax(); 
                    this.payment = $("#paymentType").val();  
                    if (this.orderMethod == 'delivery') {
                        var address = document.getElementById('edt-txt').value;
                    }
                    var datepicker = document.getElementById('datepicker').value;
                    var timepicker = document.getElementById('timepicker').value;
                    var inputFirstName = document.getElementById('inputFirstName').value;
                    var inputLastName = document.getElementById('inputLastName').value;
                    var inputMobile = document.getElementById('inputMobile').value;
                    var inputEmail = document.getElementById('inputEmail').value;
                    var firstNameRec = document.getElementById('firstNameRec').value;
                    var lastNameRec = document.getElementById('lastNameRec').value;
                    var mobileRec = document.getElementById('mobileRec').value; 
                    var instructions = document.getElementById('FormControlTextareaSpNotes').value; 
                    @if($orderMethod == 'delivery')  
                        var postcode = document.getElementById('post-code').value; 
                    @endif  
                    
                    var validation_errors = [];

                    if(this.total < 0){ 
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Your order value must be grater than 00.00.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        }); 
                        return false; 
                    } 
                    if (this.orderMethod == 'delivery') {
                        if(address == '' || address.replace(/\s/g, "").length == 0){ 
                            var msg = 'Please fill the address.';
                            validation_errors.push(msg);
                        } 
                    }
                    if(datepicker == ''){ 
                        var msg = 'Please select the date.';
                        validation_errors.push(msg); 
                    }
                    if(timepicker == ''){ 
                        var msg = 'Please select the time.';
                        validation_errors.push(msg); 
                    } 
                    // if(timepicker < $this.default_time){ 
                    //     var msg = 'Please select the future time.';
                    //     validation_errors.push(msg); 
                    // }
                    if(inputFirstName == ''){  
                        var msg = 'Please select the sender first name.';
                        validation_errors.push(msg); 
                    }
                    if(inputLastName == ''){ 
                        var msg = 'Please select the sender last name.';
                        validation_errors.push(msg); 
                    }
                    if(inputMobile == ''){ 
                        var msg = 'Please select the sender mobile.';
                        validation_errors.push(msg); 
                    }
                    if(inputEmail == ''){ 
                        var msg = 'Please select the sender e-mail.';
                        validation_errors.push(msg); 
                    }
                    if(firstNameRec == ''){ 
                        var msg = 'Please select the collector first name.';
                        validation_errors.push(msg); 
                    }
                    if(lastNameRec == ''){ 
                        var msg = 'Please select the collector last name.';
                        validation_errors.push(msg); 
                    }
                    if(mobileRec == ''){  
                        var msg = 'Please select the collector mobile number.';
                        validation_errors.push(msg); 
                    }  
                    if(this.orderMethod == 'delivery'){
                        if(instructions.length > 300){ 
                            var msg = 'Rider Instructions Please set minimum characters.';
                            validation_errors.push(msg);
                        }
                        if(postcode == ''){  
                            var msg = 'Please set the postcode.';
                            validation_errors.push(msg); 
                        } 
                    }else{
                        if(instructions.length > 300){ 
                            var msg = 'Special Note please set minimum characters.';
                            validation_errors.push(msg);
                        }
                    } 
                    if (validation_errors === '[]' || validation_errors.length == 0) {  
                        if (this.payment == 'card') { 
                            
                        } else if (this.payment == 'Cash' || this.payment == 'Mobile') {
    
                            if(this.orderMethod == 'delivery'){ 
                                var inputMobile = '';
                                if (document.getElementById('inputMobile')) {
                                    inputMobile = document.getElementById('inputMobile').value
                                }
    
                                if (this.promocode && !this.promotion) {
                                    jQuery('#validation-errors').slideDown().find('div').html('Please click apply promocode to reduce the amount from your cart.');
                                    jQuery('html,body').animate({
                                        scrollTop: 0
                                    }, 200);
                                    return false;
                                }   
    
                                axios.post('{{route('delivery.validate')}}', {
                                    _token: '{{csrf_token()}}',
                                    senderFirstName: document.getElementById('inputFirstName').value,
                                    senderLastName: document.getElementById('inputLastName').value, 
                                    senderLandline: document.getElementById('inputLandline').value,
                                    email: document.getElementById('inputEmail').value,
                                    phone: inputMobile, 
                                    address: document.getElementById('edt-txt').value, 
                                    postcode: this.postcode,
                                    firstNameRec: document.getElementById('firstNameRec').value,
                                    lastNameRec: document.getElementById('lastNameRec').value, 
                                    mobileRec: document.getElementById('mobileRec').value,
                                    landlineRec: document.getElementById('landlineRec').value,
                                    emailRec: document.getElementById('emailRec').value, 
                                    instructions: document.getElementById('FormControlTextareaSpNotes').value,
                                    restaurant_id: document.getElementById('restaurant_id').value, 
                                    payment: this.payment, 
                                    cart_date: datepicker,
                                    cart_time: timepicker
                                }).then(function (response) {  
                                    if (response.data.message == 'success') { 
                                        $this.doDelivery();
                                    }
                                }).catch(function (error) { 
                                    
                                    if (error.response.status == 419) {  
                                        if(error.response.data.message){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text:  error.response.data.message,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                    if (error.response.status == 422) {
                                        //$this.validations = error.response.data.errors; 
                                        var phone_error_msg = ''; 
                                        if(error.response.data.errors.phone){
                                            phone_error_msg = error.response.data.errors.phone[0];
                                        }else if(error.response.data.errors.senderLandline){
                                            phone_error_msg = error.response.data.errors.senderLandline[0];
                                        }else if(error.response.data.errors.mobileRec){
                                            phone_error_msg = error.response.data.errors.mobileRec[0];
                                        }else if(error.response.data.errors.landlineRec){
                                            phone_error_msg = error.response.data.errors.landlineRec[0];
                                        }

                                        if(phone_error_msg){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text: phone_error_msg,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        } 
                                    }
                                    if (error.response.status == 400) {  
                                        if(error.response.data.message){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text:  error.response.data.message,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                    if (error.response.status == 423) {  
                                        if(error.response.data.message){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text:  error.response.data.message,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                });
                            }
    
                            if(this.orderMethod == 'takeaway'){
    
                                var inputMobile = '';
                                if (document.getElementById('inputMobile')) {
                                    inputMobile = document.getElementById('inputMobile').value
                                }
    
                                if (this.promocode && !this.promotion) {
                                    jQuery('#validation-errors').slideDown().find('div').html('Please click apply promocode to reduce the amount from your cart.');
                                    jQuery('html,body').animate({
                                        scrollTop: 0
                                    }, 200);
                                    return false;
                                }  
                                axios.post('{{route('takeaway.validate')}}', {
                                    _token: '{{csrf_token()}}',
                                    senderFirstName: document.getElementById('inputFirstName').value,
                                    senderLastName: document.getElementById('inputLastName').value, 
                                    senderLandline: document.getElementById('inputLandline').value,
                                    email: document.getElementById('inputEmail').value,
                                    phone: inputMobile,   
                                    firstNameRec: document.getElementById('firstNameRec').value,
                                    lastNameRec: document.getElementById('lastNameRec').value, 
                                    mobileRec: document.getElementById('mobileRec').value,
                                    landlineRec: document.getElementById('landlineRec').value,
                                    emailRec: document.getElementById('emailRec').value, 
                                    restaurant_id: document.getElementById('restaurant_id').value, 
                                    payment: this.payment,
                                    cart_date: datepicker,
                                    cart_time: timepicker
                                }).then(function (response) { 
                                    if (response.data.message == 'success') { 
                                        $this.doTakeaway();
                                    }
                                }).catch(function (error) { 
                                    if (error.response.status == 422) {
                                        //$this.validations = error.response.data.errors;
                                        var phone_error_msg = ''; 
                                        if(error.response.data.errors.phone){
                                            phone_error_msg = error.response.data.errors.phone[0];
                                        }else if(error.response.data.errors.senderLandline){
                                            phone_error_msg = error.response.data.errors.senderLandline[0];
                                        }else if(error.response.data.errors.mobileRec){
                                            phone_error_msg = error.response.data.errors.mobileRec[0];
                                        }else if(error.response.data.errors.landlineRec){
                                            phone_error_msg = error.response.data.errors.landlineRec[0];
                                        }

                                        if(phone_error_msg){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text: phone_error_msg,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                    if (error.response.status == 400) {  
                                        if(error.response.data.message){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text:  error.response.data.message,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                    if (error.response.status == 423) {  
                                        if(error.response.data.message){
                                            $.toast({
                                                heading: '{{__('Oh Sorry!')}}',
                                                text:  error.response.data.message,
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            }); 
                                        }
                                    }
                                });
                            } 
                        } else {
                            if (this.promocode && !this.promotion) {
                                jQuery('#validation-errors').slideDown().find('div').html('Please click apply promocode to reduce the amount from your cart.');
                                jQuery('html,body').animate({
                                    scrollTop: 0
                                }, 200);
                                return false;
                            }
                            axios.post('{{route('delivery.validate')}}', {
                                _token: '{{csrf_token()}}',
                                address: document.getElementById('address').value,
                                street: document.getElementById('street').value,
                                city: document.getElementById('city').value,
                                county: document.getElementById('county').value,
                                postcode: document.getElementById('postcode').value,
                                restaurant_id: document.getElementById('restaurant_id').value,
                                email: document.getElementById('email').value,
                                payment: $this.payment
                            }).then(function (response) {
                                // console.log(response);
                                if (response.data.message == 'success') {
                                    event.target.submit();
                                }
                            }).catch(function (error) {
                                console.log(error);
                                if (error.response.status == 422) {
                                    $this.validations = error.response.data.errors;
                                }
                                if (error.response.status == 400) {
                                    jQuery('#validation-errors').slideDown().find('div').html(error.response.data.message);
                                    jQuery('html,body').animate({
                                        scrollTop: 0
                                    }, 200);
                                }
                            });
                        }  
                    }else{ 
                        var text = '';
                        for (var e = 0; e < validation_errors.length; e++){   
                            text = text + '\n' + validation_errors[e]
                        }  
                        swal(
                            '{{__('Oh Sorry!')}}', 
                            text, 
                            'error'
                        );
                        return false;
                    }
                },
                doDelivery: function () { 
                    let $this = this;
                    axios.post('{{route('delivery.store')}}', {
                        _token: '{{csrf_token()}}',
                        senderFirstName: document.getElementById('inputFirstName').value,
                        senderLastName: document.getElementById('inputLastName').value, 
                        senderLandline: document.getElementById('inputLandline').value,
                        email: document.getElementById('inputEmail').value,
                        phone: document.getElementById('inputMobile').value, 
                        address: document.getElementById('edt-txt').value, 
                        postcode: document.getElementById('post-code').value,
                        firstNameRec: document.getElementById('firstNameRec').value,
                        lastNameRec: document.getElementById('lastNameRec').value, 
                        mobileRec: document.getElementById('mobileRec').value,
                        landlineRec: document.getElementById('landlineRec').value,
                        emailRec: document.getElementById('emailRec').value, 
                        instructions: document.getElementById('FormControlTextareaSpNotes').value,
                        restaurant_id: document.getElementById('restaurant_id').value, 
                        payment: $this.payment,
                        vat: $this.vat_value
                    }).then(function (response) { 
                        swal("Order Completed", "Please check your e-mail", "success").then((value) => {
                            window.location.href = '{{route('user.restaurant.menu')}}'
                        }); 
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                doTakeaway: function () {
                    let $this = this;
                    axios.post('{{route('takeaway.store')}}', {
                        _token: '{{csrf_token()}}',
                        senderFirstName: document.getElementById('inputFirstName').value,
                        senderLastName: document.getElementById('inputLastName').value, 
                        senderLandline: document.getElementById('inputLandline').value,
                        email: document.getElementById('inputEmail').value,
                        phone: document.getElementById('inputMobile').value,  
                        firstNameRec: document.getElementById('firstNameRec').value,
                        lastNameRec: document.getElementById('lastNameRec').value, 
                        mobileRec: document.getElementById('mobileRec').value,
                        landlineRec: document.getElementById('landlineRec').value,
                        emailRec: document.getElementById('emailRec').value, 
                        restaurant_id: document.getElementById('restaurant_id').value, 
                        payment: $this.payment,
                        vat: $this.vat_value
                    }).then(function (response) { 
                        swal("Order Completed", "Please check your e-mail", "success").then((value) => {
                            window.location.href = '{{route('user.restaurant.menu')}}'
                        }); 
                    }).catch(function (error) {
                        console.log(error);
                    });
                },  
                stripeTokenHandler: function (token, form) {
                    // Insert the token ID into the form so it gets submitted to the server
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token.id);
                    form.appendChild(hiddenInput);

                    var $this = this;

                    var phone = '';
                    if (document.getElementById('phone')) {
                        phone = document.getElementById('phone').value
                    }

                    if (this.promocode && !this.promocode_validaton) {
                        jQuery('#validation-errors').slideDown().find('div').html('Please click apply promocode to reduce the amount from your cart.');
                        jQuery('html,body').animate({
                            scrollTop: 0
                        }, 200);
                        return false;
                    }

                    axios.post('{{route('delivery.validate')}}', {
                        _token: '{{csrf_token()}}',
                        address: document.getElementById('address').value,
                        street: document.getElementById('street').value,
                        city: document.getElementById('city').value,
                        county: document.getElementById('county').value,
                        postcode: document.getElementById('postcode').value,
                        restaurant_id: document.getElementById('restaurant_id').value,
                        phone: phone,
                        email: document.getElementById('email').value,
                        payment: $this.payment,
                    }).then(function (response) {
                        if (response.data.message == 'success') {
                            form.submit();
                        }
                    }).catch(function (error) {
                        if (error.response.status == 422) {
                            $this.validations = error.response.data.errors;
                        }
                        if (error.response.status == 400) {
                            jQuery('#validation-errors').slideDown().find('div').html(error.response.data.message);
                            jQuery('html,body').animate({
                                scrollTop: 0
                            }, 200);
                        }
                    })

                    // Submit the form
                    // form.submit();
                },  
                validatePromocode: function () {
                    if (!this.promocode) {
                        this.promocode_error = 'Please enter valid promocode';
                        return false;
                    }

                    if (Object.keys(this.promotion).length !== 0) {
                        this.promocode_error = 'Already added a promocode';
                        this.promocode = '';
                    }

                    var $this = this;

                    axios.post('{{route('promotion.validate')}}', {
                        _token: '{{csrf_token()}}',
                        promocode: $this.promocode,
                        restaurant_id: '{{session('restaurant_id')}}'
                    }).then(function (response) {
                        if (response.data.message == 'success') {
                            if (response.data.data.validation) {

                                if (response.data.data.promotion) {
                                    var promotion = response.data.data.promotion;

                                    $this.promotion = promotion;

                                    $this.promocode_validation = JSON.stringify(promotion);

                                    $this.getTotals();

                                    $this.promocode = '';
                                }

                            } else {
                                if (response.data.data.promotion.length) {
                                    jQuery('#validation-errors').slideDown().find('div').html(response.data.data.promotion);
                                } else {
                                    jQuery('#validation-errors').slideDown().find('div').html('Invalid Promocode!');
                                }
                                jQuery('html,body').animate({
                                    scrollTop: 0
                                }, 200);

                                $this.promocode = '';
                            }
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                },
                setRecipientDetails: function (event) {

                    var $this = this;
                    if (event.target.checked) { 
                        $this.sender_fname = $this.recipient_fname; 
                        $this.sender_lname = $this.recipient_lname; 
                        $this.sender_mobile = $this.recipient_mobile; 
                        $this.sender_land = $this.recipient_land; 
                        $this.sender_email = $this.recipient_email; 
                    }  
                },
                selectAddress: function () {
                    jQuery('#addressModal').modal('show');
                },
                setAddress: function (addressId) {
                    var selectedAddress = this.addresses.filter(function (address) {
                        return address.id == addressId;
                    });

                    if (selectedAddress.length) {
                        document.getElementById('address').value = selectedAddress[0].address;
                        document.getElementById('street').value = selectedAddress[0].street;
                        document.getElementById('city').value = selectedAddress[0].city;
                        document.getElementById('county').value = selectedAddress[0].county;
                        document.getElementById('postcode').value = selectedAddress[0].postcode;
                    }

                    jQuery('#addressModal').modal('hide');
                },
                validatePostcode: function (event) {
                    if (this.postcode.length === 2) {
                        this.postcode = this.postcode + ' ';
                    }
                },
                countRemainingCharacters: function () {
                    this.remainingCount = 255 - this.instructions.length;
                },
            },
            computed: {
                card_type: function () {

                    let card_number = this.card_number.split('-').join('');

                    let amex = /^3[47][0-9]{13}$/;
                    let master = /^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/;
                    let visa = /^4[0-9]{12}(?:[0-9]{3})?$/;

                    if (amex.test(card_number)) {
                        return 'amex';
                    }

                    if (visa.test(card_number)) {
                        return 'visa';
                    }

                    if (master.test(card_number)) {
                        return 'master';
                    }

                }
            }
        });  

        function openCity(evt, cityName) {
            $("#paymentType").val(cityName);
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // for validate text input to only numeric input
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }  

        $(document).ready(function($) {
            $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });
            $("#timepicker").timepicker({});
        });

    </script> 
    <style>
        .panel-body.selected {
            background: rgba(2, 143, 56, 0.34);
        } 
    </style>
@endsection
