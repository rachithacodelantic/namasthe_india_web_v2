@extends('layouts.app')
@section('title', 'reservation/create')
@section('content')
<link href="{{asset('css/reservations.css')}}" rel="stylesheet">
<link href="{{ asset('css/about_us.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .contactus_header{
      color:#bc040b !important;
    }
    .jq-toast-wrap.bottom-left {
        position: fixed;
        top: 14%;
        left: 76%;
    }
</style>

<div class="main-img-figure">
    <img src="{{ asset('storage/site/top_banner.png') }}" class="img-responsive" alt="Image">
</div>

<div id="reservation_div">
    <div class="container-fluid" id="right-content-reserve-align">
         <div class="row">
            <div class="col-xs-6 col-md-4">
               <div class="row top-contents"> 
                  <h3>RESERVE A <span id="span-text-color">TABLE</span></h3>
                  <h6>Contact Our Restaurant</h6> 
               </div>
               <div class="row"> 
                  <div class="left-content-form-text">
                     <div class="col-xs-4 col-md-12">
                        <h3><b>{{$branch->name}}</b><br></h3>
                        <p> {{$branch->phone}} <br>
                           {{$branch->email}}
                        </p>
                     </div>
                  </div> 
               </div> 
            </div>
            <div class="col-xs-6 col-8" id="right-content-contact-us"> 
               <div class="row right-content-bot">
                  <div class="row">
                     <div class="tab tab-res">
                        <button class="tablinks" style="border-bottom:none;">
                           <p>Find a Table</p>
                        </button> 
                     </div>
                  </div>
                  <hr>
                  <div id="online" class="">
                     <form class="rec-details"> 
                        <div class="form-row">
                           <div class="col-md-4"><input type="text" v-model="cart_date" id="datepicker" placeholder="Pick Date *" /></div>
                           <div class="col-md-4"><input type="text" v-model="cart_time" id="timepicker" placeholder="Pick Time *" /></div>
                           <div class="form-group col-md-4">
                                <input class="form-control" type="number" placeholder="Pax *" v-model="head_count" min="1" /> 
                           </div>
                        </div> 
                        <div class="form-row">
                           <div class="col-md-12" id="message"><textarea style="width: 100%;" v-model="special_request" rows="5" name="message" placeholder="Special Requests"></textarea></div> 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6"><input style="margin-top: 20px;" type="text" v-model="phone" value="+44" :class="{'has-error':reservation_validation.phone}" placeholder="Phone Number" onkeypress="javascript:return isNumber(event)" maxlength="15"></div>
                            <div class="form-group col-md-6"><input style="margin-top: 20px;" type="email" v-model="email" :class="{'has-error':reservation_validation.email}" placeholder="E-mail *"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <br /><br />
                                <input id="add-to-cart-h-mid" type="submit" @click.prevent="createBooking" value="Confirm"> 
                            </div>
                        </div>
                     </form>
                  </div> 
               </div> 
            </div>
         </div>
    </div> 
</div>  

    <script type="text/javascript"> 

        var data = {
            menus:{!! json_encode($restaurant->menus) !!},
            selected_menu_items: [],
            style: 'list',
            reservation: false,
            delivery: false,
            takeaway: false,
            cart: {!! json_encode($cart) !!},
            total: 0,
            vat:{!! json_encode(getVat(),true) !!},
            sub_total: 0,
            requests: '{{$requests}}',
            cart_date: '{{$cart_date}}',
            cart_time: '{{$cart_time}}',
            defaultTime: '{{$cart_time}}',
            restaurant:{!! json_encode($restaurant) !!},
            favoured: false,
            menu_id: 0,
            type: 'reservation',
            restaurant_id: '{{$restaurant_id}}',
            date: '{{\Carbon\Carbon::now()->toDateString()}}',
            time: '{{\Carbon\Carbon::now()->format('h:i A')}}',
            res_date: '',
            res_time: '',
            head_count: '',
            phone: '+44',
            email: '',
            special_request: '',
            reservation_validation: {},
            review_validation: {},
            step: 1,
            reservation_data: [],
            reservation_requests: '',
            rating: 5,
            review: '',
            sorted_reviews: [],
            sort: 'desc',
            variants: [],
            addons: [],
            menu_item: {},
            selected_addons: [],
            selected_variant: {},
            reservation_restaurant: {},
            reservation_settings: {},
            guest: false,
            remainingCount1: 255,
            remainingCount2: 255,
            remainingCount3: 255,
        };

        var restaurant = new Vue({
            el: '#reservation_div',
            data: data,
            mounted: function () {

                if (this.restaurant.id != this.restaurant_id) {
                    this.cart = [];
                }

                if (this.type == 'delivery') {
                    this.setDeliveryItems(this.menus[0].menu_items);

                    this.menu_id = this.menus[0].id;

                    this.setMethod('delivery', false);
                } else if (this.type == 'takeaway') {
                    this.setTakeawayItems(this.menus[0].menu_items);

                    this.menu_id = this.menus[0].id;

                    this.setMethod('takeaway', false);
                } else if (this.type == 'reservation') {
                    this.setReservationItems(this.menus[0].menu_items);

                    this.menu_id = this.menus[0].id;

                    this.setMethod('reservation', false);
                } else {
                    this.setReservationItems(this.menus[0].menu_items);

                    this.menu_id = this.menus[0].id;

                    this.setMethod('reservation', false);
                } 

                this.getTotals();

                if (this.restaurant.user) {
                    this.favoured = true;
                }

                this.sorted_reviews = this.restaurant.reviews;

                this.equalHeight();
                @if(auth()->check())
                    @if(auth()->user()->email== setting('guest_email_id'))
                    this.guest = true;
                @endif
                @endif

            },
            methods: {
                setMenu: function (menu) {
                    if (this.delivery) {
                        this.setDeliveryItems(menu.menu_items);
                    }
                    if (this.takeaway) {
                        this.setTakeawayItems(menu.menu_items);
                    }
                    if (this.reservation) {
                        this.setReservationItems(menu.menu_items);
                    }

                    if (menu.id == 0) {
                        this.menu_id = 0;
                    } else {
                        this.menu_id = menu.menu_items[0].menu_id;
                    }
                },
                setDeliveryItems: function (menu_items) {
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.delivery;
                    });
                    this.equalHeight();
                },
                setTakeawayItems: function (menu_items) {
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return menu_item.takeaway;
                    });
                    this.equalHeight();
                },
                setReservationItems: function (menu_items) {
                    this.selected_menu_items = menu_items.filter(function (menu_item) {
                        return true;
                    });
                    this.equalHeight();
                }, 
                equalHeight: function () {
                    this.$nextTick(function () {
                        // gridHeaderHeight();
                    });
                }, 
                setStyle: function (style) {
                    this.style = style;
                    if (style == 'grid') {
                        this.$nextTick(function () {
                            gridHeaderHeight();
                        })
                    }
                },
                setMethod: function (method, exception = false) {
 
                    if (this.cart.length && exception && method != 'reservation') {
                        var $this = this; 
                    }

                    const menu = this.getCurrentMenu();

                    switch (method) {
                        case 'reservation':
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            this.setReservationItems(menu.menu_items);
                            break;
                        case 'delivery':
                            this.reservation = false;
                            this.delivery = true;
                            this.takeaway = false;
                            this.setDeliveryItems(menu.menu_items);
                            break;
                        case 'takeaway':
                            this.reservation = false;
                            this.delivery = false;
                            this.takeaway = true;
                            this.setTakeawayItems(menu.menu_items);
                            break;
                        default:
                            this.reservation = true;
                            this.delivery = false;
                            this.takeaway = false;
                            this.setTakeawayItems(menu.menu_items);
                            break;
                    }

                    this.sendAjax();
                },
                getCurrentMenu: function () {
                    const $this = this;
                    const menu = this.menus.filter(function (menu) {
                        return menu.id == $this.menu_id
                    });
                    return menu[0];
                },
                addToCart: function (menu_item, variant) {

                    this.selected_addons = [];

                    this.selected_variant = variant;

                    if (menu_item.addons.length) {

                        // this.variants = menu_item.variants;
                        this.addons = menu_item.addons;

                        // if (menu_item.variants.length) {
                        //     this.selected_variant = this.variants[0].id;
                        // }
                        this.menu_item = menu_item;

                        jQuery('#addons-modal').modal('show');

                        return false;
                    }

                    this.addMenuItem(menu_item);
                }, 
                addMenuItem: function (menu_item) {

                    var $this = this;

                    const selected_item = this.cart.filter(function (item) {
                        return item.id == menu_item.id;
                    });

                    if (selected_item.length) {
                        if (menu_item.variants.length) {
                            var $this = this;

                            menu_item.quantity = 1;

                            // menu_item.selected_variant = this.selected_variant;


                            var show_addons = [];

                            menu_item.addons.forEach(function (addon) {
                                $this.selected_addons.forEach(function (selected_addon) {
                                    if (addon.id == selected_addon) {
                                        show_addons.push(addon);
                                    }
                                });
                            });


                            var item = this.cart.push(Object.assign({}, menu_item));

                            if (this.selected_variant) {
                                this.cart[item - 1].show_variant = this.selected_variant;
                                this.cart[item - 1].selected_variant = this.selected_variant.id;
                            }

                            if (show_addons) {
                                this.cart[item - 1].selected_addons = this.selected_addons;
                                this.cart[item - 1].show_addons = show_addons;
                            }


                            this.selected_addons = [];

                            this.selected_variant = {};
                        } else {
                            selected_item[0].quantity++;
                            this.$forceUpdate();
                            this.selected_variant = {};
                            this.selected_addons = [];
                        }
                    } else {

                        var $this = this;

                        menu_item.quantity = 1;

                        // menu_item.selected_variant = this.selected_variant;


                        var show_addons = [];

                        menu_item.addons.forEach(function (addon) {
                            $this.selected_addons.forEach(function (selected_addon) {
                                if (addon.id == selected_addon) {
                                    show_addons.push(addon);
                                }
                            });
                        });


                        var item = this.cart.push(menu_item);

                        if (this.selected_variant) {
                            this.cart[item - 1].show_variant = this.selected_variant;
                            this.cart[item - 1].selected_variant = this.selected_variant.id;
                        }

                        if (show_addons) {
                            this.cart[item - 1].selected_addons = this.selected_addons;
                            this.cart[item - 1].show_addons = show_addons;
                        }


                        this.selected_addons = [];

                        this.selected_variant = {};
                    }


                    this.getTotals();
                    this.sendAjax();

                    jQuery('#addons-modal').modal('hide'); 
                },
                removeFromCart: function (menu_item) {
                    const selected_item = this.cart.filter(function (item) {
                        return item.id == menu_item.id
                    });

                    if (selected_item.length > 0) {
                        if (selected_item[0].quantity > 1) {
                            selected_item[0].quantity--;
                            this.$forceUpdate();
                        } else {
                            let $this = this;
                            this.cart.forEach(function (item, index) {
                                if (selected_item[0].id == item.id) {
                                    $this.cart.splice(index, 1);
                                }
                            });
                        }
                    }

                    this.getTotals();
                    this.sendAjax();
                },
                getTotals: function () {
                    this.total = 0;

                    this.sub_total = 0;

                    const $this = this;

                    var vat = 0;

                    this.cart.forEach(function (item) {
                        var price = item.price;


                        if (item.show_variant) {
                            price = item.show_variant.pivot.price;
                        }

                        var total_item_price = parseFloat(price);

                        if (item.show_addons) {
                            item.show_addons.forEach(function (addon) {
                                $this.total += parseFloat(addon.pivot.price);
                                $this.sub_total += parseFloat(addon.pivot.price);
                                total_item_price += parseFloat(addon.pivot.price);
                            });
                        }

                        $this.total += item.quantity * price;
                        $this.sub_total += item.quantity * price;

                        var vat_percentage = 0;


                        switch (item.vat_category) {
                            case 'food':
                                vat_percentage = $this.vat.food;
                                break;
                            case 'alcohol':
                                vat_percentage = $this.vat.alcohol;
                                break;
                        }

                        vat += total_item_price * vat_percentage * item.quantity;
                    });

                    this.vat_value = vat;
                },
                sendAjax: function () {
                    const cart = this.cart;

                    let restaurant_id = this.restaurant.id;

                    let cart_date = this.cart_date;
                    let cart_time = this.cart_time;

                    let type = 'takeaway';

                    if (this.delivery) {
                        type = 'delivery';
                    }

                    if (this.takeaway) {
                        type = 'takeaway';
                    }

                    if (this.reservation) {
                        type = 'reservation';
                    }


                    let requests = this.requests;

                    axios.post('{{route('cart.store')}}', {
                        _token: '{{csrf_token()}}',
                        cart: cart,
                        requests: requests,
                        type: type,
                        cart_date: cart_date,
                        cart_time: cart_time,
                        restaurant_id: restaurant_id,
                    })
                        .then(function (response) {
                            // console.log(response);
                        })
                        .catch(function (error) {
                            console.log(error);
                        }); 
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });

                    return formatter.format(price) 
                },
                checkout: function (url) {
                    this.sendAjax();
                    var cart_time = moment('{{\Carbon\Carbon::now()->toDateString()}} ' + this.cart_time);
                    var delivery_time = '{{$restaurant->delivery_minutes}}';
                    if (delivery_time == '') {
                        delivery_time = 0;
                    }
                    var now = moment('{{\Carbon\Carbon::now()->toDateTimeString()}}');
                    var future_time = now.add(delivery_time, 'minutes');

                    var cart_date = this.cart_date;

                    if (cart_date == '') {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please select a valid date',
                            theme: 'error'
                        });
                        // this.cart_time = now;
                        return false;
                    }

                    if (future_time.isAfter(cart_time)) {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please select a time after ' + future_time.format('YYYY-MM-DD h:mm a'),
                            theme: 'error'
                        });
                        // this.cart_time = now;
                        return false;
                    }

                    if (this.delivery) {
                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            postcode: '',
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        })
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    setTimeout(function () {
                                        window.location.href = url;
                                    }, 1000)
                                } else {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Error occured. Please try again',
                                        theme: 'error'
                                    });
                                    return false;
                                }
                            })
                            .catch(function (error) {
                                if (error.response.status == 400) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: error.response.data.message,
                                        theme: 'error'
                                    });
                                    return false;
                                }
                                if (error.response.status == 419) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please login to continue',
                                        theme: 'error'
                                    });
                                    return false;
                                }

                                if (error.response.status == 402) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: error.response.data.message,
                                        theme: 'error'
                                    });
                                    window.location.href = '{{route('user.address',['delivery'=>true])}}'
                                    return false;
                                }
                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please login to continue!',
                                        theme: 'error'
                                    });
                                    return false;
                                }
                            });

                    } else if (this.takeaway) {
                        var $this = this;
                        axios.post('{{route('user.restaurant.validate-takeaway-order')}}', {
                            _token: '{{csrf_token()}}',
                            restaurant_id: '{{$restaurant->id}}',
                            cart_time: $this.cart_time,
                            cart_date: $this.cart_date,
                        })
                            .then(function (response) {
                                if (response.data.message == 'success') {
                                    setTimeout(function () {
                                        window.location.href = url;
                                    }, 1000)
                                } else {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Error occured. Please try again',
                                        theme: 'error'
                                    });
                                    return false;
                                }
                            })
                            .catch(function (error) {
                                if (error.response.status == 400) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: error.response.data.message,
                                        theme: 'error'
                                    });
                                    return false;
                                }


                                if (error.response.status == 419) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please login to continue',
                                        theme: 'error'
                                    });
                                    return false;
                                }
                                if (error.response.status == 401) {
                                    $.alert({
                                        title: '{{__('Oh Sorry!')}}',
                                        content: 'Please login to continute!',
                                        theme: 'error'
                                    });
                                    return false;
                                }
                            });

                    } else {
                        setTimeout(function () {
                            window.location.href = url;
                        }, 1000)
                    }
                }, 
                setFavourite: function (menu_item) {
                    const $this = this;
                    axios.post('{{route('user.favourite-menu-item')}}', {
                        _token: '{{csrf_token()}}',
                        menu_item_id: menu_item.id
                    })
                        .then(function (response) {
                            menu_item.favoured = response.data.status == 'added';
                            $.alert({title: 'Success!', content: response.data.message, theme: 'success'});
                        })
                        .catch(function (error) {
                            if (error.response.status == 401) {
                                $.alert({
                                    title: '{{__('Oh Sorry!')}}',
                                    content: 'Please Login to Continue!',
                                    theme: 'error'
                                });
                            }
                        });
                },
                createBooking: function () {

                    this.res_time = $("#timepicker").val(); 
                    var time = moment('{{\Carbon\Carbon::now()->toDateString()}} ' + this.res_time);
                    var delivery_time = '{{$restaurant->delivery_minutes}}';

                    var now = moment('{{\Carbon\Carbon::now()->toDateTimeString()}}');
                    var future_time = now.subtract(1, 'minutes');  
                    this.res_date = $("#datepicker").val(); 
                    var cart_date = this.res_date;

                    if (cart_date == '') { 
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Please select a valid date',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        return false;
                    } 

                    if (this.res_time == '') { 
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Please select a valid time',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        return false;
                    }

                    if (this.head_count == '') { 
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Please select a pax count',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        return false;
                    }

                    if (this.email == '') { 
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Please select an e-mail',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        return false;
                    }else{ 
                        const signup_email = this.email;
                        if (!validateEmail(signup_email)) { 
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: 'The email is not valied.',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                            return false;
                        }
                    } 

                    // if (this.phone == '') { 
                    //     $.toast({
                    //         heading: '{{__('Oh Sorry!')}}',
                    //         text: 'Please select a phone number',
                    //         showHideTransition: 'fade',
                    //         icon: 'error'
                    //     });
                    //     return false;
                    // }

                    if(this.res_time < this.defaultTime){  
                        $.toast({
                            heading: '{{__('Oh Sorry!')}}',
                            text: 'Please select the future time.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        return false;
                    }

                    const $this = this;
                    // alert($this.res_time);
                    axios.post('{{route('reservation.store')}}', {
                        _token: '{{csrf_token()}}',
                        restaurant_id: $this.restaurant.id,
                        head_count: $this.head_count,
                        phone: $this.phone,
                        email: $this.email,
                        date: $this.res_date,
                        time: $this.res_time,
                        requests: $this.special_request
                    }).then(function (response) {
                        if (response.data.message == 'success') { 
                            $this.reservation_data = response.data.data.reservation;
                            $this.reservation_restaurant = response.data.data.restaurant;
                            $this.reservation_settings = response.data.data.setting;
                            $this.step = 2;

                            swal("Booking Completed", "Please check your e-mail", "success").then((value) => {
                                window.location.href = '{{route('reservation.create')}}'
                            }); 

                        } else {
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: 'Save Failed!',
                                showHideTransition: 'fade',
                                icon: 'error'
                            }); 
                        }
                    }).catch(function (error) {
                        if (error.response.status == 422) {
                            // $this.reservation_validation = error.response.data.errors;
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: 'The number must be a valid telephone number in UK!',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                        }

                        if (error.response.status == 401) { 
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: 'Please Login to Continue!',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                        }
                        if (error.response.status == 400) { 
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: error.response.data.message,
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                        } 
                    });
                },
                confirmBooking: function () {
                    const $this = this;
                    axios.post('{{route('reservation.index')}}/' + $this.reservation_data.id, {
                        _token: '{{csrf_token()}}',
                        _method: 'put',
                        requests: $this.reservation_requests,
                        confirmed: true
                    }).then(function (response) {
                        if (response.data.message == 'success') {
                            $this.step = 3;

                            setTimeout(function () {
                                window.location.href = '{{route('reservation.thank.you')}}';
                            }, 3000);

                        } else {
                            $.alert({title: '{{__('Oh Sorry!')}}', content: 'Confirm Failed!', theme: 'error'});
                        }
                    }).catch(function (error) {
                        if (error.response.status == 422) {
                            // $this.reservation_validation = error.response.data.errors;
                            $.toast({
                                heading: '{{__('Oh Sorry!')}}',
                                text: 'The number must be a valid telephone number in UK!',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                        }

                        if (error.response.status == 401) {
                            $.alert({
                                title: '{{__('Oh Sorry!')}}',
                                content: 'Please Login to Continue!',
                                theme: 'error'
                            });
                        }
                    });
                },
                createReview: function () {
                    let review = this.review;
                    let rating = this.rating;
                    const restaurant = this.restaurant;

                    axios.post('{{route('review.store')}}', {
                        _token: '{{csrf_token()}}',
                        review: review,
                        rating: rating,
                        restaurant_id: restaurant.id
                    })
                        .then(function (response) {
                            if (response.data.message == 'success') {
                                restaurant.reviews = response.data.data.reviews;
                                restaurant.percentages = response.data.data.percentages;
                                restaurant.rating = response.data.data.rating;
                                $.alert({title: 'Success!', content: 'Saved Successfully!', theme: 'success'});
                            }
                        })
                        .catch(function (error) {
                            if (error.response.status == 422) {
                                $this.review_validation = error.response.data.errors;
                            }

                            if (error.response.status == 401) {
                                $.alert({
                                    title: '{{__('Oh Sorry!')}}',
                                    content: 'Please Login to Continue!',
                                    theme: 'error'
                                });
                            }
                        });
                },
                sortReviews: function () {
                    if (this.sort == 'desc') {
                        this.sort = 'asc';
                    } else {
                        this.sort = 'desc';
                    }
                    this.sorted_reviews = _.orderBy(this.restaurant.reviews, ['created_at'], [this.sort]);
                },
                dateValidation: function (event) {
                    var value = new Date(event.target.value);
                    var today = new Date('{{\Carbon\Carbon::now()->toFormattedDateString()}}');

                    if (value.getTime() < today.getTime()) {
                        $.alert({
                            title: '{{__('Oh Sorry!')}}',
                            content: 'Please select a valid future date!',
                            theme: 'error'
                        });
                    }
                }, 
                addVariant: function (menu_item, variant) {

                    // menu_item.selected_variant = variant.id;
                    //
                    // var show_variant = variant;
                    //
                    // if (show_variant) {
                    //     menu_item.show_variant = show_variant;
                    // }
                    //
                    this.addToCart(menu_item, variant);
                },
                countRemainingCharacters: function () {
                    this.remainingCount1 = 255 - this.requests.length;
                    this.remainingCount2 = 255 - this.requests.length;
                    this.remainingCount3 = 255 - this.reservation_requests.length;
                },
                preventKeyDown: function (event) {
                    event.preventDefault();
                }
            },
        });    

        $(document).ready(function($) {
            $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });
            $("#timepicker").timepicker({});
        });

        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        } 

    </script>
    <style type="text/css">
        @media (max-width: 992px) {
            .subslider {
                float: none !important;
            }
        }
    </style> 
@endsection
