
<div class="tab">
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.profile')}}')" id="defaultOpen">My Details</button>
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.address')}}')">My Address Book</button>
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.deliveries')}}')"> Deliveries</button> 
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.takeaways')}}')"> Takeaways</button> 
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.reservations')}}')"> Reservations</button> 
    <hr>
    <button class="tablinks" onclick="openCity(event, '{{route('user.favourites')}}')"> Favorites</button>
</div>

<!-- <hr class="breadcrumb-hr mydetail-bread">

<div class="row main-breadcrumb">
    <ul>
        <li @if($active=='orders') class="active" @endif>
            <a href="{{route('user.reservations')}}">My Orders & Reservations </a>
        </li>
        <li @if($active=='favourites') class="active" @endif>
            <a href="{{route('user.favourites')}}">My Favorites </a>
        </li>
        <li @if($active=='details') class="active" @endif>
            <a href="{{route('user.profile')}}">My Details </a>
        </li>
    </ul>
</div> -->
