<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />

    @if(Route::currentRouteName()=='user.home')
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('home_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.restaurant.menu')
        <title>{{ setting('menu_meta_title') }}</title>
        <meta name="description" content="{{ setting('menu_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.restaurant.reviews')
        <title>{{ setting('review_meta_title') }}</title>
        <meta name="description" content="{{ setting('review_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.about')
        <title>{{ setting('about_meta_title') }}</title>
        <meta name="description" content="{{ setting('about_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.contact')
        <title>{{ setting('contact_meta_title') }}</title>
        <meta name="description" content="{{ setting('contact_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.faq')
        <title>{{ setting('faq_meta_title') }}</title>
        <meta name="description" content="{{ setting('faq_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.terms')
        <title>{{ setting('terms_meta_title') }}</title>
        <meta name="description" content="{{ setting('terms_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.privacy')
        <title>{{ setting('privacy_meta_title') }}</title>
        <meta name="description" content="{{ setting('privacy_meta_description') }}">
    @else
        <title>{{$platform_name}}</title>
@endif


    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="google" content="notranslate">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('css/style.css?ver='.\Carbon\Carbon::now()->format('YmdHis'))}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('css/stylesheet.css?ver='.\Carbon\Carbon::now()->format('YmdHis'))}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive-s.css?rere')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-family.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/nouislider.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-clockpicker.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/all.min.css')}}"/>
    <!-- @if(setting('site_theme') == 'orange-peel')
        <link rel="stylesheet" type="text/css" href="{{asset('css/themes/orange_peel.css')}}"/>
    @endif
    @if(setting('site_theme') == 'whiskey')
        <link rel="stylesheet" type="text/css" href="{{asset('css/themes/whiskey.css')}}"/>
    @endif
    @if(setting('site_theme') == 'thunderbird')
        <link rel="stylesheet" type="text/css" href="{{asset('css/themes/thunderbird.css')}}"/>
    @endif
    @if(setting('site_theme') == 'amber')
        <link rel="stylesheet" type="text/css" href="{{asset('css/themes/amber.css')}}"/>
    @endif
    @if(setting('site_theme') == 'apple')
        <link rel="stylesheet" type="text/css" href="{{asset('css/themes/apple.css')}}"/>
    @endif -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css?ver=1.0')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:light' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Kaushan Script' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Bebas Neue' rel='stylesheet'>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- <script defer src="{{asset('js/bootstrap.min.js')}}"></script> -->

    <script src="{{asset('js/vue.js')}}"></script>
    <script src="{{asset('js/axios.min.js')}}"></script>
    <script defer src="{{asset('js/nouislider.min.js')}}"></script>
    <script src="{{asset('js/lodash.js')}}"></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-clockpicker.min.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>

    <link rel="shortcut icon" type="image/png" href="{{ asset('storage/'.setting('favicon')) }}"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>

{{--    <script src="https://js.stripe.com/v3/"></script>--}}

<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        // window.addEventListener('load', function () {
        //     jQuery('.timepicker input').timepicker({
        //         minuteStep: 1
        //     });
        // });
    </script>

    <script type="text/javascript">
        Vue.component('star-rating', {
            template: '<span><i v-for="i in parseInt(rating)" class="fa fa-star active" aria-hidden="true"></i>' +
                '<i v-for="i in 5-parseInt(rating)" class="fa fa-star" aria-hidden="true"></i></span>',
            props: ['rating'],
            mounted: function () {

            },
            watch: {},
            destroyed: function () {

            }
        });
    </script>

    <script type="text/javascript">
        Vue.component('date-picker', {
            template: '<input type="text" :id="id" :name="id" :placeholder="placeholder" autocomplete="false" readonly="true" :value="value" @input="$emit(\'input\',$event.target.value)"/>',
            props: ['id', 'placeholder', 'value'],
            mounted: function () {
                var vm = this;
                // jQuery(this.$el)
                //     .datepicker({
                //         format: '{{ setting('date_format') }}'+' DD',
                //         todayHighlight: true,
                //         autoclose: true,
                //         closeOnDateSelect: true,
                //         startDate: "today"
                //     })
                //     .trigger('change')
                //     .on('changeDate', function () {
                //         vm.$emit('input', this.value);
                //     });
            },
            watch: {
                value: function (value) {
                    jQuery(this.$el)
                        .val(value)
                        .trigger('change');
                },
            },
            destroyed: function () {

            }
        });

        Vue.component('time-picker', {
            template: '<input type="text" :id="id" :name="id" :placeholder="placeholder" autocomplete="false" :value="value" @input="$emit(\'input\',$event.target.value)" readonly="true"/>',
            props: ['id', 'placeholder', 'value'],
            mounted: function () {
                var vm = this;

                jQuery(this.$el).timepicker({
                    minuteStep: 1
                }).trigger('change')
                    .on('changeTime.timepicker', function () {
                        vm.$emit('input', this.value);
                        vm.$emit('validate');
                    });
            },
            watch: {
                value: function (value) {
                    jQuery(this.$el)
                        .val(value)
                        .trigger('change');
                },
            },
            destroyed: function () {

            }
        });
    </script>
</head>
<body> 

<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '{{ setting('google_analytics_id') }}', 'auto');
    ga('send', 'pageview');
</script>
<!-- End Google Analytics -->  

<header>
    <div id="restaurantHome">
          <div class="container">
          <div class="top-nav">
            <ul>
              <li><i class="fa fa-phone"></i><a href="tel:{{$restaurant->phone}}">{{$restaurant->phone}}</a></li>
              <li><i class="fa fa-envelope-o"></i><a href="mailto:{{$restaurant->email}}">{{$restaurant->email}}</a></li>
            </ul>
          </div>
        </div>
           <nav class="navbar navbar-expand-lg navbar-light " style="margin-bottom: -1px;border-radius: 0px;">
            <div class="container">
            <a class="navbar-brand" href=""><img src="img/logo.png" class="img-fluid"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon" style="width: 30px; height: 2.5px; vertical-align: middle; background: #fff;"></span>
              <span class="navbar-toggler-icon" style="width: 40px; height: 2.5px; vertical-align: middle; background: #fff;"></span>
              <span class="navbar-toggler-icon" style="width: 30px; height: 2.5px; vertical-align: middle; background: #fff;"></span>
            </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link home" href="">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link services" href="{{route('user.restaurant.menu')}}">Menu</a>
              </li>
              <li class="nav-item">
                <a class="nav-link project" href="">Special Offers</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link blog" href="">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link about" href="">locations</a>
              </li>
              <li class="nav-item">
                <a class="nav-link con" href="">Contact Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link con"><span style="margin-left: 20px;">{{ setting('currency')}} @{{ currency(total) }}</span></a>
              </li>
            </ul>
          </div>
          </div>
        </nav>
    </div>
  </header> 

<div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
            </div>
        </div>
    </div>
    @yield('content')
</div> 
<footer>
  <div class="top_footer">
    <div class="container">
      <div class="row">
        <div class="col-md-2">
          <img src="img/footer_logo.png" class="img-fluid footer_logo">
        </div>
        <div class="col-md-3">
          <h1>About Fonix pizza</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <ul>
            <li>
                <a target="_blank" href="https://www.facebook.com/fonixpizza/"><i class="fa fa-facebook-f"></i></a>
            </li>
            <li>
                <a target="_blank" href="https://twitter.com/fonix_pizza"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a target="_blank" href="https://www.instagram.com/fonixpizza/"><i class="fa fa-instagram"></i></a>
            </li>
        </ul>
        </div>
        <div class="col-md-2">
          <h1>information</h1>
          <p class="ttu">About</p>
          <p class="ttu">Delivery Information</p>
          <p class="ttu">privacy policy</p>
          <p class="ttu">terms & conditions</p>
          <p class="ttu">contact us</p>
          <p class="ttu">blog</p>
        </div>
        <div class="col-md-2">
          <h1>Categories</h1>
          <p class="ttu">pizza</p>
          <p class="ttu">burgers</p>
          <p class="ttu">salads</p>
          <p class="ttu">drinks</p>
          <p class="ttu">starters</p>
          <p class="ttu">seafood</p>
        </div>
        <div class="col-md-3">
          <h1>get in touch</h1>
          <h4>Restaurant Fonix Pizza <br> & Grill - Pizz</h4>
          <P>Amfi Eursenteret,</P>
          <p>Vormsund,160</p>
          <p>+36 72 332 010</p>
          <p>fonixpizza.com</p>
          <div class="open">
            <h5>Open</h5>
            <p>Monday to Friday 9.00am - 5.00 pm</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copy_right">
    <p>All right reserved@fonixpizza.com  |  Privacy Policy</p>
  </div>
</footer>

@guest
    <!-- Sign up Modal -->
    <div class="modal fade" id="signupModal" role="dialog">
        @include('includes.progress_loader')
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>Welcome to {{ $restaurant_info->name }}</h2>
                    <form class="signin-form" action="{{route('auth.sign-up.validate')}}"
                          @submit.prevent="signUp($event)" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.first_name}">
                                    <label>First Name*</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                           placeholder="Enter First Name">
                                    <span v-if="signup.first_name" class="help-block" role="alert">
                                        <strong>@{{ signup.first_name[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.last_name}">
                                    <label>Last Name*</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                           placeholder="Enter Last Name">
                                    <span v-if="signup.last_name" class="help-block" role="alert">
                                        <strong>@{{ signup.last_name[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.email}">
                                    <label>Enter Email*</label>
                                    <input type="email" class="form-control" id="signup-email" name="email"
                                           placeholder="Enter Email Address">
                                    <span v-if="signup.email" class="help-block" role="alert">
                                        <strong>@{{ signup.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.postcode}">
                                    <label>Enter Postcode*</label>
                                    <input type="text" class="form-control" id="signup-postcode" name="postcode"
                                           placeholder="Enter Postcode">
                                    <span v-if="signup.postcode" class="help-block" role="alert">
                                        <strong>@{{ signup.postcode[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.password}">
                                    <label>Enter Password*</label>
                                    <input type="password" class="form-control" id="signup-password" name="password"
                                           placeholder="Enter Password">
                                    <span v-if="signup.password" class="help-block" role="alert">
                                        <strong>@{{ signup.password[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.password_confirmation}">
                                    <label>Confirm Password*</label>
                                    <input type="password" class="form-control" id="cpassword"
                                           name="password_confirmation"
                                           placeholder="Re-enter Password">
                                    <span v-if="signup.password_confirmation" class="help-block" role="alert">
                                        <strong>@{{ signup.password_confirmation[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Create Account" class="btn btn-signin">
                            </div>
                        </div>
                        <hr class="yellow-hr">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Sign in Modal -->
    <div class="modal fade" id="signinModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>{{__('Sign In')}}</h2>
                    <form class="signin-form" action="{{route('login')}}" @submit.prevent="signIn($event)">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.email}">
                                    <label>Email*</label>
                                    <input type="email" class="form-control" id="signin-email"
                                           placeholder="Enter Email Address">
                                    <span v-if="signin.email" class="help-block" role="alert">
                                        <strong>@{{ signin.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.password}">
                                    <label>Enter Password*</label>
                                    <input type="password" class="form-control" id="signin-password"
                                           placeholder="Enter Password">
                                    <span v-if="signin.password" class="help-block" role="alert">
                                        <strong>@{{ signin.password[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="submit" value="{{__('Sign In')}}" class="btn btn-signin">
                                @if(setting('login_as_guest'))
                                    <br>
                                    <br>
                                    <span class="no-account">
                                        <a href="{{route('user.guest')}}" style="font-size: 16px;vertical-align: middle;">Login as a guest</a>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr class="yellow-hr">
                        <div class="no-account">
                            <h4>Don’t have an account?
                                <a href="#" id="join-us" data-toggle="modal" data-target="#signupModal"> Join Us</a></h4>
                            <label class="forgot-password">
                                <a href="" data-toggle="modal" data-target="#forgotPasswordModal">Forgot Password!</a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        window.addEventListener('load', function (event) {
            document.getElementById('join-us').addEventListener('click', function () {
                jQuery('#signinModal').modal('hide');
            });
        });
    </script>

    <!-- Reset Password Modal -->
    <div class="modal fade" id="forgotPasswordModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>Reset Password</h2>
                    <form class="signin-form" action="{{ route('password.email') }}"
                          @submit.prevent="resetPassword($event)">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':reset_password.email}">
                                    <label>Email*</label>
                                    <input type="email" class="form-control" id="reset-password-email"
                                           placeholder="Enter Email Address">
                                    <span v-if="reset_password.email" class="help-block" role="alert">
                                        <strong>@{{ reset_password.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Reset Password" class="btn btn-signin">
                            </div>
                        </div>
                        <hr class="yellow-hr">
                    </form>
                </div>
            </div>

        </div>
    </div>

@endguest

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script> -->
    
<script type="text/javascript">
    // window.addEventListener('load', function () {
    //     jQuery('.clockpicker').clockpicker({
    //         autoclose: true,
    //         'default': 'now'
    //     });
    // })
</script>

<script>
    function showDropDelivery() {
        var element = document.getElementById('dropdown-content-delivery');
        if (element.style.display == 'none') {
            element.style.display = 'block';
        } else {
            element.style.display = 'none';
        }
    }
</script>


<script>
    $(document).ready(function () {
        $('img[src$=".svg"]').each(function () {
            var $img = jQuery(this);
            var imgURL = $img.attr('src');
            var attributes = $img.prop("attributes");

            $.get(imgURL, function (data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Remove any invalid XML tags
                $svg = $svg.removeAttr('xmlns:a');

                // Loop through IMG attributes and apply on SVG
                $.each(attributes, function () {
                    $svg.attr(this.name, this.value);
                });

                // Replace IMG with SVG
                $img.replaceWith($svg);
            }, 'xml');
        });
    });
</script>
@guest
    <script type="text/javascript">
        var data = {
            signup: {
                first_name: false,
                last_name: false,
                email: false,
                password: false,
                postcode: false,
            },
            signin: {
                email: false,
                password: false
            },
            reset_password: {
                email: false,
            },
            loading: false
        };  

        var signup = new Vue({
            data: data,
            el: '#signupModal',
            methods: {
                signUp: function (event) {
                    this.loading = true;
                    var action = event.target.action;

                    var target = event.target;

                    var $this = this;

                    this.signup = {
                        first_name: false,
                        last_name: false,
                        email: false,
                        password: false,
                        postcode: false,
                    };

                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        first_name: target.querySelector('#first_name').value,
                        last_name: target.querySelector('#last_name').value,
                        email: target.querySelector('#signup-email').value,
                        password: target.querySelector('#signup-password').value,
                        postcode: target.querySelector('#signup-postcode').value,
                        password_confirmation: target.querySelector('#cpassword').value,
                    })
                        .then(function (response) {
                            target.action = '{{route('register')}}';
                            target.submit();
                            $this.loading = false;
                        })
                        .catch(function (error) {
                            $this.loading = false;
                            if (error.response.status == 422) {
                                var errors = error.response.data.errors;
                                if (typeof errors.first_name != 'undefined') {
                                    $this.signup.first_name = errors.first_name;
                                }
                                if (typeof errors.last_name != 'undefined') {
                                    $this.signup.last_name = errors.last_name;
                                }
                                if (typeof errors.email != 'undefined') {
                                    $this.signup.email = errors.email;
                                }
                                if (typeof errors.password != 'undefined') {
                                    $this.signup.password = errors.password;
                                }
                                if (typeof errors.postcode != 'undefined') {
                                    $this.signup.postcode = errors.postcode;
                                }

                            }
                        });
                }
            }
        });

        var singin = new Vue({
            data: data,
            el: '#signinModal',
            methods: {
                signIn: function (event) {
                    var action = event.target.action;

                    var target = event.target;

                    var $this = this;

                    this.signin = {
                        email: false,
                        password: false
                    };

                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        email: target.querySelector('#signin-email').value,
                        password: target.querySelector('#signin-password').value,
                    })
                        .then(function (response) {
                            window.location.reload();
                        })
                        .catch(function (error) {
                            if (error.response.status == 422) {
                                var errors = error.response.data.errors;
                                if (typeof errors.email != 'undefined') {
                                    $this.signin.email = errors.email;
                                }
                                if (typeof errors.password != 'undefined') {
                                    $this.signin.password = errors.password;
                                }
                            }
                        });
                }
            }
        });
        var reset_password = new Vue({
            data: data,
            el: '#forgotPasswordModal',
            methods: {
                resetPassword: function (event) {
                    var action = event.target.action;

                    var target = event.target;

                    var $this = this;

                    this.reset_password = {
                        email: false,
                    };

                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        email: target.querySelector('#reset-password-email').value,
                    })
                        .then(function (response) {
                            // window.location.reload();
                            $.alert({
                                title: 'Success!',
                                content: 'Please check email for password reset instructions',
                                theme: 'success'
                            });
                        })
                        .catch(function (error) {
                            if (error.response.status == 422) {
                                var errors = error.response.data.errors;
                                if (typeof errors.email != 'undefined') {
                                    $this.reset_password.email = errors.email;
                                }
                            }
                        });
                }
            }
        });
    </script>
@endguest

<script type="text/javascript">

    var data = {  
            menus: '',
            deals: '',
            cuisines: '',
            selected_menu_items: [],
            style: 'list',
            reservation: false,
            delivery: false,
            takeaway: false,
            cart: {!! json_encode( Session::get('cart') ) !!},
            total: 0,
            vat: '',
            vat_value: '',
            sub_total: 0,
            requests: '',
            cart_date: '',
            cart_time: '',
            restaurant:{!! json_encode($restaurant) !!},
            favoured: false,
            menu_id: 0,
            type: '', 
            date: '{{\Carbon\Carbon::now()->toDateString()}}',
            time: '{{\Carbon\Carbon::now()->format('h:i A')}}',
            head_count: '',
            phone: '',
            email: '',
            reservation_validation: {},
            review_validation: {},
            step: 1,
            reservation_data: [],
            reservation_requests: '',
            rating: 5,
            review: '',
            sorted_reviews: [],
            sort: 'desc',
            variants: [],
            addons: [],
            menu_item: {},
            selected_addons: [],
            selected_variant: {},
            reservation_restaurant: {},
            reservation_settings: {},
            guest: false,
            remainingCount1: 255,
            remainingCount2: 255,
            remainingCount3: 255,
            loading: false,
            deals_menus: {},
            selected_deal_items: {},
            selectedDealItem: {},
            deal_id: '',
            deal_name: '',
            deal_price: '',
            deal_items: [],
        }; 
        // console.log(data.cart);
        // alert();

        var restaurantHome = new Vue({
            el: '#restaurantHome',
            data: data,
            mounted: function () { 

                this.getTotals();
 
            },
            methods: { 
                getTotals: function () {
                    this.total = 0;

                    this.sub_total = 0;

                    const $this = this;

                    var vat = 0;

                    this.cart.forEach(function (item) {
                        var price = item.price;


                        if (item.show_variant) {
                            price = item.show_variant.pivot.price;
                        }

                        var total_item_price = parseFloat(price);

                        if (item.show_addons) {
                            item.show_addons.forEach(function (addon) {
                                $this.total += parseFloat(addon.pivot.price);
                                $this.sub_total += parseFloat(addon.pivot.price);
                                total_item_price += parseFloat(addon.pivot.price);
                            });
                        }

                        $this.total += item.quantity * price;
                        $this.sub_total += item.quantity * price;

                        var vat_percentage = 0;


                        switch (item.vat_category) {
                            case 'food':
                                vat_percentage = $this.vat.food;
                                break;
                            case 'alcohol':
                                vat_percentage = $this.vat.alcohol;
                                break;
                        }

                        vat += total_item_price * vat_percentage * item.quantity;
                    });

                    this.vat_value = vat;
                    //alert(this.vat_value);
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });

                    return formatter.format(price)

                },
            }
        });

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                axios.post('{{route('user.location')}}', {
                    _token: '{{csrf_token()}}',
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                }).then(function (response) {

                });
            });
        }
    }

    window.addEventListener('load', function () {
        getLocation();
    });
</script>

@if ($errors->any())
    <script type="text/javascript">
        window.addEventListener('load', function (ev) {
            $.alert({
                title: '{{__('Oh Sorry!')}}',
                content: '<ul>@foreach ($errors->all() as $error) <li>{{ __($error) }} </li>@endforeach</ul>',
                theme: 'error'
            });
        });
    </script>
@endif

@if (session('success')!=null)
    <script type="text/javascript">
        window.addEventListener('load', function (ev) {
            $.alert({
                title: 'Success!',
                content: ' {!! session('success')  !!}  ',
                theme: 'success'
            });
        });
    </script>
@endif


<script type="text/javascript">
    window.addEventListener('load', function () {
        // jQuery('.date').datepicker({
        //     format: '{{ setting('date_format') }}',
        //     todayHighlight: true,
        //     autoclose: true,
        //     closeOnDateSelect: true,
        //     startDate: "today"
        // });

        // document.querySelector('.loader').classList.remove('show');
    });
</script>
</body>
</html>

