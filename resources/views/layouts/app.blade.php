<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" /> -->

    @if(Route::currentRouteName()=='user.home')
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('home_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.restaurant.menu')
        <!-- <title>{{ setting('menu_meta_title') }}</title> -->
        <!-- <title>{{$platform_url}}/@yield('title')</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('menu_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.restaurant.reviews')
        <!-- <title>{{ setting('review_meta_title') }}</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('review_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.about')
        <!-- <title>{{ setting('about_meta_title') }}</title> -->
        <!-- <title>{{$platform_url}}/about-us</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('about_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.contact')
        <!-- <title>{{ setting('contact_meta_title') }}</title> -->
        <!-- <title>{{$platform_url}}/contact-us</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('contact_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.faq')
        <!-- <title>{{ setting('faq_meta_title') }}</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('faq_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.terms')
        <!-- <title>{{ setting('terms_meta_title') }}</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('terms_meta_description') }}">
    @elseif(Route::currentRouteName()=='user.privacy')
        <!-- <title>{{ setting('privacy_meta_title') }}</title> -->
        <!-- <title>{{$platform_url}}/privacy-policy</title> -->
        <title>{{ setting('home_meta_title') }}</title>
        <meta name="description" content="{{ setting('privacy_meta_description') }}">
    @else
        <!--<title>{{$platform_name}}</title>-->
        <!-- <title>{{$platform_url}}/@yield('title')</title> -->
        <title>{{ setting('home_meta_title') }}</title>
    @endif  

    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="google" content="notranslate"> 

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="viewport"
        content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, target-densitydpi=device-dpi"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="true">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i|Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.cdnfonts.com/css/kaushan-script" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300" rel="stylesheet">

    <!-- Main Stylesheet File -->  
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('css/testimonial.css')}}" rel="stylesheet">
    <link href="{{asset('css/footer.css')}}" rel="stylesheet">
    <link href="{{asset('fonts_new/Montserrat/stylesheet.css')}}" rel="stylesheet">

    <!-- Testimonial -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://themes.audemedia.com/html/goodgrowth/css/owl.theme.default.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js" type="text/javascript"></script>

    <!-- clockpicker -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-clockpicker.min.css')}}"/> 

    <!-- Toast -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.css')}}"/>

    <script src="{{asset('js/testimonial.js')}}"></script>
    <script src="{{asset('js/plus_minus.js')}}"></script> 
    <!-- stars -->
    <script src="{{asset('js/star.js')}}"></script>
    <!-- Modernizr -->
    <script src="{{asset('js/modernizr.js')}}"></script>
    <script src="{{asset('js/vue.js')}}"></script>
    <script src="{{asset('js/axios.min.js')}}"></script> 
    <script src="{{asset('js/lodash.js')}}"></script> 
    <script src="{{asset('admin/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-clockpicker.min.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script> 
    <!-- stripe -->
    <script src="https://js.stripe.com/v3/"></script>

    <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon-96x96.png') }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script> 
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"> 
    <!-- Latest compiled and minified JavaScript -->
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script> 
    <script type="text/javascript">
        Vue.component('star-rating', {
            template: '<span><i v-for="i in parseInt(rating)" class="fa fa-star active" aria-hidden="true"></i>' +
                '<i v-for="i in 5-parseInt(rating)" class="fa fa-star" aria-hidden="true"></i></span>',
            props: ['rating'],
            mounted: function () {

            },
            watch: {},
            destroyed: function () {

            }
        });
    </script> 
    <script type="text/javascript">
        Vue.component('date-picker', {
            template: '<input type="text" :id="id" :name="id" :placeholder="placeholder" autocomplete="false" readonly="true" :value="value" @input="$emit(\'input\',$event.target.value)"/>',
            props: ['id', 'placeholder', 'value'],
            mounted: function () {
                var vm = this;
                // jQuery(this.$el)
                //     .datepicker({
                //         format: '{{ setting('date_format') }}'+' DD',
                //         todayHighlight: true,
                //         autoclose: true,
                //         closeOnDateSelect: true,
                //         startDate: "today"
                //     })
                //     .trigger('change')
                //     .on('changeDate', function () {
                //         vm.$emit('input', this.value);
                //     });
            },
            watch: {
                value: function (value) {
                    jQuery(this.$el)
                        .val(value)
                        .trigger('change');
                },
            },
            destroyed: function () {

            }
        });

        Vue.component('time-picker', {
            template: '<input type="text" :id="id" :name="id" :placeholder="placeholder" autocomplete="false" :value="value" @input="$emit(\'input\',$event.target.value)" readonly="true"/>',
            props: ['id', 'placeholder', 'value'],
            mounted: function () {
                var vm = this;

                jQuery(this.$el).timepicker({
                    minuteStep: 1
                }).trigger('change')
                    .on('changeTime.timepicker', function () {
                        vm.$emit('input', this.value);
                        vm.$emit('validate');
                    });
            },
            watch: {
                value: function (value) {
                    jQuery(this.$el)
                        .val(value)
                        .trigger('change');
                },
            },
            destroyed: function () {

            }
        });
    </script>
</head>
<body>  
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '{{ setting('google_analytics_id') }}', 'auto');
    ga('send', 'pageview');
</script>
<!-- End Google Analytics -->    
<div class="container-fluid" id="restaurantHome">
  <div class="row" id="menu-header"> 
    <!---- nav top bar section ---->
    <div class="row container-fluid" id="nav-top-bar">
        <div class="col-sm-12 col-md-12 col-lg-12 no-left-pd no-right-pd">
            @if(Route::currentRouteName()=='login')
                <a href="tel://+44 203 773 3854">
                    <img src="{{ asset('img/Phone.png') }}" alt="Image">+44 203 773 3854
                </a>
                <a href="mailto:admin@codelantic.com">
                    <img src="{{ asset('img/mail.png') }}" alt="Image">admin@codelantic.com
                </a>
            @else
                <a href="tel://+44 203 773 3854">
                    <img src="{{ asset('img/Phone.png') }}" alt="Image">{{$restaurant->phone}} 
                </a>
                <a href="mailto:{{$restaurant->email}}">
                    <img src="{{ asset('img/mail.png') }}" alt="Image">{{$restaurant->email}}
                </a>
            @endif  
            <!--<span class="search-input" href="#contact"> -->
            <!--    <input type="text" placeholder=" " name="search">-->
            <!--    <i class="fa fa-search"></i>-->
            <!--</span> -->
        </div> 
    </div>
    <!---- //nav top bar section ----> 
    <!---- navigation bar section ---->
    <nav class="navbar" id="navbar">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
            <i class="fa fa-bars" aria-hidden="true"></i> 
          </button>
          <a class="comp-logo" href="{{route('user.home')}}">
            <img class="img-responsive" src="{{ asset('img/logo.png') }}" id="logo" alt="logo">
        </a>
        </div>
        <div class="collapse navbar-collapse" id="mainNavbar">
          <ul class="nav navbar-nav @if(\Illuminate\Support\Facades\Auth::user()) logged-nav @endif">
            <li><a class="home_header" href="{{route('user.home')}}">HOME</a></li>
            <li><a class="menu_header" href="{{route('user.restaurant.menu')}}">MENU</a></li>
            <!-- <li><a class="offers_header" href="{{route('user.restaurant.offers')}}">SPECIAL OFFERS</a></li> -->
            <!--<li><a class="deals_header" href="{{route('user.restaurant.deals')}}">SPECIAL OFFERS</a></li>-->
            <li><a class="about_header" href="{{route('user.about')}}">ABOUT</a></li>
            <li><a class="location_header" href="{{route('user.location')}}">LOCATIONS</a></li>
            @if(setting('static_site') == 0) 
                <li>
                    <a><button onclick="window.location.href='{{route("reservation.create")}}'" type="button" class="btn rounded-btn">RESERVATIONS</button></a>
                </li>   
                <li>
                    <a href="{{route('user.restaurant.menu')}}">
                        <button type="button" class="btn rounded-btn cart-btn">
                        <img src="{{ asset('img/bag.png') }}" class="white-bag" alt="Cart Image">
                        <img src="{{ asset('img/bag2.png') }}" class="red-bag" alt="Cart Image">
                        {{ setting('currency') }} <span id="total-val"></span> 
                    </button>
                    </a>
                </li>
            @endif 
            @guest
                <li><a href="{{route('login')}}"><img src="{{ asset('img/user.png') }}" class="user-icon" alt="user"></a></li> 
            @else 
                <li><a><button onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();" type="button" class="btn rounded-btn">Log Out</button></a></li>   
                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    @csrf
                </form>
                @if(\Illuminate\Support\Facades\Auth::user()->email != setting('guest_email_id'))
                    <li><a href="{{route('user.profile')}}"><img src="{{ asset('img/user.png') }}" class="user-icon" alt="user"></a></li> 
                @endif 
            @endguest 
          </ul> 
        </div>
    </nav> 
    <!---- //navigation bar section ----> 
    {{-- <nav class="tab-only"> 
        @if(Route::currentRouteName()=='login')        
          <div class="topnavT">
            <a href="#home" class="phone-num"><img src="images/phone.png" id="img-phone" alt="phone">+44 203 773 3854</a>
            <a href="#news"><img src="images/mail.png" id="img-phone" alt="mail">admin@codelantic.com </a>
            <a href="#contact"> <input type="text" id="search" placeholder=" " name="search">
              <i class="fa fa-search"></i>
            </a> 
          </div> 
        @else
          <div class="topnavT">
            <a href="#home" class="phone-num"><img src="images/phone.png" id="img-phone" alt="phone">{{$restaurant->phone}}</a>
            <a href="#news"><img src="images/mail.png" id="img-phone" alt="mail">{{$restaurant->email}}</a>
            <a href="#contact"> <input type="text" id="search" placeholder=" " name="search">
              <i class="fa fa-search"></i>
            </a> 
          </div>  
        @endif 
      <div class="topnavb" id="myTopnavBar">
        <a href="#home"><img class="img-responsive" src="img/logo.png" id="logo" alt="logo"></a> 
        <a href="{{route('user.profile')}}"><img src="{{ asset('img/user.png') }}" id="user" alt="user"></a>
        <a class="navbar-a active home_header" href="{{route('user.home')}}">HOME</a>
        <a class="navbar-a menu_header" href="{{route('user.restaurant.menu')}}">MENU</a>
        <!-- <a class="navbar-a" href="{{route('user.restaurant.offers')}}">SPECIAL OFFERS</a> -->
        <!--<a class="navbar-a" href="{{route('user.restaurant.deals')}}">SPECIAL OFFERS</a>-->
        <a class="navbar-a" href="{{route('user.about')}}">ABOUT</a>
        <a class="navbar-a" href="{{route('user.location')}}">LOCATIONS</a> 
        <a href="javascript:void(0);" class="icon" onclick="tabFunction()">
          <i class="fa fa-bars"></i>
        </a>
      </div>
        @if(setting('static_site') == 0)
          <!-- <a href="reservations.html"><button type="button" class="btn" id="reservation">RESERVATIONS</button></a>
          <a href="shopping_cart.html"><button type="button" class="btn btn-shopping"><img src="images/bag.png" id="shopping" alt="button">$ 125.00</button></a> --> 
            <li>
                <a><button onclick="window.location.href='{{route("reservation.create")}}'" type="button" class="btn" id="reservation">RESERVATIONS</button></a>
            </li> 
            <li>
                <!-- <a href="{{route('user.restaurant.menu')}}"><button type="button" class="btn btn-shopping"><img src="{{ asset('img/bag2.png') }}" id="shopping" alt="button">{{ setting('currency') }} @{{ currency(total+vat_value+deliveryCost) }}</button></a> -->
                <a href="{{route('user.restaurant.menu')}}"><button type="button" class="btn btn-shopping"><img src="{{ asset('img/bag.png') }}" id="shopping" alt="button">{{ setting('currency') }} <span id="total-val"></span> <!--@{{ currency(total+vat_value) }}--></button></a>
            </li>
        @endif 
        @guest
            <!--<li><a href="{{route('login')}}"><img src="{{ asset('img/user.png') }}" id="user" alt="user"> </a></li> -->
        @else 
            <li><a><button onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();" type="button" class="btn log-out-reservation" id="reservation">LogOut</button></a></li>   
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
            @if(\Illuminate\Support\Facades\Auth::user()->email!= setting('guest_email_id'))
                <!--<li><a href="{{route('user.profile')}}"><img src="{{ asset('img/user.png') }}" id="user" alt="user"></a></li> -->
            @endif 
        @endguest 
      </nav>  --}}
  </div>
</div>
<div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif 
                @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                @endif
            </div>
        </div>
    </div>
    @yield('content')
</div>  
<!---- footer section ---->
<section id="footer">
    <div class="row top-container">
        <div class="col-sm-12 col-md-2 col-lg-2 mob-hide">
            <a class="site-logo" href="#">
                <img class="img-responsive" src="{{ asset('img/footer_logo.png') }}" alt="logo">
            </a>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3 mob-hide">
            <h4 class="col-title">About Namaste India</h4>
            <p class="about-txt">Namaste India is a fully licensed Indian restaurant operating in UK. Here at Namaste India, we happily serve authentic Indian Cuisine such as: North Indian, South Indian and Vegetarian food, we use the freshest ingredients to make sure your experience with us is delightful.</p>
            <div class="social-links">
                <div class="social-links"> 
                    <a href="https://www.facebook.com/Namaste-India-London-107783867366096/"><i class="fa fa-facebook-f"></i></a>
                    <a href="https://www.instagram.com/namasteindialondon/"><i class="fa fa-instagram"></i></a>
                    <a href="https://twitter.com/NamasteindiaUk"><i class="fa fa-twitter"></i></a> 
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <h4 class="col-title collapsible">Information <i class="fa fa-angle-down" aria-hidden="true"></i></h4>
            <ul class="ft-links collapse-content">
                <li><a href="{{ route('user.about') }}"> About Us</a></li>
                <li><a href="{{ route('user.faq') }}"> FAQ</a></li>
                <!--<li><a href="{{ route('user.privacy') }}"> Privacy Policy</a></li>-->
                <li><a href="{{ route('user.terms') }}"> Terms & Conditions</a></li>
                <li><a href="{{ route('user.contact') }}"> Contact Us</a></li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-2 ft-col">
            <h4 class="col-title collapsible">Categories <i class="fa fa-angle-down" aria-hidden="true"></i></h4>
            <ul class="ft-links collapse-content">
                <?php $m = 0; ?>
                @foreach ($restaurant_menus as $res_menu)
                    <?php if($m < 5){ ?>
                        <li><a style="text-transform:uppercase;" href="{{ route('user.restaurant.menu', ['menu'=> $res_menu->id]) }}">
                            {{ $res_menu->name }}</a></li>
                    <?php
                        $m++;
                    }
                    ?>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-12 col-md-3 ft-col">
            <h4 class="col-title collapsible">Get In Touch <i class="fa fa-angle-down" aria-hidden="true"></i></h4>
            <div class="collapse-content">
            @if (Route::currentRouteName() == 'login')
                <span class="addr-name">Namaste India</span>
            @else
                <span class="addr-name">{{ $restaurant->name }}</span>
            @endif  
            <span>{{ $restaurant->county }} {{ $restaurant->city }}, </span> 
            <span>{{ $restaurant->country }},</span>
            <span>{{ $restaurant->province }} {{ $restaurant->postcode }}</span> 
            <h6 class="col-sub-title"> Open </h6>
            <span class="time-txt">Today {{ $opening_time }} - {{ $closing_time }}</span>
            </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3 mob-about-sec mob-display ft-col">
            <a class="site-logo" href="#">
                <img class="img-responsive" src="{{ asset('img/footer_logo.png') }}" alt="logo">
            </a>
            <h4 class="col-title">About Food Lover</h4>
            <p class="about-txt">Namaste India is a fully licensed Indian restaurant operating in UK. Here at Namaste India, we happily serve authentic Indian Cuisine such as: North Indian, South Indian and Vegetarian food, we use the freshest ingredients to make sure your experience with us is delightful.</p>
            <div class="social-links">
                <a href="https://www.facebook.com/Namaste-India-London-107783867366096/"><i class="fa fa-facebook-f"></i></a>
                <a href="https://www.instagram.com/namasteindialondon/"><i class="fa fa-instagram"></i></a>
                <a href="https://twitter.com/NamasteindiaUk"><i class="fa fa-twitter"></i></a> 
            </div>
        </div>
    </div>
    <div class="row bottom-container">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script> | All rights reserved @ restaurant.com</p>
        </div>
    </div>
</section>
<!---- //footer section ----> 
@guest
    <!-- Sign up Modal -->
    <div class="modal fade" id="signupModal" role="dialog" style="z-index: 99999;">
        @include('includes.progress_loader')
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h4>Welcome to {{ $restaurant_info->name }}</h4>
                    <form class="signin-form" action="{{route('auth.sign-up.validate')}}"
                          @submit.prevent="signUp($event)" method="post" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.first_name}">
                                    <label>First Name*</label>
                                    <input autocomplete="off" type="text" onkeypress="return /[a-z]/i.test(event.key)" class="form-control" id="first_name" name="first_name"
                                           placeholder="Enter First Name" maxlength="100">
                                    <span v-if="signup.first_name" class="help-block" role="alert">
                                        <strong>@{{ signup.first_name[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.last_name}">
                                    <label>Last Name*</label>
                                    <input autocomplete="off" type="text" onkeypress="return /[a-z]/i.test(event.key)" class="form-control" id="last_name" name="last_name"
                                           placeholder="Enter Last Name" maxlength="100">
                                    <span v-if="signup.last_name" class="help-block" role="alert">
                                        <strong>@{{ signup.last_name[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.email}">
                                    <label>Enter Email*</label>
                                    <input type="email" class="form-control" id="signup-email" name="email"
                                           placeholder="Enter Email Address" maxlength="150">
                                    <span v-if="signup.email" class="help-block" role="alert">
                                        <strong>@{{ signup.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.postcode}">
                                    <label>Enter Postcode*</label>
                                    <input type="text" class="form-control" id="signup-postcode" name="postcode"
                                           placeholder="Enter Postcode" maxlength="12">
                                    <span v-if="signup.postcode" class="help-block" role="alert">
                                        <strong>@{{ signup.postcode[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.password}">
                                    <label>Enter Password*</label>
                                    <input type="password" class="form-control" id="signup-password" name="password"
                                           placeholder="Enter Password" maxlength="25">
                                    <span v-if="signup.password" class="help-block" role="alert">
                                        <strong>@{{ signup.password[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signup.password_confirmation}">
                                    <label>Confirm Password*</label>
                                    <input type="password" class="form-control" id="cpassword" name="password_confirmation" placeholder="Re-enter Password" maxlength="25">
                                    <span v-if="signup.password_confirmation" class="help-block" role="alert">
                                        <strong>@{{ signup.password_confirmation[0] }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Create Account" class="btn btn-signin">
                            </div>
                        </div>
                        <hr class="yellow-hr">
                    </form>
                </div>
            </div>
        </div>
    </div> 
    <!-- Sign in Modal -->
    <div class="modal fade" id="signinModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>{{__('Sign In')}}</h2>
                    <form class="signin-form" action="{{route('login')}}" @submit.prevent="signIn($event)">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.email}">
                                    <label>Email*</label>
                                    <input type="email" class="form-control" id="signin-email"
                                           placeholder="Enter Email Address">
                                    <span v-if="signin.email" class="help-block" role="alert">
                                        <strong>@{{ signin.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.password}">
                                    <label>Enter Password*</label>
                                    <input type="password" class="form-control" id="signin-password"
                                           placeholder="Enter Password">
                                    <span v-if="signin.password" class="help-block" role="alert">
                                        <strong>@{{ signin.password[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="submit" value="{{__('Sign In')}}" class="btn btn-signin">
                                @if(setting('login_as_guest'))
                                    <br>
                                    <br>
                                    <span class="no-account">
                                        <a href="{{route('user.guest')}}" style="font-size: 16px;vertical-align: middle;">Login as a guest</a>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr class="yellow-hr">
                        <div class="no-account">
                            <h4>Don’t have an account?
                                <a href="#" id="join-us" data-toggle="modal" data-target="#signupModal"> Join Us</a></h4>
                            <label class="forgot-password">
                                <a href="" data-toggle="modal" data-target="#forgotPasswordModal">Forgot Password!</a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>   
    <script type="text/javascript">
        window.addEventListener('load', function (event) {
            document.getElementById('join-us').addEventListener('click', function () {
                jQuery('#signinModal').modal('hide');
            });
        });
    </script> 
    <!-- Sign in Modal -->
    <div class="modal fade" id="signinModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>{{__('Sign In')}}</h2>
                    <form class="signin-form" action="{{route('login')}}" @submit.prevent="signIn($event)">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.email}">
                                    <label>Email*</label>
                                    <input type="email" class="form-control" id="signin-email"
                                           placeholder="Enter Email Address">
                                    <span v-if="signin.email" class="help-block" role="alert">
                                        <strong>@{{ signin.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':signin.password}">
                                    <label>Enter Password*</label>
                                    <input type="password" class="form-control" id="signin-password"
                                           placeholder="Enter Password">
                                    <span v-if="signin.password" class="help-block" role="alert">
                                        <strong>@{{ signin.password[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="submit" value="{{__('Sign In')}}" class="btn btn-signin">
                                @if(setting('login_as_guest'))
                                    <br>
                                    <br>
                                    <span class="no-account">
                                        <a href="{{route('user.guest')}}" style="font-size: 16px;vertical-align: middle;">Login as a guest</a>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr class="yellow-hr">
                        <div class="no-account">
                            <h4>Don’t have an account?
                                <a href="#" id="join-us" data-toggle="modal" data-target="#signupModal"> Join Us</a></h4>
                            <label class="forgot-password">
                                <a href="" data-toggle="modal" data-target="#forgotPasswordModal">Forgot Password!</a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div> 
    <!-- Reset Password Modal -->
    <div class="modal fade" id="forgotPasswordModal" role="dialog" style="z-index: 99999;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="padding: 0 20px;">
                <div class="modal-header" style="padding: 10px 15px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h2>Reset Password</h2>
                    <form class="signin-form" action="{{ route('password.email') }}"
                          @submit.prevent="resetPassword($event)">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" :class="{'has-error':reset_password.email}">
                                    <label>Email*</label>
                                    <input type="email" class="form-control" id="reset-password-email"
                                           placeholder="Enter Email Address">
                                    <span v-if="reset_password.email" class="help-block" role="alert">
                                        <strong>@{{ reset_password.email[0] }}</strong>
                                 </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Reset Password" class="btn btn-signin" id="closemodal">
                            </div>
                        </div>
                        <hr class="yellow-hr">
                    </form>
                </div>
            </div>

        </div>
    </div> 
@endguest 
<script type="text/javascript" src="{{asset('js/jquery.flexslider.js')}}"></script> 
<script type="text/javascript">
    // window.addEventListener('load', function () {
    //     jQuery('.clockpicker').clockpicker({
    //         autoclose: true,
    //         'default': 'now'
    //     });
    // })

    $(function(){
        // SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: true,
            itemWidth: 210,
            itemMargin: 5,
            minItems: 1,
            maxItems: 10,
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });

    function showDropDelivery() {
        var element = document.getElementById('dropdown-content-delivery');
        if (element.style.display == 'none') {
            element.style.display = 'block';
        } else {
            element.style.display = 'none';
        }
    } 

    $(document).ready(function () {
        
        $('#closemodal').click(function() {
            $('#forgotPasswordModal').modal('hide');
        });

        $('img[src$=".svg"]').each(function () {
            var $img = jQuery(this);
            var imgURL = $img.attr('src');
            var attributes = $img.prop("attributes");

            $.get(imgURL, function (data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Remove any invalid XML tags
                $svg = $svg.removeAttr('xmlns:a');

                // Loop through IMG attributes and apply on SVG
                $.each(attributes, function () {
                    $svg.attr(this.name, this.value);
                });

                // Replace IMG with SVG
                $img.replaceWith($svg);
            }, 'xml');
        });
    });

    function tabFunction() {
        var x = document.getElementById("myTopnavBar");
        if (x.className === "topnavb") {
          x.className += " responsive";
        } else {
          x.className = "topnavb";
        }
      }

</script>
@guest
    <script type="text/javascript">

        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        var data = {
            signup: {
                first_name: false,
                last_name: false,
                email: false,
                password: false,
                postcode: false,
                password_confirmation: false,
            },
            signin: {
                email: false,
                password: false
            },
            reset_password: {
                email: false,
            },
            loading: false
        };  

        var signup = new Vue({
            data: data,
            el: '#signupModal',
            methods: {
                signUp: function (event) {
                    this.loading = true;
                    var action = event.target.action; 
                    var target = event.target; 
                    var $this = this; 
                    this.signup = {
                        first_name: false,
                        last_name: false,
                        email: false,
                        password: false,
                        postcode: false,
                        password_confirmation: false,
                    }; 
                    const signup_email = target.querySelector('#signup-email').value;
                    // if(signup_email == ''){
                    //     // $this.signup.email = 'The email field is required.';
                    // }else{
                    //     if (!validateEmail(signup_email)) {
                    //         $this.signup.email = 'The email is not valied.'; 
                    //         event.preventDefault();
                    //     }else{ 
                    //         event.preventDefault();
                    //         $this.loading = false;
                    //     }
                    // }
                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        first_name: target.querySelector('#first_name').value,
                        last_name: target.querySelector('#last_name').value,
                        email: target.querySelector('#signup-email').value,
                        password: target.querySelector('#signup-password').value,
                        postcode: target.querySelector('#signup-postcode').value,
                        password_confirmation: target.querySelector('#cpassword').value,
                    }).then(function (response) {
                        target.action = '{{route('register')}}';
                        target.submit();
                        $this.loading = false;
                    }).catch(function (error) {
                        $this.loading = false;
                        if (error.response.status == 422) {
                            var errors = error.response.data.errors;
                            if (typeof errors.first_name != 'undefined') {
                                $this.signup.first_name = errors.first_name;
                            }
                            if (typeof errors.last_name != 'undefined') {
                                $this.signup.last_name = errors.last_name;
                            }
                            if (typeof errors.email != 'undefined') {
                                $this.signup.email = errors.email;
                            } 
                            if (typeof errors.password != 'undefined') {
                                $this.signup.password = errors.password;
                            }
                            if (typeof errors.postcode != 'undefined') {
                                $this.signup.postcode = errors.postcode;
                            }
                            if (typeof errors.password_confirmation != 'undefined') {
                                $this.signup.password_confirmation = errors.password_confirmation;
                            }
                        }
                    });
                }
            }
        });

        var singin = new Vue({
            data: data,
            el: '#signinModal',
            methods: {
                signIn: function (event) {
                    var action = event.target.action;

                    var target = event.target;

                    var $this = this;

                    this.signin = {
                        email: false,
                        password: false
                    };

                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        email: target.querySelector('#signin-email').value,
                        password: target.querySelector('#signin-password').value,
                    })
                        .then(function (response) {
                            window.location.reload();
                        })
                        .catch(function (error) {
                            if (error.response.status == 422) {
                                var errors = error.response.data.errors;
                                if (typeof errors.email != 'undefined') {
                                    $this.signin.email = errors.email;
                                }
                                if (typeof errors.password != 'undefined') {
                                    $this.signin.password = errors.password;
                                }
                            }
                        });
                }
            }
        });
        var reset_password = new Vue({
            data: data,
            el: '#forgotPasswordModal',
            methods: {
                resetPassword: function (event) {
                    var action = event.target.action; 
                    var target = event.target; 
                    var $this = this; 
                    this.reset_password = {
                        email: false,
                    };

                    axios.post(action, {
                        _token: '{{csrf_token()}}',
                        email: target.querySelector('#reset-password-email').value,
                    })
                        .then(function (response) {
                            // window.location.reload();
                            // $.alert({
                            //     title: 'Success!',
                            //     content: 'Please check email for password reset instructions',
                            //     theme: 'success'
                            // }); 
                            jQuery('#forgotPasswordModal').modal('hide');  
                            swal(
                                'Success!', 
                                'Please check the email for password reset instructions.', 
                                'success'
                            ); 
                        })
                        .catch(function (error) {
                            if (error.response.status == 422) {
                                var errors = error.response.data.errors;
                                if (typeof errors.email != 'undefined') {
                                    $this.reset_password.email = errors.email;
                                }
                            }
                        });
                }
            }
        });
    </script>
@endguest

<script type="text/javascript">

    var data = {  
        menus: '',
        deals: '',
        cuisines: '',
        selected_menu_items: [],
        style: 'list',
        reservation: false,
        delivery: false,
        takeaway: false,
        cart: {!! json_encode( Session::get('cart') ) !!},
        total: 0,
        vat:{!! json_encode(getVat(),true) !!},
        vat_value: 0,
        sub_total: 0, 
        @if(Route::currentRouteName()=='login')
            deliveryCost:0,
        @else 
            deliveryCost:{!! json_encode( Session::get('delivery_cost') ) !!},
        @endif  
        promocode: '',
        promocode_validation: '',
        promocode_error: '',
        promotion: {},
        lbl_restaurant_discount: 0,
        restaurant_discount: 0,
        site_discount: 0,
        requests: '',
        cart_date: '',
        cart_time: '',
        @if(Route::currentRouteName()=='login')
            restaurant:{},
        @else
            restaurant:{!! json_encode($restaurant) !!},
        @endif  
        favoured: false,
        menu_id: 0,
        type: '', 
        date: '{{\Carbon\Carbon::now()->toDateString()}}',
        time: '{{\Carbon\Carbon::now()->format('h:i A')}}',
        head_count: '',
        phone: '',
        email: '',
        reservation_validation: {},
        review_validation: {},
        step: 1,
        reservation_data: [],
        reservation_requests: '',
        rating: 5,
        review: '',
        sorted_reviews: [],
        sort: 'desc',
        variants: [],
        addons: [],
        menu_item: {},
        selected_addons: [],
        selected_variant: {},
        reservation_restaurant: {},
        reservation_settings: {},
        guest: false,
        remainingCount1: 255,
        remainingCount2: 255,
        remainingCount3: 255,
        loading: false,
        deals_menus: {},
        selected_deal_items: {},
        selectedDealItem: {},
        deal_id: '',
        deal_name: '',
        deal_price: '',
        deal_items: [],
    };  

        var restaurantHome = new Vue({
            el: '#restaurantHome',
            data: data,
            mounted: function () {  
                // console.log(value);
                this.getTotals(); 
            },
            methods: { 
                // getTotals: function () {
                //     this.total = 0; 
                //     this.sub_total = 0; 
                //     const $this = this; 
                //     var vat = 0;
                //     var count = 0;
                //     var restaurant_discount = 0;

                //     this.cart.forEach(function (item) {
                //         var price = item.price;  

                //         if (item.show_variant) {
                //             price = item.show_variant.pivot.price;
                //         }

                //         if (item.show_crust) {
                //             price = price + item.show_crust.pivot.price;
                //         } 

                //         var total_item_price = parseFloat(price); 

                //         if (item.show_addons) {
                //             item.show_addons.forEach(function (addon) {
                //                 $this.total += parseFloat(addon.pivot.price);
                //                 $this.sub_total += parseFloat(addon.pivot.price);
                //                 total_item_price += parseFloat(addon.pivot.price);
                //             });
                //         }

                //         $this.total += item.quantity * price;
                //         $this.sub_total += item.quantity * price;

                //         var vat_percentage = 0;  

                //         switch (item.vat_category) {
                //             case 'food':
                //                 vat_percentage = $this.vat.food;
                //                 break;
                //             case 'alcohol':
                //                 vat_percentage = $this.vat.alcohol;
                //                 break;
                //         }  
                //         vat += total_item_price * vat_percentage * item.quantity; 
                //         if ($this.restaurant.discount == 1) {   
                //             // console.log(123);
                //             restaurant_discount = $this.getPromotionPrice($this.restaurant.discount_type, $this.restaurant.discount_value, $this.sub_total);
                //             $this.restaurant_discount = restaurant_discount;
                //             $this.lbl_restaurant_discount = restaurant_discount; 
                //         } 
                //     });  
                //     this.vat_value = vat; 
                //     // var sum_val = this.currency(this.total + this.vat_value + this.deliveryCost - restaurant_discount);  
                    
                //     var sum_val = vat + this.sub_total + this.delivery_cost - restaurant_discount;  
                //     console.log(sum_val);
                //     jQuery('#total-val').html(sum_val);  
                //     this.$forceUpdate(); 
                // },
                getTotals: function () {
                    this.total = 0; 
                    this.sub_total = 0; 
                    const $this = this; 
                    var discount = 0; 
                    var site_discount = 0 
                    var restaurant_discount = 0; 
                    var vat = 0;
                    var sum_val = '0.00';

                    if($this.cart){
                        this.cart.forEach(function (item) {
                            var price = item.price;    
                            if (item.show_variant) {
                                price = item.show_variant.pivot.price;
                            } 
                            if (item.show_crust) {
                                price = price + item.show_crust.pivot.price;
                            }  
                            if (item.show_addons) {
                                item.show_addons.forEach(function (addon) {
     
                                    price = price + addon.pivot.price;
                                });
                            } 
                            var total_item_price = parseFloat(price);
    
                            if ($this.promotion.method == 'menu_item') {
                                if ($this.promotion.id == item.id) {
                                    discount = $this.getPromotionPrice(item.promo_type, item.promo_value, price);
                                    $this.promotion.reduction = discount;
                                    console.log(discount);
                                }
                            }
    
                            $this.sub_total += item.quantity * price;  
                            var vat_percentage = 0; 
                            switch (item.vat_category) {
                                case 'food':
                                    vat_percentage = $this.vat.food;
                                    break;
                                case 'alcohol':
                                    vat_percentage = $this.vat.alcohol;
                                    break;
                            } 
                            vat += total_item_price * vat_percentage * item.quantity;
                            
                            if ($this.restaurant.discount == 1) {
                                if ($this.promotion.method == 'restaurant') {
                                    discount = $this.getPromotionPrice($this.promotion.type, $this.promotion.value, $this.sub_total);
                                    $this.promotion.reduction = discount;
                                } 
                                if ($this.restaurant.promocode && $this.restaurant.promo_range) {
                                    restaurant_discount = $this.getPromotionPrice($this.restaurant.discount_type, $this.restaurant.discount_value, $this.sub_total);
                                    $this.restaurant_discount = restaurant_discount;
                                    $this.lbl_restaurant_discount = restaurant_discount;
                                } 
                            }  
                            $this.vat_value = vat;  
                            $this.total = vat + $this.sub_total - discount + $this.deliveryCost - restaurant_discount - site_discount;  
                            sum_val = $this.currency(vat + $this.sub_total - discount + $this.deliveryCost - restaurant_discount - site_discount);  
                            
                        });  
                    }
                    jQuery('#total-val').html(sum_val); 
                }, 
                getPromotionPrice(type, value, price) {
                    if (type == 'percentage') {
                        return price * value * 0.01;
                    } else {
                        return value;
                    }
                },
                currency: function (price) {
                    const formatter = new Intl.NumberFormat('en-US', {
                        currency: 'USD',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    }); 
                    return formatter.format(price) 
                },
                // setFavourite: function (menu_item_id) {
                //     const $this = this;
                //     axios.post('{{route('user.favourite-menu-item')}}', {
                //         _token: '{{csrf_token()}}',
                //         menu_item_id: menu_item_id
                //     }).then(function (response) {
                //         menu_item.favoured = response.data.status == 'added';
                //         $.alert({title: 'Success!', content: response.data.message, theme: 'success'});
                //     }).catch(function (error) {
                //         if (error.response.status == 401) {
                //             $.alert({
                //                 title: '{{__('Oh Sorry!')}}',
                //                 content: 'Please Login to Continue!',
                //                 theme: 'error'
                //             });
                //         }
                //     });
                // },
            }
        });

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                axios.post('{{route('user.location')}}', {
                    _token: '{{csrf_token()}}',
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                }).then(function (response) {

                });
            });
        }
    }

    window.addEventListener('load', function () {
        //getLocation();
    });
</script>

@if ($errors->any())
    <script type="text/javascript">
        window.addEventListener('load', function (ev) {
            // $.alert({
            //     title: '{{__('Oh Sorry!')}}',
            //     content: '<ul>@foreach ($errors->all() as $error) <li>{{ __($error) }} </li>@endforeach</ul>',
            //     theme: 'error'
            // }); 
            swal(
                '{{__('Oh Sorry!')}}', 
                '@foreach ($errors->all() as $error) {{ __($error) }} \n @endforeach', 
                'error'
            );
        });
    </script>
@endif

@if (session('success')!=null)
    <script type="text/javascript">
        window.addEventListener('load', function (ev) {
            // $.alert({
            //     title: 'Success!',
            //     content: ' {!! session('success')  !!}  ',
            //     theme: 'success'
            // }); 
            swal(
                'Success!', 
                '{!! session('success')  !!}', 
                'success'
            ); 
        });
    </script>
@endif


<script type="text/javascript">
    // window.addEventListener('load', function () {
        // jQuery('.date').datepicker({
        //     format: '{{ setting('date_format') }}',
        //     todayHighlight: true,
        //     autoclose: true,
        //     closeOnDateSelect: true,
        //     startDate: "today"
        // });

        // document.querySelector('.loader').classList.remove('show');
//     });
    
// });

 
// Add active class to the current button (highlight it)
// var header = document.getElementById("myNavbar");
// var btns = header.getElementsByClassName("navbar-a");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function() {
//   var current = document.getElementsByClassName("active");
//   current[0].className = current[0].className.replace(" active", "");
//   this.className += " active";
//   });
// }
 
</script> 
<!---- collapsible contents ---->
<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;
    
    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
          content.style.display = "none";
        } else {
          content.style.display = "block";
        }
      });
    }
</script>
<!---- //collapsible contents ---->

<!---- hide top bar when scroll down ---->
<script>
    // var prevScrollpos = window.pageYOffset;
    // window.onscroll = function() {
    // var currentScrollPos = window.pageYOffset;
    //   if (prevScrollpos > currentScrollPos) {
    //     document.getElementById("nav-top-bar").style.top = "0";
    //     document.getElementById("navbar").style.top = "45px"
    //   } else {
    //     document.getElementById("nav-top-bar").style.top = "-50px";
    //     document.getElementById("navbar").style.top = "0"
    //   }
    //   prevScrollpos = currentScrollPos;
    // }

    $(function() {
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

            if (scroll <= 400) {
                document.getElementById("nav-top-bar").style.top = "0";
                document.getElementById("navbar").style.top = "45px"
            } else {
                document.getElementById("nav-top-bar").style.top = "-50px";
                document.getElementById("navbar").style.top = "0"
            }
        });
    });
</script>
<!---- //hide top bar when scroll down ---->
 
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- Toast -->
<script type="text/javascript" src="{{asset('js/jquery.toast.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
