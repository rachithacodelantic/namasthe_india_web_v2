@extends('layouts.app')
@section('title', 'login')
@section('content')
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <section>
        <div class="container">
            <div class="row" id="sign-in-form">
                <div class="col-md-2"></div> 
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 left-sec"> 
                    <h5 class="sub-heading">Checkout as a Guest or Register </h5> 
                    <p>Is this your first time here?Please checkout as a guestin order to make ordering as safe and
                        easy.</p>
                    <a href="{{ route('user.guest') }}" class="btn btn-default btn-sm submit-btn">Guest Login</a>
                    <p>Register with us for future convenience.</p>
                    <a href="#" data-toggle="modal" data-target="#signupModal" class="link-txt">Join Us</a>
                </div> 
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 right-sec">
                    <h5 class="sub-heading">Login</h5>
                    <p>Please Log in below</p>
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" id="email" value="{{ old('email') }}" name="email"
                                placeholder=" Enter Email Address">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: #bc040b;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" id="password" placeholder="Enter Password"
                                name="password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: #bc040b;">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" value="LOGIN" class="btn btn-default btn-sm submit-btn">
                        <a href="" data-toggle="modal" data-target="#forgotPasswordModal" class="link-txt">Forgot
                            Password</a>
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        window.addEventListener('load', function() {
            jQuery('.button-signin').css('display', 'none');
            jQuery('.button-signup').css('display', 'none');

            jQuery('#sign-in').click(function(event) {
                event.preventDefault();
                jQuery('#sign-up').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('#sign-up-form').slideUp(200);
                jQuery('#sign-in-form').slideDown(200);
            });
            jQuery('#sign-up').click(function(event) {
                event.preventDefault();
                jQuery('#sign-in').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('#sign-in-form').slideUp(200);
                jQuery('#sign-up-form').slideDown(200);
            });
        });

    </script>
@endsection
