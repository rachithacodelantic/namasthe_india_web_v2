<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Deals;
use App\DealsItem;
use App\MenuItem;
use App\Repository\DealsRepository;
use App\Repository\MenuRepository;
use App\Repository\RestaurantRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class MenuController
 * @package App\Http\Controllers\Admin
 */
class DealsController extends Controller
{
    protected $deals;

    protected $deal_items;

    /**
     * MenuController constructor.
     * @param MenuRepository $menu_repository
     * @param DealsRepository $deals_repository
     */
    public function __construct(
        RestaurantRepository $restaurant_repository,
        DealsRepository $deals_repository
    )
    {
        $this->restaurant = $restaurant_repository;
        $this->deals = $deals_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deals = Auth::user()->restaurant()->withTrashed()->first()->deals;

        $deal_items = $this->restaurant->getAllMenuItems(1);

        return view('admin.deals.index', [
            'menus' => $deals,
            'deal_items' => $deal_items,
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenus()
    {
        $menus = Auth::user()->restaurant()->withTrashed()->first()->menus()->get();

        return response()->json([
            'message' => 'success',
            'data' => [
                'menus' => $menus
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealsItem(Request $request)
    {
        $deals = Auth::user()
            ->restaurant
            ->deals()
            ->orderBy('created_at', 'desc')
            ->paginate(10);


        foreach ($deals as $deal) {
            $menu_items = MenuItem::join('deals_item', 'deals_item.menu_item_id', '=', 'menu_items.id')
                ->where('deals_item.deals_id', '=', $deal->id)
                ->select('menu_items.id', 'menu_items.name')
                ->get();

            $all_menu_items = MenuItem::
                select('menu_items.id', 'menu_items.name')
                ->whereNotIn('menu_items.id', function($query) use($deal)
                {
                    $query->select('menu_items.id')
                        ->from('menu_items')
                        ->join('deals_item', 'deals_item.menu_item_id', '=', 'menu_items.id')
                        ->where('deals_item.deals_id', '=', $deal->id);
                })
                ->get();

            $deal->selectedMenuItemsValues = $deal->dealsItems;
            $deal->image = asset('storage/'.$deal->image);
            $deal->availableMenuItems = $all_menu_items;
            $deal->selectedMenuItems = $menu_items;
        }

        return response()->json([
            'message' => 'success',
            'data' => [
                'deals' => $deals
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:deals,name',
            'price' => 'required',
            'description' => 'required',
            'vat_category' => 'required'
        ]);
        $image= $request->image;
        if ($image) {

            $extension = explode(';', explode('image/', $image)[1])[0];

            $result = substr($image, strpos($image, 'image/', '6'), 4);

            $file_name = 'deals/deal_' . Carbon::now()->format('YmdHis') . '.' . $extension;

            $image = str_replace('data:image/' . $extension . ';base64,', '', $image);
            $image = str_replace(' ', '+', $image);

            Storage::disk('local')->put($file_name, base64_decode($image));

            $request->request->set('image', $file_name);

        }
        $request->request->set('restaurant_id', Auth::user()->restaurant()->withTrashed()->first()->id);

        $deals = $this->deals->createByRequest($request->restaurant_id, $request->all());

        foreach ($request->selectedMenuItemsValues as $item) {
            $deal_items = new DealsItem();
            $deal_items->deals_id = $deals->id;
            $deal_items->menu_item_id = $item;
            $deal_items->save();
        }

        if ($deals) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'deals' => $deals
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required'],
            'price' => 'required',
            'description' => 'required',
            'vat_category' => 'required'
        ]);

        $deals = Deals::find($id);
        $image= $request->image;
        if ($image) {
            $extension = explode(';', explode('image/', $image)[1])[0];
            $result = substr($image, strpos($image, 'image/', '6'), 4);
            $file_name = 'deals/deal_' . Carbon::now()->format('YmdHis') . '.' . $extension;
            $image = str_replace('data:image/' . $extension . ';base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            Storage::disk('local')->put($file_name, base64_decode($image));
            $request->request->set('image', $file_name);
        } else {
            $request->request->set('image', $deals->logo);
        }

        $request->request->set('restaurant_id', Auth::user()->restaurant()->withTrashed()->first()->id);
        $deals->fill($request->all());
        $deals->save();

        DealsItem::where('deals_id', '=', $deals->id)->delete();

        foreach ($request->selectedMenuItemsValues as $item) {
            $deal_items = new DealsItem();
            $deal_items->deals_id = $deals->id;
            $deal_items->menu_item_id = $item;
            $deal_items->save();
        }

        if ($deals) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'deals' => $deals
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete(Request $request)
    {
        $deal_id = $request->deal_id;
        DealsItem::where('deals_id', '=', $deal_id)->delete();
        Deals::find($deal_id)->delete();

        return response()->json([
            'message' => 'success',
            'data' => [

            ]
        ]);
    }
}
