<?php

namespace App\Http\ViewComposers;

use App\Branches; 
use App\Repository\RestaurantRepository;
use App\Repository\BranchRepository; 
use Carbon\Carbon;

class UserLayoutViewComposer
{
    protected $restaurant;
    protected $branches; 

    /**
     * PageController constructor.
     * @param RestaurantRepository $restaurant_repository
     */
    public function __construct(
        RestaurantRepository $restaurant_repository,
        BranchRepository $branch_repository
    )
    {
        $this->restaurant = $restaurant_repository;
        $this->branches = $branch_repository;
    }

    public function compose($view)
    {  
        $branch_id = 1; 
        $restaurant = $this->restaurant->getRestaurant(1); 
        $menus = $this->branches->getMenuBranches($branch_id);   

        $todayDate = date('Y-m-d'); 
        $offlineDateBegin = date('Y-m-d', strtotime($restaurant->online_from_time)); 
        $offlineDateEnd = date('Y-m-d', strtotime($restaurant->online_to_time));
        if (($todayDate >= $offlineDateBegin) && ($todayDate <= $offlineDateEnd)){
            $offlineMode = 'yes';
        } else {
            $offlineMode = 'no';
        } 

        $opening_time = '';
        $closing_time = '';
        $cart_date = Carbon::parse($todayDate);  
        $branch = $this->branches->get($branch_id); 
        $restaurant->opening_hours = $branch->openingHours()->orderBy('day', 'asc')->orderBy('opening_time', 'desc')->get();
        foreach ($restaurant->opening_hours as $opening_hour) { 
            if ($opening_hour->day == $cart_date->dayOfWeek) { 
                $opening_time = $opening_hour->opening_time;
                $closing_time = $opening_hour->closing_time; 
            }
        } 
        
        $restaurant->promo_range = false; 
        if (Carbon::now()->startOfDay()->lte(Carbon::parse($restaurant->expiry_date)->startOfDay()) && Carbon::now()->startOfDay()->gte(Carbon::parse($restaurant->start_date)->startOfDay())) {
            $restaurant->promo_range = true;
        }

        $view->with('restaurant', $restaurant);
        $view->with('restaurant_menus', $menus);
        $view->with('restaurant_info', $restaurant);
        $view->with('offlineMode', $offlineMode);
        $view->with('opening_time', $opening_time);
        $view->with('closing_time', $closing_time);
    }
}
