<?php

namespace App\Http\Controllers\MasterAdmin;

use App\MainSliderImages; 
use App\Repository\MainSliderImagesRepositary;     
use App\Restaurant;
use App\Repository\RestaurantRepository;       
use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan; 

class SiteSettingController extends Controller
{
    protected $slider_images;  
    protected $restaurant;

    /**
     * SiteSettingController constructor. 
     * @param MainSliderImagesRepositary $slider_images_repository 
     */
    public function __construct(MainSliderImagesRepositary $slider_images_repository, RestaurantRepository $restaurant_repository)
    {
        $this->slider_images = $slider_images_repository; 
        $this->restaurant = $restaurant_repository;
    }

    public function index()
    {
        return view('master-admin.settings.index');
    }

    public function store(Request $request)
    { 
        $section = 'features';
        $rules = SiteSetting::getValidationRules($section);
        $data = $this->validate($request, $rules);
        $validSettings = array_keys($rules);
        foreach ($data as $key => $val) {
            if( in_array($key, $validSettings) ) {
                SiteSetting::add($key, $val, SiteSetting::getDataType($key, $section));
                $type = SiteSetting::getInputType($key, $section);
                // handle file upload
                if (in_array($type, [$key, 'file'])) {
                    $this->uploadFile($key, $request, $section);
                }

                // if($request->hasFile('uploads')){

                //     foreach ($request->uploads as $upload) {
                //         dd(12);
                //     }
                // }
            }
        }
        Artisan::call('config:cache');

        return redirect()->back()->with('success', 'Settings has been saved.');
    }

    public function general()
    {
        $section = 'general';
        return view('master-admin.settings.general_setting', compact('section'));
    } 

    public function mail()
    {
        $section = 'mail';
        return view('master-admin.settings.general_setting', compact('section'));
    }

    public function api()
    {
        $section = 'api';
        return view('master-admin.settings.general_setting', compact('section'));
    }

    public function app()
    {
        $section = 'app-settings';
        return view('master-admin.settings.general_setting', compact('section'));
    }

    public function getHomePage()
    { 
        $images = $this->slider_images->getSliderImages(1); 
        foreach ($images as $image) {
            $image->path = asset('storage/'. $image->path);
        }
        return view('master-admin.settings.home_page_setting', ['images' => $images]);
    }

    public function getMenuPage()
    {
        return view('master-admin.settings.menu_page_setting');
    }

    public function getAboutPage()
    {
        return view('master-admin.settings.about_page_setting');
    }

    public function getContactPage()
    {
        return view('master-admin.settings.contact_page_setting');
    }

    public function getTermsPage()
    {
        return view('master-admin.settings.terms_page_setting');
    }

    public function getPrivacyPage()
    {
        return view('master-admin.settings.privacy_page_setting');
    }

    public function getFaqPage()
    {
        return view('master-admin.settings.faq_page_setting');
    }

    public function getReviewPage()
    {
        return view('master-admin.settings.review_page_setting');
    }

    public function getDeliveryInfo()
    {
        return view('master-admin.settings.delivery_page_setting');
    }

    public function postPageData(Request $request)
    { 
        $section = $request->section;
        $rules = SiteSetting::getValidationRules($section);
        // dd($rules);
        $data = $this->validate($request, $rules);
        $validSettings = array_keys($rules);
        foreach ($data as $key => $val) {
            if( in_array($key, $validSettings) ) {
                SiteSetting::add($key, $val, SiteSetting::getDataType($key, $section));
                $type = SiteSetting::getInputType($key, $section);
                // handle file upload
                // dd($request);    

                if (in_array($type, [$key, 'file'])) {
                    $this->uploadFile($key, $request, $section);
                }
            }
        }

        if($request->hasFile('uploads')){
            // dd($request->banner_text);
            $upload_file = $request->uploads;
            $uploadedPath = $upload_file->store('site'); 
            // dd($uploadedPath);
            $this->slider_images->createByRequest(1, $request->slider_name, $request->banner_text, $uploadedPath);
        }

        Artisan::call('config:cache');

        return redirect()->back()->with('success', 'Settings has been saved.');
    }

    private function uploadFile($key, $request, $section)
    {
        $uploadedPath = SiteSetting::getFilePath($key, $section);
        $settingName = $key;

        if ($request->$settingName) {
            $uploadedPath = $request->file($settingName)->store('site');
            SiteSetting::set($settingName, $uploadedPath);
            File::Delete($uploadedPath);
        }
        return $uploadedPath;
    }

    public function getManageAttributesPage()
    {
        return view('master-admin.settings.attributes_setting');
    }

    public function removeSliderImage(Request $request)
    {
        $image_id = $request->image_id; 
        $imageMenuItem = $this->slider_images->getSliderImagesMenuItems($image_id); 
        $image = $this->slider_images->get($image_id); 
        // dd($image['path']);

        // $menuItems = $image->menuItems;
        // dd($menuItems->getOriginal('pivot_menu_items_id'));
        
        // foreach ($menuItems as $menuItem) {
        //     dd($menuItem->pivot_menu_items_id);
        // }
        if($imageMenuItem){
            $this->slider_images->deleteSliderImageItem($image_id);  
        } 
        $this->slider_images->deleteSlider($image_id); 
        File::Delete($image['path']);
        $delete = true;
        if ($delete) {
            return response()->json([
                'message' => 'success'
            ]);
        } 
    }
}
