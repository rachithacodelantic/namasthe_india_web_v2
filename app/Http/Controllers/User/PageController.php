<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller; 
use App\Deals;
use App\MenuItem;
use App\Branches; 
use App\Review;
use App\Repository\BranchRepository; 
use App\Repository\DealsRepository;
use App\Repository\MainSliderImagesRepositary; 
use App\Repository\CuisineRepository;
use App\Repository\MenuRepository;
use App\Repository\MenuItemRepository;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\SitePromotionRepository;
use App\Repository\ReviewRepository;
use App\Rules\Telephone;
use App\Support\Geolocation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Session;

/**
 * Class PageController
 * @package App\Http\Controllers\User
 */
class PageController extends Controller
{
    protected $restaurant;
    protected $cuisine;
    protected $geolocation;
    protected $menu;
    protected $menu_item;
    protected $site_promotions;
    protected $reservation;
    protected $slider_images; 
    protected $deals;
    protected $branches; 
    protected $review;

    /**
     * PageController constructor.
     * @param RestaurantRepository $restaurant_repository
     * @param CuisineRepository $cuisine_repository
     * @param Geolocation $geolocation
     * @param MenuRepository $menu_repository
     * @param SitePromotionRepository $site_promotion
     * @param ReservationRepository $reservation_repository
     * @param MainSliderImagesRepositary $slider_images_repository
     * @param MenuItemRepository $menu_item_repository
     * @param DealsRepository $deals_repository
     */
    public function __construct(
        RestaurantRepository $restaurant_repository,
        DealsRepository $deals_repository,
        CuisineRepository $cuisine_repository,
        Geolocation $geolocation,
        MenuRepository $menu_repository,
        SitePromotionRepository $site_promotion,
        ReservationRepository $reservation_repository,
        MainSliderImagesRepositary $slider_images_repository,
        MenuItemRepository $menu_item_repository,
        BranchRepository $branch_repository,
        ReviewRepository $review_repository
    )
    {
        $this->restaurant = $restaurant_repository;
        $this->cuisine = $cuisine_repository;
        $this->geolocation = $geolocation;
        $this->menu = $menu_repository;
        $this->site_promotions = $site_promotion;
        $this->reservation = $reservation_repository;
        $this->slider_images = $slider_images_repository; 
        $this->menu_item = $menu_item_repository; 
        $this->deals = $deals_repository;
        $this->branches = $branch_repository;
        $this->review = $review_repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    { 
        $branches = Branches::all(); 
        $popular_items = $this->restaurant->getPopularItems(1);
        $restaurant = $this->restaurant->getRestaurant(1);
        $images = $this->slider_images->getSliderImages(1); 
        // $branch_id = 1; 
        if(Session::get('branch_id')){ 
            $branch_id = Session::get('branch_id'); 
            Session::put('branch_id', $branch_id); 
        }else{
            $branch_id = 1; 
            Session::put('branch_id', 1); 
        }
 
        $top_sale_product = DB::table('menu_items')
            ->leftJoin('delivery_items','menu_items.id','=','delivery_items.menu_item_id')
            ->selectRaw('menu_items.*, COALESCE(sum(delivery_items.quantity),0) total')
            ->whereBetween('delivery_items.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->groupBy('menu_items.id')
            ->orderBy('total','desc')
            ->take(1)
            ->get();  
        if($top_sale_product->isEmpty()){
            $top_sale_product = null;
        }else{
            $top_sale_product = $this->menu_item->get($top_sale_product[0]->id);
            $top_sale_product->variants = $top_sale_product->variants()->get(); 
            foreach ($top_sale_product->variants as $variant_key => $variant) {
                $variant->pivot = $variant->pivot;
                if ($variant_key == 0) {
                    $variant->selected = true;
                } else {
                    $variant->selected = false;
                }
            } 
            $top_sale_product->addons = $top_sale_product->addons()->get(); 
            foreach ($top_sale_product->addons as $addon) {
                $addon->selected = false;
                $addon->pivot = $addon->pivot;
            }
        } 

        $new_tast = MenuItem::latest()->first();
        $new_tast->variants = $new_tast->variants()->get(); 
        foreach ($new_tast->variants as $variant_key => $variant) {
            $variant->pivot = $variant->pivot;
            if ($variant_key == 0) {
                $variant->selected = true;
            } else {
                $variant->selected = false;
            }
        } 
        $new_tast->addons = $new_tast->addons()->get(); 
        foreach ($new_tast->addons as $addon) {
            $addon->selected = false;
            $addon->pivot = $addon->pivot;
        }

        foreach ($images as $image) {
            $imageMenuItem = $this->slider_images->getSliderImagesMenuItems($image->id);
            foreach ($imageMenuItem as $imageMenu) {
                $menuItem = $this->menu_item->get($imageMenu->menu_items_id); 
                if($menuItem){
                    $image->name = $menuItem->name;
                    $image->description = $menuItem->description;
                    $image->special_tag_description = '';
                    $image->price = $menuItem->price;
                    $image->id = $menuItem->id;
                    $image->menu_id = $menuItem->menu_id;
                    $image->type = 'menu_item';

                    $image->variants = $menuItem->variants()->get(); 
                    foreach ($image->variants as $variant_key => $variant) {
                        $variant->pivot = $variant->pivot;
                        if ($variant_key == 0) {
                            $variant->selected = true;
                        } else {
                            $variant->selected = false;
                        }
                    } 
                    $image->addons = $menuItem->addons()->get(); 
                    foreach ($image->addons as $addon) {
                        $addon->selected = false;
                        $addon->pivot = $addon->pivot;
                    }
                }
            }  

            $imageDealItem = $this->slider_images->getSliderImagesDealItems($image->id);
            foreach ($imageDealItem as $imageDeal) {
                $dealItem = $this->deals->get($imageDeal->menu_items_id); 
                if($dealItem){
                    $image->name = $dealItem->name;
                    $image->description = $dealItem->description;
                    $image->special_tag_description = '';
                    $image->price = $dealItem->price;
                    $image->id = $dealItem->id;
                    $image->type = 'deal';
                }
            } 
            $image->path = asset('storage/'. $image->path);
        }

        foreach ($popular_items as $popular_item) {
            $popular_item->logo = getStorageUrl() . $popular_item->logo;
            $popular_item->favoured = false;

            if ($popular_item->users()->where('user_id', '=', Auth::id())->count()) {
                $popular_item->favoured = true;
            }
        }

        // $restaurant->opening_hours = $restaurant->openingHours()->get();

        // foreach ($restaurant->opening_hours as $opening_key => $opening_hour) {
        //     if (Carbon::SUNDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Sunday');
        //     }
        //     if (Carbon::MONDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Monday');
        //     }
        //     if (Carbon::TUESDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Tuesday');
        //     }
        //     if (Carbon::WEDNESDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Wednesday');
        //     }
        //     if (Carbon::THURSDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Thursday');
        //     }
        //     if (Carbon::FRIDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Friday');
        //     }
        //     if (Carbon::SATURDAY == $opening_hour->day) {
        //         $opening_hour->day = __('Saturday');
        //     }
        // }  
 
        // $menus = $this->branches->getMenuBranches($branch_id);  
        $menus = $this->menu->all();
        foreach ($menus as $menu) {
            // $menu_items = $menu
            //     ->menuItems()
            //     ->whereNull('deleted')
            //     // ->whereNotNull('promo_code')
            //     // ->where('promo_date', '>=', Carbon::now()->toDateString())
            //     // ->orderBy('promo_date', 'desc')
            //     ->get();
            $menu_items = $this->menu_item->getMenuItemByMenu($menu->id);

            foreach ($menu_items as $menu_item) {
                // if ($menu_item->menu->restaurant) {
                    $menu_item->url = route('user.restaurant.menu');
                    $menu_item->restaurant_name = $menu_item->menu->restaurant->name;
                    $menu_item->promocode = $menu_item->promo_code; 

                    $menu_item->variants = $menu_item->variants()->get();

                    foreach ($menu_item->variants as $variant_key => $variant) {
                        $variant->pivot = $variant->pivot;
                        if ($variant_key == 0) {
                            $variant->selected = true;
                        } else {
                            $variant->selected = false;
                        }
                    }

                    $menu_item->addons = $menu_item->addons()->get();

                    foreach ($menu_item->addons as $addon) {
                        $addon->selected = false;
                        $addon->pivot = $addon->pivot;
                    }
                // }
            }
            $menu->menu_items = $menu_items;
        } 

        $reviews = Review::all(); 

        foreach ($reviews as $review) {
            $review->user = $review->user()->first();
            $review->date = Carbon::parse($review->created_at)->toFormattedDateString();
        }

        $weekly_offers = DB::table('offers') 
            // ->orWhereBetween('start', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]) 
            // ->orWhereBetween('close', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]) 
            ->whereNull('deleted_at')
            ->take(10)
            ->get();  

        if ($request->session()->get('cart')) {
            $cart = $request->session()->get('cart');
        } else {
            $cart = [];
        }

        $weekly_deals = [];
        $deals = Deals::all();  
        foreach($deals as $item){ 
            $weekly_deals[] = $item; 
        } 

        return view('user.page.index', [
            'branches' => $branches,
            'branch' => $branch_id,
            'menus' => $menus,
            'cart' => $cart,
            'popular_items' => $popular_items,
            'restaurant' => $restaurant,
            'top_sale_product' => $top_sale_product,
            'new_tast' => $new_tast,
            'slider_images' => $images,
            'reviews' => $reviews,
            'weekly_offers' => $weekly_offers,
            'deals' => $weekly_deals
        ]);
    } 

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $term = $request->term;
        $type = $request->type;

        if (!$term) {
            $term = "";
        }

        if (!$type) {
            $type = 'delivery';
        }

        $restaurants = $this->restaurant->search($term, $type);

        $cuisines = $this->cuisine->all();

        if (Auth::check()) {
            $city = Auth::user()->city;
        } else {
            $city = false;
        }

        $PublicIP = $request->ip();
        $json = file_get_contents("http://ipinfo.io/$PublicIP/geo?token=".config('services.ip_info_token'));
        $json = json_decode($json, true);

        if (isset($json['city']) && isset($json['region']) && isset($json['country'])) {
            $user_address = $json['city'] . ', ' . $json['region'] . ', ' . $json['country'];
        } else {
            $user_address = 'null';
        }

        if (session('latitude') && session('longitude')) {
            $user_address = session('latitude') . ', ' . session('longitude');
        }

        $default_address = $user_address;


        foreach ($restaurants as $restaurant) {
            $restaurant->cuisines = $restaurant->cuisines()->get();
            $restaurant->reviews = $restaurant->reviews()->get();
            $restaurant->opening_hours = $restaurant->openingHours()->orderBy('day', 'asc')->orderBy('opening_time', 'desc')->get();

            foreach ($cuisines as $cuisine) {
                foreach ($restaurant->cuisines as $restaurant_cuisine) {
                    if ($cuisine->id == $restaurant_cuisine->id) {
                        $cuisine->count++;
                    }
                }
                $cuisine->selected = true;
            }

            $restaurant_address = $restaurant->name . ', ' . $restaurant->city . ', ' . $restaurant->province . ', ' . $restaurant->county . ' ' . $restaurant->postcode;


            if (Auth::check()) {
                if (Auth::user()->addresses()->count()) {
                    $user_address = Auth::user()->addresses()->where('default', '=', true)->first();
                    if ($user_address) {
                        $default_address = $user_address->address . ', ' . $user_address->street . ', ' . $user_address->city . ', ' . $user_address->county . ' ' . $user_address->postcode;
                        $restaurant->distance = $this->geolocation->getDistance($restaurant_address, $default_address);
                    }
                }
            }

            if ($user_address) {
                $restaurant->distance = $this->geolocation->getDistance($restaurant_address, $default_address);
            } 

            if ($restaurant->logo) {
                $restaurant->logo = getStorageUrl() . $restaurant->logo;
            } else {
                $restaurant->logo = asset('img/default.jpg');
            }
            $selected_opening_hours = [];

            $restaurant->pre_order_time = '';

            foreach ($restaurant->opening_hours as $opening_key => $opening_hour) {

                if (Carbon::now()->dayOfWeek == $opening_hour->day) {
                    $selected_opening_hours[] = $opening_hour;
                }

                $opening_hour_day = $opening_hour->day;

                if (Carbon::SUNDAY == $opening_hour->day) {
                    $opening_hour->day = __('Sunday');
                }
                if (Carbon::MONDAY == $opening_hour->day) {
                    $opening_hour->day = __('Monday');
                }
                if (Carbon::TUESDAY == $opening_hour->day) {
                    $opening_hour->day = __('Tuesday');
                }
                if (Carbon::WEDNESDAY == $opening_hour->day) {
                    $opening_hour->day = __('Wednesday');
                }
                if (Carbon::THURSDAY == $opening_hour->day) {
                    $opening_hour->day = __('Thursday');
                }
                if (Carbon::FRIDAY == $opening_hour->day) {
                    $opening_hour->day = __('Friday');
                }
                if (Carbon::SATURDAY == $opening_hour->day) {
                    $opening_hour->day = __('Saturday');
                }

                $opening = Carbon::parse($opening_hour->opening_time);

                $opening_hour->opening = $opening->format('h:i A');

                $closing = Carbon::parse($opening_hour->closing_time);

                $opening_hour->closing = $closing->format('h:i A');

                $now = Carbon::now();

                if ($now->dayOfWeek == $opening_hour_day) {
                    if ($now->lt($opening)) {
                        $restaurant->pre_order_time = __('Opening at') . ' ' . $opening->format('h:i A');
                    }

                    if ($now->gt($closing)) {
                        if (count($restaurant->opening_hours) == $opening_key + 1) {
                            $restaurant->pre_order_time = __('Opening in ') . $opening_hour->day . ' ' . $restaurant->opening_hours[0]->opening;
                        } else {
                            if (Carbon::SUNDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Sunday');
                            }
                            if (Carbon::MONDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Monday');
                            }
                            if (Carbon::TUESDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Tuesday');
                            }
                            if (Carbon::WEDNESDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Wednesday');
                            }
                            if (Carbon::THURSDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Thursday');
                            }
                            if (Carbon::FRIDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Friday');
                            }
                            if (Carbon::SATURDAY == $restaurant->opening_hours[$opening_key + 1]->day) {
                                $next_day = __('Saturday');
                            }

                            if ($restaurant->opening_hours[$opening_key + 1]->day == $now->addDay()->dayOfWeek) {
                                $restaurant->pre_order_time = __('Pre order opening at tomorrow') . ' ' . $opening_hour->opening;
                            } else {
                                $restaurant->pre_order_time = __('Pre order opening at next') . ' ' . $next_day . ' ' . $opening_hour->opening;
                            }
                        }
                    }
                    if ($now->lt($closing) && $now->gt($opening)) {
                        $restaurant->pre_order_time = __('Open');
                    }

                    if ($now->lt($closing) && $now->gt($closing->subHour())) {
                        $restaurant->pre_order_time = __('Closing at') . ' ' . $closing->addHour()->format('h:i A');
                    }
                }

            }

            $restaurant->selected_opening_hours = $selected_opening_hours;

            $restaurant->rating = 5;
            $total = 0;

            if (count($restaurant->reviews)) {
                foreach ($restaurant->reviews as $review) {
                    $total += $review->rating;
                }
                $restaurant->rating = number_format($total / count($restaurant->reviews), 2, '.', '');
            }


        }

        $request->session()->put('type', $type);

        $offers = [];

        $menus = $this->menu->all();

        foreach ($menus as $menu) {
            $menu_items = $menu
                ->menuItems()
                ->whereNull('deleted')
                ->whereNotNull('promo_code')
                ->where('promo_date', '>=', Carbon::now()->toDateString())
                ->orderBy('promo_date', 'desc')
                ->get();

            foreach ($menu_items as $menu_item) {
                if ($menu_item->menu->restaurant) {
                    $menu_item->url = route('user.restaurant.menu');
                    $menu_item->restaurant_name = $menu_item->menu->restaurant->name;
                    $menu_item->promocode = $menu_item->promo_code;
                    if ($menu_item->promo_usage > $menu_item->deliveries()->count() + $menu_item->takeaways()->count()) {
                        $offers[] = $menu_item;
                    }
                }
            }
        }

        $promotion_restaurants = $this->restaurant->promotionRestaurants(Carbon::now()->toDateString());

        foreach ($promotion_restaurants as $promotion_restaurant) {
            $promotion_restaurant->url = route('user.restaurant.menu');
            $promotion_restaurant->promo_type = $promotion_restaurant->discount_type;
            $promotion_restaurant->promo_value = $promotion_restaurant->discount_value;
            $promotion_restaurant->promo_date = $promotion_restaurant->expiry_date;
            $offers[] = $promotion_restaurant;
        }

        $promotions_restaurants = $this->restaurant->all();

        foreach ($promotions_restaurants as $promotions_restaurant) {
            $promotions = $promotions_restaurant->promotions()
                ->where('expiry_date', '>=', Carbon::now()->toDateString())
                ->where('start_date', '<=', Carbon::now()->toDateString())
                ->get();
            foreach ($promotions as $promotion) {
                if ($promotion->restaurant) {
                    $promotion->logo = $promotion->restaurant->logo;
                    $promotion->url = route('user.restaurant.menu');
                    $promotion->restaurant_name = $promotion->restaurant->name;
                    $promotion->promo_type = $promotion->type;
                    $promotion->promo_value = $promotion->value;
                    $promotion->promo_date = $promotion->expiry_date;
                    if ($promotion->limit > $promotion->deliveries()->count() + $promotion->takeaways()->count()) {
                        $offers[] = $promotion;
                    }
                }
            }
        }

        $site_promotions = $this->site_promotions->current(Carbon::now()->toDateString());

        foreach ($site_promotions as $site_promotion) {
            $site_promotion->logo = '';
            $site_promotion->url = route('user.home');
            $site_promotion->restaurant_name = '';
            $site_promotion->promo_type = $site_promotion->type;
            $site_promotion->promo_value = $site_promotion->value;
            $site_promotion->promo_date = $site_promotion->expiry_date;
            if ($site_promotion->limit > $site_promotion->deliveries()->count() + $site_promotion->takeaways()->count()) {
                $offers[] = $site_promotion;
            }
        }

        return view('user.page.search', [
            'results' => $restaurants,
            'cuisines' => $cuisines,
            'term' => $term,
            'type' => $type,
            'offers' => $offers
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $restaurant = $this->restaurant->getRestaurant(1); 
        return view('user.page.about', compact('restaurant'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function location()
    {
        $restaurant = $this->restaurant->getRestaurant(1); 
        $branches = Branches::all(); 
        return view('user.page.location', ['branches' => $branches, 'restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blog()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.blog', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function careers()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.careers', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function press()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.press', ['restaurant' => $restaurant]);
    }

    public function faq()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.faq', ['restaurant' => $restaurant]);
    }

    public function deliveryInfo()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.delivery-info', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        $restaurant = $this->restaurant->getRestaurant(1);  
        $branches = Branches::all(); 
        $branch_id = Session::get('branch_id');    
        $current = $this->branches->get($branch_id);
        return view('user.page.contact', ['branches' => $branches, 'restaurant' => $restaurant, 'current' => $current, 'branch' => $branch_id]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postContact(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => new Telephone,
            'message' => 'required',
        ]);
        $request->session()->flash('success', 'Mail sent successfully!');
        Mail::raw('Contact us Email: \r\nFirst name: ' . $request->first_name . '\r\nLast name: ' . $request->last_name . '\r\nEmail: ' . $request->email . '\r\nPhone: ' . $request->phone . '\r\nMessage: ' . $request->message, function ($message) {
            $message->to(setting('contact_email_id'))->subject('Contact Us email');
        });
        // return redirect(route('contact.thank.you'));
        return redirect(route('user.contact')); 
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function why()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.why', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancellations()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.cancellations', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.terms', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy()
    {
        $restaurant = $this->restaurant->getRestaurant(1);
        return view('user.page.privacy', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function landing()
    {
        // dd(Auth::user()->role);
        if (Auth::check() && Auth::user()->role == 'admin') {
            return redirect(route('admin.delivery.index')); 
        }else if(Auth::check() && Auth::user()->role == 'branch_admin'){
            return redirect(route('admin.delivery.index')); 
        } else {
            // dd(11);
            $cuisines = $this->cuisine->all();
            return view('admin.page.landing', ['cuisines' => $cuisines]);
        }
    }

    /**
     * @param $locale
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function locale($locale, Request $request)
    {
        $request->session()->put('language', $locale);
        return redirect(url()->previous());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactThankYou()
    {
        return view('user.page.contact-thank-you', ['restaurant' => $restaurant]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservationThankYou()
    {

        $reservation_id = session('reservation_id');

        $reservation = $this->reservation->get($reservation_id);

        $reservation->restaurant = $reservation->restaurant()->withTrashed()->first();

        $restaurant_address = $reservation->restaurant->city . '+' . $reservation->restaurant->province . '+' . $reservation->restaurant->county . '+' . $reservation->restaurant->postcode;

        $reservation->restaurant->query_address = str_replace(' ', '+', $restaurant_address);

        $reservation->date = Carbon::parse($reservation->time)->toFormattedDateString();
        $reservation->time = Carbon::parse($reservation->time)->format('g:i A');

        return view('user.page.reservation-thank-you', ['reservation' => $reservation]);
    } 
}
