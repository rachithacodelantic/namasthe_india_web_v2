<?php 
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Newsletter;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{  
    public function store(Request $request)
    {   
        if (!Newsletter::isSubscribed($request->sub_email)) 
        {
            Newsletter::subscribePending($request->sub_email);
            return response()->json([
                'message' => 'Thanks For Subscribe.'
            ], 200); 
        }else{
        	return response()->json([
                'message' => 'Sorry! You have already subscribed.'
            ], 419);
        }     
    }
}
