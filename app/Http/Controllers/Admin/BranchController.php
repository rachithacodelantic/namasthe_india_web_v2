<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Branches; 
use App\User; 
use App\Repository\BranchRepository; 
use App\Repository\RestaurantRepository;
use App\Repository\CuisineRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchController extends Controller
{ 
    protected $branches; 
    protected $restaurant;
    protected $cuisine;

    /**
     * BranchController constructor. 
     * @param DealsRepository $deals_repository
     * @param RestaurantRepository $restaurant_repository
     */
    public function __construct(
        CuisineRepository $cuisine_repository, 
        RestaurantRepository $restaurant_repository,
        BranchRepository $branch_repository
    )
    {
        $this->cuisine = $cuisine_repository;
        $this->restaurant = $restaurant_repository;
        $this->branches = $branch_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $branches = Auth::user()->restaurant()->withTrashed()->first()->branches; 
        $branches = Branches::all();
        return view('admin.branches.index', [
            'branches' => $branches 
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBranches(Request $request)
    {
        // $branches = Auth::user()
        //     ->restaurant
        //     ->branches()
        //     ->orderBy('created_at', 'desc')
        //     ->paginate(10);

        // $branches = $this->branches->getBranches(Auth::user()->restaurant()->withTrashed()->first()->id); 
        $branches = $this->branches->getBranches(1); 
        foreach ($branches as $branch) {
            if ($branch->trashed()) {
                $branch->active = false;
            } else {
                $branch->active = true;
            }
        }

        return response()->json([
            'message' => 'success',
            'data' => [
                'branches' => $branches
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $weekdays = [
            1 => __('Monday'),
            2 => __('Tuesday'),
            3 => __('Wednesday'),
            4 => __('Thursday'),
            5 => __('Friday'),
            6 => __('Saturday'),
            0 => __('Sunday'),
        ];

        $times = [
            '12 AM',
            '12.30 AM',
            '1 AM',
            '1.30 AM',
            '2 AM',
            '2.30 AM',
            '3 AM',
            '3.30 AM',
            '4 AM',
            '4.30 AM',
            '5 AM',
            '5.30 AM',
            '6 AM',
            '6.30 AM',
            '7 AM',
            '7.30 AM',
            '8 AM',
            '8.30 AM',
            '9 AM',
            '9.30 AM',
            '10 AM',
            '10.30 AM',
            '11 AM',
            '11.30 AM',
            '12 PM',
            '12.30 PM',
            '1 PM',
            '1.30 PM',
            '2 PM',
            '2.30 PM',
            '3 PM',
            '3.30 PM',
            '4 PM',
            '4.30 PM',
            '5 PM',
            '5.30 PM',
            '6 PM',
            '6.30 PM',
            '7 PM',
            '7.30 PM',
            '8 PM',
            '8.30 PM',
            '9 PM',
            '9.30 PM',
            '10 PM',
            '10.30 PM',
            '11 PM',
            '11.30 PM',
        ];

        $cuisines = $this->cuisine->all();  
        $users = DB::table("users")->select('*')->where('role', '=', 'branch_admin')->whereNotIn('id', function($query) {
            $query->select('users_id')->from('branches');  
        })->get();  
        // dd($branch);

        return view('admin.branches.add', [ 
            'weekdays' => $weekdays,
            'times' => $times,
            'cuisines' => $cuisines,
            'users' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:branches,name',
            'address' => 'required',
            'county' => 'required',
            'country' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'phone' => 'required',
            'cuisines' => 'required',
            'seats' => 'required', 
        ]);

        $filtered = [];  
        if ($request->days) {
            foreach ($request->days as $day) {
                $filtered = Arr::where($request->days, function ($value, $key) use ($day) {
                    return $value['day'] == $day['day'];
                });
            }
        }

        foreach ($filtered as $key => $filter) {
            $filtered[$key]['opening_time'] = Carbon::parse($filter['opening_time'])->toDateTimeString();
            $filtered[$key]['closing_time'] = Carbon::parse($filter['closing_time'])->toDateTimeString();
        }

        $filtered = collect($filtered);  
        $filtered = $filtered->sortBy(function ($filter) {
            return [$filter['day'], $filter['opening_time']];
        })->toArray(); 
        sort($filtered);

        $weekdays = [
            1 => __('Monday'),
            2 => __('Tuesday'),
            3 => __('Wednesday'),
            4 => __('Thursday'),
            5 => __('Friday'),
            6 => __('Saturday'),
            0 => __('Sunday'),
        ];

        foreach ($filtered as $key => $item) {
            if (array_key_exists($key + 1, $filtered)) {
                if ($item['day'] == $filtered[$key + 1]['day']) {
                    if (Carbon::parse($filtered[$key + 1]['opening_time'])->lt(Carbon::parse($item['closing_time']))) {
                        $validator = Validator::make($request->all(), [
                            'days' => [
                                function ($attribute, $value, $fail) use ($weekdays, $item) {
                                    $fail($weekdays[$item['day']] . ' has time conflicts. Please resolve to proceed');
                                }
                            ]
                        ]);
                        if ($validator->fails()) {
                            return redirect(route('admin.branches.edit', $branch))
                                ->withErrors($validator)
                                ->withInput();
                        }
                    }
                }
            }
        }

        // $request->online_from_time = Carbon::parse($request->online_from_time)->toDateTimeString();
        // $request->online_to_time = Carbon::parse($request->online_to_time)->toDateTimeString();

        if ($request->parking == 'true') {
            $request->request->set('parking', true);
        } else {
            $request->request->set('parking', false);
        }

        if ($request->delivery == 'true') {
            $request->request->set('delivery', true);
        } else {
            $request->request->set('delivery', false);
        }

        if ($request->takeaway == 'true') {
            $request->request->set('takeaway', true);
        } else {
            $request->request->set('takeaway', false);
        }

        if ($request->reserve == 'true') {
            $request->request->set('reserve', true);
        } else {
            $request->request->set('reserve', false);
        }   

        $request->request->set('restaurant_id', 1);
        $request->request->set('email', $request->address);

        // $branch->update($request->except([
        //     'days',
        //     '_token',
        //     '_method',
        //     'street',
        //     'selected',
        //     'cuisines', 
        // ])); 
        $branch = $this->branches->createByRequest($request->restaurant_id, $request->all());

        if ($request->cuisines) {
            foreach ($request->cuisines as $cuisine) {
                $branch->cuisines()->attach($cuisine);
            }
        }

        if ($request->days) {
            $branch->openingHours()->forceDelete();
            foreach ($request->days as $key => $day) {
                if ($day['opening_time'] || $day['closing_time']) {
                    $day['branches_id'] = $branch->id;
                    $branch->openingHours()->create($day);
                }
            }
        }

        if ($request->wantsJson()) {
            return response()->json([
                'message' => 'success',
                'data' => []
            ]);
        } else {
            $request->session()->flash('success', 'Branch Added successfully!');
            return redirect(route('admin.branches.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $weekdays = [
            1 => __('Monday'),
            2 => __('Tuesday'),
            3 => __('Wednesday'),
            4 => __('Thursday'),
            5 => __('Friday'),
            6 => __('Saturday'),
            0 => __('Sunday'),
        ];

        $times = [
            '12 AM',
            '12.30 AM',
            '1 AM',
            '1.30 AM',
            '2 AM',
            '2.30 AM',
            '3 AM',
            '3.30 AM',
            '4 AM',
            '4.30 AM',
            '5 AM',
            '5.30 AM',
            '6 AM',
            '6.30 AM',
            '7 AM',
            '7.30 AM',
            '8 AM',
            '8.30 AM',
            '9 AM',
            '9.30 AM',
            '10 AM',
            '10.30 AM',
            '11 AM',
            '11.30 AM',
            '12 PM',
            '12.30 PM',
            '1 PM',
            '1.30 PM',
            '2 PM',
            '2.30 PM',
            '3 PM',
            '3.30 PM',
            '4 PM',
            '4.30 PM',
            '5 PM',
            '5.30 PM',
            '6 PM',
            '6.30 PM',
            '7 PM',
            '7.30 PM',
            '8 PM',
            '8.30 PM',
            '9 PM',
            '9.30 PM',
            '10 PM',
            '10.30 PM',
            '11 PM',
            '11.30 PM',
        ];

        $cuisines = $this->cuisine->all(); 
        $branch = $this->branches->get($id); 
        $branch_user = User::find($branch->users_id);
        $branch->user = $branch_user->email;
        $branch->user_id = $branch_user->id;
        // dd($branch->user); 

        $users = DB::table("users")->select('*')->where('role', '=', 'branch_admin')->whereNotIn('id', function($query) {
            $query->select('users_id')->from('branches');  
        })->get();

        if ($branch->online_from_time) {
            $branch->online_from_time = Carbon::parse($branch->online_from_time)->format('Y-m-d\TH:i');
        }
        if ($branch->online_to_time) {
            $branch->online_to_time = Carbon::parse($branch->online_to_time)->format('Y-m-d\TH:i');
        }   

        return view('admin.branches.edit', [
            'branch' => $branch, 
            'weekdays' => $weekdays,
            'times' => $times,
            'cuisines' => $cuisines,
            'users' => $users,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branches $branch)
    { 
        $filtered = [];  
        if ($request->days) {
            foreach ($request->days as $day) {
                $filtered = Arr::where($request->days, function ($value, $key) use ($day) {
                    return $value['day'] == $day['day'];
                });
            }
        }

        foreach ($filtered as $key => $filter) {
            $filtered[$key]['opening_time'] = Carbon::parse($filter['opening_time'])->toDateTimeString();
            $filtered[$key]['closing_time'] = Carbon::parse($filter['closing_time'])->toDateTimeString();
        }

        $filtered = collect($filtered);  
        $filtered = $filtered->sortBy(function ($filter) {
            return [$filter['day'], $filter['opening_time']];
        })->toArray(); 
        sort($filtered);

        $weekdays = [
            1 => __('Monday'),
            2 => __('Tuesday'),
            3 => __('Wednesday'),
            4 => __('Thursday'),
            5 => __('Friday'),
            6 => __('Saturday'),
            0 => __('Sunday'),
        ];

        foreach ($filtered as $key => $item) {
            if (array_key_exists($key + 1, $filtered)) {
                if ($item['day'] == $filtered[$key + 1]['day']) {
                    if (Carbon::parse($filtered[$key + 1]['opening_time'])->lt(Carbon::parse($item['closing_time']))) {
                        $validator = Validator::make($request->all(), [
                            'days' => [
                                function ($attribute, $value, $fail) use ($weekdays, $item) {
                                    $fail($weekdays[$item['day']] . ' has time conflicts. Please resolve to proceed');
                                }
                            ]
                        ]);
                        if ($validator->fails()) {
                            return redirect(route('admin.branches.edit', $branch))
                                ->withErrors($validator)
                                ->withInput();
                        }
                    }
                }
            }
        }

        // $request->online_from_time = Carbon::parse($request->online_from_time)->toDateTimeString();
        // $request->online_to_time = Carbon::parse($request->online_to_time)->toDateTimeString();

        if ($request->parking == 'true') {
            $request->request->set('parking', true);
        } else {
            $request->request->set('parking', false);
        }

        if ($request->delivery == 'true') {
            $request->request->set('delivery', true);
        } else {
            $request->request->set('delivery', false);
        }

        if ($request->takeaway == 'true') {
            $request->request->set('takeaway', true);
        } else {
            $request->request->set('takeaway', false);
        }

        if ($request->reserve == 'true') {
            $request->request->set('reserve', true);
        } else {
            $request->request->set('reserve', false);
        }   
        $request->request->set('email', $request->address); 

        $branch->update($request->except([
            'days',
            '_token',
            '_method',
            'street',
            'selected',
            'cuisines', 
        ])); 

        $branch->cuisines()->detach();

        // $branch->update([
        //     'online_from_time' => $request->online_from_time,
        //     'online_to_time' => $request->online_to_time,
        // ]); 

        if ($request->cuisines) {
            foreach ($request->cuisines as $cuisine) {
                $branch->cuisines()->attach($cuisine);
            }
        }

        if ($request->days) {
            $branch->openingHours()->forceDelete();
            foreach ($request->days as $key => $day) {
                if ($day['opening_time'] || $day['closing_time']) {
                    $day['branches_id'] = $branch->id;
                    $branch->openingHours()->create($day);
                }
            }
        }

        if ($request->wantsJson()) {
            return response()->json([
                'message' => 'success',
                'data' => []
            ]);
        } else {
            $request->session()->flash('success', 'Updated successfully!');
            return redirect(route('admin.branches.edit', $branch));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactive(Request $request)
    {
        $branch_id = $request->branch_id; 
        $branch = $this->branches->get($branch_id); 

        if ($branch->trashed()) {
            $result = $branch->restore();
            $message = 'Branch reactivated successfully!';
        } else {
            $result = $branch->delete();
            $message = 'Branch deactivated successfully!';
        } 
        if ($result) {
            return response()->json([
                'message' => 'success',
                'result' => $message,
                'data' => []
            ]);
        }
    }
}
