<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\MainSliderImages;  
use App\Branches;  
use App\MenuitemSliders;
use App\BranchMenuItems;
use App\CuisineMenuItem;
use App\MenuItemMenu;
use App\Attributes;
use App\Menu;
use App\MenuItem;
use App\MenuItemReviews;
use App\Http\Controllers\Controller; 
use App\Repository\MainSliderImagesRepositary;  
use App\Repository\MenuItemReviewsRepository;  
use App\Repository\BranchRepository; 
use App\Repository\MenuRepository;
use App\Repository\MenuItemRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Class MenuItemController
 * @package App\Http\Controllers\Admin
 */
class MenuItemController extends Controller
{
    protected $slider_images;
    protected $menu;
    protected $menu_item;
    protected $menu_item_reviews;

    /**
     * MenuItemController constructor.
     * @param MenuRepository $menu_repository
     * @param MenuItemRepository $menu_item_repository
     * @param MainSliderImagesRepositary $slider_images_repository
     * @param MenuItemReviewsRepository $menuItem_reviews_repository
     */
    public function __construct(MenuRepository $menu_repository, MenuItemRepository $menu_item_repository, MainSliderImagesRepositary $slider_images_repository, MenuItemReviewsRepository $menuItem_reviews_repository)
    {
        $this->menu = $menu_repository;
        $this->menu_item = $menu_item_repository;
        $this->slider_images = $slider_images_repository; 
        $this->menu_item_reviews = $menuItem_reviews_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cuisinename' => 'required',
            'menu_id' => 'required',
            'name' => 'required|unique:menu_items,name',
            'price' => 'required|numeric|min:0',
            'description' => 'nullable|max:255',
            // 'logo' => 'required',
        ],[
            'cuisinename.required' => 'Cuisine is required',
            'menu_id.required' => 'Category is required',
        ]);

        if ($request->all_branches == false && $request->branches == []) {
            $this->validate($request, [
                'branches' => 'required|unique:menu_items,branches'
            ]);
        }

        $request->request->set('user_id', Auth::id()); 
        $logo = $request->logo;

        if ($logo) { 
            $extension = explode(';', explode('image/', $logo)[1])[0]; 
            $result = substr($logo, strpos($logo, 'image/', '6'), 4); 
            $file_name = 'cuisines/logo_' . Carbon::now()->format('YmdHis') . '.' . $extension; 
            // $logo = str_replace('data:image/' . $extension . ';base64,', '', $logo);
            // $logo = str_replace(' ', '+', $logo); 
            // Storage::disk('local')->put($file_name, base64_decode($logo)); 
            $logo = $request->logo;
            $filename = 'logo_' . Carbon::now()->format('YmdHis') . '.' . $extension; 
            $image_resize = Image::make($logo);              
            $image_resize->crop(391, 386); 
            $image_resize->save(storage_path('app/public/cuisines/' .$filename));    
            $request->request->set('logo', $file_name);
            
            if($request->logo){ 
                $img = Image::make($logo);
                $img->resize(75, 70, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(storage_path('app/public/cuisines/mobile/cuisines/' .$filename));
            }
            
            if($request->available_status == true){
                $request->request->set('available_status', 1);
            }else{
                $request->request->set('available_status', 0);
            }
            if($request->available_item == true){
                $request->request->set('available_item', 1);
            }else{
                $request->request->set('available_item', 0);
            } 
        }
        if (\config('app.product_type') == 'single') {
            $menu_item = $this->menu_item->createByRequest($request->all());
            foreach ($request->cuisinename as $cuisine_id) {
                $menu_item->cuisines()->attach($cuisine_id);
            }

            foreach ($request->variants as $variant) {
                $variant_array = ['name' => $variant['name']];
                $variant_object = $this->menu_item->firstOrCreateVariant($variant_array);
                $menu_item->variants()->attach($variant_object->id, ['price' => $variant['pivot']['price']]);
            } 

            foreach ($request->addons as $addon) {
                $addon_array = ['name' => $addon['name']];
                $addon_object = $this->menu_item->firstOrCreateAddon($addon_array);
                // $menu_item->addons()->attach($addon_object->id, ['price' => $addon['pivot']['price']]);
                if (in_array($addon['name'], $request->selected_addons)) {  
                    $menu_item->addons()->attach($addon_object->id, ['price' => $addon['pivot']['price'], 'fixed' => 1]);
                }else{ 
                    $menu_item->addons()->attach($addon_object->id, ['price' => $addon['pivot']['price']]);
                } 
            } 

            foreach ($request->crust_types as $crust_type) {
                $crust_type_array = ['name' => $crust_type['name']];
                $crust_type_object = $this->menu_item->firstOrCreateCrustType($crust_type_array);
                $menu_item->crustType()->attach($crust_type_object->id, ['price' => $crust_type['pivot']['price']]);
            }

            // foreach ($request->payable_attributes as $payable_attribute) {
            //     $payable_attribute_array = ['name' => $payable_attribute['name']];
            //     $payable_attribute_object = Attributes::firstOrCreate($payable_attribute_array); 
            //     $menu_item->attributes()->attach($payable_attribute_object->id, ['values' => $payable_attribute['pivot']['values']]);
            // }

            // foreach ($request->non_payable_attributes as $non_payable_attribute) {
            //     $non_payable_attribute_array = ['name' => $non_payable_attribute['name']];
            //     $non_payable_attribute_array = Attributes::firstOrCreate($non_payable_attribute_array); 
            //     $menu_item->attributes()->attach($non_payable_attribute_array->id, ['values' => $non_payable_attribute['pivot']['values']]);
            // } 

            // foreach ($request->similar_menu_name as $similar_menu_id) {
            //     $menu_item->menus()->attach($similar_menu_id);
            // }

            if($request->slider){  
                $this->slider_images->createMenuitemSlider($menu_item->id, $request->slider);
            }

            if ($request->all_branches) {
                $branches = Branches::all();
                foreach ($branches as $value) {
                    $this->menu_item->createBranchMenuItem($value->id, $menu_item->id);
                }
            }else{  
                $branches = $request->branches;
                if (Auth::user()->role == 'admin') {
                    foreach ($branches as $value) {
                        $this->menu_item->createBranchMenuItem($value, $menu_item->id);
                    }
                }else{
                    $this->menu_item->createBranchMenuItem($branches, $menu_item->id);
                } 
            }
        } else {
            $menu_item = $this->menu_item->createClone($request->all());

            foreach ($request->variants as $variant) {
                $variant['menu_item_id'] = $menu_item->id;
                $this->menu_item->createVariantClone($variant);
            }

            foreach ($request->addons as $addon) {
                $addon['menu_item_id'] = $menu_item->id;
                $this->menu_item->createAddonClone($addon);
            }

            if($request->slider){  
                $this->slider_images->createMenuitemSlider($menu_item->id, $request->slider);
            }
        } 
        $menu_item->logo = asset('storage/'.$menu_item->logo);  
        if ($menu_item) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'menu_item' => $menu_item
                ]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\MenuItem $menuItem
     * @return \Illuminate\Http\Response
     */
    public function show(MenuItem $menuItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\MenuItem $menuItem
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuItem $menuItem)
    {
        //
    }

    /**
     * @param Request $request
     * @param MenuItem $menuItem
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, MenuItem $menuItem)
    {  
        $this->validate($request, [
            'cuisine_id' => 'required',
            'menu_id' => 'required',
            'name' => ['required'],
            'price' => 'required|numeric|min:0',
            'description' => 'nullable|max:255',
        ], [
            'cuisine_id.required' => 'Cuisine is required',
            'menu_id.required' => 'Category is required'
        ]);

        if ($request->all_branches == false && $request->branches == []) {
            $this->validate($request, [
                'branches' => 'required|unique:menu_items,branches'
            ]);
        }

        $request->request->set('user_id', Auth::id());
        $request->request->set('menu_item_id', $menuItem->id); 

        if ($request->logo) {

            $logo = $request->logo;
            $extension = explode(';', explode('image/', $logo)[1])[0];
            $result = substr($logo, strpos($logo, 'image/', '6'), 4);
            $file_name = 'cuisines/logo_' . Carbon::now()->format('YmdHis') . '.' . $extension;
            // $logo = str_replace('data:image/' . $extension . ';base64,', '', $logo);
            // $logo = str_replace(' ', '+', $logo);
            // Storage::disk('local')->put($file_name, base64_decode($logo));

            $logo = $request->logo;
            $filename = 'logo_' . Carbon::now()->format('YmdHis') . '.' . $extension; 
            $image_resize = Image::make($logo);              
            $image_resize->crop(391, 386); 
            $image_resize->save(storage_path('app/public/cuisines/' .$filename));  
            $request->request->set('logo', $file_name);
            
            if($request->logo){ 
                $img = Image::make($logo);
                $img->resize(75, 70, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(storage_path('app/public/cuisines/mobile/cuisines/' .$filename));
            }
        } else {
            $request->request->set('logo', $menuItem->logo);
        }
        $request->request->set('menu_id', $menuItem->menu()->first()->id); 
        if($request->available_status == true){
            $request->request->set('available_status', 1);
        }else{
            $request->request->set('available_status', 0);
        } 
        if($request->available_item == true){

            $request->request->set('available_item', 1);
        }else{ 
            $request->request->set('available_item', 0);
        }

        if (\config('app.product_type') == 'single') {
            $menuItem->update($request->except(['_token', 'addons', 'variants', 'deleted', 'user_id'])); 
            CuisineMenuItem::where('menu_item_id', '=', $menuItem->id)->delete();
            if (is_array($request->cuisine_id)) {
                foreach ($request->cuisine_id as $cuisine_id) {
                    $assignedCuisine = New CuisineMenuItem();
                    $assignedCuisine->cuisine_id = $cuisine_id;
                    $assignedCuisine->menu_item_id = $menuItem->id;
                    $assignedCuisine->save();
                }
            }
            MenuItemMenu::where('menuitem_id', '=', $menuItem->id)->delete(); 
            if (is_array($request->similar_menu_id)) {
                foreach ($request->similar_menu_id as $similar_menu_id) { 
                    $assignedSimilarMenu = New MenuItemMenu();
                    $assignedSimilarMenu->menu_id = $similar_menu_id;
                    $assignedSimilarMenu->menuitem_id = $menuItem->id;
                    $assignedSimilarMenu->save();
                }
            }
            $variants_ids = []; 
            foreach ($request->variants as $variant) {
                $variant_object = $menuItem->variants()->updateOrCreate([
                    'variants.id' => $variant['id'],
                ], [
                    'name' => $variant['name'],
                ]);
                $variants_ids[$variant_object->id] = ['price' => $variant['pivot']['price']];
                if (!$variant_object->wasRecentlyCreated) {
                    $variant_object->update($variant);
                }
            }
            $addons_ids = [];  
            foreach ($request->addons as $addon) {
                $addon_object = $menuItem->addons()->updateOrCreate(['addons.id' => $addon['id']], ['name' => $addon['name']]);
                if (in_array($addon['id'], $request->selected_addons)) { 
                    $addons_ids[$addon_object->id] = ['price' => $addon['pivot']['price'], 'fixed' => 1];
                }else{ 
                    $addons_ids[$addon_object->id] = ['price' => $addon['pivot']['price']];
                } 
                if (!$addon_object->wasRecentlyCreated) {
                    $addon_object->update($addon);
                }
            }
            $crust_types_ids = [];  
            foreach ($request->crust_types as $crust_type) {
                $crust_type_object = $menuItem->crustType()->updateOrCreate([
                    'crust_types.id' => $crust_type['id'],
                ], ['name' => $crust_type['name'],]);
                $crust_types_ids[$crust_type_object->id] = ['price' => $crust_type['pivot']['price']];
                if (!$crust_type_object->wasRecentlyCreated) {
                    $crust_type_object->update($crust_type);
                }
            }  
            // $payable_attributes_ids = [];  
            // // dd($request->payable_attributes);
            // foreach ($request->payable_attributes as $payable_attribute) {
            //     $payable_attribute_object = $menuItem->attributes()->get();
            //     // dd($payable_attribute['id']);
            //     $payable_attributes_ids[$payable_attribute['id']] = ['values' => $payable_attribute['pivot']['values']];
            //     // if ($payable_attribute_object) {
            //     //     $payable_attribute_object->update($payable_attribute);
            //     // }
            // } 
            // // dd($payable_attributes_ids);
 
            // $non_payable_attributes_ids = [];  
            // // dd($request->non_payable_attributes);
            // foreach ($request->non_payable_attributes as $non_payable_attribute) {
            //     // $non_payable_attribute_object = $menuItem->attributes()->get(['attributes.id' => $non_payable_attribute['id']]);
            //     $non_payable_attribute_object = Attributes::where('id','=',$non_payable_attribute['id'])->get();
            //     // dd($non_payable_attribute_object);
            //     if($non_payable_attribute_object){
            //         $payable_attributes_ids[$non_payable_attribute_object[0]['id']] = ['values' => $non_payable_attribute['pivot']['values']];
            //         $menuItem->attributes()->sync($non_payable_attributes_ids);
            //     }else{
            //         $non_payable_attribute_array = ['name' => $non_payable_attribute['name']];
            //         $non_payable_attribute_array = Attributes::firstOrCreate($non_payable_attribute_array); 
            //         $menu_item->attributes()->attach($non_payable_attribute_array->id, ['values' => $non_payable_attribute['pivot']['values']]);
            //     }
            //     // if ($payable_attribute_object) {
            //     //     $payable_attribute_object->update($payable_attribute);
            //     // }
            // } 

            $menuItem->variants()->sync($variants_ids);
            $menuItem->addons()->sync($addons_ids);
            $menuItem->crustType()->sync($crust_types_ids);
            // $menuItem->attributes()->sync($payable_attributes_ids);
            // $menuItem->attributes()->sync($non_payable_attributes_ids);

            if($request->slider){
                MenuitemSliders::where('menu_items_id', '=', $menuItem->id)->where('deal', '=', 0)->delete(); 
                $this->slider_images->createMenuitemSlider($request->menu_item_id, $request->slider);
            }else{
                //MenuitemSliders::where('menu_items_id', '=', $menuItem->id)->delete(); 
            }

            if ($request->all_branches) {
                //$menuItem->branchMenuItems()->forceDelete();
                $branches = Branches::all();
                foreach ($branches as $value) {
                    BranchMenuItems::where('menuitem_id', '=', $request->menu_item_id)->where('branches_id', '=', $value->id)->delete(); 
                    $this->menu_item->createBranchMenuItem($value->id, $request->menu_item_id);
                }
            }else{  
                //$menuItem->branchMenuItems()->forceDelete();
                // $rest_branches = Branches::all();
                // foreach ($rest_branches as $value) {
                //     BranchMenuItems::where('menuitem_id', '=', $request->menu_item_id)->where('branches_id', '=', $value->id)->delete();
                // }
                $branches = $request->branches;
                if (Auth::user()->role == 'admin') {
                    foreach ($branches as $value) { 
                        BranchMenuItems::where('menuitem_id', '=', $request->menu_item_id)->where('branches_id', '=', $value)->delete();
                        $this->menu_item->createBranchMenuItem($value, $request->menu_item_id);
                    }
                }else{
                    $this->menu_item->createBranchMenuItem($branches, $request->menu_item_id);
                } 
            }
        } else {

            $menu_item = $menuItem->menuItemClone()->firstOrCreate($request->except(['_token', 'addons', 'variants', 'deleted', 'user_id']));
            $this->menu_item->deleteVariantClone($menu_item->id);
            foreach ($request->variants as $variant) {
                $variant['menu_item_id'] = $menu_item->id;
                $this->menu_item->createVariantClone($variant);
            }
            $this->menu_item->deleteAddonClone($menu_item->id);
            foreach ($request->addons as $addon) {
                $addon['menu_item_id'] = $menu_item->id;
                $this->menu_item->createAddonClone($addon);
            }
            if ($request->deleted) {
                $menu_item->delete();
            }
            if($request->slider){
                MenuitemSliders::where('menu_items_id', '=', $menuItem->id)->where('deal', '=', 0)->delete(); 
                $this->slider_images->createMenuitemSlider($request->menu_item_id, $request->slider);
            }else{
                //MenuitemSliders::where('menu_items_id', '=', $menuItem->id)->delete(); 
            }
            if ($request->all_branches) {
                $branches = Branches::all();
                foreach ($branches as $value) {
                    $this->menu_item->createBranchMenuItem($value->id, $menu_item->id);
                }
            }else{  
                $branches = $request->branches;
                if (Auth::user()->role == 'admin') {
                    foreach ($branches as $value) {
                        $this->menu_item->createBranchMenuItem($value, $menu_item->id);
                    }
                }else{
                    $this->menu_item->createBranchMenuItem($branches, $menu_item->id);
                } 
            }
        }
        $menuItem->logo = asset('storage/'.$menuItem->logo);
        // $available_sliders = DB::table("main_slider_images")->select('*')->whereNotIn('id', function($query) {
        //    $query->select('main_slider_images_id')->from('menuitem_sliders');
        // })->get();
        if ($menuItem) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'menu_item' => $menuItem
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\MenuItem $menuItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuItem $menuItem)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete(Request $request)
    {
        $menu_item_id = $request->menu_item_id; 
        $menu_item = $this->menu_item->get($menu_item_id); 
        $menu_item->menu_item_id = $menu_item_id; 
        if ($menu_item->delete()) {
            return response()->json([
                'message' => 'success',
                'data' => []
            ]);
        } 
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDeleteSlider(Request $request)
    {
        $menu_item_id = $request->menu_item_id; 
        $delete = MenuitemSliders::where('menu_items_id', '=', $menu_item_id)->where('deal', '=', 0)->delete(); 
        if ($delete) {
            return response()->json([
                'message' => 'success',
                'data' => []
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDeleteBranch(Request $request)
    {
        $menu_item_id = $request->menu_item_id; 
        $branch_id = $request->branch_id;
        $delete = BranchMenuItems::where('menuitem_id', '=', $menu_item_id)->where('branches_id', '=', $branch_id)->delete(); 
        if ($delete) {
            return response()->json([
                'message' => 'success',
                'data' => []
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenuItemById(Request $request)
    {
        // $menu = Auth::user()->restaurant()->withTrashed()->first()->menus()->where('id', '=', $request->menu_id)->first();
        $menu = $this->menu->all()->where('id', '=', $request->menu_id)->first(); 
        $menuItems = $menu->menuItems()->select('id', 'name')->whereNull('deleted')->get(); 
        return response()->json([
            'message' => 'success',
            'data' => [
                'menuItems' => $menuItems
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenuItemReviews(Request $request)
    { 
        $reviews = $this->menu_item_reviews->getReviews($request->menu_item_id); 
        foreach ($reviews as $review) { 
            $user = User::where('id', '=', $review->user_id)->first();
            $review->user = $user['first_name'] .' '. $user['last_name'];
        }
        return response()->json([
            'message' => 'success',
            'data' => [
                'reviews' => $reviews
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactiveReview(Request $request)
    {
        $review_id = $request->review_id; 
        $review = MenuItemReviews::where('id', '=', $review_id)->first();

        $result = $review->delete(); 
        if ($result) {
            return response()->json([
                'message' => 'success', 
                'data' => []
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptReview(Request $request)
    {
        $review_id = $request->review_id; 
        $review = MenuItemReviews::where('id', '=', $review_id)->first(); 
        $review->approve_status = 1;
        $review->save(); 
        if ($review->save()) {
            return response()->json([
                'message' => 'success', 
                'data' => []
            ]);
        }
    }
}
