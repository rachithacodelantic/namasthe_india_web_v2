<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PaymentMethod;
use App\Branches; 
use App\Repository\PaymentMethodRepository;
use App\Repository\BranchRepository; 
use Illuminate\Http\Request;

/**
 * Class PaymentMethodController
 * @package App\Http\Controllers\MasterAdmin
 */
class PaymentMethodController extends Controller
{ 
    protected $payment_method;
    protected $branches; 

    public function __construct(PaymentMethodRepository $payment_method_repository, BranchRepository $branch_repository)
    {
        $this->payment_method = $payment_method_repository;
        $this->branches = $branch_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $payment_methods = $this->payment_method->all();
        $branches = Branches::all();
        foreach ($payment_methods as $payment_method) {
        	$payment_method->branch = $this->branches->get($payment_method->branches_id)->name; 
        } 
    	return view('admin.payment-method.index', ['branches' => $branches]); 
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMethods(Request $request)
    { 
        $payment_methods = $this->payment_method->all();
        foreach ($payment_methods as $payment_method) {
        	$payment_method->branch = $this->branches->get($payment_method->branches_id)->name; 
        } 
        return response()->json([
            'message' => 'success',
            'data' => [
                'payment_methods' => $payment_methods
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master-admin.payment-method.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    { 
        $checked_method = $this->payment_method->checkMethod($request->all()); 
        if ($checked_method->count()) {
    		return response()->json([
                'message' => 'This method is already assigned to branch.'
            ], 402);
        }else{  
        	// $payment_method = $this->payment_method->create($request->all()); 
        	$payment_method = new PaymentMethod();
            $payment_method->branches_id = $request->branches_id;
            $payment_method->name = $request->name;
            $payment_method->save();

            return response()->json([
	            'message' => 'success'
	        ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PaymentMethod $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentMethod $paymentMethod)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\PaymentMethod $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentMethod $paymentMethod)
    {
        return view('master-admin.payment-method.edit', ['payment_method' => $paymentMethod]);
    }

    /**
     * @param Request $request
     * @param PaymentMethod $paymentMethod
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, PaymentMethod $paymentMethod)
    {
        $this->validate($request, ['name' => 'required']);

        $payment_method = $paymentMethod->update($request->all());

        if ($payment_method) {
            return redirect(route('master-admin.payment-method.edit', $paymentMethod));
        }
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, PaymentMethod $paymentMethod)
    { 
        $delete = PaymentMethod::findOrFail($request->method_id); 
    	$delete->delete();

        if ($delete) {
            return response()->json([
                'message' => 'success'
            ]);
        }
    }
}

