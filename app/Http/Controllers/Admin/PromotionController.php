<?php

namespace App\Http\Controllers\Admin;

use App\Promotion;
use App\Repository\RestaurantRepository;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PromotionController extends Controller
{ 
    protected $restaurant;

    /**
     * RestaurantController constructor. 
     * @param RestaurantRepository $restaurant_repository
     */
    public function __construct(RestaurantRepository $restaurant_repository)
    { 
        $this->restaurant = $restaurant_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant_id = 1;
        $restaurant = $this->restaurant->getRestaurant($restaurant_id); 
        // $restaurant = Auth::user()->restaurant()->withTrashed()->first();
        $promotions = $restaurant->promotions()->withTrashed()->get();

        foreach ($promotions as $promotion) {

            $promotion->deleted = false;

            if ($promotion->trashed()) {
                $promotion->deleted = true;
            }

            $promotion->usage_count = $promotion->deliveries()->count() + $promotion->takeaways()->count();
        }

        return view('admin.promotion.index', [
            'promotions' => $promotions,
            'restaurant' => $restaurant
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restaurant_id = 1;
        if (config('app.product_type') == 'single') { 
            // $restaurant = Auth::user()->restaurant()->withTrashed()->first();
            $restaurant = $this->restaurant->getRestaurant($restaurant_id);  
            $restaurant->update($request->except([
                'promotions',
                '_token',
                'usage_count'
            ]));

            // dd($request->promotions);

            if ($request->promotions) {
                $restaurant->promotions()->forceDelete();

                foreach ($request->promotions as $promotion) {
                    // dd($promotion['person_limit']);
                    $promotion['restaurant_id'] = $restaurant->id;
                    $promotion_copy = $promotion;
                    unset($promotion_copy['deleted']);
                    unset($promotion_copy['usage_count']);
                    // dd($promotion_copy);
                    // $clone = $restaurant->promotions()->firstOrCreate($promotion_copy);
                    // $clone = $restaurant->promotions()->Create($promotion_copy);

                    $clone_new = DB::table('promotions')->insert([
                        'restaurant_id' => $promotion['restaurant_id'],
                        'promocode' => $promotion['promocode'],
                        'type' => $promotion['type'],
                        'value' => $promotion['value'],
                        'expiry_date' => $promotion['expiry_date'],
                        'limit' => $promotion['limit'],
                        'person_limit' => $promotion['person_limit'],
                        'start_date' => $promotion['start_date'], 
                    ]);

                    // dd($clone); 

                    if (isset($promotion['deleted'])) {
                        if ($promotion['deleted']) {
                            $clone = $restaurant->promotions()->firstOrCreate($promotion_copy);
                            $clone->delete();
                        } else {

                            // $clone = DB::table('promotions')->insert([
                            //     'restaurant_id' => $promotion['restaurant_id'],
                            //     'promocode' => $promotion['promocode'],
                            //     'type' => $promotion['type'],
                            //     'value' => $promotion['value'],
                            //     'expiry_date' => $promotion['expiry_date'],
                            //     'limit' => $promotion['limit'],
                            //     'person_limit' => $promotion['person_limit'],
                            //     'start_date' => $promotion['start_date'], 
                            // ]); 
                            // $clone->restore();
                        }
                    }
                }
            } 

            return response()->json([
                'message' => 'success',
                'data' => [

                ]
            ]);
        } else {
            $restaurant_id = 1;
            $restaurant = $this->restaurant->getRestaurant($restaurant_id);  
            // $restaurant = Auth::user()->restaurant()->withTrashed()->first();

            $count = $restaurant->restaurantClone()->count();

            if ($count) {
                $restaurant->restaurantClone()->update($request->except([
                    'promotions',
                    '_token',
                    'usage_count'
                ]));
            } else {
                $restaurant->restaurantClone()->create($restaurant->toArray());
                $restaurant->restaurantClone()->update($request->except([
                    'promotions',
                    '_token',
                    'usage_count'
                ]));
            }

            if ($request->promotions) {
                $restaurant->promotionClones()->forceDelete();
                foreach ($request->promotions as $promotion) {
                    $promotion['restaurant_id'] = $restaurant->id;
                    $promotion_copy = $promotion;
                    unset($promotion_copy['deleted']);
                    unset($promotion_copy['usage_count']);
                    // $clone = $restaurant->promotionClones()->firstOrCreate($promotion_copy);
                    $clone_new = DB::table('promotions')->insert([
                        'restaurant_id' => $promotion['restaurant_id'],
                        'promocode' => $promotion['promocode'],
                        'type' => $promotion['type'],
                        'value' => $promotion['value'],
                        'expiry_date' => $promotion['expiry_date'],
                        'limit' => $promotion['limit'],
                        'person_limit' => $promotion['person_limit'],
                        'start_date' => $promotion['start_date'], 
                    ]);

                    if (isset($promotion['deleted'])) {
                        if ($promotion['deleted']) {
                            $clone = $restaurant->promotionClones()->firstOrCreate($promotion_copy);
                            $clone->delete();
                        } else {
                            // $clone->restore();
                        }
                    }
                }
            } 

            return response()->json([
                'message' => 'success',
                'data' => [

                ]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        //
    }
}
