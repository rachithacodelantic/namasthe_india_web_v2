<?php

namespace App\Http\Controllers\Admin;

use App\MainSliderImages;     
use App\Http\Controllers\Controller;
use App\Menu;
use App\MenuItem;
use App\User;
use App\Branches; 
use App\Cuisine;
use App\Restaurant;
use App\Attributes;
use App\Repository\MainSliderImagesRepositary; 
use App\Repository\RestaurantRepository;
use App\Repository\BranchRepository; 
use App\Repository\AddonRepository;
use App\Repository\CrustTypeRepository;
use App\Repository\MenuItemRepository;
use App\Repository\MenuRepository;
use App\Repository\VariantRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class MenuController
 * @package App\Http\Controllers\Admin
 */
class MenuController extends Controller
{
    protected $menu;
    protected $menu_item;
    protected $variant;
    protected $addon;
    protected $slider_images;
    protected $branches; 
    protected $restaurant;

    /**
     * MenuController constructor.
     * @param MenuRepository $menu_repository
     * @param MenuItemRepository $menu_item_repository
     * @param VariantRepository $variant_repository
     * @param AddonRepository $addon_repository
     * @param CrustTypeRepository $crustType_repository
     * @param MainSliderImagesRepositary $slider_images_repository
     */
    public function __construct(
        MenuRepository $menu_repository,
        MenuItemRepository $menu_item_repository,
        VariantRepository $variant_repository,
        MainSliderImagesRepositary $slider_images_repository, 
        AddonRepository $addon_repository,
        CrustTypeRepository $crustType_repository,
        BranchRepository $branch_repository,
        RestaurantRepository $restaurant_repository
    )
    {
        $this->menu = $menu_repository;
        $this->menu_item = $menu_item_repository;
        $this->variant = $variant_repository;
        $this->addon = $addon_repository;
        $this->crust_type = $crustType_repository; 
        $this->slider_images = $slider_images_repository; 
        $this->branches = $branch_repository;
        $this->restaurant = $restaurant_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        if (Auth::user()->role == 'admin') { 
            $images = DB::table("main_slider_images")->select('*')->whereNotIn('id', function($query) {
                $query->select('main_slider_images_id')->from('menuitem_sliders');
                //$query->select('main_slider_images_id')->from('menuitem_sliders')->where('deal', '=', 0); 
            })->get(); 
            //$images = $this->slider_images->getSliderImages(1);  

            // dd(Auth::user(Auth::user()->restaurant()->withTrashed()->first()->branches));
            // $menus = Auth::user()->restaurant()->withTrashed()->first()->menus;
            $menus = $this->menu->all(); 
            $variants = $this->variant->all();
            $addons = $this->addon->all(); 
            $crust_types = $this->crust_type->all(); 
            $payable_attributes = Attributes::where('payable', 1)->get();
            $non_payable_attributes = Attributes::where('payable', 0)->get();
            $branches = Branches::all();
        }else{
            $images = '';
            $menus = ''; 
            $userBranch = $this->branches->getByUser(Auth::user()->id); 
            if($userBranch){
                $menus = $this->branches->getMenuBranches($userBranch->id); 
            }
            $variants = $this->variant->all();
            $addons = $this->addon->all(); 
            $crust_types = $this->crust_type->all(); 
            $payable_attributes = Attributes::where('payable', 1)->get();
            $non_payable_attributes = Attributes::where('payable', 0)->get();
            $branches = $userBranch; 
        } 
        $restaurant_id = 1;
        $restaurant = $this->restaurant->getRestaurant($restaurant_id);  
        return view('admin.menu.index', [
            'restaurant' => $restaurant,
            'menus' => $menus,
            'variants' => $variants,
            'addons' => $addons,
            'crust_types' => $crust_types,
            'payable_attributes' => $payable_attributes,
            'non_payable_attributes' => $non_payable_attributes,
            'sliders' => $images,
            'branches' => $branches,
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenus()
    {
        if (Auth::user()->role == 'admin') { 
            $images = DB::table("main_slider_images")->select('*')->whereNotIn('id', function($query) {
               $query->select('main_slider_images_id')->from('menuitem_sliders');
            })->get(); 
            //$images = $this->slider_images->getSliderImages(1);  
            //$menus = Auth::user()->restaurant()->withTrashed()->first()->menus; 
            $menus = $this->menu->all();
        }else{
            $images = '';
            $menus = '';
            $userBranch = $this->branches->getByUser(Auth::user()->id); 
            if($userBranch){
                $menus = $this->branches->getMenuBranches($userBranch->id); 
            } 
        } 
        //$menus = Auth::user()->restaurant()->withTrashed()->first()->menus()->get();

        return response()->json([
            'message' => 'success',
            'data' => [
                'menus' => $menus,
                'sliders' => $images,
            ]
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCuisines()
    {
        // $cuisines = Auth::user()->restaurant()->withTrashed()->first()->cuisines()->get();
        $cuisines = Cuisine::all(); 
        return response()->json([
            'message' => 'success',
            'data' => [
                'cuisines' => $cuisines
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenu(Request $request)
    {
        if ($request->menu_id == 0) {  
            $menu_items = [];
            if (Auth::user()->role == 'admin') {  
                //$menus = Auth::user()->restaurant()->withTrashed()->first()->menus()->get();
                $menus = $this->menu->all();
                foreach ($menus as $menu) {
                    $items = $menu->menuItems()->whereNull('deleted')->paginate(6); 
                    $menu->menu_items_pagination = [
                        'current_page' => $items->currentPage(),
                        'from' => $items,
                        'last_page' => $items->lastPage(),
                        'path' => $items,
                        'per_page' => $items->perPage(),
                        'to' => $items,
                        'total' => $items->total(),
                    ];
                    foreach ($items as $item) {
                        $item->isInAllBranchStatus = 1;
                        $item->variants = $item->variants()->get();
                        $item->addons = $item->addons()->get();
                        $item->crust_types = $item->crustType()->get(); 
                        $item->payable_attributes = $item->attributes()->where('payable', 1)->get(); 
                        $item->non_payable_attributes = $item->attributes()->where('payable', 0)->get(); 
                        $item->cuisine_id = $item->cuisines()->pluck('cuisine_id');
                        $item->similar_menu_id = $item->menus()->pluck('menu_id');
                        $item->slider_id = $item->sliderImages()->get();
                        $item->branches = $item->branchMenuItems()->get(); 
                        $branches = Branches::all(); 
                        foreach ($branches as $service) { 
                            // dd($item->branches);
                            if ($item->branches->contains('branches_id', $service->id)){
                                $item->isInAllBranchStatus = 1; 
                            }else{
                                // dd($service->id);
                                $item->isInAllBranchStatus = 0;
                            }
                        }

                        if ($item->trashed()) {
                            $item->deleted = true;
                        } else {
                            $item->deleted = false;
                        }
                        $item->logo = asset('storage/'.$item->logo);
                        $menu_items[] = $item;
                    }
                }
                $menus[0]->menu_items = $menu_items;
                return response()->json([
                    'message' => 'success',
                    'data' => [
                        'menu' => $menus[0]
                    ]
                ]);
            }else{
                $menus = '';
                $userBranch = $this->branches->getByUser(Auth::user()->id); 
                if($userBranch){

                    $menus = $this->branches->getMenuBranches($userBranch->id); 

                    foreach ($menus as $menu) { 
                        // $items = $menu->menuItems()->whereNull('deleted')->paginate(6); 
                        $items = $this->menu_item->getMenuItemByMenu($menu->id); 
                        // dd($items);
                        $menu->menu_items_pagination = [
                            'current_page' => $items->currentPage(),
                            'from' => $items,
                            'last_page' => $items->lastPage(),
                            'path' => $items,
                            'per_page' => $items->perPage(),
                            'to' => $items,
                            'total' => $items->total(),
                        ];
                        foreach ($items as $item) {

                            $item->branches = $item->branchMenuItems()->get(); 
                            if ($item->branches[0]['id'] == $userBranch->id) { 
                                $item->isInAllBranchStatus = 1;
                                $item->variants = $item->variants()->get();
                                $item->addons = $item->addons()->get();
                                $item->crust_types = $item->crustType()->get(); 
                                $item->payable_attributes = $item->attributes()->where('payable', 1)->get(); 
                                $item->non_payable_attributes = $item->attributes()->where('payable', 0)->get(); 
                                $item->cuisine_id = $item->cuisines()->pluck('cuisine_id');
                                $item->similar_menu_id = $item->menus()->pluck('menu_id');
                                $item->slider_id = $item->sliderImages()->get(); 

                                if ($item->trashed()) {
                                    $item->deleted = true;
                                } else {
                                    $item->deleted = false;
                                }
                                $item->logo = asset('storage/'.$item->logo);
                                $menu_items[] = $item;
                            }
                        }
                    }
                    $menus[0]->menu_items = $menu_items;
                    return response()->json([
                        'message' => 'success',
                        'data' => [
                            'menu' => $menus[0]
                        ]
                    ]);
                } 
            } 
            // foreach ($menus as $menu) {
            //     $items = $menu->menuItems()->whereNull('deleted')->paginate(6); 
            //     $menu->menu_items_pagination = [
            //         'current_page' => $items->currentPage(),
            //         'from' => $items,
            //         'last_page' => $items->lastPage(),
            //         'path' => $items,
            //         'per_page' => $items->perPage(),
            //         'to' => $items,
            //         'total' => $items->total(),
            //     ];
            //     foreach ($items as $item) {
            //         $item->isInAllBranchStatus = 1;
            //         $item->variants = $item->variants()->get();
            //         $item->addons = $item->addons()->get();
            //         $item->cuisine_id = $item->cuisines()->pluck('cuisine_id');
            //         $item->slider_id = $item->sliderImages()->get();
            //         $item->branches = $item->branchMenuItems()->get(); 
            //         $branches = Branches::all(); 
            //         foreach ($branches as $service) { 
            //             // dd($item->branches);
            //             if ($item->branches->contains('branches_id', $service->id)){
            //                 $item->isInAllBranchStatus = 1; 
            //             }else{
            //                 // dd($service->id);
            //                 $item->isInAllBranchStatus = 0;
            //             }
            //         }

            //         if ($item->trashed()) {
            //             $item->deleted = true;
            //         } else {
            //             $item->deleted = false;
            //         }
            //         $item->logo = asset('storage/'.$item->logo);
            //         $menu_items[] = $item;
            //     }
            // }
            // $menus[0]->menu_items = $menu_items;
            // return response()->json([
            //     'message' => 'success',
            //     'data' => [
            //         'menu' => $menus[0]
            //     ]
            // ]);
        } else {
            // $menu = Auth::user()->restaurant()->withTrashed()->first()->menus()->where('id', '=', $request->menu_id)->first(); 
            
            // $menu = $this->menu->all()->where('id', '=', $request->menu_id)->first();
            // $menu_items_object = $menu->menuItems()->whereNull('deleted')->paginate(10); 
            // $menu->menu_items = $menu_items_object->items(); 
            // $menu->menu_items_pagination = [
            //     'current_page' => $menu_items_object->currentPage(),
            //     'from' => $menu_items_object,
            //     'last_page' => $menu_items_object->lastPage(),
            //     'path' => $menu_items_object,
            //     'per_page' => $menu_items_object->perPage(),
            //     'to' => $menu_items_object,
            //     'total' => $menu_items_object->total(),
            // ]; 
            // foreach ($menu->menu_items as $menu_item) {
            //     $menu_item->variants = $menu_item->variants()->get();
            //     $menu_item->addons = $menu_item->addons()->get();
            //     $menu_item->cuisine_id = $menu_item->cuisines()->pluck('cuisine_id');
            //     $menu_item->branches = $menu_item->branchMenuItems()->get();

            //     if ($menu_item->trashed()) {
            //         $menu_item->deleted = true;
            //     } else {
            //         $menu_item->deleted = false;
            //     }
            //     $menu_item->logo = asset('storage/'.$menu_item->logo);
            // }  
            // // dd($menu);
            // return response()->json([
            //     'message' => 'success',
            //     'data' => [
            //         'menu' => $menu
            //     ]
            // ]);

            if (Auth::user()->role == 'admin') {  
                //$menus = Auth::user()->restaurant()->withTrashed()->first()->menus()->get(); 
                $menu = $this->menu->all()->where('id', '=', $request->menu_id)->first();
                $menu_items_object = $menu->menuItems()->whereNull('deleted')->paginate(10); 
                $menu->menu_items = $menu_items_object->items(); 
                $menu->menu_items_pagination = [
                    'current_page' => $menu_items_object->currentPage(),
                    'from' => $menu_items_object,
                    'last_page' => $menu_items_object->lastPage(),
                    'path' => $menu_items_object,
                    'per_page' => $menu_items_object->perPage(),
                    'to' => $menu_items_object,
                    'total' => $menu_items_object->total(),
                ]; 
                foreach ($menu->menu_items as $menu_item) {
                    $menu_item->variants = $menu_item->variants()->get();
                    $menu_item->addons = $menu_item->addons()->get();
                    $menu_item->crust_types = $menu_item->crustType()->get(); 
                    $menu_item->payable_attributes = $menu_item->attributes()->get(); 
                    $menu_item->cuisine_id = $menu_item->cuisines()->pluck('cuisine_id');
                    $menu_item->similar_menu_id = $menu_item->menus()->pluck('menu_id');
                    $menu_item->branches = $menu_item->branchMenuItems()->get();

                    if ($menu_item->trashed()) {
                        $menu_item->deleted = true;
                    } else {
                        $menu_item->deleted = false;
                    }
                    $menu_item->logo = asset('storage/'.$menu_item->logo);
                }  
                // dd($menu);
                return response()->json([
                    'message' => 'success',
                    'data' => [
                        'menu' => $menu
                    ]
                ]);
            }else{ 
                $menus = '';
                $userBranch = $this->branches->getByUser(Auth::user()->id); 
                if($userBranch){

                    $menu = $this->branches->getMenuBranch($userBranch->id, $request->menu_id); 

                    // $menu = $this->menu->all()->where('id', '=', $request->menu_id)->first();
                    // $menu_items_object = $menu->menuItems()->whereNull('deleted')->paginate(10); 

                    $menu_items_object = $this->menu_item->getMenuItemByMenu(27); 
                    // $items = $this->menu_item->getMenuItemByMenu($menu->id); 

                    // $menu->menu_items = $menu_items_object->items(); 
                    // $menu->menu_items_pagination = [
                    //     'current_page' => $menu_items_object->currentPage(),
                    //     'from' => $menu_items_object,
                    //     'last_page' => $menu_items_object->lastPage(),
                    //     'path' => $menu_items_object,
                    //     'per_page' => $menu_items_object->perPage(),
                    //     'to' => $menu_items_object,
                    //     'total' => $menu_items_object->total(),
                    // ];

                    // foreach ($items as $item) {
                    foreach ($menu_items_object as $menu_item) {
                        $menu_item->variants = $menu_item->variants()->get();
                        $menu_item->addons = $menu_item->addons()->get();
                        $menu_item->crust_types = $menu_item->crustType()->get(); 
                        $menu_item->payable_attributes = $menu_item->attributes()->where('payable', 1)->get(); 
                        $menu_item->cuisine_id = $menu_item->cuisines()->pluck('cuisine_id');
                        $item->similar_menu_id = $item->menus()->pluck('menu_id');
                        // $menu_item->branches = $menu_item->branchMenuItems()->get();

                        if ($menu_item->trashed()) {
                            $menu_item->deleted = true;
                        } else {
                            $menu_item->deleted = false;
                        }
                        $menu_item->logo = asset('storage/'.$menu_item->logo);

                        $menu_items[] = $menu_item;
                    }  
                    // dd($menu);
                    $menu[0]->menu_items = $menu_items;
                    return response()->json([
                        'message' => 'success',
                        'data' => [
                            'menu' => $menu[0]
                        ]
                    ]);
  
                        // $items = $this->menu_item->getMenuItemByMenu($menu->id);  
                        // $menu->menu_items_pagination = [
                        //     'current_page' => $items->currentPage(),
                        //     'from' => $items,
                        //     'last_page' => $items->lastPage(),
                        //     'path' => $items,
                        //     'per_page' => $items->perPage(),
                        //     'to' => $items,
                        //     'total' => $items->total(),
                        // ];
                        // foreach ($items as $item) {

                        //     $item->branches = $item->branchMenuItems()->get();
                        //     // dd($item->branches[0]['id']);
                        //     if ($item->branches[0]['id'] == $userBranch->id) {

                        //         $item->isInAllBranchStatus = 1;
                        //         $item->variants = $item->variants()->get();
                        //         $item->addons = $item->addons()->get();
                        //         $item->cuisine_id = $item->cuisines()->pluck('cuisine_id');
                        //         $item->slider_id = $item->sliderImages()->get(); 

                        //         if ($item->trashed()) {
                        //             $item->deleted = true;
                        //         } else {
                        //             $item->deleted = false;
                        //         }
                        //         $item->logo = asset('storage/'.$item->logo);
                        //         $menu_items[] = $item;
                        //     }
                        // } 
                } 
            } 
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            // 'name' => 'required|unique:menus,name,deleted_at',
            'name' => 'required|min:1|unique:menus,name,NULL,id,deleted_at,NULL'
        ]); 
        if ($request->all_branches == false && $request->branches == []) {
            $this->validate($request, [
                'branches' => 'required|unique:menus,branches'
            ]);
        }

        // $request->request->set('restaurant_id', Auth::user()->restaurant()->withTrashed()->first()->id);
        $request->request->set('restaurant_id', 1);

        if (\config('app.product_type') == 'single') {
            $menu = $this->menu->create($request->restaurant_id, $request->name);
        } else {
            $menu = $this->menu->createClone($request->all());
        }

        if ($request->all_branches) {
            $branches = Branches::all();
            foreach ($branches as $value) {
                $this->menu->createBranchMenu($value->id, $menu->id);
            }
        }else{  
            $branches = $request->branches;
            if (Auth::user()->role == 'admin') {
                foreach ($branches as $value) {
                    $this->menu->createBranchMenu($value, $menu->id);
                }
            }else{
                $this->menu->createBranchMenu($branches, $menu->id);
            } 
        }

        if ($menu) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'menu' => $menu
                ]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * @param Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $request->request->set('restaurant_id', 1);
        if (\config('app.product_type') == 'single') {
            $update = $menu->update($request->all());
        } else {
            $update = $menu->menuClone()->create($request->all());
        }
        if ($update) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'menu' => $menu
                ]
            ]);
        }
    }

    /**'
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $menu_name = $request->name;
        $restaurant_id = 1;
        if (\config('app.product_type') == 'single') {
            $delete = $this->menu->delete($restaurant_id, $menu_name); 
        } else {
            $menu_clone = $this->menu->getOrCreate($restaurant_id, $menu_name);
            $delete = $menu_clone->delete();
        }
        if ($delete) {
            return response()->json([
                'message' => 'success',
                'data' => [

                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Menu $menu)
    {
        $delete = $menu->delete();

        if ($delete) {
            return response()->json([
                'message' => 'success',
                'data' => [

                ]
            ]);
        }
    }
}
