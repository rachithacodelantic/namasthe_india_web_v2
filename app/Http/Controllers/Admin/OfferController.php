<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Offers;
use App\OfferItem;
use App\MenuItem;
use App\Branches;     
use App\Repository\OffersRepository;
use App\Repository\BranchRepository; 
use App\Repository\MenuRepository;
use App\Repository\RestaurantRepository; 
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class OfferController extends Controller
{
    protected $offers; 
    protected $offer_items; 
    protected $branches; 

    /**
     * OfferController constructor.
     * @param MenuRepository $menu_repository
     * @param OffersRepository $offers_repository 
     */
    public function __construct(
        RestaurantRepository $restaurant_repository,
        OffersRepository $offers_repository, 
        BranchRepository $branch_repository
    )
    {
        $this->restaurant = $restaurant_repository;
        $this->offers = $offers_repository; 
        $this->branches = $branch_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offers::all();
        foreach ($offers as $offer) { 
            $offer->image = asset('storage/'.$offer->image); 
        } 
        $offer_items = $this->restaurant->getAllMenuItems(1);  
        // dd($offers);

        return view('admin.offers.index', [
            'menus' => $offers,
            'offer_items' => $offer_items 
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOffersItem(Request $request)
    { 
        // $deals = Auth::user()->restaurant->deals()->orderBy('created_at', 'desc')->paginate(10);
        $offers = Offers::orderBy('created_at', 'desc')->paginate(10);
        foreach ($offers as $offer) {
            
            $menu_items = MenuItem::join('offer_item', 'offer_item.menu_item_id', '=', 'menu_items.id')
                ->where('offer_item.offers_id', '=', $offer->id)
                ->select('menu_items.id', 'menu_items.name')
                ->get();

            $all_menu_items = MenuItem::
                select('menu_items.id', 'menu_items.name')
                ->whereNotIn('menu_items.id', function($query) use($offer)
                {
                    $query->select('menu_items.id')
                        ->from('menu_items')
                        ->join('offer_item', 'offer_item.menu_item_id', '=', 'menu_items.id')
                        ->where('offer_item.offers_id', '=', $offer->id);
                })->get();

            $offer->selectedMenuItemsValues = $offer->dealsItems; 
            $offer->image = asset('storage/'.$offer->image);
            $offer->availableMenuItems = $all_menu_items; 
            $offer->selectedMenuItems = $menu_items; 
        }  

        return response()->json([
            'message' => 'success',
            'data' => [
                'offers' => $offers
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:offers,name',
            'price' => 'required',
            'start' => 'required',
            'close' => 'required',
            'description' => 'required',
            'vat_category' => 'required'
        ]);
        $image= $request->image;
        if ($image) {

            $extension = explode(';', explode('image/', $image)[1])[0]; 
            $result = substr($image, strpos($image, 'image/', '6'), 4); 
            $file_name = 'offers/offer_' . Carbon::now()->format('YmdHis') . '.' . $extension; 
            // $image = str_replace('data:image/' . $extension . ';base64,', '', $image);
            // $image = str_replace(' ', '+', $image); 
            // Storage::disk('local')->put($file_name, base64_decode($image));  

            $image = $request->image;
            $filename = 'offer_' . Carbon::now()->format('YmdHis') . '.' . $extension; 
            $image_resize = Image::make($image);              
            $image_resize->crop(267, 145);
            // $image_resize->save('offers/' .$filename);
            $image_resize->save(storage_path('app/public/offers/' .$filename));  
            $request->request->set('image', $file_name); 
        }
        // $request->request->set('restaurant_id', Auth::user()->restaurant()->withTrashed()->first()->id);
        $request->request->set('restaurant_id', 1); 

        $start = date('Y-m-d', strtotime($request->start)); 
        $start = Carbon::parse($start);
        $request->request->set('start', $start);

        $end = date('Y-m-d', strtotime($request->end)); 
        $end = Carbon::parse($end);
        $request->request->set('end', $end);

        $offers = $this->offers->createByRequest($request->restaurant_id, $request->all());

        foreach ($request->selectedMenuItemsValues as $item) {
            $offer_items = new OfferItem();
            $offer_items->offers_id = $offers->id;
            $offer_items->menu_item_id = $item;
            $offer_items->save();
        } 
        if ($offers) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'offers' => $offers
                ]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required'],
            'price' => 'required',
            'start' => 'required',
            'close' => 'required',
            'description' => 'required',
            'vat_category' => 'required'
        ]);

        $offers = Offers::find($id);
        $image = $request->image;
        if ($image) {
            $extension = explode(';', explode('image/', $image)[1])[0];
            $result = substr($image, strpos($image, 'image/', '6'), 4);
            $file_name = 'offers/offer_' . Carbon::now()->format('YmdHis') . '.' . $extension;
            // $image = str_replace('data:image/' . $extension . ';base64,', '', $image);
            // $image = str_replace(' ', '+', $image);
            // Storage::disk('local')->put($file_name, base64_decode($image));  

            $image = $request->image;
            $filename = 'offer_' . Carbon::now()->format('YmdHis') . '.' . $extension;

            $image_resize = Image::make($image);              
            $image_resize->crop(267, 145);
            // $image_resize->save('offers/' .$filename);
            $image_resize->save(storage_path('app/public/offers/' .$filename)); 
 
            $file_name = 'offers/'. $filename;
            $request->request->set('image', $file_name);
        } else {
            $request->request->set('image', $offers->logo);
        }

        // $request->request->set('restaurant_id', Auth::user()->restaurant()->withTrashed()->first()->id);
        $request->request->set('restaurant_id', 1);
        // dd($request->start);
        $start = date('Y-m-d', strtotime($request->start)); 
        $request->request->set('start', $start);
        $end = date('Y-m-d', strtotime($request->end)); 
        $request->request->set('end', $end);
        // dd($start);
        $offers->fill($request->all());
        $offers->save(); 
        OfferItem::where('offers_id', '=', $offers->id)->delete();

        foreach ($request->selectedMenuItemsValues as $item) {
            $deal_items = new OfferItem();
            $deal_items->offers_id = $offers->id;
            $deal_items->menu_item_id = $item;
            $deal_items->save();
        } 

        if ($offers) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'offers' => $offers
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete(Request $request)
    {
        $offer_id = $request->offer_id;
        OfferItem::where('offers_id', '=', $offer_id)->delete();
        Offers::find($offer_id)->delete();

        return response()->json([
            'message' => 'success',
            'data' => [

            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
