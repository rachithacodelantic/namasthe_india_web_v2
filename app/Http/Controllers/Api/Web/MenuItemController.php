<?php 
namespace App\Http\Controllers\Api\Web;

use App\Http\Controllers\Controller;
use App\User;
use App\MenuItem;
use App\MenuItemReviews; 
use App\Repository\MenuRepository;
use App\Repository\MenuItemRepository;
use App\Repository\MenuItemReviewsRepository;  
use App\Repository\RestaurantRepository;
use Illuminate\Http\Request;

/**
 * Class MenuItemController
 * @package App\Http\Controllers\Api\Web
 */
class MenuItemController extends Controller
{ 
    protected $menu; 
    protected $restaurant; 
    protected $menu_item;
    protected $menu_item_reviews;

    /**
     * MenuItemController constructor.
     * @param MenuRepository $menu_repository
     * @param RestaurantRepository $restaurant_repository
     * @param MenuItemRepository $menu_item_repository
     */
    public function __construct(MenuRepository $menu_repository, RestaurantRepository $restaurant_repository, MenuItemRepository $menu_item_repository, MenuItemReviewsRepository $menuItem_reviews_repository)
    {
        $this->menu = $menu_repository;
        $this->restaurant = $restaurant_repository;
        $this->menu_item = $menu_item_repository;
        $this->menu_item_reviews = $menuItem_reviews_repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $items = $this->menu_item->getAllItems();
        return response()->json([
            'message' => 'success',
            'data' => [
                'menuItems' => $items
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function byMenu(Request $request)
    { 
        $branch_id = $request->branch_id;  
        $branch_menu_items = [];
        if ($request->menu_id > 0) {
            $menu = $this->menu->get($request->menu_id);  
            if($menu){
                $menu_items = $menu->menuItems()->get(); 
                foreach ($menu_items as $menu_item) {  
                    $menu_item_branches = $menu_item->branchMenuItems()->get();   
                    if (!$menu_item_branches->isEmpty()){ 
                        if ($menu_item_branches[0]['id'] == $branch_id) {
                            $menuItem = $this->menu_item->get($menu_item->id);  
                            $menu_item->favoured = false; 
                            if($request->user_id == null){
                                $menu_item->favoured = false;
                            }else{
                                if ($menu_item->users()->where('user_id', '=', $request->user_id)->count()) {
                                    $menu_item->favoured = true;
                                }
                            }
                            $addons = $menuItem->addons()->get(); 
                            $variants = $menuItem->variants()->get(); 
                            $crusts = $menuItem->crustType()->get(); 
                            $menu_item->addons = $addons;
                            $menu_item->variants = $variants;
                            $menu_item->crusts = $crusts;  
                            $menu_item->similar_menus = $menu_item->menus()->get(); 
                            foreach ($menu_item->similar_menus as $similar_menu) { 
                                $similar_menu->similar_menu_items = $this->menu_item->getMenuItemByMenuWithoutPaginate($similar_menu->id); 
                                foreach ($similar_menu->similar_menu_items as $key => $similar_menu_item) {
                                    $variants = $similar_menu_item->variants()->get();
                                    if(!$variants->isEmpty()){ 
                                        $similar_menu->similar_menu_items->forget($key);
                                    }
                                    $addons = $similar_menu_item->addons()->get();
                                    if(!$addons->isEmpty()){ 
                                        $similar_menu->similar_menu_items->forget($key);
                                    }
                                    $crust_types = $similar_menu_item->crustType()->get();
                                    if(!$crust_types->isEmpty()){ 
                                        $similar_menu->similar_menu_items->forget($key);
                                    }
                                }
                            }

                            if ($menu_item->logo) {
                                // $menu_item->logo = getStorageUrl() . $menu_item->logo;
                                $menu_item->logo = getStorageUrl() . 'cuisines/mobile/' . $menu_item->logo;
                            } else {
                                $menu_item->logo = asset('img/default.jpg');
                            }
                            $branch_menu_items[] = $menu_item;
                        }
                    }
                }
            }
        } else {  
            $menu_items = $this->restaurant->getPopularItems(1); 
            foreach ($menu_items as $menu_item) {  
                $menu_item_branches = $menu_item->branchMenuItems()->get();    
                $menuItem = $this->menu_item->get($menu_item->id); 
                $menu_item->favoured = false; 
                    if($request->user_id == null){
                        $menu_item->favoured = false;
                    }else{
                        if ($menu_item->users()->where('user_id', '=', $request->user_id)->count()) {
                            $menu_item->favoured = true;
                        }
                    }
                $addons = $menuItem->addons()->get(); 
                $variants = $menuItem->variants()->get(); 
                $crusts = $menuItem->crustType()->get(); 
                $menu_item->addons = $addons;
                $menu_item->variants = $variants;
                $menu_item->crusts = $crusts; 
                $menu_item->similar_menus = $menu_item->menus()->get(); 
                foreach ($menu_item->similar_menus as $similar_menu) { 
                    $similar_menu->similar_menu_items = $this->menu_item->getMenuItemByMenuWithoutPaginate($similar_menu->id); 
                    foreach ($similar_menu->similar_menu_items as $key => $similar_menu_item) {
                        $variants = $similar_menu_item->variants()->get();
                        if(!$variants->isEmpty()){ 
                            $similar_menu->similar_menu_items->forget($key);
                        }
                        $addons = $similar_menu_item->addons()->get();
                        if(!$addons->isEmpty()){ 
                            $similar_menu->similar_menu_items->forget($key);
                        }
                        $crust_types = $similar_menu_item->crustType()->get();
                        if(!$crust_types->isEmpty()){ 
                            $similar_menu->similar_menu_items->forget($key);
                        }
                    }
                }

                if ($menu_item->logo) {
                    // $menu_item->logo = getStorageUrl() . $menu_item->logo;
                    $menu_item->logo = getStorageUrl() . 'cuisines/mobile/' . $menu_item->logo;
                } else {
                    $menu_item->logo = asset('img/default.jpg');
                }
                $branch_menu_items[] = $menu_item; 
            }
        } 
        return response()->json([
            'status' => 'success',
            'data' => $branch_menu_items
        ]);
    }

    public function get(Request $request)
    {
        $id = $request->id; 
        $branch_id = $request->branch_id;
        $menu_item = $this->menu_item->get($id); 
        $menu_item->logo = getStorageUrl() . $menu_item->logo;  

        $menu_item_branches = $menu_item->branchMenuItems()->get();   
        if ($menu_item_branches[0]['id'] == $branch_id) {
            $menuItem = $this->menu_item->get($menu_item->id); 
            $addons = $menuItem->addons()->get(); 
            $variants = $menuItem->variants()->get(); 
            $crusts = $menuItem->crustType()->get(); 
            $menu_item->addons = $addons;
            $menu_item->variants = $variants;
            $menu_item->crusts = $crusts;  

            $menu_item->similar_menus = $menu_item->menus()->get(); 
            foreach ($menu_item->similar_menus as $similar_menu) { 
                $similar_menu->similar_menu_items = $this->menu_item->getMenuItemByMenuWithoutPaginate($similar_menu->id); 
                foreach ($similar_menu->similar_menu_items as $key => $similar_menu_item) {
                    $variants = $similar_menu_item->variants()->get();
                    if(!$variants->isEmpty()){ 
                        $similar_menu->similar_menu_items->forget($key);
                    }
                    $addons = $similar_menu_item->addons()->get();
                    if(!$addons->isEmpty()){ 
                        $similar_menu->similar_menu_items->forget($key);
                    }
                    $crust_types = $similar_menu_item->crustType()->get();
                    if(!$crust_types->isEmpty()){ 
                        $similar_menu->similar_menu_items->forget($key);
                    }
                }
            }

            $total_count = 0; 
            $total = 0; 
            $average = 0;
            $reviews = $this->menu_item_reviews->getReviews($menu_item->id); 
            foreach ($reviews as $rating) { 

                $user = User::findOrFail($rating->user_id);   
                $rating->user = $user->first_name .' '. $user->last_name;
                $total_count ++;
                $total += $rating->rating;
            }

            if ($total_count <= 0) {
                $average = 0;
            } else {
                $average = $total / $total_count;
            }
            $menu_item->comments = $reviews;
            $menu_item->overall_rating = $average;
        }

        return response()->json([
            'status' => 'success',
            'data' => $menu_item
        ]);
    } 
}
