<?php 
namespace App\Http\Controllers\Api\Web; 

use App\Http\Controllers\Controller;
use App\Menu;
use App\Restaurant;
use App\Branches; 
use App\DealsItem;
use App\Offers;
use App\OfferItem;
use App\MenuItem;
use App\Repository\BranchRepository; 
use App\Repository\DealsRepository;
use App\Repository\OffersRepository;
use App\Repository\RestaurantRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class MenuController
 * @package App\Http\Controllers\Api\Web
 */
class MenuController extends Controller
{
    protected $branches; 
    protected $restaurant;
    protected $deal_item;
    protected $offers; 
    protected $offer_items; 

    /**
     * MenuController constructor.
     * @param RestaurantRepository $restaurant_repository
     */
    public function __construct(RestaurantRepository $restaurant_repository, DealsRepository $deals_repository, OffersRepository $offers_repository, BranchRepository $branch_repository)
    {
        $this->restaurant = $restaurant_repository;
        $this->branches = $branch_repository;
        $this->deal_item = $deals_repository;
        $this->offers = $offers_repository; 
    }

    public function all(Request $request)
    {
        $restaurant_id = 1;
        $branch_id = $request->branch_id;         
        if($branch_id){ 
            $restaurant = $this->restaurant->getRestaurant($restaurant_id); 
            $branch = $this->branches->get($branch_id); 

            $menus = $this->branches->getMenuBranches($branch_id);  
            $popular = Menu::hydrate([[
                'id' => 0,
                'restaurant_id' => $restaurant->id,
                'name' => 'Popular',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'deleted_at' => null
            ]])->first();

            $popular->selected = true; 
            $restaurant_menus = [];
            $restaurant_menu_items = [];
            $restaurant_menus[] = $popular;

            foreach ($menus as $menu) {
                if ($menu->name != 'Popular') {

                    $menu->selected = false;
                    $restaurant_menus[] = $menu; 
                    // $restaurant_menu_items[] = $menu->menuItems()->get();  
                }
            }

            $restaurant->menus = $restaurant_menus; 
            $restaurant->menuItems = $restaurant_menu_items; 
            return response()->json([
                'status' => 'success',
                'menues' => $restaurant->menus,
                // 'menuItems' => $restaurant->menuItems
            ]);
        }
    }

    /**
     * Display the deals.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */  
    public function getDeals()
    {
        $restaurant = $this->restaurant->getFirst(1);
        foreach ($restaurant->deals as $deal) {
            if ($deal->image) {
                $deal->image = asset('storage/'. $deal->image);
            } else {
                $deal->image = asset('img/default.jpg');
            }
            //$deal->deals_items = $deal->dealsItems;
            $deal->deals_items = DealsItem::join('menu_items', 'deals_item.menu_item_id', '=', 'menu_items.id')
                                ->join('menus', 'menu_items.menu_id',  '=', 'menus.id')
                                ->where('deals_id', '=', $deal->id)
                                ->select('menus.*')
                                ->groupBy('menu_items.menu_id')->get();

            // foreach ($deal->deals_items as $menus) {
            foreach ($deal->deals_items as $key => $menu) {
                $menu_items = DealsItem::join('menu_items', 'deals_item.menu_item_id', '=', 'menu_items.id')
                                     ->where('deals_id', '=', $deal->id)
                                     ->where('menu_items.menu_id', '=', $menu->id)
                                     ->select('menu_items.*', 'deals_item.id as deals_items_id')->get();

                $deal->deals_items[$key]->menu_items = $menu_items;
            } 
        } 
        return response()->json([
            'status' => 'success',
            'deals' => $restaurant->deals
        ]);
    }

    /**
     * Display the offers.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function getOffers()
    {
        $restaurant = $this->restaurant->getFirst(1);
        $offers = Offers::all(); 
        $restaurant->offers = $offers; 
        foreach ($restaurant->offers as $offers) { 
            if ($offers->image) {
                $offers->image = asset('storage/'. $offers->image);
            } else {
                $offers->image = asset('img/default.jpg');
            }
        } 
        return response()->json([
            'status' => 'success',
            'offers' => $restaurant->offers
        ]);
    }

    /**
     * Get offer items.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function getOffersItemByID(Request $request)
    {
        $items = OfferItem::join('menu_items', 'offer_item.menu_item_id', '=', 'menu_items.id')
                    ->join('menus', 'menu_items.menu_id',  '=', 'menus.id')
                    ->where('offers_id', '=', $request->offer_id)
                    ->select('menus.*', 'offer_item.offers_id')
                    ->groupBy('menu_items.menu_id')->get(); 

        foreach ($items as $key => $menu) {
            $menu_items = MenuItem::join('offer_item', 'offer_item.menu_item_id', '=', 'menu_items.id')
                ->where('menu_items.menu_id', $menu->id)
                ->where('offer_item.offers_id', '=', $request->offer_id)
                ->select('offer_item.id as offer_items_id', 'menu_items.name', 'menu_items.price')
                ->get();

            $items[$key]->menu_items = $menu_items;
        } 
        return response()->json([
            'message' => 'success',
            'data' => [
                'items' => $items
            ]
        ]); 
    }
}
