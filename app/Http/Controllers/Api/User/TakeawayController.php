<?php

namespace App\Http\Controllers\Api\User;

use App\Support\Socket;
use App\Takeaway;
use App\TakeawayDealItems;
use App\TakeawayOfferItems;
use App\Branches;  
use App\TakeawayItem;  
use App\Repository\TakeawayRepository;
use App\Repository\MenuItemRepository; 
use App\Repository\RestaurantRepository;
use App\Repository\DealsRepository;
use App\Repository\OffersRepository;
use App\Repository\BranchRepository;
use App\User;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;
use App\Setting;

/**
 * Class TakeawayController
 * @package App\Http\Controllers\Api\User
 */
class TakeawayController extends Controller
{
    protected $takeaway;
    protected $socket;
    protected $restaurant;
    protected $menu_item;
    protected $branches; 
    protected $deal_item;
    protected $offer_item;
    //protected $takawayData;

    /**
     * TakeawayController constructor.
     * @param TakeawayRepository $takeaway_repository
     * @param Socket $socket
     */
    public function __construct(TakeawayRepository $takeaway_repository, RestaurantRepository $restaurant_repository, Socket $socket, BranchRepository $branch_repository, MenuItemRepository $menu_item_repository, DealsRepository $deals_repository, OffersRepository $offers_repository)
    {
        $this->takeaway = $takeaway_repository;
        $this->socket = $socket;
        //$this->takawayData = new Takeaway(); 
        $this->restaurant = $restaurant_repository;
        $this->menu_item = $menu_item_repository;
        $this->branches = $branch_repository;
        $this->deal_item = $deals_repository;
        $this->offer_item = $offers_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $takeaways = Auth::user()->takeaways->orderBy('created_at', 'desc')->paginate(10); 
        foreach ($takeaways as $takeaway) {
            
            $name = User::find($takeaway->user_id); 
            $takeaway->user = $name->first_name . " " . $name->last_name;
            $takeaway->takeaway_items = $takeaway->takeawayItems;
            $takeaway->restaurant = $takeaway->restaurant;
            foreach ($takeaway->takeaway_items as $takeaway_item) {
                //$takeaway->restaurant = $takeaway_item->menuItem->menu->restaurant;
                $takeaway_item->menu_item = $takeaway->menuItem;
            }
            $takeaway->time = Carbon::parse($takeaway->time)->diffForHumans();

            switch ($takeaway->status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }

            $takeaway->progress = $progress;
        }

        if ($takeaways) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'takeaways' => $takeaways
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Pusher\PusherException
     */
    public function store(Request $request)
    { 
        $promotion = $request->promotion;
        $request->request->set('user_id', Auth::id());
//        $request->request->set('time', Carbon::now()->toDateTimeString());
        $request->request->set('time', Carbon::parse($request->date . ' ' . $request->time)->toDateTimeString());
        $restaurant = $this->restaurant->getRestaurant($request->restaurant_id); 
        $total = 0; 
        $discount = 0; 
        $subtotal = 0; 
        $vat = 0;
        $cart = $request->cart;
        $token = $request->token;

        foreach ($cart as $item) {

            $menu_item_total_price = 0;

            if($item['item_type'] == 'single') { 
                $menu_item = $this->menu_item->get($item['menu_item_id']);
            } elseif($item['item_type'] == 'offer') {
                $menu_item = $this->offer_item->get($item['menu_item_id']); 
            } else {
                // $menu_item = $this->deal_item->get($item['id']);
                $menu_item = $this->deal_item->get($item['menu_item_id']);
            }
            
            //$menu_item = $this->menu_item->get($item['menu_item_id']);
            if ($menu_item) {
                $menu_item_price = 0;

                if (!empty($item['varients']) && $item['item_type'] == 'single') {
                    $variant = $this->menu_item->getVariant($item['menu_item_id'], $item['varients'][0]['id']);
                    if ($variant) {
                        $menu_item_price = $variant->pivot->price * $item['quantity'];
                    } else {
                        $menu_item_price = $menu_item->price * $item['quantity'];
                    }
                } else {
                    $menu_item_price = $menu_item->price * $item['quantity'];
                }
                $subtotal += $menu_item_price;
                
                if (!empty($item['varients']) && $item['item_type'] == 'single') {
                    $variant = $this->menu_item->getVariant($item['menu_item_id'], $item['varients'][0]['id']);
                    // $menu_item_price = $variant->pivot->price * $item['quantity'];
                    $menu_item_price = $item['varients'][0]['price'] * $item['quantity'];
                } else {
                    $menu_item_price = $menu_item->price * $item['quantity'];
                }
                $subtotal += $menu_item_price;
            }
            
            for($i = 1; $i <= $item['quantity']; $i++){
                if ($promotion) {
                    if ($promotion->method == 'menu_item' && $promotion->id == $menu_item->id) {
                        if ($promotion->promo_type == 'percentage') {
                            $discount = $menu_item_price * $promotion->promo_value * 0.01;
                        } else {
                            $discount = $promotion->promo_value;
                        }
                    }
                }
                $menu_item_total_price = $menu_item_price;
                //$subtotal += $menu_item_price;

                if (!empty($item['addons']) && $item['item_type'] == 'single') {
                    foreach ($item['addons'] as $selected_addon) {
                        $addon = $this->menu_item->getAddon($item['menu_item_id'], $selected_addon['id']);
                        $subtotal += $addon->pivot->price;
                        $menu_item_total_price += $addon->pivot->price;
                    }
                }

                if (!empty($item['crusts']) && $item['item_type'] == 'single') {  
                    // dd($item['crusts']); 
                    $crust_type = $this->menu_item->getCrust($item['menu_item_id'], $item['crusts'][0]['id']);  
                    $subtotal += $crust_type->pivot->price;
                    $menu_item_total_price += $crust_type->pivot->price; 
                }
                //$vat += getVat()[$menu_item->vat_category] * $menu_item_total_price;
                $vat = 0;
            }
        }

        $vat = $request->vat;
        $request->request->set('vat', $vat);

        if ($promotion) {
            if ($promotion->method == 'site' || $promotion->method == 'restaurant') {
                if ($promotion->type == 'percentage') {
                    $discount = $subtotal * $promotion->value * 0.01;
                } else {
                    $discount = $promotion->value;
                }
            }
        }

        $request->request->set('menu_item_id', null);
        $request->request->set('promotion_id', null);
        $request->request->set('site_promotion_id', null);
        $request->request->set('site_discount', null);
        $request->request->set('restaurant_discount', null);

        if ($promotion) {
            switch ($promotion->method) {
                case 'site':
                    $request->request->set('site_promotion_id', $promotion->id);
                    break;
                case 'restaurant':
                    $request->request->set('promotion_id', $promotion->id);
                    break;
                case 'menu_item':
                    $request->request->set('menu_item_id', $promotion->id);
                    break;
            }
        }

        $setting = Setting::first();
        $site_discount = 0;
        $restaurant_discount = 0; 

        if (Carbon::now()->startOfDay()->lte(Carbon::parse($restaurant->expiry_date)->startOfDay()) && Carbon::now()->startOfDay()->gte(Carbon::parse($restaurant->start_date)->startOfDay())) {
            if ($restaurant->discount_type == 'percentage') {
                $restaurant_discount = $subtotal * $restaurant->discount_value * 0.01;
            } else {
                $restaurant_discount = $restaurant->discount_value;
            }
            $request->request->set('restaurant_discount', $restaurant_discount);
        } 

        $total = $subtotal + $vat - $discount - $site_discount - $restaurant_discount; 
        $request->request->set('total', $total); 
        $takeaway = $this->takeaway->create($request->all()); 

        foreach ($cart as $item) { 
            for($i = 1; $i <= $item['quantity']; $i++){

                if($item['item_type'] == 'single') {
                    $takeaway_item = $this->takeaway->createTakeawayItem([
                        'takeaway_id' => $takeaway['id'],
                        'menu_item_id' => $item['menu_item_id'],
                        'quantity' => 1,
                    ]); 
                } elseif($item['item_type'] == 'offer') {  

                    $takeaway_item = new TakeawayItem();
                    $takeaway_item->takeaway_id = $takeaway->id;
                    $takeaway_item->offer_id = $item['menu_item_id'];
                    $takeaway_item->quantity = 1;
                    $takeaway_item->save();

                    foreach ($item['offer_menuItems'] as $selected_offer_item) {
                        $takeaway_offer_items = New TakeawayOfferItems();
                        $takeaway_offer_items->takeaway_item_id = $takeaway_item->id;
                        $takeaway_offer_items->offer_item_id = $selected_offer_item['offer_items_id'];
                        $takeaway_offer_items->save();
                    }
                } else {
                    $takeaway_item = $this->takeaway->createTakeawayItem([
                        'takeaway_id' => $takeaway['id'],
                        'deal_id' => $item['menu_item_id'],
                        'quantity' => 1,
                    ]); 
    
                    foreach ($item['deals'] as $selected_deal_item) {
                        $takeaway_deal_items = New TakeawayDealItems();
                        $takeaway_deal_items->takeaway_item_id = $takeaway_item->id;
                        $takeaway_deal_items->deal_item_id = $selected_deal_item['id'];
                        $takeaway_deal_items->save();
                    }
                }

                if (isset($item['addons']) && $item['item_type'] == 'single') {
                    foreach ($item['addons'] as $selected_addon) {
                        /*$selected_addon_menu_item = $this->menu_item->getSelectedAddonMenuItem($selected_addon['pivot']['menu_item_id'], $selected_addon['pivot']['addon_id']);
                        $takeaway_item->addonMenuItems()->attach($selected_addon_menu_item->id);*/
                        
                        $selected_addon_menu_item = $this->menu_item->getSelectedAddonMenuItem($item['menu_item_id'], $selected_addon['id']);
                        $takeaway_item->addonMenuItems()->attach($selected_addon_menu_item->id);
                    }
                }

                if (!empty($item['crusts']) && $item['item_type'] == 'single') {  
                    
                    // $selected_crust_menu_item = $this->menu_item->getSelectedMenuItemCrust($item['crusts']['pivot']['menu_item_id'], $item['crusts']['pivot']['crust_type_id']); 
                    $selected_crust_menu_item = $this->menu_item->getSelectedMenuItemCrust($item['menu_item_id'], $item['crusts'][0]['id']); 
                    $takeaway_item->update([
                        // 'menu_item_crust_id' => $selected_crust_menu_item->id
                        'menu_item_crust_id' => $selected_crust_menu_item['id']
                    ]); 
                }

                if (!empty($item['varients']) && $item['item_type'] == 'single') {
                    /*$selected_menu_item_variant = $this->menu_item->getSelectedMenuItemVariant($item['varients']['pivot']['menu_item_id'], $item['varients']['pivot']['variant_id']);
                    $takeaway_item->update([
                        'menu_item_variant_id' => $selected_menu_item_variant->id
                    ]);*/
                    
                    $selected_menu_item_variant = $this->menu_item->getSelectedMenuItemVariant($item['menu_item_id'], $item['varients'][0]['id']);
                    $takeaway_item->update([
                        // 'menu_item_variant_id' => $selected_menu_item_variant->id
                        'menu_item_variant_id' => $selected_menu_item_variant['id']
                    ]);
                }
            }
        } 
        
        $charge = null;

        if ($request->token) {
            Stripe::setApiKey('sk_test_51Gua6bGjMqZXqM3Y8TMVcFD8FSLEbJEnCtwMApZ7u6i1SeL7ka7k4b7eNVIIOZtxqjwqR7NQGJMXZKoRIkzcm2D100sXtXc8Ut');
            //Stripe::setApiKey('sk_test_doeAFOHiBxFVWgx9ZKovkr6Z00DJir49HI');
            // Stripe::setApiKey('sk_live_R4gjsB5gUo5Jcwy7zMB3Ordz00WShL8EsZ');
            $charge = Charge::create(['amount' => round($total, 2) * 100, 'currency' => 'NOK', 'source' => $token]);
        }

        if ($charge && $request->payment == 'Pay Online') {

            $payment = [
                'type' => 'card',
                'payment' => $charge
            ]; 
            $takeaway->update(['payment' => json_encode($payment)]); 
            $takeaway->user = $takeaway->user()->first(); 
            $takeaway->items = $takeaway->takeawayItems()->get();   
            $this->socket->push($takeaway->attributesToArray(), 'create takeaway', str_slug(config('app.name')).'_'. $request->restaurant_id);
            
            $this->sendResponse($takeaway); 
            return response()->json([
                'message' => 'success',
                'data' => [
                    'takeaway' => $takeaway,
                    'charge' => 'card'
                ]
            ]); 
 
        } else if ($request->payment == 'mobilepayment') {
            $takeaway->update(['payment' => json_encode([
                'type' => 'mobile',
                'payment' => null 
            ])]); 
            
            $this->sendResponse($takeaway); 
            $this->socket->push($takeaway->attributesToArray(), 'create takeaway', str_slug(config('app.name')).'_'. $request->restaurant_id);
            return response()->json([
                'message' => 'success',
                'data' => [
                    'takeaway' => $takeaway,
                    'charge' => 'mobile'
                ]
            ]);
        } else {
            $takeaway->update(['payment' => json_encode([
                'type' => 'cash',
                'payment' => null 
            ])]);
            
            $this->sendResponse($takeaway);  
            //$takeaway->user = $takeaway->user()->first(); 
            //$takeaway->items = $takeaway->takeawayItems()->get();  
            $this->socket->push($takeaway->attributesToArray(), 'create takeaway', str_slug(config('app.name')).'_'. $request->restaurant_id);  
            return response()->json([
                'message' => 'success',
                'data' => [
                    'takeaway' => $takeaway,
                    'charge' => 'cash'
                ]
            ]);
        }
    }
    
    /**
     * @param Takeaway 
     */
    public function sendResponse($takeaway)
    {  
        if (\auth()->user()->email == setting('guest_email_id')) {
            $user_email = $takeaway->email;
        } else {
            $user_email = $takeaway->user->email;
        }
        // $user_email = $takeaway->email;
        $restaurant = $takeaway->restaurant()->first();
        $theme = setting('site_theme');

        if($theme == 'orange-peel') {
            $color_theme = '#ffbf00';
            $button_color = '#d38a0c';
        } elseif ($theme == 'whiskey') {
            $color_theme = '#d3a971';
            $button_color = '#d38a0c';
        } elseif ($theme == 'thunderbird') {
            $color_theme = '#cb1511';
            $button_color = '#900906';
        } elseif ($theme == 'amber') {
            $color_theme = '#ffbf00';
            $button_color = '#d38a0c';
        } elseif ($theme == 'apple') {
            $color_theme = '#409843';
            $button_color = '#2d5840';
        } else {
            $color_theme = '#2A8F38';
            $button_color = '#db6d13';
        }

        $theme = [
            'color_theme'       => $color_theme,
            'button_color'      => $button_color,
            'restaurant_email'  => $restaurant->email,
            'restaurant_tel'    => $restaurant->phone,
            'current_year'      => date('Y'),
            'terms_url'         => config('app.url').'/terms-and-conditions',
            'app_name'          => config('app.name'),
            'app_logo'          => asset('storage/'. $restaurant->logo)
        ];

        //send confirmation mail
        Mail::send(['html' => 'user.email.takeaway-confirmed'], ['takeaway' => $takeaway, 'theme' => $theme],
        function ($message) use ($user_email) {
            $message->to($user_email)->subject('Order received');
        }); 
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Takeaway $takeaway
     * @return \Illuminate\Http\Response
     */
    public function show(Takeaway $takeaway)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Takeaway $takeaway
     * @return \Illuminate\Http\Response
     */
    public function edit(Takeaway $takeaway)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Takeaway $takeaway
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Takeaway $takeaway)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Takeaway $takeaway
     * @return \Illuminate\Http\Response
     */
    public function destroy(Takeaway $takeaway)
    {
        //
    }
}
