<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantRepository;
use App\Repository\BranchRepository; 
use App\Branches;  
use App\Reservation;
use App\User;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{
    protected $reservation;
    protected $restaurant;
    protected $branches;

    public function __construct(ReservationRepository $reservation_repository, RestaurantRepository $restaurant_repository, BranchRepository $branch_repository)
    {
        $this->reservation = $reservation_repository;
        $this->restaurant = $restaurant_repository;
        $this->branches = $branch_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Auth::user()->reservations;

        foreach ($reservations as $reservation) {

            $name = User::find($reservation->user_id); 
            $reservation->user = $name->first_name . " " . $name->last_name;
            $reservation->restaurant = $reservation->restaurant()->first(); 
            $restaurant_address = $reservation->restaurant->city . '+' . $reservation->restaurant->province . '+' . $reservation->restaurant->county . '+' . $reservation->restaurant->postcode; 
            $reservation->restaurant->query_address = str_replace(' ', '+', $restaurant_address); 
            $reservation->date = Carbon::parse($reservation->time)->toFormattedDateString();
            $reservation->time = Carbon::parse($reservation->time)->format('g:i A');
        }


        if ($reservations) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'reservations' => $reservations
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->set('user_id', Auth::id()); 
        $request->request->set('time', Carbon::parse($request->date . ' ' . $request->time)->toDateTimeString()); 

        $restaurant = $this->restaurant->getRestaurant(1);   
        if($restaurant['seats'] < $request['head_count']){
            return response()->json([
                'message' => 'The restaurant almost full.Can you try different date?'
            ], 400);
        } 

        $cart_date = Carbon::parse($request->date . ' ' . $request->time);   
        $day_flag = true; 
        $branch_id = $request->branch_id;  
        $branch = $this->branches->get($branch_id); 
        $restaurant->opening_hours = $branch->openingHours()->orderBy('day', 'asc')->orderBy('opening_time', 'desc')->get(); 
        foreach ($restaurant->opening_hours as $opening_hour) {
            if ($opening_hour->day == $cart_date->dayOfWeek) {
                $cart_time = Carbon::parse($cart_date->toTimeString());
                $opening_time = Carbon::parse($opening_hour->opening_time);
                $closing_time = Carbon::parse($opening_hour->closing_time);
                if ($cart_time->gt($opening_time) && $cart_time->lt($closing_time)) {
                    $day_flag = false;
                }
            }
        }

        if ($day_flag) {
            return response()->json([
                'message' => 'Sorry please set order time within opening hours'
            ], 400);
        }

        $reservation = $this->reservation->create($request->all()); 
        if ($reservation) {
            if (\auth()->user()->email == setting('guest_email_id')) {
                $user_email = $reservation->email;
            } else {
                $user_email = $reservation->user->email;
            }
            // $user_email = $reservation->email;
            $restaurant = $reservation->restaurant()->first();
            $theme = setting('site_theme');
    
            if($theme == 'orange-peel') {
                $color_theme = '#ffbf00';
                $button_color = '#d38a0c';
            } elseif ($theme == 'whiskey') {
                $color_theme = '#d3a971';
                $button_color = '#d38a0c';
            } elseif ($theme == 'thunderbird') {
                $color_theme = '#cb1511';
                $button_color = '#900906';
            } elseif ($theme == 'amber') {
                $color_theme = '#ffbf00';
                $button_color = '#d38a0c';
            } elseif ($theme == 'apple') {
                $color_theme = '#409843';
                $button_color = '#2d5840';
            } else {
                $color_theme = '#2A8F38';
                $button_color = '#db6d13';
            }
    
            $theme = [
                'color_theme'       => $color_theme,
                'button_color'      => $button_color,
                'restaurant_email'  => $restaurant->email,
                'restaurant_tel'    => $restaurant->phone,
                'current_year'      => date('Y'),
                'terms_url'         => config('app.url').'/terms-and-conditions',
                'app_name'          => config('app.name'),
                'app_logo'          => asset('storage/'. $restaurant->logo)
            ];
    
            //send verification code
            Mail::send(['html' => 'user.email.reservation-confirmed'], ['reservation' => $reservation, 'theme' => $theme],
                function ($message) use ($user_email) {
                    $message->to($user_email)
                        ->subject('Booking received');
                });
            return response()->json([
                'message' => 'success',
                'data' => [
                    'reservation' => $reservation
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Reservation save failed. Please try again.'
            ], 400);
        }
    } 

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        $update = $reservation->update($request->all());

        if ($update) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'reservation' => $update
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Reservation update failed. Please try again'
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
