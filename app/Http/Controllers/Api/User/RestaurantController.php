<?php

namespace App\Http\Controllers\Api\User;

use App\Branches;  
use App\Menu;
use App\Repository\BranchRepository; 
use App\Repository\PaymentMethodRepository;
use App\Repository\RestaurantRepository;
use App\Restaurant;
use App\Support\Geolocation;
use Carbon\Carbon;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class RestaurantController
 * @package App\Http\Controllers\Api\User
 */
class RestaurantController extends Controller
{
    protected $restaurant; 
    protected $geolocation; 
    protected $payment_method;
    protected $branches; 

    /**
     * RestaurantController constructor.
     * @param RestaurantRepository $restaurant_repository
     * @param Geolocation $geolocation
     * @param PaymentMethodRepository $payment_method_repository
     */
    public function __construct(RestaurantRepository $restaurant_repository, Geolocation $geolocation, PaymentMethodRepository $payment_method_repository, BranchRepository $branch_repository)
    {
        $this->restaurant = $restaurant_repository;
        $this->geolocation = $geolocation;
        $this->payment_method = $payment_method_repository;
        $this->branches = $branch_repository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse0
     */
    public function index(Request $request)
    {
        $type = $request->type; 

        $PublicIP = $request->ip();
        $json = file_get_contents("http://ipinfo.io/$PublicIP/geo?token=".config('services.ip_info_token'));
        $json = json_decode($json, true);

        $postcode = $request->postcode;

        if (isset($json['city']) && isset($json['region']) && isset($json['country'])) {
            $user_address = $json['city'] . ', ' . $json['region'] . ', ' . $json['country'];
        } else {
            $user_address = 'null';
        }

        switch ($type) {
            case 'interests':
                if (Auth::check()) {
                    $restaurants = $this->restaurant->getInterestingRestaurants(Auth::id());
                } else {
                    $restaurants = $this->restaurant->allByPostcode($postcode);
                }
                break;
            case 'recommended':
                $restaurants = $this->restaurant->allByPostcode($postcode);
                break;
            case 'popular':
                $restaurants = [];
                $all_restaurants = $this->restaurant->getPopularRestaurants();
                foreach ($all_restaurants as $restaurant) {
                    $restaurant_address = $restaurant->name . ', ' . $restaurant->city . ', ' . $restaurant->province . ', ' . $restaurant->county . ' ' . $restaurant->postcode;

                    $restaurant->distance = $this->geolocation->getDistance($restaurant_address, $user_address);
                    if ($restaurant->distance < 20) {
                        $restaurants[] = $restaurant;
                    }
                }
                break;
            default:
                $restaurants = $this->restaurant->all();
                break;
        }

        foreach ($restaurants as $key => $restaurant) {
            $reviews = $restaurant->reviews()->get();

            $restaurant->logo = getStorageUrl() . $restaurant->logo;

            $restaurant->sort = $key;

            $restaurant->price_range = $restaurant->price_range + 1;


            $total = 0;
            $count = 0;

            foreach ($reviews as $review) {
                $total += $review->rating;
                $count++;
            }

            if ($total && $count) {
                $restaurant->rating = number_format($total / $count, 0, '.', '');
            } else {
                $restaurant->rating = 5;
            }

            if((1 <= $restaurant->rating) && ($restaurant->rating <= 2)){
                $restaurant->rate = "One";
            }else if ((2 <= $restaurant->rating) && ($restaurant->rating <= 3)) {
                $restaurant->rate = "Two";
            }else if ((3 <= $restaurant->rating) && ($restaurant->rating <= 4)) {
                $restaurant->rate = "Three"; 
            }else if ((4 <= $restaurant->rating) && ($restaurant->rating <= 5)) { 
                $restaurant->rate = "Four"; 
            }else if ((5 <= $restaurant->rating) && ($restaurant->rating <= 6)) {
                $restaurant->rate = "Five"; 
            }
            
            /*if($restaurant->rating == 1){
                $restaurant->rate = "One"; 
            }else if($restaurant->rating == 2){
                $restaurant->rate = "Two"; 
            }else if($restaurant->rating == 3){
                $restaurant->rate = "Three"; 
            }else if($restaurant->rating == 4){
                $restaurant->rate = "Four"; 
            }else if($restaurant->rating == 5){
                $restaurant->rate = "Five"; 
            }*/

            if (Auth::check()) {
                if (Auth::user()->addresses()->count()) {
                    $user_address = Auth::user()->addresses()->where('default', '=', true)->first();
                    if ($user_address) {
                        $default_address = $user_address->street . ', ' . $user_address->city . ', ' . $user_address->county . ' ' . $user_address->postcode;
                        $restaurant_address = $restaurant->name . ', ' . $restaurant->city . ', ' . $restaurant->province . ', ' . $restaurant->county . ' ' . $restaurant->postcode;
                        $this->geolocation->getDistance($restaurant_address, $default_address);
                    }
                }
            }

            $restaurant->delivery_locations = $restaurant->deliveryLocations()->get(); 
            $restaurant->cuisines = $restaurant->cuisines()->get(); 
            $restaurant->reviews = [];
        }

        return response()->json([
            'message' => 'success',
            'data' => [
                'restaurants' => $restaurants
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant, Request $request)
    { 
        $restaurant->opening_hours = $restaurant->openingHours()->get(); 
        $restaurant->reviews = $restaurant->reviews()->get(); 
        $restaurant->url = route('user.restaurant.menu'); 
        $restaurant->logo = getStorageUrl() . $restaurant->logo; 
        $restaurant->cuisines = $restaurant->cuisines()->get(); 
        // $restaurant->delivery_locations = $restaurant->deliveryLocations()->get(); 
        $restaurant->price_range = $restaurant->price_range + 1; 
        $menus = $restaurant->menus()->get();

        foreach ($restaurant->media as $media) {
            $media->path = getStorageUrl() . $media->path;
        }

        $popular = Menu::hydrate([[
            'id' => 0,
            'restaurant_id' => $restaurant->id,
            'name' => 'Popular',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            'deleted_at' => null
        ]])->first();

        $popular->selected = true; 

        $popular->menu_items = $this->restaurant->getPopularItems($restaurant->id);    
        $payment_methods = $this->payment_method->all(); 
        $cart_date = Carbon::now()->toDateString(); 
        $cart_time = Carbon::now()->addMinutes($restaurant->clock_minutes)->addMinute()->format('h:i A');

        return response()->json([
            'message' => 'success',
            'data' => [
                'restaurant' => $restaurant,
                'payment_methods' => $payment_methods,
                'food_vat' => setting('food_vat'),
                'alcohol_vat' => setting('alcohol_vat'),
                //'cart_date' => $cart_date,
                //'cart_time' => $cart_time,
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function getBranches(Restaurant $restaurant, Request $request)
    {   
        $branches = Branches::where('restaurant_id', '=', 1)->orderBy('created_at', 'asc')->paginate(10);
        foreach ($branches as $branch) {
            if ($branch->trashed()) {
                $branch->active = false;
            } else {
                $branch->active = true;
            }
        }

        return response()->json([
            'message' => 'success',
            'data' => [
                'branches' => $branches, 
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurant $restaurant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        //
    } 

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function favourite(Request $request)
    { 
        $restaurant_id = $request->restaurant_id; 
        $restaurant = $this->restaurant->getRestaurant($restaurant_id); 
        $restaurant->price_range = $restaurant->price_range + 1; 
        $user_id = Auth::id(); 
        $restaurant_user = $restaurant->users()->where('user_id', '=', $user_id)->first(); 
        if ($restaurant_user) {
            $restaurant_user_result = $restaurant->users()->where('user_id', '=', $user_id)->detach();
        } else {
            $restaurant_user_result = $restaurant->users()->attach($user_id);
        } 
        if (!$restaurant_user_result) {
            return response()->json([
                'message' => 'Successfully added to favourites list'
            ]);
        } else {
            return response()->json([
                'message' => 'Successfully removed from favourites list'
            ]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function favourites()
    {
        $user = Auth::user(); 
        foreach ($user->restaurants as $restaurant) { 
            $restaurant->logo = getStorageUrl() . $restaurant->logo;
            $restaurant->price_range = $restaurant->price_range + 1; 
            $total = 0;
            $count = 0; 
            foreach ($restaurant->reviews as $review) {
                $total += $review->rating;
                $count++;
            } 
            if ($total && $count) {
                $restaurant->rating = number_format($total / $count, 0, '.', '');
            } else {
                $restaurant->rating = 5;
            }
        } 
        if ($user) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'restaurants' => $user->restaurants
                ]
            ]); 
        } else {
            return response()->json([
                'message' => 'failed'
            ], 400);
        } 
    }

    /**
     * Display current branch
     *
     * @param \App\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function getCurrentBranch(Restaurant $restaurant, Request $request)
    {  
        $branch_id = $request->branch_id; 
        $restaurant = $this->restaurant->getRestaurant(1);
        $current_branch = $this->branches->get($branch_id); 

        return response()->json([
            'message' => 'success',
            'data' => [
                'restaurant' => $restaurant,
                'branch' => $current_branch,
            ]
        ]);
    }
    
}
