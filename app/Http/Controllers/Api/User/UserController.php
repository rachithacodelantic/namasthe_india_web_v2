<?php
namespace App\Http\Controllers\Api\User;

use App\Repository\TakeawayRepository;
use App\Repository\ReservationRepository;
use App\Repository\DeliveryRepository;
use App\Repository\AddressRepository;
use App\Repository\CardDetailsRepositary;
use App\Repository\RestaurantRepository;
use App\Repository\MenuItemRepository;
use App\Repository\UserRepository;
use App\Repository\MenuItemReviewsRepository;  
use App\Repository\DealsRepository;
use App\Repository\OffersRepository;
use App\Repository\BranchRepository;
use App\Takeaway;
use App\Reservation;
use App\Delivery;
use App\MenuItemReviews;  
use App\User;
use App\Offers;
use App\MenuItem;
use App\Variant;
use App\Branches; 
use App\CardDetails;
use App\PasswordReset;
use App\Setting;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as passportClient;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Password;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\User
 */
class UserController extends Controller
{
    protected $user;
    protected $takeaway;
    protected $reservation;
    protected $delivery;
    protected $address;
    protected $payment_methods;
    protected $restaurant;
    protected $menu_item;
    protected $menu_item_reviews;
    protected $deal_item; 
    protected $offer_item; 
    protected $branches; 

    /**
     * UserController constructor.
     * @param TakeawayRepository $takeaway_repository
     * @param ReservationRepository $reservation_repository
     * @param DeliveryRepository $delivery_repository
     * @param UserRepository $user_repository
     * @param AddressRepository $address_repository
     * @param RestaurantRepository $restaurant_repository
     * @param MenuItemRepository $menu_item_repository
     */
    public function __construct
    (
        TakeawayRepository $takeaway_repository,
        ReservationRepository $reservation_repository,
        DeliveryRepository $delivery_repository,
        UserRepository $user_repository,
        AddressRepository $address_repository,
        RestaurantRepository $restaurant_repository,
        CardDetailsRepositary $cards_repository,
        MenuItemRepository $menu_item_repository, 
        MenuItemReviewsRepository $menuItem_reviews_repository,
        BranchRepository $branch_repository,
        OffersRepository $offers_repository,
        DealsRepository $deals_repository
    )
    {
        $this->user = $user_repository;
        $this->takeaway = $takeaway_repository;
        $this->reservation = $reservation_repository;
        $this->delivery = $delivery_repository;
        $this->address = $address_repository;
        $this->payment_methods = $cards_repository;
        $this->restaurant = $restaurant_repository;
        $this->menu_item = $menu_item_repository;
        $this->menu_item_reviews = $menuItem_reviews_repository;
        $this->deal_item = $deals_repository;
        $this->offer_item = $offers_repository;
        $this->branches = $branch_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $email_check = $this->user->getUserByEmail($request->email);
        if ($email_check) {
            return response()->json([
                'message' => 'Email exists. Use different email or login.',
            ], 400);
        }
        $request->request->set('password', bcrypt($request->password));
        $user = $this->user->create($request->all());
        if ($user) {
            return response()->json([
                'message' => 'Registered successfully',
                'data' => [
                    'user' => $user
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Registration failed please try again',
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        $user_address = $this->address->defaultAddress($user->id);
        $user_card = $this->payment_methods->defaultCard($user->id);
        $default_address = '';
        $default_address_postcode = '';
        $default_card_number = '';
        $default_card_cvc = '';
        $default_card_expire = '';
        $delivery_charge = 0;  

        if (!$user_address->isEmpty()) {
            $default_address = $user_address[0]['address'] .', '. $user_address[0]['street'] .', '. $user_address[0]['city'] .', '. $user_address[0]['county'];
            $default_address_postcode = $user_address[0]['postcode']; 

            $restaurant = $this->restaurant->getRestaurant(1);
            foreach ($restaurant->deliveryLocations as $delivery_location) {
                if ($user_address[0]['postcode'] == $delivery_location->postcode) { 
                    $delivery_charge = $delivery_location->delivery_cost;
                }
            }
        }else{
            $default_address = '';
            $default_address_postcode = ''; 
        }

        // if ($user_card){ 
        //     $default_card_number = $user_card[0]['card_number'];
        //     $default_card_cvc = $user_card[0]['cvc'];  
        //     $default_card_expire = $user_card[0]['expire'];  
        // }else{
        //     $default_card_number = '';
        //     $default_card_cvc = ''; 
        //     $default_card_expire = ''; 
        // }

        if ($user) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'location' => $user->location,
                    'email' => $user->email,
                    //'password' => $user->password,
                    'role' => $user->role,
                    'phone' => $user->phone,
                    'country' => $user->country,
                    'city' => $user->city,
                    'province' => $user->province,
                    'postcode' => $user->postcode,
                    'default_address' => $default_address,
                    'default_address_postcode' => $default_address_postcode,
                    'delivery_charge' => $delivery_charge,
                    'default_card_number' => $default_card_number,
                    'default_card_cvc' => $default_card_cvc,
                    'default_card_expire' => $default_card_expire,
                    'food_vat' => setting('food_vat'),
                    'alcohol_vat' => setting('alcohol_vat'),
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'User not found.'
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {  
        $userId = Auth::id();
        $user = User::findOrFail($userId);  
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email|max:225|'. Rule::unique('users')->ignore($user->id),
        ]); 
        
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Update failed!'
            ], 400);
        }else{
            if ($request->password) { 
                $request->request->set('password', bcrypt($request->password));
            }

            $update = $user->update($request->all());

            if ($update) {
                return response()->json([
                    'message' => 'Updated successfully!',
                    'data' => [
                        'user' => $user
                    ]
                ]);
            } else {
                return response()->json([
                    'message' => 'Update failed!'
                ], 400);
            } 
        } 
    }

    /** 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {    
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email|max:225|'
        ]); 
        
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Update failed!'
            ], 400);
        }else{

            $user = User::where('email', $request->email)->first();
            // dd($user);
            if ($user) { 
                if ($request->password) { 
                    $request->request->set('password', bcrypt($request->password));
                } 
                $update = $user->update($request->all());

                if ($update) {
                    return response()->json([
                        'message' => 'Updated successfully!',
                        'data' => [
                            'user' => $user
                        ]
                    ]);
                } else {
                    return response()->json([
                        'message' => 'Update failed!'
                    ], 400);
                } 
            } else {
                return response()->json([
                    'message' => 'Update failed!'
                ], 400);
            } 
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function orders(Request $request)
    {
        $type = $request->type;
        $takeaways = Auth::user()->takeaways;
        foreach ($takeaways as $takeaway) {
            $subtotal = 0;
            $discount = 0;
            $takeaway->promotion = $takeaway->promotion;
            //$takeaway->menu_item = $takeaway->menuItem()->withTrashed()->first();
            $takeaway->site_promotion = $takeaway->site_promotion;
            $takeaway->takeaway_items = $takeaway->takeawayItems;
            // $takeaway->restaurant = $takeaway->restaurant()->withTrashed()->first();
            //$takeaway->restaurant = $takeaway->restaurant->id;
            foreach ($takeaway->takeaway_items as $takeaway_item) {
                $takeaway_item->menu_item = $takeaway_item->menuItem()->withTrashed()->first();
                $takeaway_item->takeaway_item_addons = $takeaway_item->takeawayItemAddons;
                if($takeaway_item->takeaway_item_addons) {
                    foreach ($takeaway_item->takeaway_item_addons as $takeaway_item_addon) {
                        $takeaway_item_addon->addon = $takeaway_item_addon->addon()->withTrashed()->first();
                    }
                }
                // $takeaway_item->variant = $takeaway_item->variant()->withTrashed()->first();
                $takeaway_item->variant = $takeaway_item->menuItemVariants;
                if ($takeaway_item->menu_item) {
                    $menu_item_price = 0;
                    if ($takeaway_item->variant) {
                        $menu_item_price = $takeaway_item->variant->price * $takeaway_item->quantity;
                    } else {
                        $menu_item_price = $takeaway_item->quantity * $takeaway_item->menu_item->price;
                    }
                    if ($takeaway->menu_item) {
                        if ($takeaway->menu_item->id == $takeaway->menu_item_id) {
                            if ($takeaway->menu_item->promo_type == 'percentage') {
                                $discount = $menu_item_price * $takeaway->menu_item->promo_value * 0.01;
                            } else {
                                $discount = $takeaway->menu_item->promo_value;
                            }
                        }
                    }
                    $subtotal += $menu_item_price;
                    if ($takeaway_item->takeawayItemAddons) {
                        foreach ($takeaway_item->takeawayItemAddons as $addon) {
                            if ($addon->addon) {
                                $subtotal += $addon->addon->price;
                            }
                        }
                    }
                }
            }
            if ($takeaway->promotion) {
                if ($takeaway->promotion->type == 'percentage') {
                    $discount = $subtotal * $takeaway->promotion->value * 0.01;
                } else {
                    $discount = $takeaway->promotion->value;
                }
            }
            if ($takeaway->sitePromotion) {
                if ($takeaway->sitePromotion->type == 'percentage') {
                    $discount = $subtotal * $takeaway->sitePromotion->value * 0.01;
                } else {
                    $discount = $takeaway->sitePromotion->value;
                }
            }
            $takeaway->reduction = $discount;
            $takeaway->time = Carbon::parse($takeaway->time)->diffForHumans();
            switch ($takeaway->takeaway_status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }
            $takeaway->progress = $progress;
            if ($takeaway->takeaway_items) {
                $selected_takeaways[] = $takeaway;
            }
            $takeaway->ticket = $takeaway->ticket()->first();
            if ($takeaway->ticket) {
                $takeaway->ticket->date = Carbon::parse($takeaway->ticket->created_at)->toFormattedDateString();
                $takeaway->ticket->user = $takeaway->ticket->user()->first();
                if ($takeaway->ticket->messages) {
                    foreach ($takeaway->ticket->messages as $message) {
                        $message->date = Carbon::parse($message->created_at)->toFormattedDateString();
                    }
                }
                $takeaway->ticket->messages = $takeaway->ticket->ticketMessages()->get();
            }
            $takeaway->sub_total = $subtotal;
            $takeaway->type = 'takeaway';
        }

        $reservations = Auth::user()->reservations;
        foreach ($reservations as $reservation) {
            $reservation->restaurant = $reservation->restaurant()->withTrashed()->first();
            //$reservation->restaurant = $reservation->restaurant->id;
            $restaurant_address = $reservation->restaurant->city . '+' . $reservation->restaurant->province . '+' . $reservation->restaurant->county . '+' . $reservation->restaurant->postcode;
            $reservation->restaurant->query_address = str_replace(' ', '+', $restaurant_address);
            $reservation->date = Carbon::parse($reservation->time)->toFormattedDateString();
            $reservation->time = Carbon::parse($reservation->time)->format('g:i A');
        }

        $deliveries = Auth::user()->deliveries;
        foreach ($deliveries as $delivery) {
            $discount = 0;
            $delivery->delivery_items = $delivery->deliveryItems;
            // $delivery->restaurant = $delivery->restaurant()->withTrashed()->first();
            // $delivery->restaurant = $delivery->restaurant->id;
            $subtotal = 0;
            $delivery->promotion = $delivery->promotion;
            //$delivery->menu_item = $delivery->menuItem()->withTrashed()->first();
            $delivery->site_promotion = $delivery->site_promotion;

            foreach ($delivery->delivery_items as $delivery_item) {
                $delivery_item->menu_item = $delivery_item->menuItem()->withTrashed()->first();
                $delivery_item->delivery_item_addons = $delivery_item->deliveryItemAddons;
                foreach ($delivery_item->delivery_item_addons as $delivery_item_addon) {
                    $delivery_item_addon->addon = $delivery_item_addon->addon;
                }
                $delivery_item->variant = $delivery_item->variant;
                if ($delivery_item->menu_item) {
                    $menu_item_price = 0;
                    if ($delivery_item->variant) {
                        $menu_item_price = $delivery_item->variant->price * $delivery_item->quantity;
                    } else {
                        $menu_item_price = $delivery_item->quantity * $delivery_item->menu_item->price;
                    }
                    if ($delivery->menu_item) {
                        if ($delivery->menu_item->id == $delivery->menu_item_id) {
                            if ($delivery->menu_item->promo_type == 'percentage') {
                                $discount = $menu_item_price * $delivery->menu_item->promo_value * 0.01;
                            } else {
                                $discount = $delivery->menu_item->promo_value;
                            }
                        }
                    }
                    $subtotal += $menu_item_price;
                    if ($delivery_item->deliveryItemAddons) {
                        foreach ($delivery_item->deliveryItemAddons as $addon) {
                            if ($addon->addon) {
                                $subtotal += $addon->addon->price;
                            }
                        }
                    }
                }
            }
            if ($delivery->promotion) {
                if ($delivery->promotion->type == 'percentage') {
                    $discount = $subtotal * $delivery->promotion->value * 0.01;
                } else {
                    $discount = $delivery->promotion->value;
                }
            }
            if ($delivery->sitePromotion) {
                if ($delivery->sitePromotion->type == 'percentage') {
                    $discount = $subtotal * $delivery->sitePromotion->value * 0.01;
                } else {
                    $discount = $delivery->sitePromotion->value;
                }
            }
            $delivery->reduction = $discount;
            $original_time = $delivery->time;
            $delivery->time = Carbon::parse($delivery->time)->diffForHumans();
            switch ($delivery->delivery_status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }

            if (!$delivery->delivery_status) {
                $delivery->delivery_status = 'initiated';
            }
            $delivery->progress = $progress;
            if ($delivery->delivery_items && $delivery->restaurant) {
                $selected_deliveries[] = $delivery;
            }
            // if ($delivery->restaurant) {
            //     $delivery_locations = $delivery->restaurant->deliveryLocations;
            //     $delivery->elapsed_time = 0;
            //     foreach ($delivery_locations as $delivery_location) {
            //         if ($delivery_location->postcode == $delivery->postcode) {
            //             $elapsed_time = Carbon::parse($original_time)->addMinutes($delivery_location->delivery_time)->diffForHumans();
            //             if ($elapsed_time > 0 and Carbon::parse($original_time)->gt(Carbon::now())) {
            //                 $delivery->elapsed_time = $elapsed_time;
            //             }
            //         }
            //     }
            // }

            // $delivery->ticket = $delivery->ticket()->first();
            // if ($delivery->ticket) {
            //     $delivery->ticket->date = Carbon::parse($delivery->ticket->created_at)->toFormattedDateString();
            //     $delivery->ticket->user = $delivery->ticket->user()->first();
            //     $delivery->ticket->messages = $delivery->ticket->ticketMessages()->get();
            //     $delivery->ticket->resolved = (bool)$delivery->ticket->resolved;
            //     if ($delivery->ticket->messages) {
            //         foreach ($delivery->ticket->messages as $message) {
            //             $message->date = Carbon::parse($message->created_at)->toFormattedDateString();
            //         }
            //     }
            // }
            $delivery->sub_total = $subtotal;
            $delivery->type = 'delivery';
        }

        $orders = [];
        foreach ($deliveries as $delivery) {
            if ($type == 'current') {
                if ($delivery->delivery_status != 'delivered') {
                    $orders[] = $delivery;
                }
            } else if ($type == 'past') {
                if ($delivery->delivery_status == 'delivered') {
                    $orders[] = $delivery;
                }
            } else {
                $orders[] = $delivery;
            }
        }

        foreach ($takeaways as $takeaway) {
            if ($type == 'current') {
                if ($takeaway->takeaway_status != 'delivered') {
                    $orders[] = $takeaway;
                }
            } else if ($type == 'past') {
                if ($takeaway->takeaway_status == 'delivered') {
                    $orders[] = $takeaway;
                }
            } else {
                $orders[] = $takeaway;
            }
        }

        foreach ($reservations as $reservation) {
            $reservation->type = 'reservation';
            if (Carbon::parse($reservation->time)->lte(Carbon::now())) {
                $reservation->reservation_status = 'expired';
            } else {
                $reservation->reservation_status = 'pending';
            }
            $orders[] = $reservation;
        }

        if ($orders) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'orders' => $orders
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrder(Request $request)
    {
        $type = $request->type;
        $order_id = $request->order_id;

        if($type == 'takeaway'){

            // $takeaway = $this->takeaway->getTakeaway($order_id);
            // $takeaway->takeaway_items = $takeaway->takeawayItems;
            // $takeaway->restaurant = $takeaway->restaurant;
            // foreach ($takeaway->takeaway_items as $takeaway_item) {
            //     $takeaway_item->menu_item = $takeaway_item->menuItem;

            // }

            $takeaway = $this->takeaway->getTakeaway($order_id);
            $takeaway->takeaway_items = $takeaway->takeawayItems;
            foreach ($takeaway->takeaway_items as $takeaway_item) {
                $takeaway_item->menu_item = $takeaway_item->menuItem; 

                $takeaway_item->deal_item = $takeaway_item->dealItem; 
 
                $takeaway_item->selected_deal_menu_items = MenuItem::join('deals_item', 'deals_item.menu_item_id', '=', 'menu_items.id')
                ->where('deals_item.deals_id', '=', $takeaway_item->deal_id)
                ->select('menu_items.id', 'menu_items.name', 'menu_items.price')
                ->get();   

                $takeaway_item->offer_item = $takeaway_item->offerItem; 
                // $takeaway_item->selectedOfferMenuItems = $takeaway_item->selectedOfferMenuItems;  
                $takeaway_item->selected_offer_menu_items = MenuItem::join('offer_item', 'offer_item.menu_item_id', '=', 'menu_items.id')
                ->where('offer_item.offers_id', '=', $takeaway_item->offer_id)
                ->select('menu_items.id', 'menu_items.name', 'menu_items.price')
                ->get(); 
 
                foreach ($takeaway_item->selected_deal_menu_items as $selected_deal_menu_item) { 
                    $selected_deal_menu_item->menu_item = $this->menu_item->getDealMenuItem($selected_deal_menu_item['menu_item_id']);
                }
                foreach ($takeaway_item->selected_offer_menu_items as $selected_offer_menu_item) { 
                    $selected_offer_menu_item->menu_item = $this->menu_item->getOfferMenuItem($selected_offer_menu_item['menu_item_id']);
                }

                $takeaway_item->variant = $takeaway_item->menuItemVariants()->first();
                if ($takeaway_item->variant) { 
                    $takeaway_variant = $this->menu_item->getVariant($takeaway_item->menuItem->id, $takeaway_item->variant->variant_id);
                    $takeaway_item->variantData = $takeaway_variant;
                }  

                $delivery_crust = '';
                // $takeaway_item->crust = $takeaway_item->crustTypeMenuItem()->first(); 
                $takeaway_item->crust = $takeaway_item->crustTypeMenuItem;  
                if ($takeaway_item->crust) {
                    $delivery_crust = $this->menu_item->getCrust($takeaway_item->menuItem->id, $takeaway_item->crust->crust_type_id);
                    $takeaway_item->crustData = $delivery_crust;
                } 

                $takeaway_item->takeaway_item_addons = $takeaway_item->addonMenuItems()->get();
                foreach ($takeaway_item->takeaway_item_addons as $addon_menu_item) {
                    $addon_menu_item->addon = $this->menu_item->getAddon($addon_menu_item['menu_item_id'], $addon_menu_item['addon_id']);
                }
            }
            $takeaway->time = Carbon::parse($takeaway->time)->diffForHumans();
            switch ($takeaway->status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }
            $takeaway->progress = $progress;
            if ($takeaway) {
                return response()->json([
                    'message' => 'success',
                    'data' => [
                        'takeaways' => $takeaway
                    ]
                ]);
            } else {
                return response()->json([
                    'message' => 'Failed'
                ], 400);
            } 

        }else if($type == 'reservation'){

            $reservationData = $this->reservation->get($order_id);
            $reservationData->restaurant = $reservationData->restaurant()->first();
            $restaurant_address = $reservationData->restaurant->city . '+' . $reservationData->restaurant->province . '+' . $reservationData->restaurant->county . '+' . $reservationData->restaurant->postcode;
            $reservationData->restaurant->query_address = str_replace(' ', '+', $restaurant_address);
            $reservationData->date = Carbon::parse($reservationData->time)->toFormattedDateString();
            $reservationData->time = Carbon::parse($reservationData->time)->format('g:i A');

            if ($reservationData) {
                return response()->json([
                    'message' => 'success',
                    'data' => [
                        'reservations' => $reservationData
                    ]
                ]);
            } else {
                return response()->json([
                    'message' => 'Failed'
                ], 400);
            }
        }else{

            $deliveryData = $this->delivery->getDelivery($order_id);
            $deliveryData->delivery_items = $deliveryData->deliveryItems;
            foreach ($deliveryData->delivery_items as $delivery_item) {
                $delivery_item->menu_item = $delivery_item->menuItem;
                //$delivery_item->menu_item_name = $delivery_item->menuItem->name;
                //$delivery_item->menu_item_price = $delivery_item->menuItem->price;  

                $delivery_item->deal_item = $delivery_item->dealItem;
                // $delivery_item->selected_deal_menu_items = $delivery_item->selectedDealMenuItems;  
 
                $delivery_item->selected_deal_menu_items = MenuItem::join('deals_item', 'deals_item.menu_item_id', '=', 'menu_items.id')
                ->where('deals_item.deals_id', '=', $delivery_item->deal_id)
                ->select('menu_items.id', 'menu_items.name', 'menu_items.price')
                ->get();   

                $delivery_item->offer_item = $delivery_item->offerItem; 
                $delivery_item->selected_offer_menu_items = $delivery_item->selectedOfferMenuItems;  
 
                foreach ($delivery_item->selected_deal_menu_items as $selected_deal_menu_item) { 
                    $selected_deal_menu_item->menu_item = $this->menu_item->getDealMenuItem($selected_deal_menu_item['menu_item_id']);
                    // dd($selected_deal_menu_item->menu_item);
                }
                foreach ($delivery_item->selected_offer_menu_items as $selected_offer_menu_item) { 
                    $selected_offer_menu_item->menu_item = $this->menu_item->getOfferMenuItem($selected_offer_menu_item['menu_item_id']);
                    // dd($selected_offer_menu_items->menu_item);
                }

                $delivery_item->variant = $delivery_item->menuItemVariants()->first(); 
                // $delivery_item->variant = Variant::join('menu_item_variant', 'menu_item_variant.menu_item_id', '=', 'menu_items.id')
                // ->where('deals_item.deals_id', '=', $delivery_item->deal_id)
                // ->select('menu_items.id', 'menu_items.name', 'menu_items.price')
                // ->get();    
                if ($delivery_item->variant) { 
                    // dd($delivery_item->variant);
                    $delivery_variant = $this->menu_item->getVariant($delivery_item->menuItem->id, $delivery_item->variant->variant_id);
                    // dd($delivery_variant);
                    $delivery_item->variantData = $delivery_variant;
                } 
                // $delivery_item->variantData = $delivery_variant;
                //$delivery_item->variant->variantName = $delivery_item->variant->variant; 

                $delivery_crust = '';
                $delivery_item->crust = $delivery_item->crustTypeMenuItem()->first(); 
                if ($delivery_item->crust) {
                    $delivery_crust = $this->menu_item->getCrust($delivery_item->menuItem->id, $delivery_item->crust->crust_type_id);
                    $delivery_item->crustData = $delivery_crust;
                }
                // $delivery_item->crustData = $delivery_crust;

                // $delivery_item->delivery_item_addons = $delivery_item->deliveryItemAddons()->get();
                // foreach ($delivery_item->delivery_item_addons as $delivery_item_addon) {
                //     $delivery_item_addon->addon = $delivery_item_addon->addon()->first();
                // }

                $delivery_item->delivery_item_addons = $delivery_item->addonMenuItems()->get();
                foreach ($delivery_item->delivery_item_addons as $addon_menu_item) {
                    $addon_menu_item->addon = $this->menu_item->getAddon($addon_menu_item['menu_item_id'], $addon_menu_item['addon_id']);
                }
            }
            $deliveryData->time = Carbon::parse($deliveryData->time)->diffForHumans();
            switch ($deliveryData->status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }
            $deliveryData->progress = $progress;
            if ($deliveryData) {
                return response()->json([
                    'message' => 'success',
                    'data' => [
                        'deliveries' => $deliveryData
                    ]
                ]);
            } else {
                return response()->json([
                    'message' => 'Failed'
                ], 400);
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderStatus(Request $request)
    { 
        $order_id = $request->order_id;
        $deliveryData = $this->delivery->getDelivery($order_id); 
        switch ($deliveryData->status) {
            case 'initiated':
                $progress = 12;
                break;
            case 'approved':
                $progress = 25;
                break;
            case 'dispatched':
                $progress = 50;
                break;
            case 'delivered':
                $progress = 88;
                break;
            default:
                $progress = 12;
                break;
        }
        $deliveryData->progress = $progress;
        if ($deliveryData) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'status' => $deliveryData->progress
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $userData = $this->user->getUserByEmail($request->email);
        if (!$userData) {
            return response()->json(['success' => false, 'message' => 'Login Failed, please check email id']);
        }else if (!Hash::check($request->password, $userData->password)) {
            return response()->json(['success' => false, 'message' => 'Login Failed, please check password']);
        }else{
            if (Auth::attempt($request->all())) {
                $passportClient = passportClient::where('password_client', 1)->first();
                return response()->json(['success' => true, 'data' => $passportClient->secret]);
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser()
    {
        $user = Auth::user();
        $user->address = $user->addresses()->where('default', '=', true)->first();
        if ($user) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'user' => $user
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCardList()
    {
        $user = Auth::user();
        $cards = $this->payment_methods->getUserCards($user->id); 
        if ($cards) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'paymentMethods' => $cards
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'You don\'t have any saved card details'
            ], 400);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMethodList(Request $request)
    {
        $branch_id = $request->branch_id; 
        $payment_methods = Branches::find($branch_id)->paymentMethods;  
        if ($payment_methods) {
            return response()->json([
                'payment_methods' => $payment_methods
            ]);
        }  
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveCardDetails(Request $request)
    {
        $request->card_number = str_replace(' ', '', $request->card_number); 
        $request->request->set('card_number', $request->card_number); 
        $card_check = $this->payment_methods->getCard($request->card_number);
        if ($card_check) {
            return response()->json([
                'message' => 'Card exists. Use different card details.',
            ], 400);
        }  

        $card = $this->payment_methods->create($request->all());
        if ($card) {
            return response()->json([
                'message' => 'Registered successfully',
                'data' => [
                    'method' => $card
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Registration failed please try again',
            ], 400);
        }
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function updateCard(Request $request, CardDetailsRepositary $payment_methods)
    {   
        $user = Auth::user(); 
        CardDetails::whereIn('user_id', [$user->id])->update(['default' => 0]); 
        $update = CardDetails::where('id', $request->method_id)->update(['default' => 1]); 
        $method = CardDetails::find($request->method_id);
        if ($update) {
            return response()->json([
                'message' => 'Card updated successfully',
                'data' => [
                    'method' => $method
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Update failed.'
            ], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToFavourite(Request $request)
    {
        $item_id = $request->item_id; 
        $user_id = Auth::id();
        $user = Auth::user(); 
        $menuitem_user = DB::table('menu_item_user')->where('menu_item_id', '=', $item_id)->where('user_id', '=', $user_id)->first(); 

        if ($menuitem_user != NULL) { 
            $menuitem_user_result = DB::table('menu_item_user')->where('menu_item_id', '=', $item_id)->where('user_id', '=', $user_id)->delete(); 
            return response()->json([
                'message' => 'Successfully removed from favourites list'
            ]);
        } else { 
            $menuitem_user_result = DB::table('menu_item_user')->insert(array('menu_item_id' => $item_id, 'user_id' => $user_id));
            return response()->json([
                'message' => 'Successfully added to favourites list'
            ]);
        } 
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function favouriteItems()
    {
        $user = Auth::user(); 
        if ($user) {
            $items = $user->menuItems;
            foreach ($items as $menu_item) {  

                if ($menu_item->logo) {
                    $menu_item->logo = getStorageUrl() . $menu_item->logo;
                } else {
                    $menu_item->logo = asset('img/default.jpg');
                }
            } 
            return response()->json([
                'message' => 'success',
                'data' => [
                    'items' => $items
                ]
            ]); 
        } else {
            return response()->json([
                'message' => 'failed'
            ], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validatePostCode(Request $request) 
    { 
        $user = Auth::user(); 
        if ($user) {
            $codeStatus = false;
            $charge = 0;
            $restaurant = $this->restaurant->getRestaurant(1);
            foreach ($restaurant->deliveryLocations as $delivery_location) {
                if ($request->postcode == $delivery_location->postcode) {
                    $codeStatus = true;
                    $charge = $delivery_location->delivery_cost;
                }
            }

            return response()->json([
                'message' => 'success',
                'data' => [
                    'status' => $codeStatus,
                    'charge' => $charge
                ]
            ]); 
        }else{
            return response()->json([
                'message' => 'failed'
            ], 400);
        } 
    }

    // public function forgot_password(Request $request)
    // {
    //     $input = $request->all();
    //     $rules = array(
    //         'email' => "required|email",
    //     );
    //     $validator = Validator::make($input, $rules);
    //     if ($validator->fails()) {
    //         $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    //     } else {
    //         try {
    //             $response = Password::sendResetLink($request->only('email'), function (Message $message) {
    //                 $message->subject($this->getEmailSubject());
    //             });
    //             switch ($response) {
    //                 case Password::RESET_LINK_SENT:
    //                     return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
    //                 case Password::INVALID_USER:
    //                     return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
    //             }
    //         } catch (\Swift_TransportException $ex) {
    //             $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
    //         } catch (Exception $ex) {
    //             $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
    //         }
    //     }
    //     return \Response::json($arr);
    // }

    public function forgot_password(Request $request)
    {
        $rand = rand(0,100000);
        $input = $request->all(); 
        $rules = array(
            'email' => "required|email",
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {  
                // $user = Auth::user(); 
                $email = $request->email; 
                $_SESSION['email'] = $email;
                $data = array('rand'=>$rand); 
                Mail::send('email', $data, function($message) {
                    $message->to($_SESSION['email'], 'Namasthe India')->subject
                    ('Namasthe India Reset Password');
                    $message->from('info@namasteindia-staging.tk','Namasthe India');
                });
            } catch (\Exception $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
            }
        } 
        return response()->json([
            'message' => 'success',
            'data' => [
                'status' => '200',
                'randomId' => $rand
            ]
        ]); 
    }

    public function addItemReview(Request $request)
    { 
        $request->request->set('user_id', Auth::id());
        $review = $this->menu_item_reviews->create($request->all());
        if ($review) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'review' => $review,
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'failed',
            ], 400);
        }
    }
}
