<?php

namespace App\Http\Controllers\Api\User;

use App\Branches; 
use App\Delivery;
use App\DeliveryDealItems;
use App\DeliveryOfferItems;
use App\User;
use Mail;
use App\Http\Controllers\Controller;
use App\Repository\DeliveryRepository;
use App\Repository\MenuItemRepository;
use App\Repository\RestaurantRepository;
use App\Repository\DealsRepository;
use App\Repository\OffersRepository;
use App\Repository\BranchRepository;
use App\Setting;
use App\Support\Socket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;

/**
 * Class DeliveryController
 * @package App\Http\Controllers\Api\User
 */
class DeliveryController extends Controller
{

    protected $delivery; 
    protected $socket; 
    protected $restaurant; 
    protected $menu_item;
    protected $branches; 
    protected $deal_item;
    protected $offer_item;

    /**
     * DeliveryController constructor.
     * @param DeliveryRepository $delivery_repository
     * @param Socket $socket
     */
    public function __construct(
        DeliveryRepository $delivery_repository,
        Socket $socket,
        RestaurantRepository $restaurant_repository,
        BranchRepository $branch_repository,
        MenuItemRepository $menu_item_repository,
        DealsRepository $deals_repository,
        OffersRepository $offers_repository
    )
    {
        $this->delivery = $delivery_repository;
        $this->socket = $socket;
        $this->restaurant = $restaurant_repository;
        $this->menu_item = $menu_item_repository;
        $this->branches = $branch_repository;
        $this->deal_item = $deals_repository;
        $this->offer_item = $offers_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveries = Auth::user()->deliveries()->orderBy('created_at', 'desc')->paginate(10); 

        foreach ($deliveries as $delivery) {
            //$delivery->delivery_items = $delivery->deliveryItems;
            //$delivery->restaurant = $delivery->restaurant()->first();
            /*foreach ($delivery->delivery_items as $delivery_item) {
                $delivery_item->menu_item = $delivery->menuItem;
            }*/

            $name = User::find($delivery->user_id); 
            $delivery->user = $name->first_name . " " . $name->last_name;
            $delivery->time = Carbon::parse($delivery->time)->diffForHumans();

            switch ($delivery->status) {
                case 'initiated':
                    $progress = 12;
                    break;
                case 'approved':
                    $progress = 25;
                    break;
                case 'dispatched':
                    $progress = 50;
                    break;
                case 'delivered':
                    $progress = 88;
                    break;
                default:
                    $progress = 12;
                    break;
            }

            $delivery->progress = $progress;
        }


        if ($deliveries) {
            return response()->json([
                'message' => 'success',
                'data' => [
                    'deliveries' => $deliveries
                ]
            ]);
        } else {
            return response()->json([
                'message' => 'Failed'
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Pusher\PusherException
     */
    public function store(Request $request)
    { 
        $promotion = $request->promotion; 
        $request->request->set('user_id', Auth::id());
        $request->request->set('time', Carbon::parse($request->date . ' ' . $request->time)->toDateTimeString());
        $request->request->set('delivery_status', 'initiated');
        $restaurant = $this->restaurant->getRestaurant($request->restaurant_id);
        $delivery_charge = 0;
        $delivery_validation = false; 

        foreach ($restaurant->deliveryLocations as $delivery_location) {
            if ($request->postcode == $delivery_location->postcode) {
                $request->request->set('delivery_charge', $delivery_location->delivery_cost);
                $delivery_charge = $delivery_location->delivery_cost;
                $delivery_validation = true;
            }
        }
        if (!$delivery_validation) {
            return response()->json([
                'message' => 'We do not deliver to this location.!'
            ], 400);
        }
        $cart = $request->cart;
        $token = $request->token;
        $total = 0;
        $discount = 0;
        $subtotal = 0;
        $vat = 0;
 
        foreach ($cart as $item) {
            
            if($item['item_type'] == 'single') {
                // $menu_item = $this->menu_item->get($item['id']);
                $menu_item = $this->menu_item->get($item['menu_item_id']);
            } elseif($item['item_type'] == 'offer') {
                $menu_item = $this->offer_item->get($item['menu_item_id']); 
            } else {
                // $menu_item = $this->deal_item->get($item['id']);
                $menu_item = $this->deal_item->get($item['menu_item_id']);
            } 

            // $menu_item = $this->menu_item->get($item['menu_item_id']);
            if ($menu_item) {
                $menu_item_price = 0;
 
                if (!empty($item['varients']) && $item['item_type'] == 'single') {
                    $variant = $this->menu_item->getVariant($item['menu_item_id'], $item['varients'][0]['id']);
                    // $menu_item_price = $variant->pivot->price * $item['quantity'];
                    $menu_item_price = $item['varients'][0]['price'] * $item['quantity'];
                } else {
                    $menu_item_price = $menu_item->price * $item['quantity'];
                }
                $subtotal += $menu_item_price;
            } 
                
            for($i = 1; $i <= $item['quantity']; $i++){
            
                if ($promotion) { 

                    if ($promotion['method'] == 'menu_item' && $promotion['id'] == $menu_item->id) {
                        if ($promotion['type'] == 'percentage') {
                            $discount = $menu_item_price * $promotion['value'] * 0.01;
                        } else {
                            $discount = $promotion['value'];
                        }
                    }
                }
                //$subtotal += $menu_item_price; 
                //$vat += getVat()[$menu_item->vat_category] * $menu_item_price;
                $vat += 0; 

                if (!empty($item['addons']) && $item['item_type'] == 'single') {
                    foreach ($item['addons'] as $selected_addon) {
                        //dd($selected_addon['id']);
                        $addon = $this->menu_item->getAddon($item['menu_item_id'], $selected_addon['id']);
                        if($addon){
                            // $subtotal += $addon->pivot->price;
                            $subtotal = $subtotal + $selected_addon['price'];
                        } 
                    }
                }

                if (!empty($item['crusts']) && $item['item_type'] == 'single') {   
                    $crust_type = $this->menu_item->getCrust($item['menu_item_id'], $item['crusts'][0]['id']);
                    // dd($crust_type->pivot->price);
                    $subtotal += $crust_type->pivot->price; 
                    // $menu_item_total_price += $crust_type->pivot->price;  
                }
            } 
        } 

        //$vat += $delivery_charge * getVat()['delivery'];
        // $vat += 0;
        $vat = $request->vat;
        $request->request->set('vat', $vat);
        $request->request->set('delivery_charge', $delivery_charge);
        if ($promotion) {
//            if ($promotion->method == 'site' || $promotion->method == 'restaurant') {
//                if ($promotion->type == 'percentage') {
//                    $discount = $subtotal * $promotion->value * 0.01;
//                } else {
//                    $discount = $promotion->value;
//                }
//            }
            if ($promotion['method'] == 'site' || $promotion['method'] == 'restaurant') {
                if ($promotion['type'] == 'percentage') {
                    $discount = $subtotal * $promotion['value'] * 0.01;
                } else {
                    $discount = $promotion['value'];
                }
            }
        }
        $request->request->set('menu_item_id', null);
        $request->request->set('promotion_id', null);
        $request->request->set('site_promotion_id', null);
        $request->request->set('site_discount', null);
        $request->request->set('restaurant_discount', null);
        if ($promotion) {
            switch ($promotion['method']) {
                case 'site':
                    $request->request->set('site_promotion_id', $promotion['id']);
                    break;
                case 'restaurant':
                    $request->request->set('promotion_id', $promotion['id']);
                    break;
                case 'menu_item':
                    $request->request->set('menu_item_id', $promotion['id']);
                    break;
            }
        }
        $setting = Setting::first();
        $site_discount = 0;
        $restaurant_discount = 0;

        if (Carbon::now()->startOfDay()->lte(Carbon::parse($restaurant->expiry_date)->startOfDay()) && Carbon::now()->startOfDay()->gte(Carbon::parse($restaurant->start_date)->startOfDay())) {
            if ($restaurant->discount_type == 'percentage') {
                $restaurant_discount = $subtotal * $restaurant->discount_value * 0.01;
            } else {
                $restaurant_discount = $restaurant->discount_value;
            }
            $request->request->set('restaurant_discount', $restaurant_discount);
        } 

        $total = $subtotal + $vat + $delivery_charge - $discount - $site_discount - $restaurant_discount;
        $request->request->set('total', $total);
        $delivery = $this->delivery->create($request->all());

        foreach ($cart as $item) {
            for($i = 1; $i <= $item['quantity']; $i++){

                if($item['item_type'] == 'single') { 

                    $delivery_item = $this->delivery->createDeliveryItem([
                        'delivery_id' => $delivery->id,
                        'menu_item_id' => $item['menu_item_id'],
                        'quantity' => 1,
                    ]);  
                } elseif($item['item_type'] == 'offer') { 

                    $delivery_item = $this->delivery->createDeliveryItem([
                        'delivery_id' => $delivery->id,
                        'offer_id' => $item['menu_item_id'],
                        'quantity' => 1,
                    ]);  
                    foreach ($item['offer_menuItems'] as $selected_offer_item) {
                        $delivery_offer_items = New DeliveryOfferItems();
                        $delivery_offer_items->delivery_item_id = $delivery_item->id;
                        $delivery_offer_items->offer_item_id = $selected_offer_item['offer_items_id'];
                        $delivery_offer_items->save();
                    }
                } else {
                    $delivery_item = $this->delivery->createDeliveryItem([
                        'delivery_id' => $delivery->id,
                        'deal_id' => $item['menu_item_id'],
                        'quantity' => 1,
                    ]); 
                    foreach ($item['deals'] as $selected_deal_item) {
                        $delivery_deal_items = New DeliveryDealItems();
                        $delivery_deal_items->delivery_item_id = $delivery_item->id;
                        $delivery_deal_items->deal_item_id = $selected_deal_item['id'];
                        $delivery_deal_items->save();

                        // $data=array(
                        //     "delivery_item_id" => $delivery_item->id,
                        //     "deal_item_id" => $selected_deal_item['deals_items_id']
                        // ); 
                        // $checkinsert=DB::table('delivery_deal_items')->insert($data);
                    }
                } 

                // if (isset($item['addons'])) {
                //     foreach ($item['addons'] as $selected_addon) {
                //         $delivery_item->deliveryItemAddons()->create([
                //             'addon_id' => $selected_addon['id']
                //         ]);
                //     }
                // }
                // if (isset($item['varients'])) {
                //     $delivery_item->update([
                //         'variant_id' => $item['varients'][0]['id']
                //     ]);
                // }

                if (isset($item['addons']) && !empty($item['addons']) && $item['item_type'] == 'single') {
                    foreach ($item['addons'] as $selected_addon) {
                        $selected_addon_menu_item = $this->menu_item->getSelectedAddonMenuItem($item['menu_item_id'], $selected_addon['id']);
                        $delivery_item->addonMenuItems()->attach($selected_addon_menu_item->id);
                        // $delivery_item->deliveryItemAddons()->create([
                        //     'addon_id' => $selected_addon['id']
                        // ]);
                    }
                }

                if (isset($item['crusts']) && !empty($item['crusts'] && $item['item_type'] == 'single')) {  
                    $selected_crust_menu_item = $this->menu_item->getSelectedMenuItemCrust($item['menu_item_id'], $item['crusts'][0]['id']);
                    $delivery_item->update([
                        // 'menu_item_crust_id' => $selected_crust_menu_item->id
                        'menu_item_crust_id' => $selected_crust_menu_item['id']
                    ]);
                }

                if (isset($item['varients']) && !empty($item['varients']) && $item['item_type'] == 'single') {
                    $selected_menu_item_variant = $this->menu_item->getSelectedMenuItemVariant($item['menu_item_id'], $item['varients'][0]['id']);
                    // dd($selected_menu_item_variant['id']);
                    $delivery_item->update([
                        // 'menu_item_variant_id' => $selected_menu_item_variant->id
                        'menu_item_variant_id' => $selected_menu_item_variant['id']
                    ]);
                }
            }
        }

        $this->socket->push($delivery->attributesToArray(), 'create delivery', config('app.name').'_'. $request->restaurant_id);
        $charge = null;

        if ($request->token) {
            // Stripe::setApiKey('sk_test_51Gua6bGjMqZXqM3Y8TMVcFD8FSLEbJEnCtwMApZ7u6i1SeL7ka7k4b7eNVIIOZtxqjwqR7NQGJMXZKoRIkzcm2D100sXtXc8Ut');
            // Stripe::setApiKey('sk_test_doeAFOHiBxFVWgx9ZKovkr6Z00DJir49HI');
            // Stripe::setApiKey('sk_live_R4gjsB5gUo5Jcwy7zMB3Ordz00WShL8EsZ');
            Stripe::setApiKey('sk_test_51Gua6bGjMqZXqM3Y8TMVcFD8FSLEbJEnCtwMApZ7u6i1SeL7ka7k4b7eNVIIOZtxqjwqR7NQGJMXZKoRIkzcm2D100sXtXc8Ut');
            $charge = Charge::create(['amount' => round($total, 2) * 100, 'currency' => 'NOK', 'source' => $token]);
        } 
              
        if ($charge && $request->payment == 'Pay Online') { 
            
            $payment = json_encode([
                'type' => 'card',
                'payment' => $charge
            ]);
            $delivery->update(['payment' => $payment]);
            $this->sendResponse($delivery); 
        } else if ($request->payment == 'mobilepayment') { 
            $payment = json_encode([
                'type' => 'mobile',
                'payment' => null
            ]);
            $delivery->update(['payment' => $payment]);
            $this->sendResponse($delivery); 
        } else {
            $delivery->update(['payment' => json_encode([
                'type' => 'cash',
                'payment' => null
            ])]);
            $this->sendResponse($delivery); 
        }
        //$delivery->vat = number_format($delivery->vat, 2);
        // $delivery->vat = 0;
        // $delivery->total = number_format($delivery->total, 2);
        // $delivery->delivery_charge = number_format($delivery->delivery_charge, 2);
        // $delivery->delivery_charge = number_format($delivery->delivery_charge, 2);
        // $delivery->delivery_items = $delivery->deliveryItems()->get();

        // foreach ($delivery->delivery_items as $delivery_item) {
        //     $delivery_item->menu_item = $delivery_item->menuItem()->first();
        //     $delivery_item->variant = $delivery_item->variant()->first();
        //     $delivery_item->delivery_item_addons = $delivery_item->deliveryItemAddons()->get();
        //     foreach ($delivery_item->delivery_item_addons as $delivery_item_addon) {
        //         $delivery_item_addon->addon = $delivery_item_addon->addon()->first();
        //     }
        // }
        return response()->json([
            'message' => 'success',
            'data' => [
                'delivery' => $delivery,
                //'charge' => $charge
            ]
        ]); 
    }
    
    /**
     * @param Delivery 
     */
    public function sendResponse($delivery)
    {  
        if (\auth()->user()->email == setting('guest_email_id')) {
            $user_email = $delivery->email;
        } else {
            $user_email = $delivery->user->email;
        }
        // $user_email = $delivery->email;
        $restaurant = $delivery->restaurant()->first();
        $theme = setting('site_theme');

        if($theme == 'orange-peel') {
            $color_theme = '#ffbf00';
            $button_color = '#d38a0c';
        } elseif ($theme == 'whiskey') {
            $color_theme = '#d3a971';
            $button_color = '#d38a0c';
        } elseif ($theme == 'thunderbird') {
            $color_theme = '#cb1511';
            $button_color = '#900906';
        } elseif ($theme == 'amber') {
            $color_theme = '#ffbf00';
            $button_color = '#d38a0c';
        } elseif ($theme == 'apple') {
            $color_theme = '#409843';
            $button_color = '#2d5840';
        } else {
            $color_theme = '#2A8F38';
            $button_color = '#db6d13';
        }

        $theme = [
            'color_theme'       => $color_theme,
            'button_color'      => $button_color,
            'restaurant_email'  => $restaurant->email,
            'restaurant_tel'    => $restaurant->phone,
            'current_year'      => date('Y'),
            'terms_url'         => config('app.url').'/terms-and-conditions',
            'app_name'          => config('app.name'),
            'app_logo'          => asset('storage/'. $restaurant->logo)
        ];

        //send confirmation mail
        Mail::send(['html' => 'user.email.delivery-confirmed'], ['delivery' => $delivery, 'theme' => $theme],
        function ($message) use ($user_email) {
            $message->to($user_email)->subject('Order received');
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function validateOrder(Request $request)
    { 
        $branch_id = $request->branch_id; 
        $restaurant_id = $request->restaurant_id; 
        $restaurant = $this->restaurant->getRestaurant(1);  
        $cart_date = Carbon::parse($request->cart_date . ' ' . $request->cart_time);
        $day_flag = true;

        $branch = $this->branches->get($branch_id); 
        $restaurant->opening_hours = $branch->openingHours()->orderBy('day', 'asc')->orderBy('opening_time', 'desc')->get();
        foreach ($restaurant->opening_hours as $opening_hour) { 
            if ($opening_hour->day == $cart_date->dayOfWeek) {
                $cart_time = Carbon::parse($cart_date->toTimeString());
                $opening_time = Carbon::parse($opening_hour->opening_time);
                $closing_time = Carbon::parse($opening_hour->closing_time); 
                if ($cart_time->gt($opening_time) && $cart_time->lt($closing_time)) {
                    $day_flag = false;
                }
            }
        } 
        if ($day_flag) {
            return response()->json([
                'message' => 'Sorry please set order time within opening hours'
            ], 423);
        }else{
            return response()->json([
                'status' => 'true'
            ]);
        }

        // foreach ($restaurant->openingHours()->orderBy('day', 'asc')->orderBy('opening_time', 'desc')->get() as $opening_hour) {
        //     if ($opening_hour->day == $cart_date->dayOfWeek) {
        //         $cart_time = Carbon::parse($cart_date->toTimeString());
        //         $opening_time = Carbon::parse($opening_hour->opening_time);
        //         $closing_time = Carbon::parse($opening_hour->closing_time);
        //         if ($cart_time->gt($opening_time) && $cart_time->lt($closing_time)) {
        //             $day_flag = false;
        //         }
        //     }
        // }

        // if ($day_flag) { 
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => 'Sorry please set order time within opening hours'
        //     ]);
        // }else{
        //     return response()->json([
        //         'status' => 'true'
        //     ]);
        // }
    } 

    /**
     * Display the specified resource.
     *
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function show(Delivery $delivery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function edit(Delivery $delivery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delivery $delivery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        //
    }
}
