<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethod
 * @package App
 */
class PaymentMethod extends Model
{ 
    protected $fillable = [
        'name',
        'branches_id',
        'logo', 
    ]; 
    protected $dates = ['deleted_at']; 
    // use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branches::class);
    }
}
