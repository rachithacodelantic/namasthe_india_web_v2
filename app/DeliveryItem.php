<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryItem
 * @package App
 */
class DeliveryItem extends Model
{
    protected $fillable = [
        'delivery_id',
        'menu_item_id',
        'deal_id',
        'offer_id',
        'quantity',
        'menu_item_variant_id',
        'menu_item_crust_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuItem()
    {
        return $this->belongsTo(MenuItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addonMenuItems()
    {
        return $this->belongsToMany(AddonMenuItem::class, 'addon_menu_item_delivery_item', 'delivery_item_id', 'addon_menu_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuItemVariants()
    {
        return $this->belongsTo(MenuItemVariant::class, 'menu_item_variant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crustTypeMenuItem()
    {
        return $this->belongsTo(CrustTypeMenuItem::class, 'menu_item_crust_id');
    }

    public function dealItem()
    {
        return $this->belongsTo(Deals::class, 'deal_id');
    }

    public function offerItem()
    {
        return $this->belongsTo(Offers::class, 'offer_id');
    }

    public function selectedDealMenuItems()
    {
        return $this->belongsToMany(DealsItem::class, 'delivery_deal_items', 'delivery_item_id', 'deal_item_id');
    }

    public function selectedOfferMenuItems()
    {
        return $this->belongsToMany(OfferItem::class, 'delivery_offer_items', 'delivery_item_id', 'offer_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryItemAddons()
    {
        return $this->hasMany(DeliveryItemAddon::class);
    }
}
