<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TakeawayItemAddon
 * @package App
 */
class TakeawayDealItems extends Model
{
    protected $fillable = ['takeaway_item_id', 'deal_item_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function takeawayItem()
    {
        return $this->belongsTo(TakeawayItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dealItem()
    {
        return $this->belongsTo(DealsItem::class, 'deal_item_id');
    }
}
