<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryOfferItems extends Model
{
    protected $fillable = [
    	'delivery_item_id', 
    	'offer_item_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryItem()
    {
        return $this->belongsTo(DeliveryItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offerItem()
    {
        return $this->belongsTo(Addon::class, 'offer_item_id');
    }
}
