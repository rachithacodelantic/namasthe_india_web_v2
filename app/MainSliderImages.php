<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class MainSliderImages extends Model
{
    protected $table = 'main_slider_images';
    protected $dates = ['deleted_at'];  

    protected $fillable = [
        'user_id',
        'restaurant_id',
        'name',
        'banner_text',
        'path',
        'order', 
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    /**
     * The menuItems that belong to the sliderImage.
     */
    public function menuItems()
    { 
        return $this->belongsToMany(MainSliderImages::class, 'menuitem_sliders', 'main_slider_images_id', 'menu_items_id');
        // return $this->belongsToMany(MenuItem::class, 'menuitem_sliders', 'main_slider_images_id', 'menu_items_id');
    }
}
