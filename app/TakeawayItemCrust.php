<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class TakeawayItemCrust extends Model
{
    protected $fillable = ['crust_id', 'takeaway_item_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function takeawayItem()
    {
        return $this->belongsTo(TakeawayItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crust()
    {
        return $this->belongsTo(Crust::class);
    }
}
