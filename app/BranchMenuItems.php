<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchMenuItems extends Model
{
    protected $table = 'branches_menuitems';  
    protected $fillable = [
        'branches_id',
        'menuitem_id', 
    ];
}
