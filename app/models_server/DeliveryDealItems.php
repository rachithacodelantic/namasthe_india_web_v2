<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TakeawayItemAddon
 * @package App
 */
class DeliveryDealItems extends Model
{
    protected $fillable = ['delivery_item_id', 'deal_item_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryItem()
    {
        return $this->belongsTo(DeliveryItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dealItem()
    {
        return $this->belongsTo(Addon::class, 'deal_item_id');
    }
}
