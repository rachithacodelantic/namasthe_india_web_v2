<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Offers extends Model
{
    protected $fillable = [
        'restaurant_id',
        'name',
        'description',
        'start',
        'close',
        'price',
        'image',
        'vat_category',
        'in_home',
    ];

    protected $dates = ['deleted_at'];
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offerItems()
    {
        return $this->hasMany(OfferItem::class);
    }}
