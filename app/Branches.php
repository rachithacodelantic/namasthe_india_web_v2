<?php 
namespace App; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Branches extends Model
{
    protected $table = 'branches';
    protected $dates = ['deleted_at']; 
    use SoftDeletes;

    protected $fillable = [
        'restaurant_id',
        'users_id',
        'name',
        'location',
        'postcode',
        'phone',
        'email',
        'description',
        'seats', 
        'takeaway',
        'delivery',
        'reserve',
        'parking',
        'county',
        'city', 
        'province', 
        'country', 
        'reserve_status',
        'latitude',
        'longtitude',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function openingHours()
    {
        return $this->hasMany(OpeningHour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cuisines()
    {
        return $this->belongsToMany(Cuisine::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany(Menu::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The menuItems that belong to the branch.
     */
    public function menuItems()
    { 
        return $this->belongsToMany(MenuItem::class, 'branches_menuitems', 'branches_id', 'menuitem_id'); 
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function takeaways()
    {
        return $this->hasMany(Takeaway::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
