<?php

namespace App; 
use Illuminate\Database\Eloquent\Relations\Pivot;

class MenuItemAttributes extends Pivot
{
	protected $table = 'attributes_menu_item';
    protected $fillable = ['attribute_id', 'menuitem_id', 'values'];

    // public function attributes()
    // {
    //     return $this->belongsTo(Attributes::class);
    // }

    // public function menuItems()
    // {
    //     return $this->belongsTo(MenuItem::class);
    // }
}
