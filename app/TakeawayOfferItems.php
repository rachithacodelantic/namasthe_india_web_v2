<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TakeawayOfferItems extends Model
{
    protected $fillable = [
    	'takeaway_item_id', 
    	'offer_item_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function takeawayItem()
    {
        return $this->belongsTo(TakeawayItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offerItem()
    {
        return $this->belongsTo(OfferItem::class, 'offer_item_id');
    }
}
