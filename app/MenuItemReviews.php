<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItemReviews extends Model
{
	protected $fillable = [
        'branch_id',
        'user_id',
        'menuitem_id',
        'rating',
        'review',
        'response',
        'approve_status',
    ]; 
    protected $table = 'menuitem_reviews';
    protected $dates = ['deleted_at']; 
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuItem()
    {
        return $this->belongsTo(MenuItem::class);
    }
}
