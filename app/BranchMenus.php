<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchMenus extends Model
{
    protected $table = 'branches_menus'; 

    protected $fillable = [
        'branches_id',
        'menus_id', 
    ];
}
