<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{     
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menuItems()
    {
        return $this->belongsToMany(MenuItem::class)->using(MenuItemAttributes::class)->withPivot('values');
    } 
}
