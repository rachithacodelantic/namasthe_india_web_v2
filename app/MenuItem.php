<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MenuItem
 * @package App
 */
class MenuItem extends Model
{
    protected $casts = [
        'delivery' => 'boolean',
        'takeaway' => 'boolean',
    ];

    protected $fillable = [
        'menu_id',
        'name',
        'logo',
        'description',
        'special_note',
        'price',
        'delivery',
        'takeaway',
        'promo_code',
        'promo_type',
        'promo_value',
        'promo_usage',
        'promo_date',
        'deleted',
        'vat_category',
        'available_status',
        'available_item',
        'vegitarian_flag',
    ];

    protected $dates = ['deleted_at'];

    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryItems()
    {
        return $this->hasMany(DeliveryItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function variants()
    {
        return $this->belongsToMany(Variant::class)->using(MenuItemVariant::class)->withPivot('price');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cuisines()
    {
        return $this->belongsToMany(Cuisine::class)->using(CuisineMenuItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addons()
    {
        return $this->belongsToMany(Addon::class)->using(AddonMenuItem::class)->withPivot('price', 'fixed');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function crustType()
    {
        return $this->belongsToMany(CrustType::class)->using(CrustTypeMenuItem::class)->withPivot('price');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributes()
    {
        return $this->belongsToMany(Attributes::class)->using(MenuItemAttributes::class)->withPivot('values');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variantClones()
    {
        return $this->hasMany(VariantClone::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addonClones()
    {
        return $this->hasMany(AddonClone::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function menuItemClone()
    {
        return $this->hasOne(MenuItemClone::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function takeaways()
    {
        return $this->hasMany(Takeaway::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * The sliderImages that belong to the menuItem.
     */
    public function sliderImages()
    {
        return $this->belongsToMany(MainSliderImages::class, 'menuitem_sliders', 'menu_items_id', 'main_slider_images_id');
    }

    /**
     * The branchMenuItems that belong to the menuItem.
     */
    public function branchMenuItems()
    {
        return $this->belongsToMany(Branches::class, 'branches_menuitems', 'menuitem_id', 'branches_id');
    } 

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_item_menus', 'menuitem_id', 'menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemReviews()
    {
        return $this->hasMany(MenuItemReviews::class);
    }
}
