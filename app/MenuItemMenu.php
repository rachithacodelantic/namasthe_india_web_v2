<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItemMenu extends Model
{
    protected $table = 'menu_item_menus';  
    protected $fillable = [
        'menu_id',
        'menuitem_id', 
    ];
}
