<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CardDetails
 * @package App
 */
class CardDetails extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'restaurant_id',
        'user_id',
        'card_number',
        'expire',
        'holder_name',
        'type',
        'cvc',
        'postcode',
        'default',
    ];

    protected $casts = ['default' => 'boolean']; 
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
