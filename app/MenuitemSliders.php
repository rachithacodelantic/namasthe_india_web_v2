<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuitemSliders extends Model
{
    protected $table = 'menuitem_sliders';
    protected $fillable = [
        'menu_items_id',
        'main_slider_images_id', 
        'deal', 
    ];
    
}
