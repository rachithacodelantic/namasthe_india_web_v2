<?php  
namespace App\Repository;

use App\Offers;
use App\OfferItem;
use Illuminate\Support\Facades\DB;

/**
 * Class OffersRepository
 * @package App\Repository
 */
class OffersRepository
{
    /**
     * @param $request
     * @return Offers
     */
    public function createByRequest(int $restaurant_id, $request)
    {
        $offers = new Offers($request);
        $offers->restaurant_id = $restaurant_id;
        $offers->save();
        return $offers;
    }

    public function get(int $offer_id)
    {
        return Offers::whereId($offer_id)->first();
    }

}
