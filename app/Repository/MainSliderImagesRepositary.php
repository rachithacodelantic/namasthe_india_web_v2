<?php  
namespace App\Repository;

use App\MainSliderImages;
use App\MenuitemSliders;
use App\Restaurant;
use Illuminate\Support\Facades\DB;

/**
 * Class MainSliderImagesRepositary
 * @package App\Repository
 */
class MainSliderImagesRepositary
{
    /** 
     * @return SliderImages
     */
    public function createByRequest(int $restaurant_id, string $name = '', string $banner_text = '', string $path)
    {
        return MainSliderImages::create([
            'restaurant_id' => $restaurant_id,
            'name' => $name,
            'banner_text' => $banner_text,
            'path' => $path 
        ]); 
    } 

    public function get(int $image_id)
    { 
        return MainSliderImages::whereId($image_id)->first();
    }

    public function getSliderImages(int $restaurant_id)
    {
        return MainSliderImages::where('restaurant_id', '=', $restaurant_id)->orderBy('created_at', 'asc')->get();
    }

    public function getSliderImagesMenuItems(int $main_slider_images_id)
    {
        return MenuitemSliders::where('main_slider_images_id','=', $main_slider_images_id)->get();
    }

    public function getSliderImagesDealItems(int $main_slider_images_id)
    {
        return MenuitemSliders::where('main_slider_images_id','=', $main_slider_images_id)->where('deal','=', 1)->get();
    }

    // public function getSliderImagesMenuItems(int $main_slider_images_id)
    // {
    //     return MenuitemSliders::where('main_slider_images_id','=', $main_slider_images_id)->where('deal','=', 1)->get();
    // }

    public function getSliderImagesDealItem(int $deal_id)
    {
        return DB::select(DB::raw("SELECT
          main_slider_images.id,
          main_slider_images.name, 
          menuitem_sliders.menu_items_id,
          menuitem_sliders.main_slider_images_id
            FROM main_slider_images
            INNER JOIN menuitem_sliders ON main_slider_images.id = menuitem_sliders.main_slider_images_id
            WHERE menuitem_sliders.menu_items_id = $deal_id AND deal = 1"));
        //return MenuitemSliders::where('menu_items_id','=', $deal_id)->where('deal','=', 1)->get();
    } 

    /**
     * @param int $slider_id
     * @return mixed
     */
    public function deleteSlider(int $slider_id)
    {
        return MainSliderImages::whereId($slider_id)->delete();
    }

    /**
     * @param int $slider_id
     * @return mixed
     */
    public function deleteSliderImageItem(int $slider_id)
    {
        return MenuitemSliders::where('main_slider_images_id','=', $slider_id)->delete();
    }

    /** 
     * @return SliderImagesMenuItem
     */
    public function createMenuitemSlider(int $menu_item, int $slider)
    {
        return MenuitemSliders::create([
            'menu_items_id' => $menu_item,
            'main_slider_images_id' => $slider,
            'deal' => 0  
        ]); 
    }

    /** 
     * @return SliderImagesDealItem
     */
    public function createDealItemSlider(int $menu_item, int $slider)
    {
        return MenuitemSliders::create([
            'menu_items_id' => $menu_item,
            'main_slider_images_id' => $slider,
            'deal' => 1  
        ]); 
    }

    /**
     * @param int $slider_id
     * @return mixed
     */
    public function deleteSliderImageItemByMenu(int $menu_item)
    {
        return MenuitemSliders::where('menu_items_id','=', $menu_item)->delete();
    }

}
