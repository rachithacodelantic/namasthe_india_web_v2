<?php  
namespace App\Repository;
 
use App\Restaurant;
use App\MenuItem;
use App\MenuItemReviews;
use Illuminate\Support\Facades\DB;

/**
 * Class MenuItemReviewsRepository
 * @package App\Repository
 */
class MenuItemReviewsRepository
{    
	/**
     * @param array $request
     * @return MenuItemReview
     */
    public function create(array $request)
    {
        $review = new MenuItemReviews($request);
        $review->save();
        return $review;
    }

    public function getReviews(int $menuItem_id)
    { 
        return MenuItemReviews::where('menuitem_id', '=', $menuItem_id)->get(); 
    } 
}
