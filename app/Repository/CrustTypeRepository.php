<?php
namespace App\Repository;
use App\CrustType;

/**
 * Class CrustTypeRepository
 * @package App\Repository
 */
class CrustTypeRepository
{
    /**
     * @return CrustType[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return CrustType::all();
    }
}
