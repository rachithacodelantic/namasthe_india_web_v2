<?php  
namespace App\Repository;

use App\Branches;
use App\Restaurant;
use App\BranchMenus;
use Illuminate\Support\Facades\DB;

/**
 * Class BranchRepository
 * @package App\Repository
 */
class BranchRepository
{
    /**
     * @param $request
     * @return Branches
     */
    public function createByRequest(int $restaurant_id, $request)
    {
        $branches = new Branches($request);
        $branches->restaurant_id = $restaurant_id;
        $branches->save();
        return $branches;
    }

    public function get(int $branch_id)
    { 
        return Branches::whereId($branch_id)->withTrashed()->first();
    }

    public function getBranches(int $restaurant_id)
    {
        return Branches::where('restaurant_id', '=', $restaurant_id)->withTrashed()->orderBy('created_at', 'asc')->paginate(10);
    }

    public function getByUser(int $user_id)
    { 
        return Branches::where('users_id', '=', $user_id)->withTrashed()->first();
    }

    public function getMenuBranches(int $branch_id)
    {
        //return BranchMenus::where('branches_id', '=', $branch_id)->get();
        return DB::select(DB::raw("SELECT
          menus.id,
          menus.name,
          menus.created_at,
          menus.updated_at, 
          branches_menus.branches_id
        FROM menus
          INNER JOIN branches_menus ON menus.id = branches_menus.menus_id
        WHERE branches_menus.branches_id = $branch_id"));
    }

    public function getMenuBranch(int $branch_id, int $menu_id)
    {
        //return BranchMenus::where('branches_id', '=', $branch_id)->get();
        return DB::select(DB::raw("SELECT
          menus.id,
          menus.name,
          menus.created_at,
          menus.updated_at, 
          branches_menus.branches_id
        FROM menus
          INNER JOIN branches_menus ON menus.id = branches_menus.menus_id
        WHERE branches_menus.branches_id = $branch_id AND branches_menus.menus_id = $menu_id"));
    }

}
