<?php 
namespace App\Repository;
use App\CardDetails;

/**
 * Class CardDetailsRepositary
 * @package App\Repository
 */
class CardDetailsRepositary
{
    /**
     * @param array $request
     * @return Cards
     */
    public function create(array $request)
    {
        $card = new CardDetails($request);
        $card->save();
        return $card;
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getUserCards($userId = ''){
        return CardDetails::where('user_id', '=', $userId)->withTrashed()->get();
    }

    /**
     * @param int $cardId
     * @return mixed
     */
    public function getCard($card_number = ''){
        return CardDetails::where('card_number', '=', $card_number)->first();
    }

    /**
     * @param int $user_id
     * @return mixed
     */
    public function defaultCard(int $user_id)
    {
        return CardDetails::where('user_id', '=', $user_id)
            ->where('default', '=', 1)
            ->get();
    }
}