<?php
/**
 * Created by PhpStorm.
 * User: janaka
 * Date: 08/03/19
 * Time: 5:48 PM
 */

namespace App\Repository;

use App\Deals;
use App\DealsItem;
use Illuminate\Support\Facades\DB;

/**
 * Class MenuItemRepository
 * @package App\Repository
 */
class DealsRepository
{
    /**
     * @param $request
     * @return Deals
     */
    public function createByRequest(int $restaurant_id, $request)
    {
        $deals = new Deals($request);
        $deals->restaurant_id = $restaurant_id;
        $deals->save();
        return $deals;
    }

    public function get(int $deal_id)
    {
        return Deals::whereId($deal_id)->first();
    }

}
