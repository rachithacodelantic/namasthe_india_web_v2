<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class DeliveryItemCrust extends Model
{
    protected $fillable = ['crust_id', 'delivery_item_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryItem()
    {
        return $this->belongsTo(DeliveryItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crust()
    {
        return $this->belongsTo(Crust::class);
    }
}
