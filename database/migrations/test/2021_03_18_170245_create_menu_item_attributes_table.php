<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes_menu_item', function (Blueprint $table) {  
            $table->increments('id'); 
            $table->integer('attributes_id');
            $table->foreign('attributes_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->integer('menu_item_id');
            $table->foreign('menu_item_id')->references('id')->on('menu_items')->onDelete('cascade');
            $table->string('values'); 
            $table->timestamps();  
            // $table->foreign('attribute_id')->references('id')->on('attributes');
            // $table->foreign('menuitem_id')->references('id')->on('menu_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes_menu_item');
    }
}
